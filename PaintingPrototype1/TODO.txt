
List of things that need to be implemented and their associated importance values

IMMEDIATE
	Implement a stabilizer.
	Add tooltips on hovering over visual elements. For example the button for merging layer is too small to contain the information that
	 it is merging down, so this has to be shown through a tooltip.

	Spamming addLayer will give the Failed to add layer error. Probably because of commands.
	At low size, there are gaps in the stroke
	Strokes behave weirdly with layers when moving layers up or down.
		Haven't been able to confirm recreate this one.

	Make stroke's RT resize so that it takes up the minimum space. Maybe do the same for layer's RT.


INTERMEDIATE
	The LayerDisplays should show what a miniature version of what is draw on the layer. Updates only after stroke completion to save 
	 resources, and stored as bitmap.
	Fix zooming so that it zooms directly towards the initially clicked position.
	Prevent statsdisplay from calling renders


EVENTUAL
	Ensure that there is no memory leaks or bloatages to avoid garbage collections. Garbage collection of new values takes 
		~10 ms, garbage collection of old values can take several seconds.
		http://www.html5rocks.com/en/tutorials/memory/effectivemanagement/
	Ensure that all interface elements are stored as bitmaps.
	Ensure that commands are working as intended, especially Layer commands.
	Use circular buffers in places where there are set max sizes on arrays
	Navigator needs something to indicate what part of the canvas is currently being viewed.


POSSIBLY NEVER
	Add dithering as a parameter for the brush shader.
	Real brush that smudges already-applied color as well as applying new color, similar to SAI's
		Reasons: This behaviour would be closer to that of an actual paint brush.
		Issues: I don't know how to implement this yet.

	Change stroke's RenderTexture to be dynamically allocated. 
		Reasons: This will free up a lot of GPU memory.
		Issues: I don't yet know what the performance cost of resizing a RenderTexture is.

	Use tilt to apply more pressure on part of the texture.
		Reasons: This behaviour would be closer to that of an actual paint brush.
		Issues: This feature is very low priority.

	Blurring brush similar to SAI's.