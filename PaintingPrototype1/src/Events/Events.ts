﻿/// <reference path="EventIDs.ts"/>

/*
	Events is used for passing messages between unrelated modules through callback functions.
	Multiple callbacks can be registered for each ID

	Separating the modules makes it easier to track down bugs as any communication between modules 
	has to go through a single channel.
*/
module KPaint.Events {
	const callbackMap: { [id: number]: ((a: any) => void)[] } = {};
	

	// register a callback
	export const register = (id: ID, callback: (a: any) => void): boolean => {
		let cb = callbackMap[id];
		if (cb === undefined || cb === null) {
			cb = [];
			callbackMap[id] = cb;
		}
		// don't allow infinite callbacks (for example from a circular event chain)
		if (cb.length > 1000) {
			console.log(['Events.register: 1000 callbacks have been set for ID: ', id, '. Fix your code.'].join(''));
			return false;
		}

		cb.push(callback);
		return true;
	}

	export const unregister = (id: ID, callback: (a: any) => void): boolean => {
		let cb = callbackMap[id];
		if (cb === undefined || cb === null)
			return false;

		// get index of callback
		let idx = cb.indexOf(callback);
		if (idx === -1)
			return false;

		cb.splice(idx, 1);
		return true;
	}


	export const emit = (id: ID, ...data: any[]): void => {
		let callbackList = callbackMap[id];
		if (callbackList === undefined || callbackList === null) {
			//console.warn(['Event ', id, ' does not have any callback associated with it.'].join(''));
			//console.trace();
			return;
		}
		for (let i = 0, n = callbackList.length; i < n; i++) {
			callbackList[i](data);
		}
	}
}