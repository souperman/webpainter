﻿module KPaint.utils{
	// measure the distance between two coordinates
	export function distance(x0: number, y0: number, x1: number, y1: number) {
		x0 = x0 > x1 ? x0 - x1 : x1 - x0;
		y0 = y0 > y1 ? y0 - y1 : y1 - y0;
		return Math.sqrt(x0 * x0 + y0 * y0);
	}

	// clamp a number to a specified range
	export function clamp(n: number, min: number, max: number) {
		if (n <= min)
			return min;
		else if (n >= max)
			return max;
		return n;
	}

	// modulus with expected behaviour
	export const mod = (n: number, m: number) => ((n % m) + m) % m;

	// expects x to be in range [0, 1]
	export const smoothstep = (x: number) => x * x * (3 - 2 * x);

	// expects x to be in range [0, 1]
	export const smootherstep = (x: number) => x * x * x * (x * (x * 6 - 15) + 10);

	// exponentially scale from 0 to 1
	export const expostep = (x: number) => {
		if (x === 0.0)
			return 0.0;
		return 2.718281828459 ** (1 - (1 / (x * x)));
	}


	function getMatrixGlobal(sprite: PIXI.Sprite | PIXI.Container): PIXI.Matrix {
		let mat = new PIXI.Matrix();
		let sr = Math.sin(sprite.rotation);
		let cr = Math.cos(sprite.rotation);
		mat.a = sprite.scale.x; //width / sprite.width; // sprite.scale.x ?
		mat.b = 0;
		mat.c = 0;
		mat.d = sprite.scale.y; //height / sprite.height; // sprite.scale.y ?
		mat.tx = sprite.x;
		mat.ty = sprite.y;
		return mat;
	}


	export function isOnHtmlCanvas(pt: Vec3, canvas: HTMLCanvasElement) {
		if (pt.x < 0 || pt.x > canvas.clientWidth)
			return false;
		if (pt.y < 0 || pt.y > canvas.clientHeight)
			return false;
		return true;
	}

	export function setOnBeforeUnload() {
		let msg: any = () => {
			return 'All progress will be lost.';
		}
		window.onbeforeunload = msg;
	}
}