﻿module KPaint {

	/*
		Used for interpolating lines. Can use different interpolation algorithms by setting 
		the interpolator varible to a function with the correct parameters and output.
		
		Grab the interpolated point from Interpolator.newPoints, which removes and returns an 
		array of points. These objects need to be recycled into their Object Pool after usage 
		as they are unique references.

	*/
	export class Interpolator {
		protected _previousPoint = new Vec3();
		
		public newPts: Vec3[] = [];		// all interpolated points go into this array
		public spacingPx = 1;			// number of pixels between each draw point



		/*
			End the line by calling the clear function (after extracting the points from newPoints).
			Clearing will recycle all stored points, and reinitialize the internal arrays
		*/
		public clear() {
			let newPts = this.newPts;
			for (let i = 0, n = newPts.length; i < n; i++) {
				op.vec3.recycle(newPts.pop());
			}

			newPts.length = 0;
		}


		/*
			Set the starting point for a line
		*/
		public setInitialDrawPoint(pointer: Vec3): void {
			this._previousPoint.copyFrom(pointer);
		}
	

		/*
			Interpolate between previous point and the supplied one.
		*/
		public interpolate(end: Vec3): void {
			let start = this._previousPoint;
			let dist = utils.distance(end.x, end.y, start.x, start.y);
			let spacingPx = this.spacingPx;
			let vec3CloneFunc = op.vec3.clone;

			// only interpolate if distance is far enough
			if (dist > spacingPx) {
				let newPts = this.newPts;
				let steps = Math.floor(dist / spacingPx);

				// percent to increment per step
				let p = spacingPx / dist;

				// how much to increment each step (p * delta)
				let xStep = p * (end.x - start.x);
				let yStep = p * (end.y - start.y);
				let zStep = p * (end.z - start.z);

				// interpolate
				for (let i = 0; i < steps; i++) {
					start.x += xStep;
					start.y += yStep;
					start.z += zStep;

					newPts.push(vec3CloneFunc(start));
				}
			}
		}


		/*
			pressure should decrease spacing

			spacingPx is the maximum spacing in pixels, the actual spacing should be adjusted between each point
			on the line. It space between points a and b should be equal to (spacingPx * b.pressure).

			For this reason I can't use my previous method, since I need to know exactly how many steps there should
			be between points a and b.

			This new method is definitely more computationally expensive, but I think time spend on interpolation 
			was rather minimal to begin with.
		*/
		public interpolate2(end: Vec3): void {
			let start = this._previousPoint;
			let dist = start.distance2D(end);
			let spacingPx = this.spacingPx;

			let endSpacing = Math.max(spacingPx * end.pressure, 1);
			
			let newPts = this.newPts;
			let p = 1;
			
			let vec3obj = op.vec3;

			let mostRecent: Vec3;
			if (newPts.length > 0) {
				mostRecent = newPts[newPts.length - 1];
			}
			while (dist > endSpacing && p > 0) {
				// There must be a way to do this without recalculating p and deltas for every point.
				p = (spacingPx * start.z) / dist;

				start.x += p * (end.x - start.x);
				start.y += p * (end.y - start.y);
				start.z += p * (end.z - start.z);

				dist = start.distance2D(end);

				// don't add perfect duplicates
				if (mostRecent === undefined || mostRecent.equals(start) === false) {
					mostRecent = vec3obj.clone(start)
					newPts.push(mostRecent);
				}
			}
		}
	}
}