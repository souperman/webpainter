﻿module KPaint {
	/* 
		Takes RGB values from the first input and puts the HSV equivalent into the second input

		Expected ranges
		rgb: [0 : 1] for all values
		hsv: [0 : 1] for all values
	*/
	export function rgbToHsv(rgb: Vec3, hsv: Vec3): void {
		var r = rgb.r,
			g = rgb.g,
			b = rgb.b,
			h: number,
			max = Math.max(r, g, b),
			min = Math.min(r, g, b),
			d = max - min;
		hsv.y = (max == 0 ? 0 : d / max);
		hsv.z = max;

		switch (max) {
			case min:
				h = 0; break;
			case r:
				h = (g - b) / d + (g < b ? 6 : 0); break;
			case g:
				h = (b - r) / d + 2; break;
			case b:
				h = (r - g) / d + 4; break;
		}
		h /= 6;
		hsv.x = h;
	}

	
	/* 
		Takes HSV values from the first input and puts the RGB equivalent into the second input

		Expected ranges
		rgb: [0 : 1] for all values
		hsv: [0 : 1] for all values
	*/
	export function hsvToRgb(hsv: Vec3, rgb: Vec3): void {
		var h = hsv.x, s = hsv.y, v = hsv.z,
			r: number, g: number, b: number,
			i: number, f: number, p: number,
			q: number, t: number;
		i = Math.floor(h * 6);
		f = h * 6 - i;
		p = v * (1 - s);
		q = v * (1 - f * s);
		t = v * (1 - (1 - f) * s);
		switch (i % 6) {
			case 0: r = v, g = t, b = p; break;
			case 1: r = q, g = v, b = p; break;
			case 2: r = p, g = v, b = t; break;
			case 3: r = p, g = q, b = v; break;
			case 4: r = t, g = p, b = v; break;
			case 5: r = v, g = p, b = q; break;
		}
		rgb.rgb(r, g, b);
	}


	/*
		Transforms the base-10 representation of rgb hex color
	*/
	export function rgbToHex(rgb: Vec3): number {
		return (Math.floor(rgb.r * 255) << 16)
			+ (Math.floor(rgb.g * 255) << 8)
			+ Math.floor(rgb.b * 255)
	}
}