﻿/// <reference path="Utils.ts"/>

module KPaint {
	export class CircularBuffer {
		public buffer: Float32Array;

		protected _next = 0;
		protected _front = -1;

		constructor(length: number) {
			this.buffer = new Float32Array(length);
		}

		public get front() { return this._front; }
		public get back() { return this._next; }
		public get length() { return this.buffer.length; }

		public push(n: number): void {
			let buffer = this.buffer;
			let next = this._next;

			buffer[next] = n;
			this._front = next;
			this._next = ((1 + next) % buffer.length);
		}

		public pop(): number {
			let buffer = this.buffer;
			let length = buffer.length;
			let next = this._next;

			next = ((1 - next % length) + length) % length;
			this._next = next;

			return buffer[next];
		}

		public reset(): void {
			let buf = this.buffer;
			for (let i = 0, n = buf.length; i < n; i++) {
				buf[i] = 0;
			}
		}
	}
}