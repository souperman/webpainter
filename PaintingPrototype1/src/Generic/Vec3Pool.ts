﻿module KPaint.op {
	export module vec3 {
		class Vec3Pool extends IPool<Vec3> {
			protected createOne() {
				return new Vec3();
			}
		}
		const _pool = new Vec3Pool('Vec3', 10000);
		_pool.allocate();

		export function get(x = 0, y = 0, z = 0): Vec3 {
			return _pool.get().xyz(x, y, z);
		}

		export function clone(vec: Vec3): Vec3 {
			return get(vec.x, vec.y, vec.z);
		}

		export function recycle(vec: Vec3): void {
			_pool.recycle(vec);
		}
	}
}