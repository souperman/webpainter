﻿module KPaint {
	/* 
		vec objects have several aliased accessors to make them easier to use for different purposes
	
		vec2: xy
		vec3: xyz, rgb, hsv, pointer
		vec4: xyzw, rgba, hsva
	
	*/

	export class Vec2 {
		constructor(
			public x = 0,
			public y = 0
		) { }

		public xy(x: number, y: number): this {
			this.x = x;
			this.y = y;
			return this;
		}

		public isZero(): boolean {
			return this.x === 0 && this.y === 0;
		}

		public dot(rhs: Vec2|Vec3): number {
			return this.x * rhs.x + this.y * rhs.y;
		}

		public length(): number {
			return Math.sqrt(this.x ** 2 + this.y ** 2);
		}

		public invert(): this {
			console.assert(!this.isZero(), 'Cannot invert zero length vector');
			this.x = 1 / this.x;
			this.y = 1 / this.y;
			return this;
		}

		public normalize(): this {
			let len = this.length();
			this.x /= len;
			this.y /= len;
			return this;
		}

		public clamp(max: number): this {
			this.normalize()
				.multiply(max);
			return this;
		}

		public plus(n: number): this {
			this.x += n;
			this.y += n;
			return this;
		}

		public plusVec(rhs: Vec2): this {
			this.x += rhs.x;
			this.y += rhs.y;
			return this;
		}

		public minus(n: number): this {
			this.x -= n;
			this.y -= n;
			return this;
		}

		public minusVec(rhs: Vec2): this {
			this.x -= rhs.x;
			this.y -= rhs.y;
			return this;
		}

		public multiply(n: number): this {
			this.x *= n;
			this.y *= n;
			return this;
		}

		public multiplyVec(rhs: Vec2): this {
			this.x *= rhs.x;
			this.y *= rhs.y;
			return this;
		}

		public divide(n: number): this {
			this.x /= n;
			this.y /= n;
			return this;
		}

		public divideVec(rhs: Vec2): this {
			this.x /= rhs.x;
			this.y /= rhs.y;
			return this;
		}

		public modulo(n: number): this {
			this.x %= n;
			this.y %= n;
			return this;
		}

		public moduloVec(rhs: Vec2): this {
			this.x %= rhs.x;
			this.y %= rhs.y;
			return this;
		}

		public distance(other: Vec2|Vec3): number {
			return Math.sqrt(this.dot(other));
		}

		public equals(rhs: Vec2): boolean {
			return this.x === rhs.x && this.y === rhs.y;
		}

		public copyFrom(source: Vec2|Vec3|Vec4): this {
			this.x = source.x;
			this.y = source.y;
			return this;
		}

		public static lerp(out: Vec2, start: Vec2, end: Vec2, percent: number): Vec2 {
			return out
				.copyFrom(end)
				.minusVec(start)
				.multiply(percent)
				.plusVec(start);
		}
		public static nlerp(out: Vec2, start: Vec2, end: Vec2, percent: number): Vec2 {
			return out
				.copyFrom(end)
				.minusVec(start)
				.multiply(percent)
				.plusVec(start)
				.normalize();
		}
	}





	
	// r, g, b
	// h, s, v
	// x, y, pressure

	export class Vec3 {
		constructor(
			public x = 0,
			public y = 0,
			public z = 0
		) { }

		// color
		get r() { return this.x; } set r(value: number) { this.x = value; }
		get g() { return this.y; } set g(value: number) { this.y = value; }
		get b() { return this.z; } set b(value: number) { this.z = value; }
		
		get h() { return this.x; } set h(value: number) { this.x = value; }
		get s() { return this.y; } set s(value: number) { this.y = value; }
		get v() { return this.z; } set v(value: number) { this.z = value; }

		// pointer
		get pressure() { return this.z; }
		set pressure(value: number) { this.z = value; }

		public xyz(x: number, y: number, z: number): this {
			this.x = x;
			this.y = y;
			this.z = z;
			return this;
		}
		public rgb(r: number, g: number, b: number): this {
			this.x = r;
			this.y = g;
			this.z = b;
			return this;
		}
		public hsv(r: number, s: number, v: number): this {
			this.x = r;
			this.y = s;
			this.z = v;
			return this;
		}

		public isZero(): boolean {
			return this.x === 0 && this.y === 0 && this.z === 0;
		}

		public dot(rhs: Vec3): number {
			return this.x * rhs.x + this.y * rhs.y + this.z * rhs.z;
		}

		public dot2D(rhs: Vec2|Vec3): number {
			return this.x * rhs.x + this.y * rhs.y;
		}

		public length(): number {
			return Math.sqrt(this.x ** 2 + this.y ** 2 + this.z ** 2);
		}

		public length2D(): number {
			return Math.sqrt(this.x ** 2 + this.y ** 2);
		}

		public distance2D(dst: Vec2 | Vec3): number {
			return Math.sqrt((dst.x - this.x) ** 2 + (dst.y - this.y) ** 2);
		}


		public plus(rhs: number): this {
			this.x += rhs;
			this.y += rhs;
			this.z += rhs;
			return this;
		}

		public plusVec(rhs: Vec3): this {
			this.x += rhs.x;
			this.y += rhs.y;
			this.z += rhs.z;
			return this;
		}

		public minus(rhs: number): this {
			this.x -= rhs;
			this.y -= rhs;
			this.z -= rhs;
			return this;
		}

		public minusVec(rhs: Vec3): this {
			this.x -= rhs.x;
			this.y -= rhs.y;
			this.z -= rhs.z;
			return this;
		}

		public multiply(percent: number): this {
			this.x *= percent;
			this.y *= percent;
			this.z *= percent;
			return this;
		}

		public multiplyVec(rhs: Vec3): this {
			this.x *= rhs.x;
			this.y *= rhs.y;
			this.z *= rhs.z;
			return this;
		}

		public divide(rhs: number): this {
			console.assert(rhs !== 0);
			this.x /= rhs;
			this.y /= rhs;
			this.z /= rhs;
			return this;
		}

		public divideVec(rhs: Vec3): this {
			this.x /= rhs.x;
			this.y /= rhs.y;
			this.z /= rhs.z;
			return this;
		}

		public modulo(n: number): this {
			this.x %= n;
			this.y %= n;
			this.z %= n;
			return this;
		}

		public moduloVec(rhs: Vec3): this {
			this.x %= rhs.x;
			this.y %= rhs.y;
			this.z %= rhs.z;
			return this;
		}

		public pow(n: number): this {
			this.x = Math.pow(this.x, n);
			this.y = Math.pow(this.y, n);
			this.z = Math.pow(this.z, n);

			return this;
		}


		public equals(rhs: Vec3): boolean {
			return this.x === rhs.x
				&& this.y === rhs.y
				&& this.z === rhs.z;
		}

		public copyFrom(rhs: Vec3|Vec4): this {
			this.x = rhs.x;
			this.y = rhs.y;
			this.z = rhs.z;
			return this;
		}


		public static lerp(out: Vec3, start: Vec3, end: Vec3, percent: number): Vec3 {
			return out
				.copyFrom(end)
				.minusVec(start)
				.multiply(percent)
				.plusVec(start);
		}
	}









	export class Vec4 {
		constructor(
			public x = 0,
			public y = 0,
			public z = 0,
			public w = 0
		) { }

		// color
		get h() { return this.x; } set h(value: number) { this.x = value; }
		get s() { return this.y; } set s(value: number) { this.y = value; }
		get v() { return this.z; } set v(value: number) { this.z = value; }

		get r() { return this.x; } set r(value: number) { this.x = value; }
		get g() { return this.y; } set g(value: number) { this.y = value; }
		get b() { return this.z; } set b(value: number) { this.z = value; }

		get a() { return this.w; } set a(value: number) { this.w = value; }

		// area
		get width() { return this.z; } set width(value: number) { this.z = value; }
		get height() { return this.w; } set height(value: number) { this.w = value; }



		public xyzw(x: number, y: number, z: number, w: number): this {
			this.x = x;
			this.y = y;
			this.z = z;
			this.w = w;
			return this;
		}

		public rgba(r: number, g: number, b: number, a: number): this {
			this.x = r;
			this.y = g;
			this.z = b;
			this.w = a;
			return this;
		}

		public hsva(h: number, s: number, v: number, a: number): this {
			this.x = h;
			this.y = s;
			this.z = v;
			this.w = a;
			return this;
		}
		
		public length() {
			return Math.sqrt((this.width - this.x) ** 2 + (this.height - this.y) ** 2);
		}

		public equals(rhs: Vec4): boolean {
			return this.x === rhs.x
				&& this.y === rhs.y
				&& this.z === rhs.z
				&& this.w === rhs.w;
		}

		public copyFrom(rhs: Vec4): this {
			this.x = rhs.x;
			this.y = rhs.y;
			this.z = rhs.z;
			this.w = rhs.w;
			return this;
		}

		public contains(x: number, y: number): boolean {
			if (this.x > x)
				return false;
			if (this.x + this.z < x)
				return false;
			if (this.y > y)
				return false;
			if (this.y + this.w < y)
				return false;
			return true;
		}

		public contains2D(vec: Vec2|Vec3): boolean {
			if (this.x > vec.x)
				return false;
			if (this.x + this.z < vec.x)
				return false;
			if (this.y > vec.y)
				return false;
			if (this.y + this.w < vec.y)
				return false;
			return true;
		}
	}
}