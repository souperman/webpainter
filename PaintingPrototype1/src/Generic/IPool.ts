﻿module KPaint {
	/*
		
	*/
	export abstract class IPool<T> {
		protected _pool: T[] = [];
		protected _uniqueId = 0;

		constructor(
			protected _name: string,
			protected _chunkSize: number
		) {}

		public get(): T {
			let pool = this._pool;
			// create new if pool is empty
			if (0 == pool.length) {
				console.warn([this._name, ' object pool empty. Allocating more.'].join(''));
				this.allocate();
			}
			return pool.pop();
		}

		public recycle(item: T): void {
			this._pool.push(item);
		}

		public allocate(): void {
			let pool = this._pool;
			for (let i = 0, n = this._chunkSize; i < n; i++) {
				pool.push(this.createOne());
			}
		}


		// each pool has it's own set of unique IDs, so make sure to not have more than 1 pool of each object
		public getUniqueId(): number {
			return this._uniqueId++;
		}

		protected abstract createOne(): T;
	}
}