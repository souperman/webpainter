﻿module KPaint.PaintingContext {
	export class Brush {
		protected _texGen: PIXI.RenderTexture;
		protected _texGenSpr: PIXI.Sprite;
		protected _brushShader: Sha.BrushShader;


		protected _texture: PIXI.RenderTexture;
		protected _interpolator = new Interpolator();
		protected _colorHsv = new Vec3();
		protected _colorRgb = new Vec3();
		protected _flow = 1.0;
		protected _size = 1;
		protected _minSizePct = 0.01;
		protected _spacing = 0.05;
		public rotation = 0.0; // ?


		constructor() {
			this._texture = new PIXI.RenderTexture(renderer, 0, 0);
			this._texGen = new PIXI.RenderTexture(renderer, 0, 0);
			this._texGenSpr = new PIXI.Sprite(this._texGen);
			this._brushShader = new Sha.BrushShader(0.01);
			this._texGenSpr.shader = this._brushShader;

			let evRegister = Events.register;
			let evId = Events.ID;
			evRegister(evId.INPUT_COLOR_CHANGE_H, (v: number[]) => this.hue = v[0]);
			evRegister(evId.INPUT_COLOR_CHANGE_S, (v: number[]) => this.saturation = v[0]);
			evRegister(evId.INPUT_COLOR_CHANGE_V, (v: number[]) => this.value = v[0]);
			evRegister(evId.INPUT_BRUSH_SIZE, (v: number[]) => this.size = v[0]);
		}


		public get size() { return this._size; }
		public set size(size: number) {
			size = utils.clamp(size, 1, 200);
			
			size = Math.floor(size);


			Events.emit(Events.ID.BRUSH_SIZE, size);
			this._size = size;
			this.spacing = this._spacing;
			

			// resize textures
			this._texGen.resize(size, size, true);
			this._texture.resize(size, size, true);
			
			this.updateTexture();
		}

		public get sizeMinPct() { return this._minSizePct; }
		public set sizeMinPct(value: number) {
			value = utils.clamp(value, 0.01, 1);

			this._minSizePct = value;
			Events.emit(Events.ID.BRUSH_SIZE_MIN, value);
		}


		public get softness() { return this._brushShader.softness; }
		public set softness(value: number) {
			value = utils.clamp(value, 0.01, 1);
			Events.emit(Events.ID.BRUSH_SOFTNESS, value);
			this._brushShader.softness = value;
			this.updateTexture();
		}

		public get flow() { return this._flow; }
		public set flow(value: number) {
			value = utils.clamp(value, 0.01, 1);
			this._flow = value;
			this._brushShader.alpha = utils.expostep(value * .6) + value * .4;
			this.updateColor();
			this.updateTexture();

			Events.emit(Events.ID.BRUSH_FLOW, value);
		}


		public get spacing() { return this._spacing; }
		public set spacing(value: number) {
			value = utils.clamp(value, 0.01, 1);
			this._spacing = value;
			Events.emit(Events.ID.BRUSH_SPACING, value);

			
			this._interpolator.spacingPx = value * this.size;
		}

		public get hue() { return this._colorHsv.h; }
		public set hue(v: number) {
			this._colorHsv.h = v;
			this.updateColor();
			this.updateTexture();
			Events.emit(Events.ID.BRUSH_COLOR_H, v);
		}

		public get saturation() { return this._colorHsv.h; }
		public set saturation(v: number) {
			this._colorHsv.s = v;
			this.updateColor();
			this.updateTexture();
			Events.emit(Events.ID.BRUSH_COLOR_S, v);
		}

		public get value() { return this._colorHsv.h; }
		public set value(v: number) {
			this._colorHsv.v = v;
			this.updateColor();
			this.updateTexture();
			Events.emit(Events.ID.BRUSH_COLOR_V, v);
		}


		public get gamma() { return this._brushShader.gamma; }
		public set gamma(v: number) {
			this._brushShader.gamma = v;
			this.updateColor();
		}


		/* 
			Return a sprite with brush's attributes
			Used for drawing
		*/
		public getSprite(pt: Vec3): BrushSprite {
			let sprite = BrushSprite.get();
			sprite.setTexture(this._texture);
			//let sprite = op.sprite.get(this._texture);

			// set the brush attributes
			//sprite.anchor.x = 0.5;								// center
			//sprite.anchor.y = 0.5;								// center
			sprite.x = pt.x;							// position
			sprite.y = pt.y;							// position
			sprite.scale = pt.z;								// size
			//sprite.scale.y = pt.z;								// size
			sprite.rotation = this.rotation;					// rotation

			return sprite;
		}



		public getSprites(pts: Vec3[]): BrushSprite[] {
			let interpolator = this._interpolator;
			let minSizePct = this._minSizePct;
			let i: number, n: number;

			// add to interpolator
			for (i = 0, n = pts.length; i < n; i++) {
				pts[i].pressure *= (1 - minSizePct);
				pts[i].pressure += minSizePct;
				interpolator.interpolate2(pts[i]);
			}
			
			// get results
			let newPts = interpolator.newPts;

			// create sprites from those results
			let results: BrushSprite[] = [];
			for (i = 0, n = newPts.length; i < n; i++) {
				results.push(this.getSprite(newPts[i]));
			}

			interpolator.clear();

			return results;
		}


		public startStroke(pt: Vec3) {
			this._interpolator.setInitialDrawPoint(pt);
		}


		public endStroke() {
			this._interpolator.clear();
		}


		public onCanvasResize(width: number, height: number) {
		}
		
		protected updateColor() {
			let shader = this._brushShader;
			let rgb = this._colorRgb;
			hsvToRgb(this._colorHsv, rgb);
			shader.red = rgb.r;
			shader.green = rgb.g;
			shader.blue = rgb.b;
		}

		protected updateTexture() {
			let texture = this._texture;
			texture.clear();
			texture.render(this._texGenSpr);
		}
	}
}