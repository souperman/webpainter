﻿/// <reference path="../Generic/IPool.ts"/>


module KPaint.PaintingContext {
	class BSprPool extends IPool<BrushSprite> {
		protected createOne() {
			return new BrushSprite();
		}
	}

	export class BrushSprite {
		// static object pools methods
		protected static _pool = new BSprPool('BrushSprite', 5000);
		public static get() { return this._pool.get(); }
		public static recycle(item: BrushSprite) { this._pool.recycle(item); }
		public static allocatePool() { this._pool.allocate(); }



		public sprite = new PIXI.Sprite();

		
		constructor() {
			this.sprite.visible = false;
			this.sprite.anchor.x = this.sprite.anchor.y = .5;
		}


		public get x() { return this.sprite.x; }
		public set x(value: number) { this.sprite.x = value; }

		public get y() { return this.sprite.y; }
		public set y(value: number) { this.sprite.y = value; }

		public get scale() { return this.sprite.scale.x; }
		public set scale(value: number) {
			let scale = this.sprite.scale;
			scale.x = scale.y = value;
		}

		public get rotation() { return this.sprite.rotation; }
		public set rotation(value: number) { this.sprite.rotation = value; }


		public removeTexture() {
			let sprite = this.sprite;
			sprite.texture = null;
			sprite.visible = false;
		}

		public setTexture(tex: PIXI.Texture) {
			let sprite = this.sprite;
			sprite.texture = tex;
			sprite.visible = true;
		}
	}
}