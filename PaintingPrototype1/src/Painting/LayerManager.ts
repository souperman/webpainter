﻿module KPaint.PaintingContext {

	class LayerPool extends IPool<Layer> {
		constructor() {
			super('Layer', 2);
			this.allocate();
		}

		protected createOne(): Layer {
			return new Layer(-1);
		}
	}



	/*
		This class manager the all Layer objects. 
	
		Creation and deletion of layer objects happens only in this class, and all references to 
		layer objects will be taken from here.

		When decribing a layer as being "above/below" it is meant in terms of what's seen on the canvas,
		which is not necessarily reflected by their actual index positions.
	*/

	export class LayerManager {
		public container = new PIXI.Container();

		public layers: Layer[] = [];
		public activeLayer: Layer = null;

		protected _size = new Vec2();
		protected _pool = new LayerPool();
		protected _indexMoveCurrent: number = null;	// tracks the movement of the active layer for a move command
		protected _activeLayerIndex: number = 0;



		constructor(width: number, height: number) {
			this._size.xy(width, height);

			let evRegister = Events.register;
			let evId = Events.ID;
			evRegister(evId.INPUT_LAYER_MOVE_START, this.moveLayerStart);
			evRegister(evId.INPUT_LAYER_MOVE_UP, this.moveLayerUp);
			evRegister(evId.INPUT_LAYER_MOVE_DOWN, this.moveLayerDown);
			evRegister(evId.INPUT_LAYER_MOVE_END, this.moveLayerEnd);
		}



		get aboveActive() {
			return this.getLayerAbove(this.activeLayer);
		}

		get belowActive() {
			return this.getLayerBelow(this.activeLayer);
		}


		/*
			Changes the alpha value of the active layer
		*/
		public setActiveLayerAlpha(value: number) {
			this.activeLayer.alpha = value;
			Events.emit(Events.ID.LAYER_ALPHA, value);
		}



		/*
			Select a layer as the active layer
		*/
		public selectLayer = (layer: Layer) => {
			if (layer === null) {
				console.error('Failed to set active layer: ', layer);
				console.trace();
				return;
			}
			this.activeLayer = layer;
			this._activeLayerIndex = this.getIndex(layer);
			Events.emit(Events.ID.LAYER_SELECTED, layer);
			this.setActiveLayerAlpha(layer.alpha);
		}



		/*
			Create a new layer
		*/
		public createLayer(): Layer {
			let idx = this._activeLayerIndex;
			if (idx < this.layers.length) {
				idx += 1;
			}

			let layer = this.getLayerFromPool();

			this.add(layer, idx);

			Events.emit(Events.ID.LAYER_CREATED, layer, idx);

			this.selectLayer(layer);

			return layer;
		}



		/*
			Add a layer to the manager. For use by the deleteLayer and newLayer command
		*/
		public addLayer(layer: Layer, idx: number) {
			if (layer === null) {
				console.error('Failed to add layer: Layer is null.');
				return;
			}

			let layerIdx = this.layers.indexOf(layer);
			if (layerIdx > -1) {
				console.warn('Failed to add layer - layer already exists in LayerManager: ', layer);
				return;
			}

			this.add(layer, idx);
			Events.emit(Events.ID.LAYER_CREATED, layer, idx);
		}



		/*
			Removes a layer from the manager. For use by the deleteLayer and newLayer command
		*/
		public removeLayer(layer: Layer): number {
			if (layer === null) {
				console.error('Failed to remove layer: Layer is null.');
				return -1;
			}

			let layerIdx = this.layers.indexOf(layer);
			if (layerIdx < 0) {
				console.warn('Failed to remove layer - layer not found: ', layer);
				this.selectLayer(this.layers[0]);
				return -1;
			}
			

			// remove
			this.remove(layerIdx);
			Events.emit(Events.ID.LAYER_DELETED, layer);


			// set a new activeLayer
			if (layerIdx < (this.layers.length)) {
				this.selectLayer(this.layers[layerIdx]);
			}
			else {
				this.selectLayer(this.layers[layerIdx - 1]);
			}

			return layerIdx;
		}



		/*
			Merge the layer with the layer directly underneath
		*/
		public mergeLayerDown(layer: Layer) {
			if (layer === null) {
				console.error('Failed to merge layer: Layer is null.');
				return;
			}
			if (this.layers.length <= 1) {
				console.warn('LayerManager must contain at least 2 layer to merge.');
				return;
			}

			// find index
			let idx = this.layers.indexOf(layer);

			// layer not found
			if (idx < 0) {
				console.error('Failed to merge layer - layer not found: ', layer);
				console.trace();
				return;
			}

			// active layer is last layer. Can't merge downwards
			if (idx === 0) {
				console.warn('Failed to merge layer - lowest layer cannot be merged from.');
				return;
			}

			
			// merge to the layer beneath the active layer
			this.layers[idx - 1].renderLayer(layer);
		}






		/*
			Find a layer based on its ID

			Returns: first layer with the supplied id.
			Failure: returns null
		*/
		public getLayerById(id: number): Layer {
			let layers = this.layers;
			for (let i = 0, n = layers.length; i < n; i++) {
				if (layers[i].id === id) {
					return layers[i];
				}
			}

			console.error('Failed to find layer with id ', id);
			return null;
		}
		


		/*
			Returns the layer above a given layer

			Returns null on failure
		*/
		public getLayerAbove(layer: Layer): Layer {
			let layers = this.layers;
			let idx = layers.indexOf(layer);
			for (let i = 0, n = layers.length - 1; i < n; i++) {
				if (layers[i].id === layer.id) {
					return layers[i + 1];
				}
			}
			return null;
		}



		/*
			Returns the layer below a given layer

			Returns null on failure
		*/
		public getLayerBelow(layer: Layer): Layer {
			let layers = this.layers;
			let idx = layers.indexOf(layer);
			for (let i = 1, n = layers.length; i < n; i++) {
				if (layers[i].id === layer.id) {
					return layers[i - 1];
				}
			}
			return null;
		}



		/*
			Returns index of a given layer.

			Returns -1 on failure
		*/
		public getIndex(layer: Layer): number {
			return this.layers.indexOf(layer);
		}



		/*
			Change the index of a layer. Moves the layer n indices. 

			n can be positive or negative
		*/
		public moveIndex(layer: Layer, n: number) {
			let idx = this.layers.indexOf(layer);

			let increment = (n >= 0 ? 1 : -1);
			let iNext: number

			let endIdx = idx + n;
			
			let eventEmitFunc = Events.emit;
			let evId = Events.ID.LAYER_MOVE

			for (let i = idx; i != endIdx; i = iNext) {
				iNext = i + increment;
				this.move(i, iNext);
				eventEmitFunc(evId, layer, iNext);
				this.selectLayer(layer);
			}
		}




		/*
			Start moving a layer
		*/
		protected moveLayerStart = () => {
			this._indexMoveCurrent = 0;
		}



		/*
			Move the active layer up 1 index
		*/
		protected moveLayerUp = () => {
			let indexMoveCurrent = this._indexMoveCurrent + 1;
			let activeLayer = this.activeLayer;

			this.move(this._activeLayerIndex, indexMoveCurrent);

			Events.emit(Events.ID.LAYER_MOVE, activeLayer, indexMoveCurrent);
			this.selectLayer(activeLayer);

			this._indexMoveCurrent = indexMoveCurrent;
		}



		/*
			Move the active layer down 1 index
		*/
		protected moveLayerDown = () => {
			let indexMoveCurrent = this._indexMoveCurrent - 1;
			let activeLayer = this.activeLayer;

			this.move(this._activeLayerIndex, indexMoveCurrent);

			Events.emit(Events.ID.LAYER_MOVE, activeLayer, indexMoveCurrent);
			this.selectLayer(activeLayer);

			this._indexMoveCurrent = indexMoveCurrent;
		}



		/*
			End a move command

			If this._indexMoveCurrent is not equal to 0, then it will emit an event causing a moveLayer 
			command to be added to the invoker.
		*/
		protected moveLayerEnd = () => {
			let indexMoveCurrent = this._indexMoveCurrent;

			// emit some event with the datas to create the 
			if (indexMoveCurrent !== 0) {
				Events.emit(Events.ID.LAYER_MOVE_END, this.activeLayer, indexMoveCurrent);
			}

			// reset variables
			this._indexMoveCurrent = null;
		}



		/*
			Every time a layer is added, it should be added to both the layers array and the container.
		*/
		protected add(layer: Layer, idx: number): void {
			this.layers.splice(idx, 0, layer);
			this.container.addChildAt(layer.container, idx);
		}



		/*
			Every time a layer's index is moved, it should be moved in both the layers array, and in the container.
		*/
		protected move(oldIdx: number, newIdx: number): void {
			this.layers.move(oldIdx, newIdx);
			this.container.children.move(oldIdx, newIdx);
		}


		
		/*
			Every time a layer is removed, it should be removed from both the layers array and the container.
		*/
		protected remove(idx: number, deleteCount = 1): void {
			this.layers.splice(idx, deleteCount);
			this.container.children.splice(idx, deleteCount);
		}
		


		/*
			Object pool functions below
		*/

		// returns (and removes) a layer from the LayerPool
		protected getLayerFromPool() {
			// otherwise grab from the pool
			let pool = this._pool;
			let l = pool.get();
			let size = this._size;

			l.id = pool.getUniqueId();
			l.name = ['Layer ', l.id.toString()].join('');
			if (!l.size.equals(size)) {
				l.resize(size.x, size.y);
			}
			return l;
		}



		// adds a layer back to the LayerPool
		public recycleLayer(layer: Layer): void {
			// remove layer from the layers array
			let layers = this.layers;
			let idx = layers.indexOf(layer);
			if (idx >= 0) {
				layers.splice(idx, 1);
			}

			layer.initialize(-1);
			this._pool.recycle(layer);
		}
	}
}