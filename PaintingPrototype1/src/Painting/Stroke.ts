﻿// attempt to improve the stroke class

module KPaint.PaintingContext {

	export enum StrokeType {
		None,
		Paint,
		Erase,
		Blur
	}
	/*
		The stroke class contains a collection of sprites that represent points in a brush stroke.

		A brush stroke begins when the pointerdown event occurs, and end when the pointerup event occurs. 
		In between these events, points should also be when pointermove events occur. 
	
		To ensure smoothness, some form of interpolation should be used between these points, since even 
		an intuous5 has a polling rate of just 200hz, while the width and height of the drawing area is 
		expected to exceeds 1000px. 
	*/
	export class Stroke {
		public container = new PIXI.Container();
		public id: number = null;
		public flattenedStroke: PIXI.Sprite;
		public type = StrokeType.None;
		public area = new Vec4();

		public previewRT: PIXI.RenderTexture = null;
		protected _maxSize = new Vec2();

		protected _strokeManager: StrokeManager;

		protected _brushSprites: BrushSprite[] = [];
		protected _particles: PIXI.ParticleContainer;
		protected _rt: PIXI.RenderTexture;
		protected _flattenable = false;


		constructor(strokeManager: StrokeManager) {
			this._strokeManager = strokeManager;
			this._particles = new PIXI.ParticleContainer(
				3000,
				{
					scale: false,
					position: false,
					alpha: false,
					rotation: false,
					uvs: false
				});
			
			this._rt = new PIXI.RenderTexture(renderer, 0, 0);
			this.flattenedStroke = new PIXI.Sprite(this._rt);
			//this.flattenedStroke.visible = false;

			this.container.addChild(this.flattenedStroke);
			this.container.addChild(this._particles);
		}


		get isFlat() { return this._flattenable === false; }


		public addPoints(sprites: BrushSprite[]): void {
			let brushSprites = this._brushSprites;
			let particles = this._particles;
			let l = particles.children.length;
			let sprite: BrushSprite;
			
			for (let i = 0, n = sprites.length; i < n; i++, l++) {
				if (l >= 3000) {
					this.flatten();
					l = 0;
				}
				sprite = sprites[i];

				brushSprites.push(sprite);
				particles.addChild(sprite.sprite);
			}
			this._flattenable = true;
		}



		/* 
			Turn the sprites in the particlecontainer into a single texture to ensure we don't have too
			many sprites in our container structure. PIXI can handle tens of thousands, but if a stroke 
			puts one sprite at each pixel it passes, a long one can actually be tens of thousands.
		*/
		public flatten(): void {
			if (!this._flattenable) {
				return;
			}
			/*let children = this._particles.children;
			let child = <PIXI.Sprite>children[0];

			let halfW = child.width / 2, halfH = child.height / 2;
			let xMin = child.x - halfW;
			let yMin = child.y - halfH;
			let xMax = child.x + halfW;
			let yMax = child.y + halfH;
			
			let ax = 99999;
			let ay = 99999;
			let axw = 0;
			let ayh = 0;

			for (let i = 1, n = children.length; i < n; i++) {
				child = <PIXI.Sprite>children[i];

				// coordinates of each corner
				ax = child.x - halfW;
				ay = child.y - halfH;
				axw = child.x + halfW;
				ayh = child.y + halfH;
				

				if (ax < xMin) xMin = ax;
				if (ay < yMin) yMin = ay;
				if (axw > xMax) xMax = axw;
				if (ayh > yMax) yMax = ayh;
			}
			let mSize = this._maxSize;
			if (xMin < 0) xMin = 0;
			if (yMin < 0) yMin = 0;
			if (xMax > mSize.x) xMax = mSize.x;
			if (yMax > mSize.y) yMax = mSize.y;

			xMin |= 0;
			yMin |= 0;
			xMax |= 0;
			yMax |= 0;

			let flattened = this.flattenedStroke;

			let increaseLeft = 0;
			let increaseTop = 0;
			let increaseRight = 0;
			let increaseBot = 0;

			
			let fw = 0, fh = 0;
			if (flattened.visible === false) {
				increaseRight = (xMax - xMin);
				increaseBot = (yMax - yMin);
				flattened.visible = true;
				console.warn(xMin, yMin, xMax, yMax);
			}
			else {
				let fx = flattened.x | 0;
				let fy = flattened.y | 0;
				fw = flattened.width;
				fh = flattened.height;

				// distance from 0,0 to x,y
				if (xMin < fx) {
					increaseLeft = (fx - xMin);
				}
				if (yMin < fy) {
					increaseTop = (fy - yMin);
				}
				console.warn('incr 1', increaseLeft, increaseTop, increaseRight, increaseBot);

				// distance to increase in the opposite directions
				if (xMax > fx + fw) {
					increaseRight = (xMax - fx + fw);
				}
				if (yMax > fy + fh) {
					increaseBot = (yMax - fy + fh);
				}
				console.warn('incr 2', increaseLeft, increaseTop, increaseRight, increaseBot);
			}

			if (increaseLeft !== 0 || increaseTop !== 0 || increaseRight !== 0 || increaseBot !== 0) {
				let rt = this._rt;
				flattened.x = increaseLeft;
				flattened.y = increaseTop;

				let prt = this.previewRT;
				prt.resize(
					fw + increaseLeft + increaseRight,
					fh + increaseTop + increaseBot,
					true
				);

				// render
				prt.render(this.container);

				// swap the texture
				flattened.texture = prt;
				flattened.x = xMin - increaseLeft;
				flattened.y = yMin - increaseTop;
				this._rt = prt;
				this.previewRT = rt;
				rt.resize(0, 0, true);

			}
			else {*/
				// render to the rendertexture
				this._rt.render(this._particles);
			//}

			//console.log(this.flattenedStroke.x|0, this.flattenedStroke.y|0, this._rt.width, this._rt.height);

			// empty the particles container
			this.recycleSprites();

			this._flattenable = false;
		}


		// initialize all variables
		public initialize(id: number, width: number, height: number, type: StrokeType): void {
			this.id = id;
			this.type = type;

			switch (type) {
				case StrokeType.None:
				case StrokeType.Paint:
					this.flattenedStroke.blendMode = PIXI.BLEND_MODES.NORMAL;
					break;
				case StrokeType.Erase:
					this.flattenedStroke.blendMode = PIXI.BLEND_MODES.ERASE;
					break;
				default:
					console.error(['Unsupported StrokeType: ', type].join(''));
					this.flattenedStroke.blendMode = PIXI.BLEND_MODES.NORMAL;
					return;
			}
			this._rt.resize(width, height);
			this._maxSize.xy(width, height);
		}


		// resets variables and recycles through the StrokeManager
		public recycle() {
			this.id = null;
			this.type = StrokeType.None;

			// remove parent
			let container = this.container;
			if (container.parent) {
				container.parent.removeChild(container);
			}


			// empty the particles container
			this.recycleSprites();

			this._rt.clear();

			//this._rt.resize(0, 0, true);
			//this.flattenedStroke.visible = false;

			this._flattenable = false;

			this._strokeManager.recycle(this);
		}


		// removes all brush sprites from the stroke and recycles them.
		public recycleSprites() {
			// empty the particles container
			this._particles.children.length = 0;

			// recycle the brushsprites
			let children = this._brushSprites;
			let brushSpr = BrushSprite;
			for (let i = 0, n = children.length; i < n; i++) {
				brushSpr.recycle(children[i]);
			}
			children.length = 0;
		}
	}
}