﻿module KPaint {
	export enum Tool {
		None,
		Paint,
		Erase,
		Blur,
		Move,
		Zoom
	}
}