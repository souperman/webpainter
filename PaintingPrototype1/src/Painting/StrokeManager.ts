﻿/*
	Manager the lifetime of strokes and functions as an invoker (from the Command Pattern)
*/
module KPaint.PaintingContext {

	class StrokePool extends IPool<Stroke> {
		protected _sm: StrokeManager;

		constructor(sm: StrokeManager) {
			super('Stroke', 11);
			this._sm = sm;
			this.allocate();
		}

		protected createOne(): Stroke {
			return new Stroke(this._sm);
		}
	}


	export class StrokeManager {
		public strokeType = StrokeType.None;
		public activeStroke: Stroke = null;

		protected _pool = new StrokePool(this);
		protected _strokes: Stroke[] = []; // all strokes outside of the stroke pool
		protected _size = new Vec2();
		
		constructor(width: number, height: number) {
			this._size.xy(width, height);

		}

		
		public beginStroke(): void {
			if (this.strokeType === StrokeType.None) {
				console.error(['Attempted to start a stroke with strokeType ', this.strokeType].join(''));
				return;
			}

			// initialize stroke
			let pool = this._pool;
			let stroke = pool.get();
			this._strokes.push(stroke);

			let size = this._size;
			stroke.initialize(pool.getUniqueId(), size.x, size.y, this.strokeType);

			// set as current
			this.activeStroke = stroke;
		}

		public endStroke(): void {
			let activeStroke = this.activeStroke;
			activeStroke = null;
		}


		public recycle(stroke: Stroke) {
			let strokes = this._strokes;
			let idx = strokes.indexOf(stroke);
			strokes.splice(idx, 1);
			this._pool.recycle(stroke);
		}
	}
}

