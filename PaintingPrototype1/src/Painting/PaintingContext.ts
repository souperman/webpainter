﻿module KPaint.PaintingContext {
	export class PaintingContext {
		public container = new PIXI.Container();
		public size = new Vec2();

		protected _isMidStroke = false;

		protected _maxCommands: number;

		protected _invoker: Command.Invoker;
		protected _layerManager: LayerManager;
		protected _strokeManager: StrokeManager;
		protected _brush: Brush;

		protected _background: PIXI.RenderTexture;
		protected _backgroundSprite: PIXI.Sprite;

		protected _previewRT: PIXI.RenderTexture;

		protected _canvasRT: PIXI.RenderTexture;
		protected _canvasSpr: PIXI.Sprite;
		protected _gammaFilter = new Sha.LinearToGammaFilter();

		protected _gammaRt: PIXI.RenderTexture = null; // used for converting to gamma before downloading

		protected _tool: Tool;
		protected _rgb = new Vec3();
		protected _hsv = new Vec3();


		constructor(width: number, height: number, maxCommands: number) {
			this.size.xy(width, height);
			this._maxCommands = maxCommands;

			this._brush = new Brush();
			this._layerManager = new LayerManager(width, height);
			this._strokeManager = new StrokeManager(width, height);
			this._invoker = new Command.Invoker(maxCommands, this._layerManager);

			this.tool = Tool.Paint;

			
			this._background = new PIXI.RenderTexture(renderer, width, height);
			this._backgroundSprite = new PIXI.Sprite(this._background);
			this._backgroundSprite.shader = new Sha.CheckerPatternFilter();
			

			this._previewRT = new PIXI.RenderTexture(renderer, 0, 0);


			// TODO: add gamma correction to the canvas sprite
			this._canvasRT = new PIXI.RenderTexture(renderer, width, height);
			this._canvasSpr = new PIXI.Sprite(this._canvasRT);
			this._canvasSpr.shader = this._gammaFilter;





			// add to container
			this.container.addChild(this._backgroundSprite);
			this.container.addChild(this._canvasSpr);


			let evRegister = Events.register;
			let evId = Events.ID;
			evRegister(evId.INPUT_LAYER_NEW, this.layerNew);
			evRegister(evId.INPUT_LAYER_CLEAR, this.layerClear);
			evRegister(evId.INPUT_LAYER_DELETE, this.layerDelete);
			evRegister(evId.INPUT_LAYER_MERGE, this.layerMerge);
			evRegister(evId.LAYER_MOVE_END, this.layerMove);
			
			evRegister(evId.INPUT_TOOL_BRUSH_PAINT, () => this.tool = Tool.Paint);
			evRegister(evId.INPUT_TOOL_BRUSH_BLUR, () => this.tool = Tool.Blur);
			evRegister(evId.INPUT_TOOL_BRUSH_ERASE, () => this.tool = Tool.Erase);
			evRegister(evId.INPUT_TOOL_MOVER, () => this.tool = Tool.Move);
			evRegister(evId.INPUT_TOOL_ZOOMER, () => this.tool = Tool.Zoom);
			evRegister(evId.INPUT_BRUSH_SOFTNESS, (value: number[]) => this.brushSoftness = value[0]);
			evRegister(evId.INPUT_BRUSH_FLOW, (value: number[]) => this.brushFlow = value[0]);
			evRegister(evId.INPUT_BRUSH_SPACING, (value: number[]) => this.brushSpacing = value[0]);
			evRegister(evId.INPUT_BRUSH_SIZE_MIN, (value: number[]) => this.brushSizeMinPct = value[0]);

			evRegister(evId.INPUT_LAYER_ALPHA_CHANGED, (value: number[]) =>this._layerManager.setActiveLayerAlpha(value[0]));
			evRegister(evId.INPUT_LAYER_SELECTED, (layer: Layer[]) => this._layerManager.selectLayer(layer[0]));
			evRegister(evId.INPUT_CANVAS_REDO, () => this._invoker.redo());
			evRegister(evId.INPUT_CANVAS_UNDO, () => this._invoker.undo());

			evRegister(evId.INPUT_DOWNLOAD_IMAGE, (value: any[]) => this.downloadImage(value[0]));
			evRegister(evId.INPUT_GAMMA_TOGGLE, this.toggleGamma);
		}

		public get isMidStroke() {
			return this._isMidStroke;
		}


		public get gamma() {
			return this._gammaFilter.gamma;
		}
		
		public set gamma(value: number) {
			this._gammaFilter.gamma = value;
			this._brush.gamma = value;
		}

		public get tool() {
			return this._tool;
		}
		public set tool(tool: Tool) {
			this._tool = tool;
			switch (tool) {
				case Tool.Paint:
					this._strokeManager.strokeType = StrokeType.Paint;
					break;
				case Tool.Erase:
					this._strokeManager.strokeType = StrokeType.Erase;
					break;
				case Tool.Blur:
					this._strokeManager.strokeType = StrokeType.Blur;
					break;
				default:
					this._strokeManager.strokeType = StrokeType.None;
					break;
			}
		}

		public get brushSize() { return this._brush.size; }
		public set brushSize(value: number) { this._brush.size = value; 	}

		public get brushSizeMinPct() { return this._brush.sizeMinPct; }
		public set brushSizeMinPct(value: number) { this._brush.sizeMinPct = value; }

		public get brushFlow() { return this._brush.flow; }
		public set brushFlow(value: number) { this._brush.flow = value; }

		public get brushSpacing() { return this._brush.spacing; }
		public set brushSpacing(value: number) { this._brush.spacing = value; }

		public get brushSoftness() { return this._brush.softness; }
		public set brushSoftness(value: number) { this._brush.softness = value; }

		public get brushRotation() { return this._brush.rotation; }
		public set brushRotation(value: number) { this._brush.rotation = value; }

		public get colorHue() { return this._brush.hue; }
		public set colorHue(value: number) { this._brush.hue = value; }

		public get colorSaturation() { return this._brush.saturation; }
		public set colorSaturation(value: number) { this._brush.saturation = value; }

		public get colorValue() { return this._brush.value; }
		public set colorValue(value: number) { this._brush.value = value; }

		protected get activeLayer() { return this._layerManager.activeLayer; }

		protected get activeStroke() { return this._strokeManager.activeStroke; }


		protected toggleGamma = () => {
			if (this.gamma === 1) {
				this.gamma = 2.2;
			}
			else {
				this.gamma = 1;
			}
		}

		



		// init after UI has been created
		public init() {
			// initial layer
			this._layerManager.createLayer();
		}
		


		/* 
			Changes the pressure curve.

			The default pressure curve is not good for painting, so adjust it.

			Using smootherstep https://en.wikipedia.org/wiki/Smoothstep#Variations
		*/
		protected adjustPressure(point: Vec3): void {
			let p = utils.clamp(point.pressure, 0, 1);
			point.pressure = utils.smootherstep(p);
		}


		public startStroke(point: Vec3): boolean {
			if (this._isMidStroke === true) {
				console.warn('Attempted to start new stroke with stroke in progress.');
				return false;
			}
			let brush = this._brush;
			
			// adjust pressure
			this.adjustPressure(point);

			// interpolator
			brush.startStroke(point);
			this._strokeManager.beginStroke();
			this.activeLayer.startStroke(this.activeStroke);

			// draw
			this.activeStroke.previewRT = this._previewRT; // test
			this.activeStroke.addPoints([brush.getSprite(point)]);

			// set pointer state
			this._isMidStroke = true;
			return true;
		}



		public addPoint(pointers: Vec3[]): boolean {
			if (this._isMidStroke === false) {
				console.warn('Attempted to add point to stroke with no stroke in progress.');
				return false;
			}

			// adjust pressure
			let pt: Vec3;
			for (let i = 0, n = pointers.length; i < n; i++) {
				pt = pointers[i];
				this.adjustPressure(pt);
			}
			
			// interpolate and draw
			let stroke = this.activeStroke;
			stroke.addPoints(this._brush.getSprites(pointers));

			// can't preview erasing strokes like with normal ones
			if (stroke.type === StrokeType.Erase) {
				stroke.flatten();
			}

			return true;
		}



		public endStroke(): boolean {
			if (this._isMidStroke === false || this.activeStroke === null) {
				console.warn('Attempted to end stroke with no stroke in progress.');
				return false;
			}
			let activeLayer = this.activeLayer;

			// add command
			let commie = this._invoker.addCommand(Command.CommandType.Brush);
			commie.layerId = activeLayer.id;
			commie.stroke = this.activeStroke;
			this._invoker.redo();


			// end stroke
			this._strokeManager.endStroke();
			activeLayer.endStroke();
			this._brush.endStroke();
			this._previewRT = this.activeStroke.previewRT; // test
		
			// set pointer state
			this._isMidStroke = false;
			return true;
		}

		
		public resizeViewArea(width: number, height: number) {
			this._brush.onCanvasResize(width, height);
			this.size.xy(width, height);
		}



		public pickColor(pointer: Vec3) {
			let data = this._canvasRT.getPixel(pointer.x, pointer.y);

			// Don't pick if color has no alpha. There's also a rare case where alpha will return as -6.246364137041382e-8
			let a = data[3];
			if (a <= 0) {
				return;
			}

			let rgb = this._rgb,
				hsv = this._hsv;

			rgb.rgb(data[0], data[1], data[2]);
			
			// textures are defined so that all transparent pixels are fully black
			// so we multiply the color by the inverse of the pixel's alpha to get the real color
			let gamma = this.gamma;
			a = Math.pow(a, 1 / gamma);
			rgb.pow(1 / gamma);
			rgb.multiply(1 / a);

			rgbToHsv(rgb, hsv);

			let evEmit = Events.emit;
			let evId = Events.ID;
			evEmit(evId.INPUT_COLOR_CHANGE_H, hsv.h);
			evEmit(evId.INPUT_COLOR_CHANGE_S, hsv.s);
			evEmit(evId.INPUT_COLOR_CHANGE_V, hsv.v);
		}


		/*
			Render layers

			TODO: use double buffer
		*/
		public render() {
			let canvasRt = this._canvasRT;
			canvasRt.clear();
			canvasRt.render(this._layerManager.container);

			Events.emit(Events.ID.CANVAS_RENDERED);
		}


		protected downloadImage(filename: string) {
			let gammaRt = this._gammaRt;
			let size = this.size;

			// render the inner canvas to a texture
			if (gammaRt === null) {
				gammaRt = new PIXI.RenderTexture(renderer, size.x, size.y);
				this._gammaRt = gammaRt;
			}
			else {
				gammaRt.resize(size.x, size.y, true);
			}
			gammaRt.clear();
			gammaRt.render(this._canvasSpr);

			// download the texture through its URI
			filename = filename || new Date().toLocaleDateString();
			let link = <HTMLAnchorElement>document.createElement('a');
			(<any>link).download = filename;
			link.href = gammaRt.getCanvas().toDataURL();
			link.click();
			link.remove();

			gammaRt.resize(0, 0, true);
		}



		protected layerNew = () => {
			if (this._layerManager.layers.length >= 11) {
				console.warn('LayerManager currently supports only up to 11 layers.');
				return;
			}
			let invoker = this._invoker;
			let commie = invoker.addCommand(Command.CommandType.NewLayer);
			commie.layerId = this.activeLayer.id;
			invoker.redo();
		}



		protected layerClear = () => {
			let invoker = this._invoker;
			let commie = invoker.addCommand(Command.CommandType.ClearLayer);
			commie.layerId = this.activeLayer.id;
			invoker.redo();
		}



		protected layerDelete = () => {
			let invoker = this._invoker;
			let commie = invoker.addCommand(Command.CommandType.DeleteLayer);
			commie.layerId = this.activeLayer.id;
			invoker.redo();
		}



		protected layerMerge = () => {
			let lm = this._layerManager;
			let below = lm.belowActive;
			if (below === null) {
				console.warn('Failed to merge layer: No layer below.');
				return;
			}
			let invoker = this._invoker;
			let commie = invoker.addCommand(Command.CommandType.MergeLayer);
			commie.layerId = lm.activeLayer.id;
			commie.layerIdBelow = below.id;
			invoker.redo();
		}



		protected layerMove = (data: any) => {
			let invoker = this._invoker;
			let commie = invoker.addCommand(Command.CommandType.MoveLayer);
			commie.layerId = data[0].id;
			commie.n = data[1];
			invoker.redo();
		}
	}
}