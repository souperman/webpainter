﻿// attempt at optimizing the layer class

module KPaint.PaintingContext {
	export class Layer {
		public container = new PIXI.Container();
		public name: string;
		public isClear = true;
		public size = new Vec2();

		protected _id: number;
		
		protected _activeStroke: Stroke = null;

		protected _alphaShader = new Sha.AlphaFilter();
		protected _rt: PIXI.RenderTexture;
		protected _sprite: PIXI.Sprite;


		constructor(id: number) {
			this.id = id;
			this.name = ['Layer ', id.toString()].join('');


			this._rt = new PIXI.RenderTexture(renderer, 0, 0);
			this._sprite = new PIXI.Sprite(this._rt);
			this._sprite.shader = this._alphaShader;

			// add to container
			this.container.addChild(this._sprite);
		}

		get alpha() {
			return this._alphaShader.a;
		}

		set alpha(value: number) {
			this._alphaShader.a = value;
		}

		get id() {
			return this._id;
		}
		set id(value: number) {
			this.name = ['Layer ', value.toString()].join('');
			this._id = value;
		}


		public startStroke(stroke: Stroke): void {
			if (stroke === null) {
				console.error('Failed to start stroke. Stroke is null.');
				return;
			}

			this._activeStroke = stroke;
			
			// add preview
			this.container.addChild(stroke.container);
		}


		public endStroke(): void {
			// remove preview
			this.container.removeChild(this._activeStroke.container);
			this._activeStroke = null;
		}


		public render(stroke: Stroke): void {
			if (stroke.isFlat === false) {
				stroke.flatten();
			}
			this._rt.render(stroke.flattenedStroke);
			this.isClear = false;
		}


		public renderLayer(layer: Layer) {
			this._rt.render(layer.container);
			this.isClear = false;
		}


		public restoreLayer(spr: PIXI.Sprite) {
			let rt = this._rt;
			rt.clear();
			rt.render(spr);
			this.isClear = false;
		}

		public copyTo(rt: PIXI.RenderTexture) {
			rt.render(this._sprite);
		}
		

		// clear the primary layer
		public clear() {
			this._rt.clear();
			this.isClear = true;
		}


		public resize(width: number, height: number) {
			this.size.xy(width, height);
			this._rt.resize(width, height, true);
		}


		public initialize(id: number) {
			// TODO: confirm this works correctly
			this.id = id;
			this.name = null;
			this.isClear = true;
			let c = this.container;
			c.visible = true;
			c.alpha = 1.0;

			// remove parent
			if (c.parent !== null) {
				c.parent.removeChild(c);
			}

			// remove any child other than the render sprite
			if (c.children.length > 1) {
				let children = c.removeChildren(1);
				for (let i = 0, n = children.length; i < n; i++) {
					children[i].destroy();
				}
			}

			this._rt.clear();
		}
	}
}