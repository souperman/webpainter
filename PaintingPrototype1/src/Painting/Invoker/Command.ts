﻿module KPaint.PaintingContext.Command {

	/*
		Enumerator used to identify what the command does
	*/
	export enum CommandType {
		None,
		Brush,
		ClearLayer,
		NewLayer,
		DeleteLayer,
		MergeLayer,
		MoveLayer
	}



	/*
		Generic command
	*/
	export class Commie {
		public invoker: Invoker;
		public lm: LayerManager;
		public layerId: number = null;
		public layerIdAbove: number = null;
		public layerIdBelow: number = null;


		public stroke: Stroke = null;
		public n: number = null;
		public m: number = null;
		

		protected _type = CommandType.None;
		protected _undoTexture: PIXI.RenderTexture;
		protected _undoTexture2: PIXI.RenderTexture;
		protected _undoSprite: PIXI.Sprite;
		protected _undoSprite2: PIXI.Sprite;
		protected _storedLayer: Layer = null;
		protected _selectedLayerId: number = null;
		protected _initialized = false;
		

		// callbacks 
		protected _do: any = null;
		protected _undo: any = null;
		protected _onShifted: any = null;
		protected _onPopped: any = null;


		constructor(invoker: Invoker, lm: LayerManager) {
			this.invoker = invoker;
			this.lm = lm;
			this._undoTexture = new PIXI.RenderTexture(renderer, 0, 0);
			this._undoTexture2 = new PIXI.RenderTexture(renderer, 0, 0);
			this._undoSprite = new PIXI.Sprite(this._undoTexture);
			this._undoSprite2 = new PIXI.Sprite(this._undoTexture2);
		}


		get layerAbove() {
			return this.lm.getLayerById(this.layerIdAbove);
		}

		get layer() {
			return this.lm.getLayerById(this.layerId);
		}

		get layerBelow() {
			return this.lm.getLayerById(this.layerIdBelow);
		}

		get type() {
			return this._type;
		}

		set type(value: CommandType) {
			this._type = value;
			this._do = null;
			this._undo = null;
			this._onShifted = null;
			this._onPopped = null;

			switch (value) {
				case CommandType.Brush:
					this._do = this._brushRedo;
					this._undo = this._brushUndo;
					this._onShifted = this._brushRecycle;
					this._onPopped = this._brushRecycle;
					break;
				case CommandType.ClearLayer:
					this._do = this._layerClearRedo;
					this._undo = this._layerClearUndo;
					break;
				case CommandType.NewLayer:
					this._do = this._layerNewRedo;
					this._undo = this._layerNewUndo;
					this._onPopped = this._layerNewPopped;
					break;
				case CommandType.DeleteLayer:
					this._do = this._layerDeleteRedo;
					this._undo = this._layerDeleteUndo;
					this._onShifted = this._layerDeleteShifted;
					break;
				case CommandType.MergeLayer:
					this._do = this._layerMergeRedo;
					this._undo = this._layerMergeUndo;
					this._onShifted = this._layerMergeRecycle;
					this._onPopped = this._layerMergeRecycle;
					break;
				case CommandType.MoveLayer:
					this._do = this._layerMoveRedo;
					this._undo = this._layerMoveUndo;
				case CommandType.None:
					break;
				default:
					console.error('Unknown CommandType: ', value);
					this._type = CommandType.None;
					return;
			}
		}

		

		public do() {
			let lm = this.lm;
			if (this._initialized === false) {
				this._selectedLayerId = lm.activeLayer.id;
			}
			lm.selectLayer(lm.getLayerById(this._selectedLayerId));
			this._do();
			this._initialized = true;
		}



		public undo() {
			this._undo();
			this.lm.selectLayer(this.lm.getLayerById(this._selectedLayerId));
		}



		public onShifted() {
			if (this._onShifted !== null) {
				this._onShifted();
			}
		}



		public onPopped() {
			if (this._onPopped !== null) {
				this._onPopped();
			}
		}



		public initialize() {
			// Only initialize the variables that can change. 
			// Invoker and layermanager will always be the same.
			this.type = CommandType.None;
			this.layerId = null;
			this.layerIdAbove = null;
			this.layerIdBelow = null;
			
			// misc
			this.stroke = null;
			this.n = null;
			this.m = null;

			this._undoTexture.resize(0, 0, true);
			this._undoTexture2.resize(0, 0, true);
			this._storedLayer = null;
			this._initialized = false;
		}
		




		/*
			Misc Storage functions  for layers
		*/


		protected storeLayerTex() {
			let layer = this.layer;
			this._undoTexture.resize(layer.size.x, layer.size.y, true);
			this._undoTexture.clear();

			//copy the layer as 1.0 alpha
			let a = layer.alpha;
			layer.alpha = 1;
			layer.copyTo(this._undoTexture);
			layer.alpha = a;
		}


		protected restoreLayerTex() {
			this.layer.restoreLayer(this._undoSprite);
		}



		// storage of an extra layer if needed (spoiler: needed for merging)
		protected storeSecondLayerTex(layer: Layer) {
			let rt = this._undoTexture2;;
			rt.resize(layer.size.x, layer.size.y, true);
			rt.clear();

			//copy the layer as 1.0 alpha
			let a = layer.alpha;
			layer.alpha = 1;
			layer.copyTo(rt);
			layer.alpha = a;
		}


		protected restoreSecondLayerTex() {
			this.layerBelow.restoreLayer(this._undoSprite2);
		}


		// set the second layer's texture size to 0x0 on recycle since it is rarely used
		protected onRecycleSecondLayer() {
			this._undoTexture2.resize(0, 0, true);
		}









		/*
			Brush.

			Save the layer and render the stroke onto the layer.

			Undo restores the layer.

			Calls stroke.recycle() when being popped or shifted off the invoker
		*/

		protected _brushRedo = () => {
			// make a copy of the layer the first time executing this
			if (this._initialized === false) {
				this.storeLayerTex();
			}

			this.lm.activeLayer.render(this.stroke);
		}


		protected _brushUndo = () => {
			this.restoreLayerTex();
		}


		protected _brushRecycle = () => {
			this.stroke.recycle();
		}







		/*
			Clear layer

			Stores the layer, then clears.

			Undoing restores the state.
		*/

		protected _layerClearRedo = () => {
			// make a copy of the layer the first time executing this
			if (this._initialized === false) {
				this.storeLayerTex();
			}
			this.layer.clear();
		}


		protected _layerClearUndo = () => {
			this.restoreLayerTex();
		}





		/*
			New layer

			Creates a new layer on first execution.
		
			Hides and unhides otherwise.

			Deletes the layer if popped off the invoker.

			Stores the layer in this._storedLayer
			Stores the layer's index in this.n
		*/

		protected _layerNewRedo = () => {
			if (this._storedLayer === null) {
				this._storedLayer = this.lm.createLayer();
			}
			else {
				this.lm.addLayer(this._storedLayer, this.n);
			}
			this.lm.selectLayer(this._storedLayer);
		}


		protected _layerNewUndo = () => {
			this.n = this.lm.removeLayer(this._storedLayer);
		}


		protected _layerNewPopped = () => {
			this.lm.recycleLayer(this._storedLayer);
		}






		/*
			Delete layer

			Hides the layer until it is shifted off the invoker, then deletes it.

			Stores the layer in this._storedLayer
			Stores the layer's index in this.n
		*/

		protected _layerDeleteRedo = () => {
			if (this._initialized === false) {
				this._storedLayer = this.layer;
			}
			this.n = this.lm.removeLayer(this._storedLayer);
		}


		protected _layerDeleteUndo = () => {
			this.lm.addLayer(this._storedLayer, this.n);
		}


		protected _layerDeleteShifted = () => {
			this.lm.recycleLayer(this._storedLayer);
		}
		




		/*
			Merge layer

			Store both initial layer, and below layer.
		
			Redo should render initial layer on the below one, and clear the initial layer.
			Undo should restore both to their original states.
		
			Needs to recycle the secondary RenderTexture when being shifted or popped
		*/

		protected _layerMergeRedo = () => {
			let layer = this.layer,
				below = this.layerBelow;

			if (this._initialized === false) {
				this.storeLayerTex();
				this.storeSecondLayerTex(below);
				this.n = layer.alpha;
				this.m = below.alpha;
			}

			// render
			below.renderLayer(layer);
			layer.clear();
		}


		protected _layerMergeUndo = () => {
			this.restoreLayerTex();
			this.restoreSecondLayerTex();
		}


		protected _layerMergeRecycle = () => {
			this.onRecycleSecondLayer();
		}





		/*
			Move layer
		*/

		protected _layerMoveRedo = () => {
			if (this._initialized === false) {
				return;
			}
			// TODO: implement
			this.lm.moveIndex(this.layer, this.n);
		}


		protected _layerMoveUndo = () => {
			this.lm.moveIndex(this.layer, -this.n);
		}
	}
}