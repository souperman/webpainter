﻿/// <reference path="Command.ts"/>

module KPaint.PaintingContext.Command {
	class CommandPool extends IPool<Commie> {
		constructor(
			public invoker: Invoker,
			public lm: LayerManager,
			max: number
		) {
			super('Command', max);
			this.allocate();
		}

		protected createOne(): Commie {
			return new Commie(this.invoker, this.lm);
		}
	}



	/*
		Uses command pattern to store and handle previous input commands allowing for undoing and redoing of them.

		_nextCommandIdx should be pointing to the next command to be executed, which means that it should be incremented
		after do() and decremented when exceeding _maxCommands
	*/
	export class Invoker {
		// pools
		protected _pool: CommandPool;

		// active commands
		protected _commands: Commie[] = [];
		protected _nextCommandIdx = 0;
		protected _maxCommands: number;

		


		constructor(maxSize: number, lm: LayerManager) {
			this._maxCommands = maxSize;
			this._pool = new CommandPool(this, lm, maxSize + 1);
		}
		


		/*
			Do (execute) a command.
		*/
		public redo = (): boolean => {
			if (
				this._nextCommandIdx >= this._commands.length
				|| this._nextCommandIdx < 0
			) {
				console.warn([
					'Cannot redo: ',
					'_nextCommandIdx: ', this._nextCommandIdx,
					', _commands.length: ', this._commands.length,
					', _maxCommands: ', this._maxCommands
				].join(''));
				return false;
			}

			// do
			this._commands[this._nextCommandIdx].do();
			Events.emit(Events.ID.INVOKER_REDONE);
			renderCoordinator.requestRender();

			this._nextCommandIdx++;
			return true;
		}



		/*
			Undo (un-execute) a command.
		*/
		public undo = (): boolean => {
			if (this._nextCommandIdx < 1) {
				console.warn([
					'Cannot undo: ',
					'_nextCommandIdx: ', this._nextCommandIdx,
					', _commands.length: ', this._commands.length,
					', _maxCommands: ', this._maxCommands
				].join(''));
				return false;
			}
			
			// undo
			this._commands[this._nextCommandIdx - 1].undo();
			Events.emit(Events.ID.INVOKER_UNDONE);
			renderCoordinator.requestRender();
			
			this._nextCommandIdx--;
			return true;
		}
		


		public addCommand(type: CommandType): Commie {
			let command = this._pool.get();
			command.type = type;


			let commands = this._commands;

			// remove commands that are infront of current position if any
			if (commands.length > this._nextCommandIdx) {
				let pool = this._pool;
				let toRecycle: Commie;
				for (let i = 0, n = commands.length - this._nextCommandIdx; i < n; i++) {
					toRecycle = commands.pop();
					toRecycle.onPopped();
					
					// TODO: recyle command
					toRecycle.initialize();
					pool.recycle(toRecycle);
				}
			}

			// add
			commands.push(command);

			// ensure the commands array doesnt exceed max length
			if (this._commands.length > this._maxCommands) {
				let toRecycle = this._commands.shift();
				toRecycle.onShifted();

				// TODO: recyle command
				toRecycle.initialize();
				this._pool.recycle(toRecycle);

				this._nextCommandIdx--;
			}

			return command;
		}
	}
}