﻿module KPaint {

	/*
		Basic object for displaying stats
	*/
	export class StatsDisplay {
		public container = new PIXI.Container();
		public area = new Vec4();

		protected _logging: number = null;

		protected _text = new PIXI.Text('-', { font: '14px arial', strokeThickness: 0.3, fill: 0xdd0000 });

		constructor(x: number, y: number, width: number, height: number) {
			this.area.xyzw(x, y, width, height);

			let text = this._text;
			text.x = x;
			text.y = y;

			this.container.addChild(text);
			this.container.visible = false;

			Events.register(Events.ID.LOG_RENDERING_STATS, this.setUpdateInterval);
		}

		public setUpdateInterval = () => {
			if (this._logging === null) {
				this._logging = setInterval(this.update, 100);
				this.container.visible = true;
			}
			else {
				clearInterval(this._logging);
				this._logging = null;
				this.container.visible = false;
			}
		}

		protected update = () => {
			let renderString = [
				'FPS: ', renderCoordinator.currentFPS.toFixed(2), '\n',
				'Rendering\n',
				'  Avg: ', renderCoordinator.avgFrameTime.toFixed(2), 'ms\n',
				'  Min: ', renderCoordinator.minFrameTime.toFixed(2), 'ms\n',
				'  Max: ', renderCoordinator.maxFrameTime.toFixed(2), 'ms\n',
				'Input\n',
				'  Avg: ', Input.avgInputHandlingMs.toFixed(2), 'ms\n',
				'  Min: ', Input.minInputHandlingMs.toFixed(2), 'ms\n',
				'  Max: ', Input.maxInputHandlingMs.toFixed(2), 'ms',
			].join('');

			this._text.text = renderString;
		}


	}
}