﻿module KPaint.Sha {
	/*
		Expects that the texture it is drawn on has a 1:1 resolution

		Inputs should be normalized [0 : 1]
	*/
	export class SelectorCircleFilter extends PIXI.AbstractFilter {
		constructor() {
			super(
				// vertex shader
				null,

				// fragment shader
				[
					'precision mediump float;',

					'varying vec2 vTextureCoord;',

					'void main(void) {',
					'	float border = 0.0;',
					'	float rad = 0.25;',
					'	float rad1 = 0.5;',


					// UV coordinate
					'	vec2 uv = vTextureCoord - 0.5;',

					// a defines where to draw
					'	float dist = sqrt(dot(uv, uv));',
					'	float a = 0.0;',
					'	a += smoothstep(rad, rad + border, dist);',
					'	a -= smoothstep(rad1 - border, rad1, dist);',

					'	gl_FragColor = vec4(vec3(1.0), a);',

					'}'
				].join('')
			);
		}
	}
}