﻿/// <reference path="Common.ts"/>

module KPaint.Sha {
	export class BrushShader extends PIXI.AbstractFilter {
		protected _softness: number
		protected _gamma: number
		protected _rgba = new Vec4();
		
		constructor(softness = 0.01) {
			super(
				null,

				[
					'precision mediump float;',

					'varying vec2 vTextureCoord;',
					
					'uniform vec4 color;',
					'uniform float radius;',
					'uniform float border;',

					SHADER_FUNC_smootherstep,

					'void main(void) {',
					'	vec2 uv =  vTextureCoord - 0.5;',

					'	float dist = sqrt(dot(uv, uv));',
					'	float t = 1.0 - smootherstep(radius, radius + border, dist);',
					
					'	gl_FragColor = vec4(color.rgb * color.a * t, color.a * t);',
					'}'
				].join(''),

				{
					color: { type: '4f', value: [1.0, 1.0, 1.0, 1.0] },
					radius: { type: '1f' },
					border: { type: '1f' }
				}
			);
			this.softness = softness;
		}

		public get red() { return this._rgba.r; }
		public set red(value: number) {
			this._rgba.r = value;
			this.uniforms.color.value[0] = Math.pow(value, this._gamma);
		}

		public get green() { return this._rgba.g; }
		public set green(value: number) {
			this._rgba.g = value;
			this.uniforms.color.value[1] = Math.pow(value, this._gamma);
		}

		public get blue() { return this._rgba.b; }
		public set blue(value: number) {
			this._rgba.b = value;
			this.uniforms.color.value[2] = Math.pow(value, this._gamma);
		}

		public get alpha() { return this._rgba.a; }
		public set alpha(value: number) {
			this._rgba.a = value;
			this.uniforms.color.value[3] = Math.pow(value, this._gamma);
		}

		public get softness() {
			return this._softness;
		}

		public set softness(value: number) {
			this._softness = value;

			let border = 0.5 * value;
			this.uniforms.border.value = border;
			this.uniforms.radius.value = 0.5 - border;
		}

		public get gamma() {
			return this._gamma;
		}

		public set gamma(value: number) {
			this._gamma = value;
		}
	}
	










	/*
	 *
	 *			O U T L I N E   F O R   T H E   B R U S H
	 *
	 */


	
	


	export class BrushOutlineShader extends PIXI.AbstractFilter {
		protected _diameter = 0;
		constructor(diameter: number) {
			super(
				null,

				[
					'precision mediump float;',

					'varying vec2 vTextureCoord;',
					
					'uniform float radius;',
					'uniform float border1;',
					'uniform float border2;',


					'void main(void) {',
					'	vec2 uv = vTextureCoord - 0.5;',

					'	float dist = sqrt(dot(uv, uv));',

					// outer outline
					'	float t = 1.0;',
					'	t += smoothstep(radius, radius + border1, dist);',
					'	t -= smoothstep(radius - border1, radius, dist);',

					// inner outline
					'	float radius2 = radius - border1;',
					'	float t2 = 1.0;',
					'	t2 += smoothstep(radius2, radius2 + border2, dist);',
					'	t2 -= smoothstep(radius2 - border2, radius2, dist);',

					// colors for each outline
					'	vec4 color1 = vec4(vec3(0.0), 1.0) * (1.0 - t);',
					'	vec4 color2 = vec4(1.0) * (1.0 - t2);',

					// blend normally
					'	gl_FragColor = color2 + color1 * (1.0 - color2.a);',
					'}'
				].join(''),

				shaderDefaultTextureUniformsGenerator(
					'border1', { type: '1f' },
					'border2', { type: '1f' },
					'radius', { type: '1f' }
				)
			);

			this.diameter = diameter;
		}

		public get diameter() {
			return this._diameter;
		}

		public set diameter(diameter: number) {
			this._diameter = diameter;

			let borders = 1 / diameter;
			this.uniforms.border1.value = borders;
			this.uniforms.border2.value = borders;
			this.uniforms.radius.value = 0.5 - borders - borders;
		}
	}
}