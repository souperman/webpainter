﻿/// <reference path="Common.ts"/>

module KPaint.Sha {
	export class ColorFilter extends PIXI.AbstractFilter {
		protected _gamma = 1;
		protected _rgb = new Vec3();

		/*
			The given color is converted from gamma to linear under the assumption that gamma is 2.2
		*/
		constructor(r: number, g: number, b: number, a = 1) {
			super(
				// vertex shader
				null,

				// fragment shader
				[
					'precision mediump float;',

					'varying vec2 vTextureCoord;',
					'varying vec4 vColor;',

					'uniform sampler2D uSampler;',
					'uniform vec3 color;',

					'void main(void) {',
					'	vec4 tex = texture2D(uSampler, vTextureCoord);',

					'	tex.rgb = color * tex.a;',

					'	gl_FragColor = tex;',
					'}'
				].join(''),

				// uniforms
				{
					color: { type: '3f', value: [] }
				}
			);

			this.r = r;
			this.g = g;
			this.b = b;
		}

		get r() {
			return this._rgb.r;
		}
		set r(value: number) {
			this._rgb.r = value;
			this.uniforms.color.value[0] = Math.pow(value, this._gamma);
		}
		get g() {
			return this._rgb.g;
		}
		set g(value: number) {
			this._rgb.g = value;
			this.uniforms.color.value[1] = Math.pow(value, this._gamma);
		}
		get b() {
			return this._rgb.b;
		}
		set b(value: number) {
			this._rgb.b = value;
			this.uniforms.color.value[2] = Math.pow(value, this._gamma);
		}
		get a() {
			return this.uniforms.alpha.value;
		}
		set a(value: number) {
			this.uniforms.alpha.value = Math.pow(value, this._gamma);
		}

		get gamma() {
			return this._gamma;
		}
		set gamma(value: number) {
			this._gamma = value;

			let rgb = this._rgb;
			this.r = rgb.r;
			this.g = rgb.g;
			this.b = rgb.b;
		}
	}
}