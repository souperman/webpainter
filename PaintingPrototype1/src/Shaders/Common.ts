﻿module KPaint.Sha {
	export const shaderDefaultTextureUniformsGenerator = (...extra: any[]) => {
		let o: any = {};

		// default
		o['uSampler'] = { type: 'sampler2D', value: 0 }
		o['projectionMatrix'] = {
			type: 'mat3', value: new Float32Array(
				[
					1, 0, 0,
					0, 1, 0,
					0, 0, 1
				])
		}

		// extra
		if (extra !== null && extra !== undefined) {
			for (let i = 0, n = extra.length; i < n; i += 2) {
				o[extra[i]] = extra[i + 1];
			}
		}
		return o;
	}

	export const shaderDefaultTextureAttributesGenerator = (...extra: any[]) => {
		let o: any = {};

		// default
		o['aVertexPosition'] = 0;
		o['aTextureCoord'] = 0;
		o['aColor'] = 0;

		// extra
		if (extra !== null && extra !== undefined) {
			for (let i = 0, n = extra.length; i < n; i += 2) {
				o.extra[i] = extra[i + 1];
			}
		}
		return o;
	}


	// copy of the default texture shader that PIXI uses
	export const SHADER_DEFAULT_TEXTURE_VERT = [
		'precision lowp float;',
		'attribute vec2 aVertexPosition;',
		'attribute vec2 aTextureCoord;',
		'attribute vec4 aColor;',

		'uniform mat3 projectionMatrix;',

		'varying vec2 vTextureCoord;',
		'varying vec4 vColor;',

		'void main(void){',
		'	gl_Position = vec4((projectionMatrix * vec3(aVertexPosition, 1.0)).xy, 0.0, 1.0);',
		'   vTextureCoord = aTextureCoord;',
		'   vColor = vec4(aColor.rgb * aColor.a, aColor.a);',
		'}'
	].join('\n');


	
	/*
		GLSL function for converting hsv to rgb.

		vec3 hsv2rgb(vec3);
	*/
	export const SHADER_FUNC_hsv2rgb = [

		// TODO: figure out which of the two functions is more correct. They each have slightly different 
		// gradients between primary hues. Sam's looks smoother, so I'm sticking to that for now.


		// https://www.shadertoy.com/view/MsS3Wc - Iñigo Quiles 
		/*'vec3 hsv2rgb(in vec3 c) {',
		'	vec3 rgb = clamp(abs(mod(c.x * 6.0 + vec3(0.0, 4.0, 2.0), 6.0) - 3.0) - 1.0, 0.0, 1.0);',
		'	rgb = rgb * rgb * (3.0 - 2.0 * rgb);',
		'	return c.z * mix(vec3(1.0), rgb, c.y);',
		'}',*/
		

		// http://lolengine.net/blog/2013/07/27/rgb-to-hsv-in-glsl - Sam Hocevar
		'vec3 hsv2rgb(vec3 c) {',
		'	vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);',
		'	vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);',
		'	return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y); ',
		'}',
	].join('\n');

	

	/*
		GLSL function for converting rgb to hsv.

		vec3 rgb2hsv(vec3);
	*/
	export const SHADER_FUNC_rgb2hsv = [
		// http://lolengine.net/blog/2013/07/27/rgb-to-hsv-in-glsl - Sam Hocevar
		'vec3 rgb2hsv(vec3 c) {',
		'	vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);',
		'	vec4 p = c.g < c.b ? vec4(c.bg, K.wz) : vec4(c.gb, K.xy);',
		'	vec4 q = c.r < p.x ? vec4(p.xyw, c.r) : vec4(c.r, p.yzx);',
		
		'	float d = q.x - min(q.w, q.y);',
		'	float e = 1.0e-10;',
		'	return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);',
		'}'
	].join('\n');


	/*
		Variation of smoothstep

		https://en.wikipedia.org/wiki/Smoothstep#Variations
	*/
	export const SHADER_FUNC_smootherstep = [
		'float smootherstep(float edge0, float edge1, float x) {',
		'	x = clamp((x - edge0)/(edge1 - edge0), 0.0, 1.0);',
		'	return x * x * x * (x * (x * 6.0 - 15.0) + 10.0);',
		'}'
	].join('\n');


	/*
		Defines constants for PI and TAU (2PI)
	*/
	export const SHADER_DEFINE_PI = [
		'\n#ifndef PI',
		'	#define PI 3.14159265359',
		'#endif',
		'#ifndef TAU',
		'	#define TAU 6.283185307179586',
		'#endif\n'
	].join('\n');



	/*
		RNG
	*/
	export const SHADER_FUNC_rand = [
		'float rand(float n) {',
		'	return fract(sin(n) * 43758.5453123);',
		'} ',

		'float rand(vec2 n) { ',
		'	return fract(sin(dot(n, vec2(12.9898, 4.1414))) * 43758.5453);',
		'}',
	].join('\n');



	/*
		pow with vectors
	*/
	export const SHADER_FUNC_pow = [
		'vec2 pow(vec2 vec, float power) {',
		'	return vec2(pow(vec.x, power), pow(vec.y, power));',
		'}',


		'vec3 pow(vec3 vec, float power) {',
		'	return vec3(pow(vec.x, power), pow(vec.y, power), pow(vec.z, power));',
		'}',


		'vec4 pow(vec4 vec, float power) {',
		'	return vec4(pow(vec.x, power), pow(vec.y, power), pow(vec.z, power), pow(vec.w, power));',
		'}',
	].join('');

	
	/*
		Defines the gamma factor
	*/
	/*const SHADER_DEFINE_GAMMA_FACTOR = [
		'\n#ifndef GAMMA_FACTOR',
		'	#define GAMMA_FACTOR 2.2',
		'#endif\n',
	].join('\n');


	// Gamma correction functions. Gamma is defined in the above string
	export const SHADER_FUNC_linearToOutput = [
		SHADER_DEFINE_GAMMA_FACTOR,

		'vec4 linearToOutput( in vec4 a ) {',
		'	#ifdef GAMMA_FACTOR',
		'		return pow(a, vec4(1.0 / float(GAMMA_FACTOR)));',
		'	#else',
		'		return a;',
		'	#endif',
		'}',
		'vec3 linearToOutput( in vec3 a ) {',
		'	#ifdef GAMMA_FACTOR',
		'		return pow(a, vec3(1.0 / float(GAMMA_FACTOR)));',
		'	#else',
		'		return a;',
		'	#endif',
		'}',
		'float linearToOutput( in float a ) {',
		'	#ifdef GAMMA_FACTOR',
		'		return pow(a, (1.0 / float(GAMMA_FACTOR)));',
		'	#else',
		'		return a;',
		'	#endif',
		'}',
	].join('\n');*/




	/*
		Converts from rgb to greyscale using the values found here
		http://alienryderflex.com/hsp.html
	
		Basic: humans see certain hues more clearly, so simply averaging the rgb values doesn't 
		generate a good greyscale. For example in HSV, a fully saturated blue is significantly 
		darker than a fully saturated red, and yet if you convert them to greyscale by averaging, 
		they will both generate the same tone.
	
		We need to take our human brightness perception into account. To do this we assign r, g, 
		and b, separate multipliers that correspond with how brightly we perceive them. These 
		multipliers add up to a total of 1.0.
	
		r * 0.299
		g * 0.587
		b * 0.114

	*/
	export const SHADER_FUNC_toGreyscale = [
		'float toGreyscale( in vec3 color ) {',
		'	return 0.299 * color.r + 0.587 * color.g + 0.114 * color.b;',
		'}',

		'vec3 toGreyscale( in vec3 color ) {',
		'	return vec3(toGreyscale(color));',
		'}',

		'vec4 toGreyscale( in vec4 color ) {',
		'	return vec4(toGreyscale(color.rgb), color.a);',
		'}',
	].join('');




	/*
		Previously used this to generator my cursor outline.

		https://en.wikipedia.org/wiki/Sobel_operator#Alternative_operators
	*/
	export const SHADER_FUNC_sobelOperator = [
	'float sobelOperator() {',
		'	float alpha = 0.0;',
					
		// top
		'	alpha -= texture2D(uSampler, vTextureCoord + vec2(-stepSize.x,	-stepSize.y)).a * 3.0;',
		'	alpha -= texture2D(uSampler, vTextureCoord + vec2(0.0,			-stepSize.y)).a * 10.0;',
		'	alpha -= texture2D(uSampler, vTextureCoord + vec2(stepSize.x,	-stepSize.y)).a * 3.0;',

		// middle
		'	alpha -= texture2D(uSampler, vTextureCoord + vec2(-stepSize.x,	0.0)).a * 10.0;',
		'	alpha += texture2D(uSampler, vTextureCoord + vec2(0.0,			0.0)).a * 52.0;',
		'	alpha -= texture2D(uSampler, vTextureCoord + vec2(stepSize.x,	0.0)).a * 10.0;',

		// bottom
		'	alpha -= texture2D(uSampler, vTextureCoord + vec2(-stepSize.x,	stepSize.y)).a * 3.0;',
		'	alpha -= texture2D(uSampler, vTextureCoord + vec2(0.0,			stepSize.y)).a * 10.0;',
		'	alpha -= texture2D(uSampler, vTextureCoord + vec2(stepSize.x,	stepSize.y)).a * 3.0;',

		'	return alpha;',
		'}',
	].join('');
}