﻿/// <reference path="../Common.ts"/>

module KPaint.Sha {
	export class HorizontalHueFilter extends PIXI.AbstractFilter {
		constructor(
			area: Vec4,
			canvas: HTMLCanvasElement
		) {
			let scaleX = 1 / (area.width / canvas.clientWidth),
				scaleY = 1 / (area.height / canvas.clientHeight);

			let drawAX = area.x / canvas.clientWidth,
				drawAY = area.y / canvas.clientHeight;
			super(
				// vertex shader
				null,

				// fragment shader
				[
					'precision mediump float;',

					'varying vec2 vTextureCoord;',
					'varying vec4 vColor;',

					'uniform sampler2D uSampler;',
					'uniform vec2 uResolution;',
					'uniform vec2 scale;',
					'uniform vec2 drawPoint;',

					SHADER_FUNC_hsv2rgb,

					'void main(void) {',
					// get [0.0, 1.0] values
					'	vec2 uv = (gl_FragCoord.xy / uResolution);',

					// flip the y-axis
					'	uv.y = 1.0 - uv.y;',

					// set starting point and scale
					'	uv -= drawPoint;',
					'	uv *= scale;',

					'	float hue = uv.x;',

					'	vec3 rgb = hsv2rgb(vec3(hue, 1.0, 1.0));',

					// brightness to alpha
					'	gl_FragColor = vec4(rgb, 1.0);',
					'}'
				].join('\n'),

				// uniforms
				{
					uResolution: { type: '2f', value: [canvas.clientWidth, canvas.clientHeight] },
					scale: { type: '2f', value: [scaleX, scaleY] },
					drawPoint: { type: '2f', value: [drawAX, drawAY] }
				}
			);
		}
	}
}