﻿/// <reference path="../Common.ts"/>

module KPaint.Sha {
	export class HorizontalValueFilter extends PIXI.AbstractFilter {
		constructor(
			area: Vec4,
			canvas: HTMLCanvasElement,
			hue: number,
			saturation: number
		) {
			let scaleX = 1 / (area.width / canvas.clientWidth),
				scaleY = 1 / (area.height / canvas.clientHeight);

			let drawAX = area.x / canvas.clientWidth,
				drawAY = area.y / canvas.clientHeight;
			super(
				// vertex shader
				null,

				// fragment shader
				[
					'precision mediump float;',

					'varying vec2 vTextureCoord;',
					'varying vec4 vColor;',

					'uniform sampler2D uSampler;',
					'uniform vec2 uResolution;',
					'uniform vec2 scale;',
					'uniform vec2 drawPoint;',
					'uniform float hue;',
					'uniform float saturation;',

					SHADER_FUNC_hsv2rgb,

					'void main(void) {',
					// get [0.0, 1.0] values
					'	vec2 uv = (gl_FragCoord.xy / uResolution);',

					// flip the y-axis
					'	uv.y = 1.0 - uv.y;',

					// set starting point and scale
					'	uv -= drawPoint;',
					'	uv *= scale;',

					'	float value = uv.x;',

					'	vec3 rgb = hsv2rgb(vec3(hue, saturation, value));',

					// brightness to alpha
					'	gl_FragColor = vec4(rgb, 1.0);',
					'}'
				].join('\n'),

				// uniforms
				{
					uResolution: { type: '2f', value: [canvas.clientWidth, canvas.clientHeight] },
					scale: { type: '2f', value: [scaleX, scaleY] },
					drawPoint: { type: '2f', value: [drawAX, drawAY] },
					hue: { type: '1f', value: hue },
					saturation: { type: '1f', value: saturation }
				}
			);
		}


		get hue(): number {
			return this.uniforms.hue.value;
		}
		set hue(value: number) {
			this.uniforms.hue.value = value;
		}
		get saturation(): number {
			return this.uniforms.saturation.value;
		}
		set saturation(value: number) {
			this.uniforms.saturation.value = value;
		}
	}
}