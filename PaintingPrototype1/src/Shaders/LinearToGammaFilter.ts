﻿/// <reference path="Common.ts"/>

module KPaint.Sha {
	export class LinearToGammaFilter extends PIXI.AbstractFilter {
		protected _gamma = 1;

		constructor() {
			super(
				null,

				[
					'precision mediump float;',

					'varying vec2 vTextureCoord;',
					'varying vec4 vColor;',

					'uniform sampler2D uSampler;',
					'uniform float invGamma;',

					'void main(void) {',
					'	vec4 tex = texture2D(uSampler, vTextureCoord);',

					'	tex = pow(tex, vec4(invGamma));',

					'	gl_FragColor = tex;',
					'}'
				].join(''),

				{
					invGamma: { type: '1f' }
				}
			);
			this.gamma = this._gamma;
		}

		get gamma() {
			return this._gamma;
		}

		set gamma(value: number) {
			this._gamma = value;
			this.uniforms.invGamma.value = 1 / value;
		}
	}
}