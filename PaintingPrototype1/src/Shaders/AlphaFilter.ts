﻿/// <reference path="Common.ts"/>

module KPaint.Sha {
	/*
		Custom shader for adjusting alpha values on a texture
	*/
	export class AlphaFilter extends PIXI.AbstractFilter {
		constructor(a = 1) {
			super(
				// vertex shader
				null,

				// fragment shader
				[
					'precision mediump float;',

					'varying vec2 vTextureCoord;',
					'varying vec4 vColor;',

					'uniform sampler2D uSampler;',
					'uniform float alpha;',

					'void main(void) {',
					'	vec4 tex = texture2D(uSampler, vTextureCoord);',

					'	tex *= alpha;',

					'	gl_FragColor = tex;',
					'}'
				].join('\n'),

				// uniforms

				{
					alpha: { type: '1f', value: a }
				}

			);
		}
		get a() {
			return this.uniforms.alpha.value;
		}
		set a(value: number) {
			this.uniforms.alpha.value = value;
		}
	}
}