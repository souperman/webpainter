﻿/// <reference path="Common.ts"/>

module KPaint.Sha {
	/*
		Generate the check pattern seen in the background in photoshop when layers are clear.
	*/


	export class CheckerPatternFilter extends PIXI.AbstractFilter {
		constructor() {
			super(
				null, 

				[
					'precision mediump float;',

					'varying vec2 vTextureCoord;',
					'varying vec4 vColor;',

					'uniform sampler2D uSampler;',
					'uniform vec2 scale;',
					'uniform vec2 checkers;',
					

					'void main(void) {',
					'	vec2 uv =  scale * vTextureCoord;',

					'	float x = mod(floor(uv.x * checkers.x), 2.0);',
					'	float y = mod(floor(uv.y * checkers.y), 2.0);',

					'	bool isDark = bool(mod(x + y, 2.0));',
					
					'	gl_FragColor = vec4(vec3(isDark?0.8:1.0), 1.0);',
					'}'
				].join(''),


				{
					scale: { type: '2f', value: [1, 1] },
					checkers: { type: '2f', value: [64.5, 64.5] }
				}
			);
		}

	}
}