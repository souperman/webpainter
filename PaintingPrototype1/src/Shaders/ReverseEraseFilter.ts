﻿module KPaint.Sha {
	/*
		An AbstractFilter to show the part of a texture that will be erased by another texture. 
		Apply this filter to the texture that's being erased from, and set the other texture as the mask.

		This can be used to store the erased data so it can be undone later on.
	*/

	export class EraseReversalFilter extends PIXI.AbstractFilter {
		constructor(maskTexture: PIXI.Texture) {
			super(
				// Vert
				null,
				
				// Frag
				[
					'precision mediump float;',

					'varying vec2 vTextureCoord;',

					'uniform sampler2D uSampler;',
					'uniform sampler2D mask;',

					'void main(void) {',
					'	vec4 original = texture2D(uSampler, vTextureCoord);',
					'	vec4 mask = texture2D(mask, vTextureCoord);',

					'	gl_FragColor = vec4(original.rgb, ceil(mask.a));',
					'}'
				].join(''),
				
				// Uniforms
				{
					mask: { type: 'sampler2D', value: maskTexture }
				}
			);
		}

		get mask(): PIXI.Texture {
			return this.uniforms.mask.value;
		}
		set mask(m: PIXI.Texture) {
			this.uniforms.mask.value = m;
		}
	}
}