﻿module KPaint.Sha {
	/*
		Expects that the texture it is drawn on has a 1:1 resolution

		Inputs should be normalized [0 : 1]
	*/
	export class HueCircleFilter extends PIXI.AbstractFilter {
		constructor(radius: number, width: number) {
			super(
				// vertex shader
				null,

				// fragment shader
				[
					'precision mediump float;',

					SHADER_DEFINE_PI,

					'varying vec2 vTextureCoord;',
					
					'uniform float radius;',
					'uniform float width;',


					SHADER_FUNC_hsv2rgb,


					'void main(void) {',
					'	float border = width * 0.1;',
					'	float rad0 = radius - width * 0.5;',
					'	float rad1 = radius + width * 0.5;',


					'	vec2 uv = vTextureCoord - 0.5;',
					'	float dist = sqrt(dot(uv, uv));',


					// a defines alpha at current pixel
					'	float a = 0.0;',
					'	a += smoothstep(rad0, rad0 + border, dist);',
					'	a -= smoothstep(rad1 - border, rad1, dist);',


					// calculate radians from center or something
					'	float angle = atan(uv.y, uv.x);',

					// The atan return values from [-Pi, Pi]. Map it to [-0.5, 0.5] by dividing by Tau then add 0.5
					'	float h = (angle / TAU) + 0.5;',

					'	gl_FragColor = vec4(hsv2rgb(vec3(h, 1.0, 1.0)) * a, a);',
					'}'
				].join(''),

				// uniforms
				{
					radius: { type: '1f', value: radius },
					width: { type: '1f', value: width }
				}
			);
		}
	}
}