﻿module KPaint.Sha {
	/*
		Filter that will map HSV saturation and value to x and y coordinates respectively, 
		given a hue and location. 
	
		Used for rendering the middle(square) section of the color wheel.

		Color wheels usually use either HSV or HSL color formats. This one uses HSV(also known as HSB) because I 
		find it easier to use than HSL. I think CIELAB is an alternative, but I've never tried
		using it.
	*/
	


	/*
		Expects that the texture it is drawn on has a 1:1 resolution

		Inputs should be normalized [0 : 1]
	*/
	export class SaturationValueFilter2 extends PIXI.AbstractFilter {
		constructor(hue: number) {
			super(
				// vertex shader
				null,

				// fragment shader
				[
					'precision mediump float;',

					'varying vec2 vTextureCoord;',

					'uniform float hue;',

					SHADER_FUNC_hsv2rgb,

					'void main(void) {',
					'	vec2 uv = vTextureCoord;',
					'	uv.y = 1.0 - uv.y;',
					
					// combine with hue
					'	vec3 hsv = vec3(hue, uv.x, uv.y);',

					// convert to rgb, then set as fragColor
					'	gl_FragColor = vec4(hsv2rgb(hsv), 1.0);',
					'}'
				].join(''),

				// uniforms
				{
					hue: { type: '1f', value: hue }
				}
			);
		}

		get hue(): number {
			return this.uniforms.hue.value;
		}
		set hue(value: number) {
			this.uniforms.hue.value = value;
		}
	}
}