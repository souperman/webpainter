﻿module KPaint.UI {

	/* 
		Cursor that is rendered on the canvas as a PIXI.Sprite object.
		
		Can be used to override the default cursor with a PIXI.Sprite based one.

		I can't figure out how to set the normal cursor's image without using a 
		url to an image or cursor object. I want to set the cursor to be the outline 
		of the current brush.
	*/
	export class CCursor {
		public sprite = new PIXI.Sprite();

		protected _rt: PIXI.RenderTexture;
		protected _shader: Sha.BrushOutlineShader;
		protected _diameter: number;
		protected _zoom = 1;

		protected _previewMode = false;
		protected _previewOffset = new Vec2();


		constructor(diameter: number) {
			this._diameter = diameter;
			
			this._rt = new PIXI.RenderTexture(renderer, diameter, diameter);
			let sprite = new PIXI.Sprite(this._rt);
			sprite.anchor.x = 0.5;
			sprite.anchor.y = 0.5;
			this.sprite = sprite;

			this._shader = new Sha.BrushOutlineShader(diameter);
			sprite.shader = this._shader;

			Events.register(Events.ID.BRUSH_SIZE, this.changeSize);
			Events.register(Events.ID.ZOOM_SET_ZOOM, this.changeZoom);
			Events.register(Events.ID.POINTER_MOVE, this.move);
			Events.register(Events.ID.INPUT_DRAG_BRUSH_SIZE_PREVIEW, this.sizePreview);
			Events.register(Events.ID.POINTER_UP, this.endSizePreview);

		}

		get x(): number {
			return this.sprite.x;
		}
		set x(x: number) {
			if (this._previewMode === true) {
				x += this._previewOffset.x
			}
			this.sprite.x = x ;
		}

		get y(): number {
			return this.sprite.y;
		}
		set y(y: number) {
			if (this._previewMode === true) {
				y += this._previewOffset.y;
			}
			this.sprite.y = y;
		}

		get visible(): boolean {
			return this.sprite.visible;
		}
		set visible(val: boolean) {
			this.sprite.visible = val;
		}

		protected move = (point: Vec2[]) => {
			this.x = point[0].x;
			this.y = point[0].y;
		}

		protected changeSize = (size: number[]) => {
			this._diameter = size[0];
			this.update();
		}

		protected changeZoom = (value: number[]) => {
			this._zoom = value[0];
			this.update();
		}


		protected sizePreview = (pointer: Vec3[]) => {
			this._previewMode = true;
			this._previewOffset.xy(pointer[0].x, pointer[0].y);
		}

		protected endSizePreview = () => {
			if (this._previewMode === true) {
				this._previewMode = false;
				this.x -= this._previewOffset.x;
				this.y -= this._previewOffset.y;
				this._previewOffset.xy(0, 0);
			}
		}


		protected update() {
			// update rendertexture
			let di = this._diameter * this._zoom;

			// resize the texture to reflect the size of the cursor
			// I want to avoid rescaling since that looks awful in PIXI
			this._rt.resize(di, di, true);

			// update shader
			this._shader.diameter = di;
		}
	}
}