﻿/// <reference path="DisplayObject.ts"/>


module KPaint.UI {
	export abstract class Clickable2 extends DisplayObject {
		public isActive = false;
		public width: number;
		public height: number;

		protected _cParent: ClickableContainer2 = null;

		constructor(x: number, y: number, width: number, height: number) {
			super(x, y);
			this.width = width;
			this.height = height;
		}


		get cParent(): ClickableContainer2 {
			return this._cParent;
		}
		set cParent(parent: ClickableContainer2) {
			this._cParent = parent;
		}

		
		/*
			The coordinates supplied are assumed to be local (within the parent if one exists)
			
			The coordinate will be used if either the click coordinate is within bounds
			or this IClickable is currently active.
		*/
		public click(pointer: Vec3, setActive = true): void {
			this.clickHandler(pointer);
			/* 
				set active after calling the handler, because in some cases the handler's logic will use the 
				element's isActive property to determine whether the click is on the element or a child element. 
			*/
			if (setActive) {
				this.isActive = true;
			}
		}


		public contains(pointer: Vec2 | Vec3): boolean {
			if (this.x > pointer.x)
				return false;
			if (this.x + this.width < pointer.x)
				return false;
			if (this.y > pointer.y)
				return false;
			if (this.y + this.height < pointer.y)
				return false;
			return true;
		}


		// deactivate if active and call releaseHandler
		public releaseClick(): void {
			this.releaseHandler();
			this.isActive = false;
		}



		protected abstract clickHandler(pointer: Vec3): void;
		protected abstract releaseHandler(): void;
	}

	export abstract class ClickableContainer2 extends Clickable2 {
		protected _clickables: Clickable2[] = [];
		protected _local = new Vec3();

		constructor(x: number, y: number, width: number, height: number) {
			super(x, y, width, height);
		}

		public addClickListener(c: Clickable2): void {
			this._clickables.push(c);
			c.cParent = this;
		}

		public removeClickable(c: Clickable2): void {
			let idx = this._clickables.indexOf(c);
			if (idx >= 0) {
				this._clickables.splice(idx, 1);
				c.cParent = null;
			}
		}

		protected clickHandler(pointer: Vec3): void {
			let clickables = this._clickables,
				i = 0,
				n = clickables.length;

			// translate to local coordinates
			let local = this._local;
			local.copyFrom(pointer);
			local.x -= this.x;
			local.y -= this.y;

			// check if any sub element is active
			for (; i < n; i++) {
				if (clickables[i].isActive) {
					clickables[i].click(local, false);
					return;
				}
			}

			// check if the click is on a sub element
			for (i = 0; i < n; i++) {
				if (clickables[i].contains(local)) {
					clickables[i].click(local, true);
					return;
				}
			}
		}

		// release any clicks that is active
		protected releaseHandler(): void {
			let clickables = this._clickables;
			for (let i = 0, n = clickables.length; i < n; i++) {
				if (clickables[i].isActive) {
					clickables[i].releaseClick();
				}
			}
		}
	}
}