﻿module KPaint.UI {
	/*
		Slider that uses an area rather than a line. The area can have a filter set on it.

		By setting a filter you can define a gradient over the area.
	*/
	export class AreaSlider extends Clickable2 {
		public centerArea = new Vec4();


		// misc
		protected _selector: Selector;
		protected _background = new PIXI.Sprite();
		protected _midY: number;
		protected _emitEvent: Events.ID;
		protected _preText = new PIXI.Text('', { font: '16px arial', strokeThickness: 1 });
		protected _postText = new PIXI.Text('', { font: '12px arial' });


		constructor(
			x: number,
			y: number,
			width: number,
			height: number,
			paddingLeft: number,
			paddingRight: number,
			preText: string,
			eventId?: Events.ID
		) {
			super(x, y, width, height);
			this._emitEvent = eventId;
			
			let ca = this.centerArea;

			x = 0;
			y = 0;

			let midX = x;
			let midY = y + height / 2;

			this._midY = midY;
			this.centerArea.xyzw(x + paddingLeft, y + height * 0.3, width - paddingLeft - paddingRight, height * 0.4);
			this._selector = new Selector(midX, midY, Math.round(width / 32));

			let pre = this._preText;
			pre.text = preText;
			pre.x = x + paddingLeft / 2;
			pre.y = midY;
			pre.anchor.y = 0.5;
			pre.anchor.x = 0.5;

			let post = this._postText;
			post.x = x + width - paddingRight / 2;
			post.y = midY;
			post.anchor.y = 0.5;
			post.anchor.x = 0.5;

			let rt = new PIXI.RenderTexture(renderer, ca.width, ca.height);
			this._background = new PIXI.Sprite(rt);
			this._background.x = ca.x;
			this._background.y = ca.y;

			this.addChildPIXI(this._background);
			this.addChildPIXI(pre);
			this.addChildPIXI(post);
			this.addChild(this._selector);
			this.percent = 0.0;
		}


		get percent(): number {
			return (this._selector.x - this.centerArea.x) / this.centerArea.width;
		}
		set percent(value: number) {
			this._selector.x = this.centerArea.x + this.centerArea.width * value;
			this._selector.y = this._midY;
			this._postText.text = this.percent.toFixed(2).toString();
		}

		get cacheAsBitmat() { return this._background.cacheAsBitmap; }
		set cacheAsBitmat(value: boolean) { this._background.cacheAsBitmap = value; }

		protected get areaFilter(): PIXI.Shader | PIXI.AbstractFilter {
			return this._background.shader;
		}

		protected set areaFilter(filter: PIXI.Shader | PIXI.AbstractFilter) {
			this._background.shader = filter;
		}

		
		// event to emit when this slider is changed
		public setEmitEvent(event: Events.ID) {
			this._emitEvent = event;
		}
		
		// slider should react to this eventId
		public setListenEvent(event: Events.ID) {
			Events.register(event, this.handleCallback);
		}


		protected handleCallback = (percent: number[]) => {
			this.percent = percent[0];
		}


		protected clickHandler(pt: Vec3): void {
			let x = pt.x,
				y = this._midY,
				sx = this.centerArea.x,
				sw = this.centerArea.width;


			if (x < sx)
				x = sx;
			if (x > sx + sw)
				x = sx + sw;

			this._selector.x = x;
			this._postText.text = this.percent.toFixed(2).toString();


			// emit event, if one is registered
			if (this._emitEvent !== null)
				Events.emit(this._emitEvent, this.percent);
		}


		protected releaseHandler(): void { }
	}





	// extensions

	export class HueSlider extends AreaSlider {
		constructor(
			x: number,
			y: number,
			width: number,
			height: number,
			paddingLeft: number,
			paddingRight: number
		) {
			super(x, y, width, height, paddingLeft, paddingRight, 'H');
			this.areaFilter = new Sha.HorizontalHueFilter(this.centerArea, renderer.view);

			this.hue([0]);

			// Events
			Events.register(Events.ID.BRUSH_COLOR_H, this.hue);
			this.setListenEvent(Events.ID.BRUSH_COLOR_H);
			this.setEmitEvent(Events.ID.INPUT_COLOR_CHANGE_H);
		}

		public hue = (v: number[]) => {

			let value = v[0];

			// different parts of the spectrum have different brightness values assigned to them
			if (value > .575 || value < .1)
				value = 1;
			else
				value = 0;
			
			this._selector.colorHsv.z = value;
			this._selector.updateColor();
		}
	}

	export class SaturationSlider extends AreaSlider {
		constructor(
			x: number,
			y: number,
			width: number,
			height: number,
			paddingLeft: number,
			paddingRight: number,
			hue: number,
			value: number
		) {
			super(x, y, width, height, paddingLeft, paddingRight, 'S');
			this.areaFilter = new Sha.HorizontalSaturationFilter(this.centerArea, renderer.view, hue, value);
			this.value([value]);

			// Events
			Events.register(Events.ID.BRUSH_COLOR_H, this.hue);
			Events.register(Events.ID.BRUSH_COLOR_V, this.value);
			this.setListenEvent(Events.ID.BRUSH_COLOR_S);
			this.setEmitEvent(Events.ID.INPUT_COLOR_CHANGE_S);
		}

		public hue = (value: number[]) => {
			(<Sha.HorizontalSaturationFilter>this.areaFilter).hue = value[0];
		}
		public value = (value: number[]) => {
			(<Sha.HorizontalSaturationFilter>this.areaFilter).value = value[0];
			
			this._selector.colorHsv.z = Math.round(1 - value[0]);
			this._selector.updateColor();
		}
	}

	export class ValueSlider extends AreaSlider {
		constructor(
			x: number,
			y: number,
			width: number,
			height: number,
			paddingLeft: number,
			paddingRight: number,
			hue: number,
			saturation: number
		) {
			super(x, y, width, height, paddingLeft, paddingRight, 'V');
			this.areaFilter = new Sha.HorizontalValueFilter(this.centerArea, renderer.view, hue, saturation);
			this.value([0]);

			// Events
			Events.register(Events.ID.BRUSH_COLOR_H, this.hue);
			Events.register(Events.ID.BRUSH_COLOR_S, this.saturation);
			Events.register(Events.ID.BRUSH_COLOR_V, this.value);
			this.setListenEvent(Events.ID.BRUSH_COLOR_V);
			this.setEmitEvent(Events.ID.INPUT_COLOR_CHANGE_V);
		}

		public hue = (value: number[]) => {
			(<Sha.HorizontalValueFilter>this.areaFilter).hue = value[0];
		}
		public saturation = (value: number[]) => {
			(<Sha.HorizontalValueFilter>this.areaFilter).saturation = value[0];
		}

		public value = (value: number[]) => {
			this._selector.colorHsv.z = Math.round(1 - value[0]);
			this._selector.updateColor();
		}
	}
}