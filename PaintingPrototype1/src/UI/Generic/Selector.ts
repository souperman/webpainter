﻿module KPaint.UI {
	/*
		A selector is sprite that shows a dot
		It has two color components: an inner circle, and a ring around the outside.
		The graphics are procedurally generated
	*/
	export class Selector extends DisplayObject {
		public colorHsv: Vec3;

		protected _selector: PIXI.Sprite;
		protected _outer: PIXI.RenderTexture;
		protected _colorFilter: Sha.ColorFilter;
		protected _radius: number;

		constructor(x: number, y: number, radius: number) {
			super(x, y);
			this._radius = radius;

			this.colorHsv = new Vec3();
			this._outer = new PIXI.RenderTexture(renderer, radius * 2, radius * 2);
			this._selector = new PIXI.Sprite(this._outer);
			let sel = this._selector;
			sel.shader = new Sha.SelectorCircleFilter();
			sel.anchor.x = 0.5;
			sel.anchor.y = 0.5;

			// color  filter
			let rgb = this._localVec;
			hsvToRgb(this.colorHsv, rgb);
			this._colorFilter = new Sha.ColorFilter(rgb.r, rgb.g, rgb.b);

			sel.filters = [this._colorFilter];

			this.addChildPIXI(sel);
		}

		public updateColor(): void {
			this._selector.cacheAsBitmap = false;
			let rgb = this._localVec;
			hsvToRgb(this.colorHsv, rgb);

			let colFilter = this._colorFilter;
			colFilter.r = rgb.r;
			colFilter.g = rgb.g;
			colFilter.b = rgb.b;

			this._selector.cacheAsBitmap = true;
		}
	}
}