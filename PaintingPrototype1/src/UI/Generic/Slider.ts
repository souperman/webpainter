﻿module KPaint.UI {
	export class Slider extends Clickable2 {
		public centerArea = new Vec4();
		

		// misc
		protected _selector: Selector;
		protected _background = new PIXI.Graphics();
		protected _midY: number;
		protected _eventEmit: Events.ID;
		protected _eventListen: Events.ID;
		protected _preText = new PIXI.Text('', { font: '14px arial', strokeThickness: 0.3 });
		protected _postText = new PIXI.Text('', { font: '12px arial' });

		protected _maxNumber = 1;


		constructor(
			x: number,
			y: number,
			width: number,
			height: number,
			paddingLeft: number,
			paddingRight: number,
			preText: string,
			eventEmit: Events.ID = null,
			eventListen: Events.ID = null
		) {
			super(x, y, width, height);

			x = 0;
			y = 0;

			let midX = x;
			let midY = y + height / 2;

			this._midY = midY;
			this.centerArea.xyzw(x + paddingLeft, y, width - paddingLeft - paddingRight, height);
			this._selector = new Selector(midX, midY, Math.round(width / 32));
			

			this.generateSliderLine();

			let pre = this._preText;
			pre.text = preText;
			pre.x = x + paddingLeft / 2;
			pre.y = midY;
			pre.anchor.y = 0.5;
			pre.anchor.x = 0.5;

			let post = this._postText;
			post.x = x + width - paddingRight / 2;
			post.y = midY;
			post.anchor.y = 0.5;
			post.anchor.x = 0.5;


			// add to container
			this.addChildPIXI(this._background);
			this.addChildPIXI(this._preText);
			this.addChildPIXI(this._postText);
			this.addChild(this._selector);
			this.percent = 0.0;



			// register event
			this._eventEmit = eventEmit;
			this._eventListen = eventListen;
			Events.register(eventListen, (n: number[]) => this.number = n[0]);
		}


		get percent(): number {
			return (this._selector.x - this.centerArea.x) / this.centerArea.width;
		}
		set percent(value: number) {
			let x = this.centerArea.x + this.centerArea.width * value;
			this._selector.x = x;
			this._selector.y = this._midY;
			this._postText.text = this.percent.toFixed(2).toString();
		}

		get number(): number {
			return (this.percent * this._maxNumber);
		}
		set number(value: number) {
			this.percent = utils.clamp(value / this._maxNumber, 0, 1);
			this._postText.text = this.number.toFixed(2).toString();
		}

		get max(): number {
			return this._maxNumber;
		}
		set max(value: number) {
			this._maxNumber = value;
		}




		public clickHandler(pt: Vec3): void {
			let x = pt.x,
				y = this._midY,
				sx = this.centerArea.x,
				sw = this.centerArea.width;


			if (x < sx)
				x = sx;
			if (x > sx + sw)
				x = sx + sw;

			this._selector.x = x;

			// emit event, if one is registered
			if (this._eventEmit !== null) {
				Events.emit(this._eventEmit, this.number);
			}
		}



		public releaseHandler(): void { }


		private generateSliderLine() {
			let a = this.centerArea;

			// background
			this._background.beginFill(0x000000)
				//.drawRect(a.x, a.y, a.width, a.height)
				.lineStyle(4, 0x000000, 1.0)
				.moveTo(a.x, this._midY)
				.lineTo(a.x + a.width, this._midY)
				.endFill();
		}
	}

}