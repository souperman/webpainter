﻿module KPaint.UI {
	export class DisplayObject {
		protected _container = new PIXI.Container();

		// for use by the toLocal and vecToLocal functions. Make sure not to send this outside of the class!
		protected _localVec = new Vec3();



		public get x() { return this._container.x; }
		public set x(value: number) { this._container.x = value; }

		public get y() { return this._container.y; }
		public set y(value: number) { this._container.y = value; }

		public get visible() { return this._container.visible; }
		public set visible(value: boolean) { this._container.visible = value; }



		constructor(x: number, y: number) {
			this.x = x;
			this.y = y;
		}



		public addChild(object: DisplayObject) {
			this._container.addChild(object._container);
		}
		public addChildAt(object: DisplayObject, idx: number) {
			this._container.addChildAt(object._container, idx);
		}
		public addChildPIXI(object: PIXI.DisplayObject) {
			this._container.addChild(object);
		}

		public removeChild(object: DisplayObject) {
			this._container.removeChild(object._container);
		}

		public setParent(object: DisplayObject) {
			object.addChild(this);
		}
		
		public setParentPIXI(container: PIXI.Container) {
			container.addChild(this._container);
		}

		public toLocal(x: number, y: number) {
			return this._localVec.xyz(x - this.x, y - this.y, 0);
		}

		public vecToLocal(vec: Vec2 | Vec3) {
			return this._localVec.xyz(vec.x - this.x, vec.y - this.y, (<Vec3>vec).z || 0);
		}
	}
}