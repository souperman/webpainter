﻿/// <reference path="Clickable.ts"/>


module KPaint.UI {
	export class Button extends Clickable2 {
		public emit: Events.ID;

		protected _stayDown = false;

		protected _bg = new PIXI.Graphics();
		protected _bgClicked = new PIXI.Graphics();
		protected _text = new PIXI.Text('', { font: '11px Arial', fill: 0x000000, strokeThickness: 0.25 });



		constructor(x: number, y: number, width: number, height: number, color: number, text: string = null, emit: Events.ID = null) {
			super(x, y, width, height);

			x = 0;
			y = 0;


			if (emit)
				this.emit = emit;

			if (text)
				this._text.text = text;
			this._text.x = width / 2;
			this._text.y = height / 2;
			this._text.anchor.x = 0.5;
			this._text.anchor.y = 0.5;

			this._bg = this.generateBg(color);

			let clickedColor = ((0xffffff - color) > color) ? (color + 0x222222) : (color - 0x222222);
			this._bgClicked = this.generateBg(clickedColor);
			this._bgClicked.visible = false;

			this.addChildPIXI(this._bg);
			this.addChildPIXI(this._bgClicked);
			this.addChildPIXI(this._text);
		}


		get stayDown() {
			return this._stayDown;
		}
		set stayDown(value: boolean) {
			this._stayDown = value;
			if (value === true) {
				this.setButtonPressed();
			}
			else {
				this.setButtonNotPressed();
			}
		}

		public setButtonPressed() {
			this._bg.visible = false;
			this._bgClicked.visible = true;
		}

		public setButtonNotPressed() {
			this._bg.visible = true;
			this._bgClicked.visible = false;
		}


		protected clickHandler(pt: Vec3) {
			if (this.emit !== null && !this.isActive) {
				this.setButtonPressed();
				Events.emit(this.emit);
			}
		}

		protected releaseHandler() {
			if (this.stayDown !== true) {
				this.setButtonNotPressed();
			}
		}


		protected generateBg(color: number): PIXI.Graphics {
			let lineColor = ((0xffffff - color) > color) ? 0xdddddd : 0x222222;

			let width = this.width,
				height = this.height;

			return new PIXI.Graphics()
				.beginFill(color)
				.lineStyle(2, lineColor, 1)
				.drawRect(0, 0, width, height)
				.moveTo(0, 0)
				.lineTo(width, 0)
				.lineTo(width, height)
				.lineTo(0, height)
				.lineTo(0, 0)
				.endFill();
		}
	}


	// a button that will stay down when pressed, and pressing it again will take it back up
	// works just like a checkbox in html
	export class TwoStateButton extends Button {
		protected _isDown = false;


		constructor(x: number, y: number, width: number, height: number, color: number, text: string = null, emit: Events.ID = null) {
			super(x, y, width, height, color, text, emit);
		}


		protected clickHandler(pt: Vec3) {
			if (this._isDown) {
				this.setButtonNotPressed();
				this._isDown = false;
			}
			else {
				this.setButtonPressed();
				this._isDown = true;
			}
		}

		protected releaseHandler() { }
	}
}