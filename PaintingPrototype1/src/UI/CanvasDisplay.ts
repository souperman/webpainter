﻿module KPaint.UI {
	export class CanvasDisplay extends DisplayObject {
		public offCenter = new Vec2();
		
		protected _area = new Vec4();
		protected _context: PaintingContext.PaintingContext;
		protected _clickPos = new Vec2(null, null);
		protected _brushStrokePoints: Vec3[] = [];


		constructor(x: number, y: number, width: number, height: number, context: PaintingContext.PaintingContext) {
			super(x, y);
			this._area.xyzw(x, y, width, height);

			this._context = context;
			
			this.addChildPIXI(context.container);

			
			// listen to events
			let evRegister = Events.register;
			let evId = Events.ID;
			// paint
			evRegister(evId.STROKE_START, this.brushStrokeStart);
			evRegister(evId.STROKE_ADDPOINTS, this.brushStroke);
			evRegister(evId.STROKE_END, this.brushStrokeEnd);
			evRegister(evId.POINTERS_DISPATCHED, this.brushStrokePointersDispatched);
			// move
			evRegister(evId.INPUT_MOVE_CANVAS, this.moveToLocation);
			evRegister(evId.MOVE_CANVAS_START, this.moveCanvas);
			evRegister(evId.MOVE_CANVAS_CONTINUE, this.moveCanvas);
			evRegister(evId.MOVE_CANVAS_END, this.moveCanvasEnd);
			// zoom
			evRegister(evId.ZOOM_CANVAS_START, this.zoomCanvas);
			evRegister(evId.ZOOM_CANVAS_CONTINUE, this.zoomCanvas);
			evRegister(evId.ZOOM_CANVAS_END, this.zoomCanvasEnd);
			evRegister(evId.INPUT_ZOOM_CANVAS, (a: number[]) => this.scale = a[0]);
			evRegister(evId.INPUT_ZOOM_CANVAS_ADD, (a: number[]) => this.scale *= 1.1);
			evRegister(evId.INPUT_ZOOM_CANVAS_SUBTRACT, (a: number[]) => this.scale *= .9);
			evRegister(evId.INPUT_ZOOM_CANVAS_RESET, this.scaleToFit);
			// misc
			evRegister(evId.INPUT_DRAG_BRUSH_SIZE, this.brushSize);
			evRegister(evId.POINTER_UP, this.brushSizeEnd);
			evRegister(evId.INPUT_PICK_COLOR, this.pickColor);
		}


		protected get scale() {
			return this._context.container.scale.x;
		}
		protected set scale(value: number) {
			value = utils.clamp(value, 0.02, 5);
			let ctxScale = this._context.container.scale;
			ctxScale.x = value;
			ctxScale.y = value;
			
			this.updatePosition();
			Events.emit(Events.ID.ZOOM_SET_ZOOM, this.scale);
		}



		protected updatePosition() {
			let ctxCon = this._context.container;
			let offSet = this.offCenter;
			let scale = this.scale;
			let area = this._area;
			let size = this._context.size;

			let xCenter = area.width / 2,
				yCenter = area.height / 2;
			
			let xScaledHalfSize = ((size.x * scale) / 2),
				yScaledHalfSize = ((size.y * scale) / 2);

			// center - scaled context halfsize + offset
			ctxCon.x = xCenter - xScaledHalfSize + offSet.x * scale;
			ctxCon.y = yCenter - yScaledHalfSize + offSet.y * scale;
		}



		/*
			Resize the area that this element uses
		*/
		public resizeArea(width: number, height: number) {
			let area = this._area;
			area.width = width;
			area.height = height;

			this._context.resizeViewArea(width, height);
			this.scaleToFit();
		}



		/*
			Set scale such that the whole canvas is on screen.
		*/
		public scaleToFit = () => {
			let area = this._area;
			let ctxSize = this._context.size;

			// pad the size so that there is some space between the canvas and the UI
			let xScale = area.width / (ctxSize.x + 40),
				yScale = area.height / (ctxSize.y + 40);

			this.scale = Math.min(xScale, yScale);
			this.offCenter.xy(0.5, 0.5);
			this.updatePosition();
		}


		public contains(global: Vec2 | Vec3): boolean {
			return this._area.contains2D(global);
		}










		/*
			Move to a normalized in the range [0, 1].

			Used by the navigator.
		*/
		protected moveToLocation = (nLoc: Vec2[]) => {
			let ctxSize = this._context.size;
			let scale = this.scale;

			let x = (1 - nLoc[0].x) * ctxSize.x,
				y = (1 - nLoc[0].y) * ctxSize.y;


			x -= (ctxSize.x) / 2;
			y -= (ctxSize.y) / 2;

			this.offCenter.xy(x, y);

			// update
			this.updatePosition();
		}


		


		/*
			Continue moving the canvas
		*/
		protected moveCanvas = (pointer: Vec3[]) => {
			let ctxSize = this._context.size;
			let offSet = this.offCenter;
			let scale = this.scale;
			let clickPos = this._clickPos;
			let area = this._area;
			let clamp = utils.clamp;

			let local = this.vecToLocal(pointer[0]);
			if (clickPos.x === null) {
				clickPos.xy(local.x, local.y);
			}

			// add to offset
			offSet.x += (local.x - clickPos.x) / scale;
			offSet.y += (local.y - clickPos.y) / scale;
			
			// save click position for next time this function is called
			clickPos.xy(local.x, local.y);

			
			// clamp the offset so it doesn't go too far offscreenz
			let xMax = area.width / 2 + (ctxSize.x * scale) / 4,
				yMax = area.height / 2 + (ctxSize.y * scale) / 4;
			offSet.x = clamp(offSet.x, -xMax, xMax);
			offSet.y = clamp(offSet.y, -yMax, yMax);
			
			// update
			this.updatePosition();
		}
		

		/*
			Stop moving the canvas
		*/
		protected moveCanvasEnd = () => {
			this._clickPos.xy(null, null);
		}







		
		
		protected _initialScale = 1;

		/*
			Continue zooming the canvas
		*/
		protected zoomCanvas = (pointer: Vec3[]) => {
			let local = this.vecToLocal(pointer[0]),
				clickPos = this._clickPos;

			if (clickPos.x === null) {
				clickPos.xy(local.x, local.y);
				this._initialScale = this.scale;
			}

			// do some weird stuff
			let d = (local.x - clickPos.x) / this._area.width;
			d *= 5;


			this.scale = this._initialScale + d;


			// update
			this.updatePosition();
		}


		protected zoomCanvasEnd = () => {
			this._clickPos.xy(null, null);
		}








		/*
			Increase brush size through ctrl-alt-dragging
		*/
		protected brushSize = (pointer: Vec3[]) => {
			let local = this.vecToLocal(pointer[0]);
			let clickPos = this._clickPos;

			if (clickPos.x === null) {
				clickPos.xy(local.x, local.y);
			}
			

			// output the size event
			let evEmit = Events.emit;
			let evId = Events.ID;
			evEmit(evId.INPUT_BRUSH_SIZE, Math.abs(local.x - clickPos.x));

			// get delta vector for the cursor preview
			local.x = 0;
			local.y = (clickPos.y - local.y);

			// output the preview cursor event
			evEmit(evId.INPUT_DRAG_BRUSH_SIZE_PREVIEW, local);
		}

		protected brushSizeEnd = (pointer: Vec3[]) => {
			this._clickPos.xy(null, null);
		}

		



		/*
			Brush strokes
		*/
		protected brushStrokeStart = (pointer: Vec3[]) => {
			let local = this.toContextCoord(pointer[0]);
			this._context.startStroke(local);
		}

		protected brushStroke = (pointer: Vec3[]) => {
			if (this._context.isMidStroke === true) {
				let local = this.toContextCoord(pointer[0]);
				this._brushStrokePoints.push(op.vec3.clone(local));
			}
		}

		protected brushStrokePointersDispatched = () => {
			if (this._brushStrokePoints.length > 0) {
				let pts = this._brushStrokePoints;
				this._context.addPoint(pts);

				let vec3op = op.vec3;
				for (let i = 0, n = pts.length; i < n; i++) {
					vec3op.recycle(pts[i]);
				}
				pts.length = 0;
			}
		}

		protected brushStrokeEnd = () => {
			if (this._brushStrokePoints.length > 0) {
				this.brushStrokePointersDispatched();
			}
			if (this._context.isMidStroke === true) {
				this._context.endStroke();
				renderCoordinator.requestRender();
			}
		}






		protected pickColor = (pointer: Vec3[]) => {
			let local = this.vecToLocal(pointer[0]);

			// convert to canvas coordinate
			let pos = this._context.container.position;

			local.x -= pos.x;
			local.y -= pos.y;

			local.divide(this.scale);

			this._context.pickColor(local);
		}



		



		/*
			Modifies _local to context version of global coordinate, and then return it. 
		*/
		protected toContextCoord(global: Vec3): Vec3 {
			let ctxCon = this._context.container;
			let scale = this.scale;
			let local = this._localVec;
			let area = this._area;


			local.x = (global.x - area.x - ctxCon.x) / scale;
			local.y = (global.y - area.y - ctxCon.y) / scale;
			local.z = global.z;
			return local;
		}

	}
}