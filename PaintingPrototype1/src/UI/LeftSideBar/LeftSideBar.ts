﻿module KPaint.UI {
	export class SideBarLeft extends ClickableContainer2 {
		// child elements
		protected _wheel: ColorWheel;
		protected _hsvSliderArea: HsvSliderArea;
		protected _buttonArea: BrushButtonArea;
		protected _colorDisplay: ColorDisplay;
		protected _brushSettings: BrushSettings;
		protected _downloadButton: Button;

		// misc
		protected _background = new PIXI.Graphics();
		protected _bgColor = 0;


		constructor(x: number, y: number, width: number, height: number, bgColor: number) {
			super(x, y, width, height);
			
			x = 0;
			y = 0;

			let spaceBetweenElements = 8;

			// background
			this._bgColor = bgColor;
			this.generateBackground();

			// sub elements
			let elHeight = width;
			this._wheel = new ColorWheel(x, y, width, width);
			let w = this._wheel;
			w.value = 0;
			
			// Hsv sliders
			y += elHeight;
			y += spaceBetweenElements;
			elHeight = width * 0.45;
			this._hsvSliderArea = new HsvSliderArea(x, y, width, elHeight, w.hue, w.saturation, w.value);
			

			// TODO: Brush/Erase buttons
			y += elHeight
			y += spaceBetweenElements;
			elHeight = width * 0.4;
			this._buttonArea = new BrushButtonArea(x, y, width * 0.6, elHeight);
			
			// color display
			this._colorDisplay = new ColorDisplay(x + width * 0.6 + elHeight / 4, y + elHeight / 4, elHeight, elHeight / 2);



			// brush sliders
			y += elHeight;
			y += spaceBetweenElements;
			elHeight = width * 0.9;
			this._brushSettings = new BrushSettings(x, y, width, elHeight);



			y += elHeight;
			y += spaceBetweenElements;
			elHeight = width * 0.15;
			this._downloadButton = new Button(x + spaceBetweenElements, y, elHeight, elHeight, bgColor + 0x111111, 'DL', Events.ID.INPUT_DOWNLOAD_IMAGE);


			// add children to container
			this.addChildPIXI(this._background);
			this.addChild(w);
			this.addChild(this._hsvSliderArea);
			this.addChild(this._buttonArea);
			this.addChild(this._colorDisplay);
			this.addChild(this._brushSettings);
			this.addChild(this._downloadButton);


			// add click listeners to children
			this.addClickListener(w);
			this.addClickListener(this._hsvSliderArea);
			this.addClickListener(this._buttonArea);
			this.addClickListener(this._colorDisplay);
			this.addClickListener(this._brushSettings);
			this.addClickListener(this._downloadButton);
		}

		public resize(width: number, height: number) {
			let bigger = height > this.height;
			this.height = height;

			if (bigger === true) {
				this.generateBackground();
			}
		}

		private generateBackground() {
			this._background
				.clear()
				.beginFill(this._bgColor)
				.drawRect(this.x, this.y, this.width, this.height)
				.endFill();
		}
	}
}