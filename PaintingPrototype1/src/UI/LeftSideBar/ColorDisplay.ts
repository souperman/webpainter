﻿module KPaint.UI {
	export class ColorDisplay extends Clickable2 {
		protected _primaryColorHsv = new Vec3(0, 0, 0);
		protected _secondaryColorHsv = new Vec3(0, 0, 1);
		
		protected _primarySprite: PIXI.Sprite;
		protected _secondarySprite: PIXI.Sprite;

		protected _colorFilterPrimary = new Sha.ColorFilter(0, 0, 0);
		protected _colorFilterSecondary = new Sha.ColorFilter(1, 1, 1);

		protected _isSwapped = false;

		constructor(x: number, y: number, width: number, height: number) {
			super(x, y, width, height);

			x = 0;
			y = 0;


			let cSize = height * 0.67;

			let graph = new PIXI.Graphics()
				.beginFill(0xff0000)
				.drawRect(0, 0, cSize, cSize)
				.endFill();
			let rt = new PIXI.RenderTexture(renderer, cSize, cSize);
			rt.render(graph);
			graph.destroy(true);
			
			let primarySprite = new PIXI.Sprite(rt);
			primarySprite.x = x;
			primarySprite.y = y;

			x += height * 0.33;
			y += height * 0.33;

			let secondarySprite = new PIXI.Sprite(rt);
			secondarySprite.x = x;
			secondarySprite.y = y;

			// add color filters
			let cfPrimary = this._colorFilterPrimary;
			let cfSecondary = this._colorFilterSecondary;
			primarySprite.filters = [cfPrimary];
			secondarySprite.filters = [cfSecondary];

			this._primarySprite = primarySprite;
			this._secondarySprite = secondarySprite;

			// remove gamma correction on the color filters
			cfPrimary.gamma = 1;
			cfSecondary.gamma = 1;

			
			// set colors
			this.updateColorPrimary();
			this.updateColorSecondary();


			// add to container
			this.addChildPIXI(secondarySprite);
			this.addChildPIXI(primarySprite);

			// events
			Events.register(Events.ID.BRUSH_COLOR_H, (v: number[]) => this.updateHue(v[0]));
			Events.register(Events.ID.BRUSH_COLOR_S, (v: number[]) => this.updateSaturation(v[0]));
			Events.register(Events.ID.BRUSH_COLOR_V, (v: number[]) => this.updateValue(v[0]));
			Events.register(Events.ID.INPUT_COLOR_SWAP, this.swap);
		}

		protected clickHandler(pt: Vec3) {
			if (!this.isActive) {
				Events.emit(Events.ID.INPUT_COLOR_SWAP);
			}
		}

		protected releaseHandler() { }

		protected swap = () => {
			// swap colors
			let hsv = this._secondaryColorHsv;
			this._secondaryColorHsv = this._primaryColorHsv;
			this._primaryColorHsv = hsv;

			this.updateColorPrimary();
			this.updateColorSecondary();

			let evEmit = Events.emit;
			let evId = Events.ID;
			evEmit(evId.INPUT_COLOR_CHANGE_H, hsv.h);
			evEmit(evId.INPUT_COLOR_CHANGE_S, hsv.s);
			evEmit(evId.INPUT_COLOR_CHANGE_V, hsv.v);
		}

		protected updateColorPrimary(): void {
			this._container.cacheAsBitmap = false;
			let rgb = this._localVec;
			hsvToRgb(this._primaryColorHsv, rgb);

			let colorFilter = this._colorFilterPrimary;
			colorFilter.r = rgb.r;
			colorFilter.g = rgb.g;
			colorFilter.b = rgb.b;

			this._container.cacheAsBitmap = true;
		}

		protected updateColorSecondary(): void {
			this._container.cacheAsBitmap = false;
			let rgb = this._localVec;
			hsvToRgb(this._secondaryColorHsv, rgb);

			let colorFilter = this._colorFilterSecondary;
			colorFilter.r = rgb.r;
			colorFilter.g = rgb.g;
			colorFilter.b = rgb.b;

			this._container.cacheAsBitmap = true;
		}

		protected updateHue = (value: number) => {
			this._primaryColorHsv.x = value;
			this.updateColorPrimary();
		}
		protected updateSaturation = (value: number) => {
			this._primaryColorHsv.y = value;
			this.updateColorPrimary();
		}
		protected updateValue = (value: number) => {
			this._primaryColorHsv.z = value;
			this.updateColorPrimary();
		}
	}
}