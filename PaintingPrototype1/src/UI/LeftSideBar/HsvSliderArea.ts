﻿module KPaint.UI {
	export class HsvSliderArea extends ClickableContainer2 {
		// child elements
		private _hSlider: HueSlider;
		private _sSlider: SaturationSlider;
		private _vSlider: ValueSlider;

		constructor(x: number, y: number, width: number, height: number, h: number, s: number, v: number) {
			super(x, y, width, height);

			x = 0;
			y = 0;
			
			let sliderX = x;
			let sliderY = y;
			let sliderWidth = width;
			let sliderHeight = height / 3;
			let paddingLeft = width * 0.15;
			let paddingRight = width * 0.15;


			// create the sliderS
			this._hSlider = new HueSlider(sliderX, sliderY, sliderWidth, sliderHeight, paddingLeft, paddingRight);
			sliderY += sliderHeight;
			this._sSlider = new SaturationSlider(sliderX, sliderY, sliderWidth, sliderHeight, paddingLeft, paddingRight, h, v);
			sliderY += sliderHeight;
			this._vSlider = new ValueSlider(sliderX, sliderY, sliderWidth, sliderHeight, paddingLeft, paddingRight, h, s);


			// add to container
			this.addChild(this._hSlider);
			this.addChild(this._sSlider);
			this.addChild(this._vSlider);

			// add children to Clickablecontainer
			this.addClickListener(this._hSlider);
			this.addClickListener(this._sSlider);
			this.addClickListener(this._vSlider);
		}
	}
}