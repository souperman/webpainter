﻿module KPaint.UI {
	export class ColorWheel extends Clickable2 {
		protected _colorRing: PIXI.Sprite;
		protected _svElement: ColorWheelSvArea;
		
		// other variables
		protected _hSelector: Selector;
		protected _hPositions: Vec2[] = [];


		constructor(x: number, y: number, width: number, height: number) {
			super(x, y, width, height);
			
			let colorRing = new PIXI.Sprite(new PIXI.RenderTexture(renderer, width, height));
			colorRing.shader = new Sha.HueCircleFilter(0.4, 0.06);
			colorRing.cacheAsBitmap = true;
			this._colorRing = colorRing;

			// selector
			this.generateHPositions();
			let initialPos = this._hPositions[0];
			this._hSelector = new Selector(initialPos.x, initialPos.y, Math.round(width / 32));


			// middle element
			this._svElement = new ColorWheelSvArea(width / 4, height / 4, width / 2, height / 2);
			this.setSelectorColor();
			

			// add to containers
			this.addChildPIXI(colorRing);
			this.addChild(this._hSelector);
			this.addChild(this._svElement);
			
			Events.register(Events.ID.BRUSH_COLOR_H, (v: number[]) => this.hueChangedCallback(v[0]));
		}


		get hue() { return this._svElement.hue; }
		set hue(value: number) { this._svElement.hue = value; }

		get saturation() { return this._svElement.hue; }
		set saturation(value: number) { this._svElement.hue = value; }

		get value() { return this._svElement.hue; }
		set value(value: number) { this._svElement.hue = value; }


		protected setSelectorPosition(x: number, y: number): void {
			// find the closest position in _hPositions
			let hPos = this._hPositions,
				closest = hPos[0],
				closestDist = 9999,
				idx = -1,
				n = hPos.length,
				pos: Vec2,
				dist: number,
				distFunc = utils.distance;

			for (let i = 0; i < n; i++) {
				pos = hPos[i];
				dist = distFunc(x, y, pos.x, pos.y);
				if (closestDist > dist) {
					closest = pos;
					closestDist = dist;
					idx = i;
				}
			}

			// set selector's position to that of closest
			this._hSelector.x = closest.x;
			this._hSelector.y = closest.y;
		
			// calculate hue from index
			let h = idx / hPos.length;
			
			this._svElement.hue = h;

			this.setSelectorColor();
		}



		protected setSelectorColor(): void {
			let h = this._svElement.hue;

			// different parts of the spectrum have different brightness values assigned to them
			if (h > .575 || h < .1)
				h = 1;
			else
				h = 0;
			this._hSelector.colorHsv.z = h;
			this._hSelector.updateColor();
		}



		// fill _hPositions with potential positions for the hue selector
		protected generateHPositions(n = 256): void {
			let cosFunc = Math.cos,
				sinFunc = Math.sin,
				PI = Math.PI;

			let hPos = this._hPositions;
			let centerX = this.x + this.width / 2;
			let centerY = this.y + this.height / 2;
			let radius = (this.width - centerX) * 0.8;
			let radian = Math.PI * 2 / (n + 1);

			let x: number,
				y: number;
			for (let i = 0; i < n; i++) {
				x = centerX + radius * cosFunc(i * radian - PI);
				y = centerY + radius * sinFunc(i * radian - PI);
				hPos.push(new Vec2(x, y));
			}
		}
		


		/*
			Once an element is clicked, it is set as the active elements, and all
			mouse movements will be sent to that element. Use releaseClick()
			to deactivate the element on mouseup.
		*/
		protected clickHandler(pt: Vec3) {
			let local = this.vecToLocal(pt);


			// check if child element is active
			if (this._svElement.isActive) {
				this._svElement.click(local);
			}
			
			// check if this element is active
			else if (this.isActive) {
				this.setSelectorPosition(local.x, local.y);
			}

			// check if within bounds of child element
			else if (this._svElement.contains(local)) {
				this._svElement.click(local);
			}

			// otherwise put the click on this element
			else {
				this.setSelectorPosition(local.x, local.y);
				this.isActive = true;
			}
		}



		protected releaseHandler(): void {
			this._svElement.releaseClick();
		}


		
		protected hueChangedCallback = (h: number) => {
			let idx = Math.round(h * (this._hPositions.length - 1));
			let pos = this._hPositions[idx];
			
			this._hSelector.x = pos.x;
			this._hSelector.y = pos.y;

			this.setSelectorColor();
		}
	}
}