﻿module KPaint.UI {
	// saturation and value area of the color wheel
	export class ColorWheelSvArea extends Clickable2 {
		protected _svAreaSprite: PIXI.Sprite;
		protected _svAreaFilter: Sha.SaturationValueFilter2;
		protected _svSelector: Selector;
		protected _colorHsv = new Vec3();

		constructor(x: number, y: number, width: number, height: number) {
			super(x, y, width, height);
			
			let colorHsv = this._colorHsv;
			colorHsv.h = 0.0;
			colorHsv.s = 0.0;
			colorHsv.v = 0.0;

			this._svSelector = new Selector(0, 0, Math.round(width / 18));
			this.updateSelectorColor();

			// filter
			this._svAreaFilter = new Sha.SaturationValueFilter2(0.0);

			// sprite to apply filter to
			let rt = new PIXI.RenderTexture(renderer, width, height);
			let spr = new PIXI.Sprite(rt);

			spr.shader = this._svAreaFilter;
			spr.cacheAsBitmap = true;

			this._svAreaSprite = spr;

			// add children to container
			this.addChildPIXI(this._svAreaSprite);
			this.addChild(this._svSelector);

			this.updateSelectorPosition();

			// events 
			Events.register(Events.ID.BRUSH_COLOR_H, (v: number[]) => this.updateHue(v[0]));
			Events.register(Events.ID.BRUSH_COLOR_S, (v: number[]) => this.updateSaturation(v[0]));
			Events.register(Events.ID.BRUSH_COLOR_V, (v: number[]) => this.updateValue(v[0]));
		}


		// using the setters will emit events
		
		public get hue(): number {
			return this._colorHsv.h;
		}
		public set hue(h: number) {
			this.updateHue(h);
			Events.emit(Events.ID.INPUT_COLOR_CHANGE_H, h);
		}


		public get saturation(): number {
			return this._colorHsv.s;
		}
		public set saturation(s: number) {
			this.updateSaturation(s);
			Events.emit(Events.ID.INPUT_COLOR_CHANGE_S, s);
		}


		public get value(): number {
			return this._colorHsv.v;
		}
		public set value(v: number) {
			this.updateValue(v);
			Events.emit(Events.ID.INPUT_COLOR_CHANGE_V, v);
		}


		// update colors without emitting an event

		protected updateHue = (value: number) => {
			let svSpr = this._svAreaSprite;
			this._colorHsv.h = value;
			svSpr.cacheAsBitmap = false;
			this._svAreaFilter.hue = value;
			svSpr.cacheAsBitmap = true;
		}


		protected updateSaturation = (value: number) => {
			this._colorHsv.s = value;
			this.updateSelectorPosition();
		}


		protected updateValue = (value: number) => {
			this._colorHsv.v = value;
			this.updateSelectorPosition();
		}


		protected clickHandler(pt: Vec3): void {
			// create local copies for adjustment
			let local = this.vecToLocal(pt),
				x = local.x,
				y = local.y;
			
			let width = this.width;
			let height = this.height;
			let selector = this._svSelector;
			
			// keep the selector within bounds
			if (x < 0)
				x = 0;
			else if (x > width)
				x = width;
			if (y < 0)
				y = 0;
			else if (y > height)
				y = height;

			// set saturation and value
			this.saturation = x / width;
			this.value = 1 - (y / height);

			// set selector position
			selector.x = x;
			selector.y = y;
		}

		protected releaseHandler(): void { }



		// updates selector position based on current color
		protected updateSelectorPosition(): void {
			let colorHsv = this._colorHsv;
			let selector = this._svSelector;

			// set selector position
			selector.x = this.width * colorHsv.s;
			selector.y = this.height * (1 - colorHsv.v);
			this._svAreaFilter.hue = colorHsv.h;
			this.updateSelectorColor();
		}


		
		protected updateSelectorColor(): void {
			let colorHsv = this._colorHsv;
			let v = colorHsv.v;
			let selector = this._svSelector;

			v *= (1 - colorHsv.s);
			v = Math.round(1 - v);

			selector.colorHsv.v = v;
			selector.updateColor();
		}
	}
}