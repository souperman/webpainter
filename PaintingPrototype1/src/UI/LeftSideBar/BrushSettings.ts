﻿module KPaint.UI {
	export class BrushSettings extends ClickableContainer2 {
		private _brushSize: Slider;
		private _brushSizeMin: Slider;
		private _brushSoft: Slider;
		private _brushAlpha: Slider;
		private _brushSpacing: Slider;


		constructor(x: number, y: number, width: number, height: number) {
			super(x, y, width, height);
			
			x = 0;
			y = 0;


			let elementHeight = height / 5;

			// brush size slider
			this._brushSize = new Slider(x, y, width, elementHeight, width * 0.2, width * 0.2, 'size', Events.ID.INPUT_BRUSH_SIZE, Events.ID.BRUSH_SIZE);
			let bSize = this._brushSize;
			bSize.max = 200;


			// brush softness
			y += elementHeight;
			this._brushSizeMin = new Slider(x, y, width, elementHeight, width * 0.2, width * 0.2, 'min', Events.ID.INPUT_BRUSH_SIZE_MIN, Events.ID.BRUSH_SIZE_MIN);
			let bSizeMin = this._brushSizeMin;


			// brush softness
			y += elementHeight;
			this._brushSoft = new Slider(x, y, width, elementHeight, width * 0.2, width * 0.2, 'soft', Events.ID.INPUT_BRUSH_SOFTNESS, Events.ID.BRUSH_SOFTNESS);
			let bSoft = this._brushSoft;


			// brush softness
			y += elementHeight;
			this._brushAlpha= new Slider(x, y, width, elementHeight, width * 0.2, width * 0.2, 'flow', Events.ID.INPUT_BRUSH_FLOW, Events.ID.BRUSH_FLOW);
			let bAlph = this._brushAlpha;


			// brush flow slider
			y += elementHeight;
			this._brushSpacing = new Slider(x, y, width, elementHeight, width * 0.2, width * 0.2, 'spac', Events.ID.INPUT_BRUSH_SPACING, Events.ID.BRUSH_SPACING);
			let bSpace = this._brushSpacing;


			// add to container
			this.addChild(bSize);
			this.addChild(bSizeMin);
			this.addChild(bSoft);
			this.addChild(bAlph);
			this.addChild(bSpace);


			// add clicks
			this.addClickListener(bSize);
			this.addClickListener(bSizeMin);
			this.addClickListener(bSoft);
			this.addClickListener(bAlph);
			this.addClickListener(bSpace);
		}
	}
}