﻿module KPaint.UI {
	export class BrushButtonArea extends ClickableContainer2 {
		private _buttons: Button[] = [];
		private _brushPaintButton: Button;
		private _brushEraseButton: Button;
		private _moverToolButton: Button;
		
		constructor(x: number, y: number, width: number, height: number) {
			super(x, y, width, height);

			x = 0;
			y = 0;

			let spacePerButton = height / 2;
			let padding = 4;
			let buttonSize = spacePerButton - padding * 2;
			let color = 0x999999;

			let brushPaintButton = new Button(x + padding, y + padding, buttonSize, buttonSize, color, 'Paint', Events.ID.INPUT_TOOL_BRUSH_PAINT);
			brushPaintButton.stayDown = true;

			x += spacePerButton;
			let brushBlurButton = new Button(x + padding, y + padding, buttonSize, buttonSize, color, 'Blur', Events.ID.INPUT_TOOL_BRUSH_BLUR);

			x += spacePerButton;
			let brushEraseButton = new Button(x + padding, y + padding, buttonSize, buttonSize, color, 'Erase', Events.ID.INPUT_TOOL_BRUSH_ERASE);

			y += spacePerButton;
			x -= spacePerButton * 2;

			let zoomerToolButton = new Button(x + padding, y + padding, buttonSize, buttonSize, color, 'Zoom', Events.ID.INPUT_TOOL_ZOOMER);

			x += spacePerButton;
			let moverToolButton = new Button(x + padding, y + padding, buttonSize, buttonSize, color, 'Move', Events.ID.INPUT_TOOL_MOVER);

			x += spacePerButton;
			let fillerToolButton = new Button(x + padding, y + padding, buttonSize, buttonSize, color, 'Pick', Events.ID.INPUT_TOOL_COLOR_PICKER);
			


			// add to buttons array
			let buttons = this._buttons;
			buttons.push(brushPaintButton);
			buttons.push(brushEraseButton);
			buttons.push(brushBlurButton);
			buttons.push(moverToolButton);
			buttons.push(zoomerToolButton);
			buttons.push(fillerToolButton);


			let button: Button;
			for (let i = 0, n = buttons.length; i < n; i++) {
				button = buttons[i];
				this.addChild(button);
				this.addClickListener(button);
			}
			
			// callbacks to set the active brush mode
			Events.register(Events.ID.INPUT_TOOL_BRUSH_PAINT, () => { this.useButton(Events.ID.INPUT_TOOL_BRUSH_PAINT); });
			Events.register(Events.ID.INPUT_TOOL_BRUSH_BLUR, () => { this.useButton(Events.ID.INPUT_TOOL_BRUSH_BLUR); });
			Events.register(Events.ID.INPUT_TOOL_BRUSH_ERASE, () => { this.useButton(Events.ID.INPUT_TOOL_BRUSH_ERASE); });
			Events.register(Events.ID.INPUT_TOOL_ZOOMER, () => { this.useButton(Events.ID.INPUT_TOOL_ZOOMER); });
			Events.register(Events.ID.INPUT_TOOL_MOVER, () => { this.useButton(Events.ID.INPUT_TOOL_MOVER); });
			Events.register(Events.ID.INPUT_TOOL_COLOR_PICKER, () => { this.useButton(Events.ID.INPUT_TOOL_COLOR_PICKER); });
		}

		protected useButton(event: Events.ID) {
			let buttons = this._buttons;
			let button: Button;
			for (let i = 0, n = buttons.length; i < n; i++) {
				button = buttons[i];
				if (button.emit === event) {
					button.stayDown = true;
				}
				else {
					button.stayDown = false;
				}
			}
		}

		// expanding upon the default handler for custom functionality
		protected clickHandler(pt: Vec3) {
			if (!super.clickHandler(pt) || this.isActive) {
				return false;
			}

			let buttons = this._buttons;
			let button: Button;
			for (let i = 0, n = buttons.length; i < n; i++) {
				button = buttons[i];
				if (button.isActive) {
					this.useButton(button.emit);
				}
			}
			
			return true;
		}
	}
}