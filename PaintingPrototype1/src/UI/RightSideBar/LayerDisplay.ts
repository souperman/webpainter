﻿module KPaint.UI {
	export class LayerDisplay extends Clickable2 {
		public layer: PaintingContext.Layer = null;

		protected _bg = new PIXI.Graphics()
		protected _name = new PIXI.Text('default', { font: '16px arial', strokeThickness: 0.5 });
		protected _alpha = new PIXI.Text('default', { font: '12px arial' });

		constructor(x: number, y: number, width: number, height: number) {
			super(x, y, width, height);

			this.addChildPIXI(this._bg);
			this.addChildPIXI(this._name);
			this.addChildPIXI(this._alpha);
		}
		

		get alpha() {
			return this.layer.alpha;
		}
		set alpha(value: number) {
			this.layer.alpha = value;
			this._alpha.text = [(value * 100).toFixed(0).toString(), '%'].join('');
		}

		get layerId() {
			return this.layer.id;
		}

		get name() {
			return this._name.text;
		}
		

		public initialize(x: number, y: number, width: number, height: number, layer: PaintingContext.Layer) {
			this.layer = layer;
			
			this.x = x;
			this.y = y;
			this.width = width;
			this.height = height;

			let name = this._name;
			name.text = layer.name;
			name.x = width / 3;
			name.y = height / 3;
			name.anchor.y = 0.5;

			this.alpha = layer.container.alpha;
			let alpha = this._alpha;
			alpha.x = width / 3;
			alpha.y = height * 2 / 3;
			alpha.anchor.y = 0.5;

			this.generateGraphics(6);
		}

		public select() {
			Events.emit(Events.ID.INPUT_LAYER_SELECTED, this.layer);
		}


		public clickHandler(): void {
			this.select();
		}


		protected releaseHandler(): void { }


		protected generateGraphics(padding: number) {
			let width = this.width,
				height = this.height;

			this._bg
				.clear()
				.lineStyle(3, 0x000000, 1)
				.moveTo(padding, padding)	// pt 0
				.lineTo(width - padding, padding)	// pt 1
				.lineTo(width - padding, height - padding)	// pt 3
				.lineTo(padding, height - padding)			// pt 4
				.lineTo(padding, padding)						// pt 0
				.endFill();
		}
	}
}