﻿module KPaint.UI {

	/*
		Container class for the buttons that define operations on layers.
		
		Buttons:
			New:	Creates a new layer above the currently selected one.
			Merge:	Merges the currently selected layer down.
			Clear:	Clears the currently selected layer.
			Delete:	Deletes the currently selected layer.
			Lock:	Locks the pixels on the currently selected layer.

		Usually there would be a button for merging every layer, but that's not completely necessary, so
		I'll only add that if I have time.
	*/
	export class LayerButtonArea extends ClickableContainer2 {
		public container = new PIXI.Container();

		private _buttonNewLayer: Button;
		private _buttonMergeLayer: Button;
		private _buttonClearLayer: Button;
		private _buttonDeleteLayer: Button;
		private _buttonLockLayer: Button;

		private _layerAlpha: Slider;

		constructor(x: number, y: number, width: number, height: number) {
			super(x, y, width, height);

			x = 0;
			y = 0;


			let spacePerButton = width / 5;
			let sliderHeight = height -  spacePerButton;
			let bPad = 4;
			let buttonSize = spacePerButton - bPad * 2;


			this._buttonNewLayer = new Button(x + bPad, y + bPad, buttonSize, buttonSize, 0x999999, 'New', Events.ID.INPUT_LAYER_NEW);

			x += spacePerButton;
			this._buttonMergeLayer = new Button(x + bPad, y + bPad, buttonSize, buttonSize, 0x999999, 'Merge', Events.ID.INPUT_LAYER_MERGE);

			x += spacePerButton;
			this._buttonClearLayer = new Button(x + bPad, y + bPad, buttonSize, buttonSize, 0x999999, 'Clear', Events.ID.INPUT_LAYER_CLEAR);

			x += spacePerButton;
			this._buttonDeleteLayer = new Button(x + bPad, y + bPad, buttonSize, buttonSize, 0x999999, 'Delete', Events.ID.INPUT_LAYER_DELETE);

			x += spacePerButton;
			this._buttonLockLayer = new TwoStateButton(x + bPad, y + bPad, buttonSize, buttonSize, 0x999999, 'Lock', Events.ID.INPUT_LAYER_LOCK_PIXELS);


			x = 0;
			y += spacePerButton;
			this._layerAlpha = new Slider(x, y, width, sliderHeight, width * 0.25, width * 0.15, 'Alpha', Events.ID.INPUT_LAYER_ALPHA_CHANGED, Events.ID.LAYER_ALPHA);

			// add to container
			this.addChild(this._buttonNewLayer);
			this.addChild(this._buttonMergeLayer);
			this.addChild(this._buttonClearLayer);
			this.addChild(this._buttonDeleteLayer);
			this.addChild(this._buttonLockLayer);
			this.addChild(this._layerAlpha);


			// add listener
			this.addClickListener(this._buttonNewLayer);
			this.addClickListener(this._buttonMergeLayer);
			this.addClickListener(this._buttonClearLayer);
			this.addClickListener(this._buttonDeleteLayer);
			this.addClickListener(this._buttonLockLayer);
			this.addClickListener(this._layerAlpha);
		}
	}
}