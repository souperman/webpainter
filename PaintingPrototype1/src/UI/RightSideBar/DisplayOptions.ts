﻿module KPaint.UI {
	export class DisplayOptions extends ClickableContainer2 {
		private _buttonZoomPlus: Button;
		private _buttonZoomMinus: Button;
		private _buttonZoomReset: Button;

		protected _zoomSlider: Slider;

		constructor(x: number, y: number, width: number, height: number) {
			super(x, y, width, height);

			x = 0;
			y = 0;
			

			let buttonSize = height / 3;
			let elWidth = width - buttonSize - 4;


			this._zoomSlider = new Slider(x, y, elWidth, height, width * 0.25, width * 0.15, 'Zoom', Events.ID.INPUT_ZOOM_CANVAS, Events.ID.ZOOM_SET_ZOOM);
			this._zoomSlider.max = 5;


			x += elWidth;
			this._buttonZoomPlus = new Button(x, y, buttonSize, buttonSize, 0x999999, '+', Events.ID.INPUT_ZOOM_CANVAS_ADD);

			y += buttonSize;
			this._buttonZoomMinus = new Button(x, y, buttonSize, buttonSize, 0x999999, '-', Events.ID.INPUT_ZOOM_CANVAS_SUBTRACT);

			y += buttonSize;
			this._buttonZoomReset = new Button(x, y, buttonSize, buttonSize, 0x999999, 'R', Events.ID.INPUT_ZOOM_CANVAS_RESET);



			// add to container
			this.addChild(this._zoomSlider);
			this.addChild(this._buttonZoomPlus);
			this.addChild(this._buttonZoomMinus);
			this.addChild(this._buttonZoomReset);


			// add clickables
			this.addClickListener(this._zoomSlider);
			this.addClickListener(this._buttonZoomPlus);
			this.addClickListener(this._buttonZoomMinus);
			this.addClickListener(this._buttonZoomReset);
		}
	}
}