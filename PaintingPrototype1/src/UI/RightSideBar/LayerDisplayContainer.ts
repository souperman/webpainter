﻿module KPaint.UI {


	// object pool
	export class LayerDisplayPool extends IPool<LayerDisplay> {
		constructor() {
			super('LayerDisplay', 4);
			this.allocate();
		}
		protected createOne(): LayerDisplay {
			return new LayerDisplay(0, 0, 0, 0);
		}
	} 



	export class LayerDisplayContainer extends ClickableContainer2 {
		protected _pool = new LayerDisplayPool();
		protected _displays: LayerDisplay[] = [];
		protected _ldPositions: Vec2[] = [];

		protected _selectedGraphic = new PIXI.Graphics();
		protected _selectedIdx = 0;
		
		protected _ldHeight: number;




		constructor(x: number, y: number, width: number, height: number) {
			super(x, y, width, height);


			this._ldHeight = Math.round(height / (height / 60));

			this._selectedGraphic
				.beginFill(0xaaaaaa)
				.drawRect(0, 0, width, this._ldHeight)
				.endFill();


			// add children
			this.addChildPIXI(this._selectedGraphic);

			// register events
			Events.register(Events.ID.LAYER_ALPHA, this.layerAlphaChange);
			Events.register(Events.ID.LAYER_SELECTED, this.layerSelected);
			Events.register(Events.ID.LAYER_CREATED, this.addLayerDisplay);
			Events.register(Events.ID.LAYER_DELETED, this.removeLayer);
			Events.register(Events.ID.LAYER_MOVE, this.moveLayer);
		}


		protected addLayerDisplay = (arr: any) => {
			// create
			let ld = this._pool.get();
			ld.initialize(0, this._selectedIdx * this._ldHeight, this.width, this._ldHeight, arr[0]);

			let idx = <number>arr[1];

			// add to container, set listener
			this.addChildAt(ld, idx + 1); // idx 0 is reserved for bg
			this._displays.splice(idx, 0, ld);
			this.addClickListener(ld);

			// Reposition display objects to ensure they stay separate.
			this.repositionDisplays();
		}


		protected layerAlphaChange = (value: number[]) => {
			let disp = this._displays[this._selectedIdx];
			disp.alpha = value[0];
		}


		protected repositionDisplays() {
			let d = this._displays;
			let h = this._ldHeight;
			for (let i = 0, n = d.length; i < n; i++) {
				d[i].y = (n - i - 1) * h;
			}
			this._selectedGraphic.y = d[this._selectedIdx].y;
		}


		protected layerSelected = (layer: PaintingContext.Layer[]) => {
			let d = this._displays;for (let i = 0, n = d.length; i < n; i++) {
				if (d[i].layerId === layer[0].id) {
					this._selectedIdx = i;
					this._selectedGraphic.y = d[i].y;
					return;
				}
			}
		}


		protected removeLayer = (layer: PaintingContext.Layer[]) => {
			let displays = this._displays;
			for (let i = 0, n = displays.length; i < n; i++) {
				if (displays[i].layerId === layer[0].id) {
					let removed = displays.splice(i, 1)[0];

					this._pool.recycle(removed)
					this.removeChild(removed);
					this.removeClickable(removed);
					break;
				}
			}
			if (this._selectedIdx !== 0) {
				this._selectedIdx--;
			}
			this.repositionDisplays();
		}

		/*
			Expects:
				arr[0]: Layer
				arr[0]: number (index)
		*/
		protected moveLayer = (arr: any) => {
			let displays = this._displays;
			for (let i = 0, n = displays.length; i < n; i++) {
				if (displays[i].layerId === arr[0].id) {
					this._displays.move(i, arr[1]);
				}
			}

			this.repositionDisplays();
		}


		protected clickHandler(pointer: Vec3): void {

			// on pointer down
			if (this.isActive === false) {
				super.clickHandler(pointer);
				Events.emit(Events.ID.INPUT_LAYER_MOVE_START);
				return;
			}

			// update local
			let local = this.vecToLocal(pointer);

			// on drag
			let selectedIdx = this._selectedIdx;
			let displays = this._displays;

			// go down
			if (selectedIdx > 0) {
				if (local.y > (displays[selectedIdx - 1].y)) {
					Events.emit(Events.ID.INPUT_LAYER_MOVE_DOWN, this._selectedIdx);
				}
			}

			// go up
			if (selectedIdx < displays.length - 1) {
				if (local.y < (displays[selectedIdx + 1].y + this._ldHeight)) {
					Events.emit(Events.ID.INPUT_LAYER_MOVE_UP, this._selectedIdx);
				}
			}
		}

		protected releaseHandler() {
			super.releaseHandler();
			Events.emit(Events.ID.INPUT_LAYER_MOVE_END);
		}
	}
}