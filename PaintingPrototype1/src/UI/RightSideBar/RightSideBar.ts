﻿module KPaint.UI {
	export class SideBarRight extends ClickableContainer2 {
		private _navigator: CanvasNavigator;
		private _displayOptions: DisplayOptions;
		private _layerButtons: LayerButtonArea;
		private _layerViews: LayerDisplayContainer;
		private _background = new PIXI.Graphics();
		private _bgColor = 0;


		constructor(x: number, y: number, width: number, height: number, bgColor: number) {
			super(x, y, width, height);
			

			x = 0;
			y = 0;

			// navigator
			let elHeight = width;
			this._navigator = new CanvasNavigator(x, y, width, elHeight, 10);


			// display options
			y += elHeight
			elHeight = width * 0.25;
			this._displayOptions = new DisplayOptions(x, y, width, elHeight);


			// layer buttons
			y += elHeight* 1.2;
			elHeight = width * 0.35;
			this._layerButtons = new LayerButtonArea(x, y, width, elHeight);


			// layers
			y += elHeight;
			height -= y;
			this._layerViews = new LayerDisplayContainer(0, y, width, height);

			// generate bg
			this._bgColor = bgColor;
			this.generateBackground();



			this.addChildPIXI(this._background);
			this.addChild(this._displayOptions);
			this.addChild(this._navigator);
			this.addChild(this._layerButtons);
			this.addChild(this._layerViews);


			this.addClickListener(this._navigator);
			this.addClickListener(this._displayOptions);
			this.addClickListener(this._layerButtons);
			this.addClickListener(this._layerViews);
		}

		public resize(width: number, height: number) {
			let bigger = height > this.height;
			this.height = height;
			if (bigger)
				this.generateBackground();
		}


		private generateBackground() {
			this._background
				.clear()
				.beginFill(this._bgColor)
				.drawRect(0, 0, this.width, this.height)
				.endFill();
		}
	}
}