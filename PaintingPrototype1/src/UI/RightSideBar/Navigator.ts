﻿
/*
	Displays a miniature version of the canvas and can be used to move the view frame around by clicking on it

	Generally these only update after the stroke is finished for performance reasons.
*/
module KPaint.UI {
	export class CanvasNavigator extends Clickable2 {
		protected _rt: PIXI.RenderTexture;
		protected _canvasSprite: PIXI.Sprite;
		protected _canvasArea = new Vec4();
		protected _location = new Vec2();

		constructor(x: number, y: number, width: number, height: number, padding: number) {
			super(x, y, width, height);
			let ctxSize = paintingContext.size;

			this._rt = new PIXI.RenderTexture(renderer, ctxSize.x, ctxSize.y);
			let cSpr = new PIXI.Sprite(this._rt);

			// anchor to center
			cSpr.anchor.x = 0.5;
			cSpr.anchor.y = 0.5;
			cSpr.x = x + width / 2;
			cSpr.y = y + height / 2;

			// set area depending on resolution
			let res = ctxSize.x / ctxSize.y;
			if (res < 1) {
				cSpr.width = width * res - padding * 2;
				cSpr.height = height - padding * 2;
			}
			else {
				cSpr.width = width - padding * 2;
				cSpr.height = height * (1 / res) - padding * 2;
			}

			this._canvasArea.xyzw(
				cSpr.x - cSpr.width / 2,
				cSpr.y - cSpr.height / 2,
				cSpr.width,
				cSpr.height
			);

			// add to container
			this._canvasSprite = cSpr;
			this.addChildPIXI(cSpr);

			this.update();
			
			// events
			Events.register(Events.ID.CANVAS_RENDERED, this.update);
		}


		public clickHandler(pointer: Vec3) {
			let ca = this._canvasArea;
			let x = (pointer.x - ca.x) / ca.width;
			let y = (pointer.y - ca.y) / ca.height;
			let clamp = utils.clamp;

			x = clamp(x, 0, 1);
			y = clamp(y, 0, 1);
			this._location.xy(x, y);

			Events.emit(Events.ID.INPUT_MOVE_CANVAS, this._location);
		}

		public releaseHandler() { }


		protected update = () => {
			if (paintingContext.isMidStroke === true) {
				return;
			}

			this._canvasSprite.cacheAsBitmap = false;
			this._rt.clear();
			this._rt.render(paintingContext.container);
			this._canvasSprite.cacheAsBitmap = true;

			renderCoordinator.requestRender();
		}
	}
}