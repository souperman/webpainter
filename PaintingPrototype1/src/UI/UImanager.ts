﻿module KPaint.UI {
	let uiManager: UIManager;


	export function init(renderer: PIXI.WebGLRenderer, ctx: PaintingContext.PaintingContext) {
		let bounds = renderer.view.getBoundingClientRect();
		uiManager = new UIManager(bounds, renderer, ctx);
		uiManager.setParentPIXI(stage);
	}


	export function clickTarget(pointer: Vec3): Input.ClickTarget {
		if (uiManager.canvasDisplay.contains(pointer)) {
			return Input.ClickTarget.Canvas;
		}
		else if (uiManager.contains(pointer)) {
			return Input.ClickTarget.UI;
		}
		return Input.ClickTarget.None;
	}


	export class UIManager extends ClickableContainer2 {
		public sideBarLeft: SideBarLeft;
		public sideBarRight: SideBarRight;
		public canvasDisplay: CanvasDisplay;
		public cursor: CCursor;


		constructor(bounds: ClientRect, renderer: PIXI.WebGLRenderer, ctx: PaintingContext.PaintingContext) {
			super(0, 0, bounds.width, bounds.height)
			let bgColor = 0x888888;

			let x = 0,
				y = 0,
				sideBarWidth = 200;

			// cursor
			this.cursor = new CCursor(0);

			// left side bar
			this.sideBarLeft = new SideBarLeft(x, y, sideBarWidth, bounds.height, bgColor);


			// the canvas that's being drawn on
			x += sideBarWidth;
			this.canvasDisplay = new CanvasDisplay(x, y, bounds.width - sideBarWidth * 2, bounds.height, ctx);

			// right side bar
			x = bounds.width - sideBarWidth;
			this.sideBarRight = new SideBarRight(x, y, sideBarWidth, bounds.height, bgColor);


			this.canvasDisplay.scaleToFit();

			// add to container
			this.addChild(this.canvasDisplay);
			this.addChildPIXI(this.cursor.sprite);
			this.addChild(this.sideBarLeft);
			this.addChild(this.sideBarRight);

			// click listeners
			this.addClickListener(this.sideBarLeft);
			this.addClickListener(this.sideBarRight);

			// events
			let evRegister = Events.register;
			let evId = Events.ID;
			evRegister(evId.PAGE_RESIZED, (a: number[]) => this.resize(a[0], a[1]));
			evRegister(evId.UI_POINTER_DOWN, (a: Vec3[]) => this.click(a[0]));
			evRegister(evId.UI_POINTER_UP, () => this.releaseClick());
			evRegister(evId.UI_POINTER_DRAG, (a: Vec3[]) => {
				if (this.isClicked()) {
					this.click(a[0]);
				}
			});
		}


		protected isClicked(): boolean {
			return this.sideBarLeft.isActive
				|| this.sideBarRight.isActive;
		}


		protected isOnSidebar(pt: Vec3): boolean {
			return this.sideBarLeft.contains(pt)
				|| this.sideBarRight.contains(pt);
		}


		protected moveCursor(pt: Vec3) {
			// Cursor
			if (utils.isOnHtmlCanvas(pt, renderer.view) && !this.isOnSidebar(pt)) {
				// set standard cursor
				document.body.style.cursor = 'none';
			}
			else {
				// set painting cursor
				document.body.style.cursor = '';
			}

			// set new position
			this.cursor.x = pt.x;
			this.cursor.y = pt.y;
		}


		protected resize(width: number, height: number) {
			this.canvasDisplay.resizeArea(width - 400, height);

			this.sideBarLeft.resize(width, height);
			this.sideBarRight.resize(width, height);
			this.sideBarRight.x = width - 200;

			renderer.resize(width, height);
		}
	}
}