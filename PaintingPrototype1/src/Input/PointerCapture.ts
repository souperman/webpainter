﻿module KPaint.Input {

	class InputDataPool extends IPool<InputData> {
		constructor() {
			super('InputData', 500);
			this.allocate();
		}

		protected createOne(): InputData {
			return new InputData(Input.None, false, false, false, new Vec3());
		}
	}


	export class PointerEventCapture {
		protected _captured: InputData[] = [];
		protected _pool = new InputDataPool();


		/*
			Store a pointer event until when it needs to be used
		*/
		public captureEvent(pointer: Vec3, data: InputData) {
			let cap = this._pool.get();
			cap.setInput(data.which, data.shift, data.alt, data.ctrl);
			cap.target = data.target;
			cap.pointer.copyFrom(pointer);

			this._captured.push(cap);
		}


		/*
			Dispatch all the captured events
		*/
		public dispatchEvents(inputMap: InputMapper) {
			let events = this._captured;
			let pool = this._pool;

			// notify for every input
			let ev: InputData;
			for (let i = 0, n = events.length; i < n; i++) {
				ev = events[i];
				inputMap.notify(ev);
				pool.recycle(ev);
			}
			events.length = 0;
			Events.emit(Events.ID.POINTERS_DISPATCHED);
		}

		public isEmpty(): boolean {
			return this._captured.length === 0;
		}
	}
}