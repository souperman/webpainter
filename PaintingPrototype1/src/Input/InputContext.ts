﻿module KPaint.Input {


	/*
		Map keyboard input to a specific event.
	*/
	export class InputContext {
		public disabled = false;

		protected _inputMap: { [s: string]: Events.ID } = {};



		public addInputEvent(event: Events.ID, input: InputData, anyTarget: boolean) {
			if (anyTarget === true) {
				input.target = ClickTarget.None;
				this.add(input.toHash(), event);

				input.target = ClickTarget.Canvas;
				this.add(input.toHash(), event);

				input.target = ClickTarget.UI;
				this.add(input.toHash(), event);
			}
			else {
				this.add(input.toHash(), event);
			}
		}

		protected add(key: number, event: Events.ID) {
			let inputMap = this._inputMap;
			if (inputMap[key] !== undefined) {
				console.warn([
					'Overwriting input event for key: ', key, '.',
					' Old event: ', inputMap[key], '.',
					' New Event: ', event, '.'
				].join(''));
			}
			inputMap[key] = event;
		}


		public onNotify(key: number, input: InputData): void {
			let evId = this._inputMap[key];
			if (evId === undefined) {
				return;
			}
			Events.emit(evId, input.pointer);
		}
	}
}