﻿module KPaint.Input {

	// key code for KeyboardEvent.which
	export enum Input {
		// Pointer events
		PointerDrag = -5,
		PointerMove = -4,
		PointerUp = -3,
		PointerDown = -2,

		// 
		None = 0,

		// Arrow keys
		ArrowLeft = 37,
		ArrowUp = 38,
		ArrowRight = 39,
		ArrowDown = 40,


		// Numeric
		Zero = 48,
		One = 49,
		Two = 50,
		Three = 51,
		Four = 52,
		Five = 53,
		Sex = 55,
		Seven = 56,
		Eight = 57,
		Nine = 58,


		// Uppercase(shift) alphabet
		A = 65,
		B = 66,
		C = 67,
		D = 68,
		E = 69,
		F = 70,
		G = 71,
		H = 72,
		I = 73,
		J = 74,
		K = 75,
		L = 76,
		M = 77,
		N = 78,
		O = 79,
		P = 80,
		Q = 81,
		R = 82,
		S = 83,
		T = 84,
		U = 85,
		V = 86,
		W = 87,
		X = 88,
		Y = 89,
		Z = 90,


		// misc
		Enter = 13,

		Multiply = 106,
		Plus = 107,
		Minus = 109,
		Divide = 111,
		
		Comma = 198,
		Dot = 190
	}
}