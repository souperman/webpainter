﻿module KPaint.Input {


	/*
		General input
	*/
	export function createGeneralInputContext(input: InputData): InputContext {

		let context = new InputContext();
		let evId = Events.ID;
		
		// Undo
		input.setInput(Input.Z, false, false, true);
		context.addInputEvent(evId.INPUT_CANVAS_UNDO, input, true);

		// Redo
		input.setInput(Input.Z, true, false, true);
		context.addInputEvent(evId.INPUT_CANVAS_REDO, input, true);

		// Tool: paint
		input.setInput(Input.Q);
		context.addInputEvent(evId.INPUT_TOOL_BRUSH_PAINT, input, true);

		// Tool: paint
		input.setInput(Input.B);
		context.addInputEvent(evId.INPUT_TOOL_BRUSH_PAINT, input, true);

		// Tool: blur
		input.setInput(Input.W);
		context.addInputEvent(evId.INPUT_TOOL_BRUSH_BLUR, input, true);

		// Tool: erase
		input.setInput(Input.E);
		context.addInputEvent(evId.INPUT_TOOL_BRUSH_ERASE, input, true);

		// Tool: mover
		input.setInput(Input.V);
		context.addInputEvent(evId.INPUT_TOOL_MOVER, input, true);

		// Tool: zoomer
		input.setInput(Input.Z);
		context.addInputEvent(evId.INPUT_TOOL_ZOOMER, input, true);

		// Tool: color picker
		input.setInput(Input.R);
		context.addInputEvent(evId.INPUT_TOOL_COLOR_PICKER, input, true);

		// Color swap
		input.setInput(Input.X);
		context.addInputEvent(evId.INPUT_COLOR_SWAP, input, true);

		// Color swap
		input.setInput(Input.G);
		context.addInputEvent(evId.INPUT_GAMMA_TOGGLE, input, true);


		// Pointer Move/Up for all combinations of modifiers
		for (let i = 0, s = false, a = false, c = false; i < 8; i++) {
			input.setInput(Input.PointerMove, s, a, c);
			context.addInputEvent(evId.POINTER_MOVE, input, true);
			input.which = Input.PointerUp;
			context.addInputEvent(evId.POINTER_UP, input, true);
			s = !s;
			if (i % 2 === 0) a = !a;
			if (i % 4 === 0) c = !c;
		}
		

		// Color selection through alt-clicking

		let prevTar = input.target;
		input.target = ClickTarget.Canvas;
		input.setInput(Input.PointerDown, false, true, false);
		context.addInputEvent(evId.INPUT_PICK_COLOR, input, false);
		input.which = Input.PointerDrag;
		context.addInputEvent(evId.INPUT_PICK_COLOR, input, false);


		// Brush size change through ctrl-alt-clicking

		input.setInput(Input.PointerDown, false, true, true);
		context.addInputEvent(evId.INPUT_DRAG_BRUSH_SIZE, input, false);
		input.which = Input.PointerDrag;
		context.addInputEvent(evId.INPUT_DRAG_BRUSH_SIZE, input, false);
		



		// set target back to previous
		input.target = prevTar;

		return context;
	}




	/*
		Clicks on the UI
	*/
	export function createUIinputContext(input: InputData): InputContext {

		let context = new InputContext();
		let evId = Events.ID;
		
		// store target before altering
		let prevTar = input.target;
		input.target = ClickTarget.UI;

		// pointer down with UI as the target
		input.setInput(Input.PointerDown);
		context.addInputEvent(evId.UI_POINTER_DOWN, input, true);
		
		// pointer up with UI as the target
		input.setInput(Input.PointerUp);
		context.addInputEvent(evId.UI_POINTER_UP, input, true);
		
		// pointer drag with UI as the target
		input.setInput(Input.PointerDrag);
		context.addInputEvent(evId.UI_POINTER_DRAG, input, true);



		// set target back to previous
		input.target = prevTar;
		return context;
	}





	
	/*
		TOOL: PAINT, ERASE, BLUR
	*/
	export function createPaintingInputContext(input: InputData): InputContext {
		// store target before altering
		let prevTar = input.target;
		input.target = ClickTarget.Canvas;

		let context = new InputContext();
		let evId = Events.ID;

		// pointer down
		input.setInput(Input.PointerDown);
		context.addInputEvent(evId.STROKE_START, input, false);
		input.shift = true;
		context.addInputEvent(evId.STROKE_START, input, false);
		
		// pointer drag
		input.setInput(Input.PointerDrag);
		context.addInputEvent(evId.STROKE_ADDPOINTS, input, false);
		input.shift = true;
		context.addInputEvent(evId.STROKE_ADDPOINTS, input, false);
		
		// pointer up
		input.setInput(Input.PointerUp);
		context.addInputEvent(evId.STROKE_END, input, false);
		input.shift = true;
		context.addInputEvent(evId.STROKE_END, input, false);

		// set target back to previous
		input.target = prevTar;
		return context
	}



	/*
		TOOL: MOVE
	*/
	export function createMovingCanvasContext(input: InputData): InputContext {
		// store target before altering
		let prevTar = input.target;
		input.target = ClickTarget.Canvas;

		let context = new InputContext();
		let evId = Events.ID;

		// pointer down
		input.setInput(Input.PointerDown);
		context.addInputEvent(evId.MOVE_CANVAS_START, input, false);
		input.shift = true;
		context.addInputEvent(evId.MOVE_CANVAS_START, input, false);
		
		// pointer drag
		input.setInput(Input.PointerDrag);
		context.addInputEvent(evId.MOVE_CANVAS_CONTINUE, input, false);
		input.shift = true;
		context.addInputEvent(evId.MOVE_CANVAS_CONTINUE, input, false);
		
		// pointer up
		input.setInput(Input.PointerUp);
		context.addInputEvent(evId.MOVE_CANVAS_END, input, false);
		input.shift = true;
		context.addInputEvent(evId.MOVE_CANVAS_END, input, false);

		// set target back to previous
		input.target = prevTar;
		return context
	}



	/*
		TOOL: ZOOM
	*/
	export function createZoomingCanvasContext(input: InputData): InputContext {
		// store target before altering
		let prevTar = input.target;
		input.target = ClickTarget.Canvas;

		let context = new InputContext();
		let evId = Events.ID;

		// pointer down
		input.setInput(Input.PointerDown);
		context.addInputEvent(evId.ZOOM_CANVAS_START, input, false);
		input.shift = true;
		context.addInputEvent(evId.ZOOM_CANVAS_START, input, false);
		
		// pointer drag
		input.setInput(Input.PointerDrag);
		context.addInputEvent(evId.ZOOM_CANVAS_CONTINUE, input, false);
		input.shift = true;
		context.addInputEvent(evId.ZOOM_CANVAS_CONTINUE, input, false);
		
		// pointer up
		input.setInput(Input.PointerUp);
		context.addInputEvent(evId.ZOOM_CANVAS_END, input, false);
		input.shift = true;
		context.addInputEvent(evId.ZOOM_CANVAS_END, input, false);

		// set target back to previous
		input.target = prevTar;
		return context
	}



	/*
		TOOL: COLOR PICKER
	*/
	export function createColorPickerContext(input: InputData): InputContext {
		// store target before altering
		let prevTar = input.target;
		input.target = ClickTarget.Canvas;

		let context = new InputContext();
		let evId = Events.ID;

		// pointer down
		input.setInput(Input.PointerDown);
		context.addInputEvent(evId.INPUT_PICK_COLOR, input, false);
		input.shift = true;
		context.addInputEvent(evId.INPUT_PICK_COLOR, input, false);
		
		// pointer drag
		input.setInput(Input.PointerDrag);
		context.addInputEvent(evId.INPUT_PICK_COLOR, input, false);
		input.shift = true;
		context.addInputEvent(evId.INPUT_PICK_COLOR, input, false);
		
		// pointer up
		input.setInput(Input.PointerUp);
		context.addInputEvent(evId.INPUT_PICK_COLOR, input, false);
		input.shift = true;
		context.addInputEvent(evId.INPUT_PICK_COLOR, input, false);

		// set target back to previous
		input.target = prevTar;
		return context
	}
}