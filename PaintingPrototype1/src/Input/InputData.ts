﻿module KPaint.Input {

	/*
		Where the pointerDown event occured
	*/
	export enum ClickTarget {
		None,
		Canvas,
		UI
	}


	/*
		Object used to pass input data between functions in the input module
	*/
	export class InputData {
		public target = ClickTarget.None;

		constructor(
			public which: Input,
			public shift: boolean,
			public alt: boolean,
			public ctrl: boolean,
			public pointer: Vec3
		) { }

		// quickly set every input variable
		public setInput(
			which: Input,
			shift = false,
			alt = false,
			ctrl = false
		) {
			this.which = which;
			this.shift = shift;
			this.alt = alt;
			this.ctrl = ctrl;
		}

		// uniquely identify any combination of input and modifiers
		public toHash(): number {
			return (this.which << 5)
				+ (this.target << 3)
				+ ((this.shift === true ? 1 : 0) << 2)
				+ ((this.alt === true ? 1 : 0) << 1)
				+ ((this.ctrl === true ? 1 : 0));
		}
	}
}