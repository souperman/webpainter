﻿/// <reference path="../Generic/CircularBuffer.ts"/>

module KPaint.Input {
	
	// stats
	//let inputHandlingMs: number[] = [];
	let inputHandlingMs = new CircularBuffer(300);

	function storeInputHandlingDuration(t: number) {
		//inputHandlingMs.shift();
		inputHandlingMs.push(t);

		let sum = 0,
			min = Number.MAX_VALUE,
			max = Number.MIN_VALUE,
			handle: number;

		let buf = inputHandlingMs.buffer;
		for (let i = 0, n = buf.length; i < n; i++) {
			handle = buf[i];
			sum += handle;
			if (handle < min) {
				min = handle;
			}
			else if (handle > max) {
				max = handle;
			}
		}

		avgInputHandlingMs = sum / buf.length;
		minInputHandlingMs = min;
		maxInputHandlingMs = max;
	}

	export let avgInputHandlingMs = 1;
	export let minInputHandlingMs = 1;
	export let maxInputHandlingMs = 1;


	// functionality
	let mouseDown = false;
	let pointerEventCapture = new PointerEventCapture();
	let currentPointer = new Vec3(0, 0, 0.5);
	let canvasClientPos = new Vec2;
	let inputMap = new InputMapper();
	let inputData = new InputData(0, false, false, false, currentPointer);
	let currentToolContext = InputContextType.Painting;
	let pointers = false;


	export function init(): void {

		// init stats collection
		/*for (let i = 0; i < 300; i++) {
			inputHandlingMs.push(10);
		}*/

		// pointer events
		let v = document.body.addEventListener;
		v('keydown', onKeyDown);

		// if the browser supports has pointer events implemented, and the browser is not firefox (because firefox has only half support)
		if ((<any>window).PointerEvent !== undefined && navigator.userAgent.toLowerCase().indexOf('firefox') < 0) {
			pointers = true;
			v('pointerdown', onPointerDown);
			v('pointerup', onPointerUp);
			v('pointermove', onPointerMove);
		}
		else {
			v('mousedown', onMouseDown);
			v('touchdown', onMouseDown);
			v('mouseup', onMouseUp);
			v('touchup', onMouseUp);
			v('mousemove', onMouseMove);
			v('touchmove', onMouseMove);
		}
		
		// add input contexts
		inputMap.addContext(InputContextType.General, createGeneralInputContext(inputData));
		inputMap.addContext(InputContextType.UI, createUIinputContext(inputData));
		inputMap.addContext(InputContextType.Painting, createPaintingInputContext(inputData));
		inputMap.addContext(InputContextType.MovingCanvas, createMovingCanvasContext(inputData));
		inputMap.addContext(InputContextType.ZoomingCanvas, createZoomingCanvasContext(inputData));
		inputMap.addContext(InputContextType.ColorPicking, createColorPickerContext(inputData));

		
		// disable all but one(painting) of the tool contexts
		inputMap.disableContext(InputContextType.MovingCanvas);
		inputMap.disableContext(InputContextType.ZoomingCanvas);
		inputMap.disableContext(InputContextType.ColorPicking);

		
		// listen to tool events so we can swap contexts
		let evRegister = Events.register;
		let evId = Events.ID;
		evRegister(evId.INPUT_TOOL_BRUSH_PAINT, () => setToolContext(InputContextType.Painting));
		evRegister(evId.INPUT_TOOL_BRUSH_ERASE, () => setToolContext(InputContextType.Painting));
		evRegister(evId.INPUT_TOOL_BRUSH_BLUR, () => setToolContext(InputContextType.Painting));
		evRegister(evId.INPUT_TOOL_MOVER, () => setToolContext(InputContextType.MovingCanvas));
		evRegister(evId.INPUT_TOOL_ZOOMER, () => setToolContext(InputContextType.ZoomingCanvas));
		evRegister(evId.INPUT_TOOL_COLOR_PICKER, () => setToolContext(InputContextType.ColorPicking));
		evRegister(evId.CANVAS_RENDERED, () => {
			dispatchPointerEvents();
			renderCoordinator.requestRender();
		});

		// window events
		window.onresize = onResize;
		window.onscroll = onScroll;
		
		// init canvasClientPos
		onScroll();
	}
	


	function setToolContext(type: InputContextType) {
		inputMap.disableContext(currentToolContext);
		currentToolContext = type;
		inputMap.enableContext(currentToolContext);
	}



	export function onPointerDown(ev: MouseEvent): void {
		let start = performance.now();

		// handling
		mouseDown = true;

		setCanvasPoint_PointerEvent(ev);
		setInputData(Input.PointerDown, ev);
		inputData.target = UI.clickTarget(currentPointer);
		if (ev.button !== 2) {
			capturePointerEvent();
			dispatchPointerEvents();
		}
		renderCoordinator.requestRender();


		storeInputHandlingDuration(performance.now() - start);
	}

	export function onMouseDown(ev: MouseEvent): void {
		let start = performance.now();

		// handling
		mouseDown = true;

		setCanvasPoint_NonPointerEvent(ev);
		setInputData(Input.PointerDown, ev);
		inputData.target = UI.clickTarget(currentPointer);
		if (ev.button !== 2) {
			capturePointerEvent();
			dispatchPointerEvents();
		}
		renderCoordinator.requestRender();


		storeInputHandlingDuration(performance.now() - start);
	}



	export function onPointerUp(ev: MouseEvent): void {
		let start = performance.now();

		// handling
		mouseDown = false;
		setCanvasPoint_PointerEvent(ev);
		setInputData(Input.PointerUp, ev);
		capturePointerEvent();
		dispatchPointerEvents();

		// reset target after notifying since notifications are dependant on target
		inputData.target = ClickTarget.None;
		renderCoordinator.requestRender();


		storeInputHandlingDuration(performance.now() - start);
	}

	export function onMouseUp(ev: MouseEvent): void {
		let start = performance.now();

		// handling
		mouseDown = false;
		setCanvasPoint_NonPointerEvent(ev);
		setInputData(Input.PointerUp, ev);
		capturePointerEvent();
		dispatchPointerEvents();

		// reset target after notifying since notifications are dependant on target
		inputData.target = ClickTarget.None;
		renderCoordinator.requestRender();


		storeInputHandlingDuration(performance.now() - start);
	}

	export function onPointerMove(ev: MouseEvent): void {
		let start = performance.now();

		// handling
		setCanvasPoint_PointerEvent(ev);
		setInputData(Input.PointerMove, ev);

		// send for pointer move
		inputMap.notify(inputData);

		if (mouseDown && ev.button !== 2) {
			// send for pointer drag
			inputData.which = Input.PointerDrag;
			capturePointerEvent();
			dispatchPointerEvents();
		}
		renderCoordinator.requestRender();
		
		storeInputHandlingDuration(performance.now() - start);
	}

	export function onMouseMove(ev: MouseEvent): void {
		let start = performance.now();

		// handling
		setCanvasPoint_NonPointerEvent(ev);
		setInputData(Input.PointerMove, ev);

		// send for pointer move
		inputMap.notify(inputData);

		if (mouseDown && ev.button !== 2) {
			// send for pointer drag
			inputData.which = Input.PointerDrag;
			capturePointerEvent();
			dispatchPointerEvents();
		}
		renderCoordinator.requestRender();
		
		storeInputHandlingDuration(performance.now() - start);
	}



	function onKeyDown(ev: KeyboardEvent) {
		let start = performance.now();

		setInputData(ev.which, ev);
		inputMap.notify(inputData);

		// render
		renderCoordinator.requestRender();


		storeInputHandlingDuration(performance.now() - start);
	}


	function onResize() {
		let width = utils.clamp(window.innerWidth - 20, 800, 1400),
			height = utils.clamp(window.innerHeight - 20, 400, 1000);
		
		Events.emit(Events.ID.PAGE_RESIZED, width, height);
		
		renderCoordinator.requestRender();
	}
	

	function onScroll() {
		let bounds = renderer.view.getBoundingClientRect();
		canvasClientPos.x = bounds.left;
		canvasClientPos.y = bounds.top;
		Events.emit(Events.ID.PAGE_SCROLL);
	}


	// the supplied MouseEvent will be modified by PEP to contain pressure information, so it needs to be 'any'
	// type in order to extract this data
	function setCanvasPoint_PointerEvent(ev: any): void {
		// translate to canvas coordinate
		currentPointer.x = ev.clientX - canvasClientPos.x;
		currentPointer.y = ev.clientY - canvasClientPos.y;
		currentPointer.pressure = (ev.pointerType === 'pen' ? ev.pressure : 1.0);
	}

	function setCanvasPoint_NonPointerEvent(ev: any): void {
		// translate to canvas coordinate
		currentPointer.x = ev.clientX - canvasClientPos.x;
		currentPointer.y = ev.clientY - canvasClientPos.y;
		currentPointer.pressure = 1.0;
	}


	function setInputData(which: Input, ev: any): void {
		inputData.setInput(which, ev.shiftKey, ev.altKey, ev.ctrlKey);
	}


	function capturePointerEvent(): void {
		pointerEventCapture.captureEvent(currentPointer, inputData);
	}
	
	
	function dispatchPointerEvents(): void {
		if (renderCoordinator.waitingToRender === false) {
			pointerEventCapture.dispatchEvents(inputMap);
			renderCoordinator.requestRender();
		}
		else if (pointerEventCapture.isEmpty() === false) {
			setTimeout(dispatchPointerEvents, 2);
		}
	}
}