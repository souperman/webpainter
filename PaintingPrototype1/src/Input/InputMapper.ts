﻿/// <reference path="../Generic/Ipool.ts"/>


module KPaint.Input {
	
	/*
		Input context type to sort the InputContexts so that they can be added on-demand, since some things require the same
		type of input. For example painting and moving canvas both use the PointerDrag event, but I only ever want to use one
		of those functions at a time, so I can remove and add InputContexts depending on what tool is selected.
	*/
	export enum InputContextType {
		// things that should always receive input are put under general. ie tool selection, color swapping
		General,

		// just for sending pointer input I think
		UI,

		// painting, erasing, or blurring all fit under painting.
		Painting,

		// move the view of the canvas
		MovingCanvas,

		// zoom in or out from the canvas
		ZoomingCanvas,

		// resize the brush (like in SAI)
		ResizingBrush,

		// picking color off of the canvas
		ColorPicking
	}


	/*
		Object for managing active InputContexts
	*/
	export class InputMapper {
		protected _contexts: { [n: number]: InputContext } = {};
		protected _data = new InputData(0, false, false, false, null);


		public addContext(type: InputContextType, context: InputContext) {
			let contexts = this._contexts;
			if (contexts[type] !== undefined) {
				console.warn(['Overwriting input context for type: ', type, '.'].join(''));
			}
			contexts[type] = context;
		}


		public enableContext(type: InputContextType) {
			this.setContextDisabled(type, false);
		}


		public disableContext(type: InputContextType) {
			this.setContextDisabled(type, true);
		}


		public notify(inputData: InputData) {
			let inputKey = inputData.toHash();

			let contexts = this._contexts;
			for (let ctxKey in contexts) {
				if (contexts[ctxKey].disabled === false) {
					contexts[ctxKey].onNotify(inputKey, inputData);
				}
			}
		}


		protected setContextDisabled(type: InputContextType, value: boolean) {
			let contexts = this._contexts;
			if (contexts[type] === undefined) {
				console.warn(['Context not found for type ', type, '.'].join(''));
				return;
			}
			contexts[type].disabled = value;
		}
	}
}