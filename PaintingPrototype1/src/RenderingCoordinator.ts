﻿/// <reference path="Generic/CircularBuffer.ts"/>

module KPaint {
	/*
		Class that decides when to render based on a max FPS.

		You can request a render through the requestRender function, and the coordinator will
		decide how long to wait before rendering.
	*/
	export class RenderingCoordinator {
		protected _context: PaintingContext.PaintingContext;

		// current requestAnimationFrame() id
		protected _requestId: number = null;
		
		// contains the last 100 rendering durations and rendering times
		protected _renderDurations = new CircularBuffer(100);
		protected _renderTimes = new CircularBuffer(100);


		// stats
		protected _currentFPS = 0;
		protected _avgFrameTime = 0;
		protected _minFrameTime = 0;
		protected _maxFrameTime = 0;
		protected _statsUpdated = false;


		
		constructor(context: PaintingContext.PaintingContext) {
			this._context = context;
		}


		get currentFPS() {
			if (this._statsUpdated === false) {
				this.updateStats();
			}
			return this._currentFPS;
		}


		// Ms per frame for the last 100 frames
		get avgFrameTime() {
			if (this._statsUpdated === false) {
				this.updateStats();
			}
			return this._avgFrameTime;
		}

		get minFrameTime() {
			if (this._statsUpdated === false) {
				this.updateStats();
			}
			return this._minFrameTime;
		}

		get maxFrameTime() {
			if (this._statsUpdated === false) {
				this.updateStats();
			}
			return this._maxFrameTime;
		}


		// whether a render request is currently being processed
		get waitingToRender(): boolean {
			return this._requestId !== null;
		}


		// request that a render happen as soon as possible
		public requestRender(): void {
			if (this._requestId === null) {
				this._requestId = requestAnimationFrame(this.render);
			}
		}


		// force the coordinator to render immediately
		public forceRender(): void {
			// clear timeout if one exists
			let requestId = this._requestId;
			if (requestId !== null) {
				cancelAnimationFrame(requestId);
			}

			this.render();
		}


		// the rendering function
		protected render = (): void => {
			// render
			let start = performance.now();
			this._context.render();
			renderer.render(stage);
			let duration = performance.now() - start;
			
			this._renderDurations.push(duration);
			this._renderTimes.push(start);

			// clear the request id
			this._requestId = null;

			this._statsUpdated = false;
		}


		// update stats for rendering
		protected updateStats(): void {
			let durations = this._renderDurations.buffer,
				times = this._renderTimes.buffer,
				arrLen = durations.length,
				arrPos = this._renderTimes.back;

			let duraSum = 0,
				duraMin = Number.MAX_VALUE,
				duraMax = Number.MIN_VALUE,
				dura: number;

			// assumes duration.length === times.length
			for (let i = 0, n = arrLen; i < n; i++) {
				dura = durations[i];
				duraSum += dura;
				if (dura < duraMin) {
					duraMin = dura;
				}
				else if (dura > duraMax) {
					duraMax = dura;
				}
			}

			let cur: number,
				prev = times[arrPos],
				deltaSum = 0;
			
			for (let i = 0, n = arrLen; i < n; i++) {
				cur = times[(i + arrPos) % arrLen];
				deltaSum += cur - prev;
				prev = cur;
			}

			this._currentFPS = 1000 / (deltaSum / (arrLen - 1));
			this._avgFrameTime = duraSum / arrLen;
			this._minFrameTime = duraMin;
			this._maxFrameTime = duraMax;
			this._statsUpdated = true;
		}
	}
}