﻿// reference all the standalone components first

/*
	Generics
*/
/// <reference path="Generic/JavaScriptExtensions.ts"/>
/// <reference path="Generic/IPool.ts"/>
/// <reference path="Generic/Vec.ts"/>
/// <reference path="Generic/Vec3Pool.ts"/>
/// <reference path="Generic/Color.ts"/>
/// <reference path="Generic/Interpolation.ts"/>
/// <reference path="Generic/Utils.ts"/>
/// <reference path="Generic/CircularBuffer.ts"/>


/*
	Events
*/
/// <reference path="Events/EventIDs.ts"/>
/// <reference path="Events/Events.ts"/>


/*
	Shaders
*/
/// <reference path="Shaders/Common.ts"/>
/// <reference path="Shaders/AlphaFilter.ts"/>
/// <reference path="Shaders/BrushFilter.ts"/>
/// <reference path="Shaders/CheckerPatternFilter.ts"/>
/// <reference path="Shaders/ColorFilter.ts"/>
/// <reference path="Shaders/HueCircleFilter.ts"/>
/// <reference path="Shaders/LinearToGammaFilter.ts"/>
/// <reference path="Shaders/ReverseEraseFilter.ts"/>
/// <reference path="Shaders/SaturationValueFilter.ts"/>
/// <reference path="Shaders/SelectorCircleFilter.ts"/>
/// <reference path="Shaders/SliderFilters/HueFilter.ts"/>
/// <reference path="Shaders/SliderFilters/SaturationFilter.ts"/>
/// <reference path="Shaders/SliderFilters/ValueFilter.ts"/>


/*
	UI
*/
/// <reference path="UI/Generic/DisplayObject.ts"/>
/// <reference path="UI/Generic/Clickable.ts"/>
/// <reference path="UI/Generic/Button.ts"/>
/// <reference path="UI/Generic/Selector.ts"/>
/// <reference path="UI/Generic/Slider.ts"/>
/// <reference path="UI/Generic/AreaSlider.ts"/>

/// <reference path="UI/LeftSideBar/ColorWheel/ColorWheel.ts"/>
/// <reference path="UI/LeftSideBar/ColorWheel/ColorWheelSvArea.ts"/>
/// <reference path="UI/LeftSideBar/BrushButtonArea.ts"/>
/// <reference path="UI/LeftSideBar/BrushSettings.ts"/>
/// <reference path="UI/LeftSideBar/ColorDisplay.ts"/>
/// <reference path="UI/LeftSideBar/HsvSliderArea.ts"/>
/// <reference path="UI/LeftSideBar/LeftSideBar.ts"/>

/// <reference path="UI/RightSideBar/Navigator.ts"/>
/// <reference path="UI/RightSideBar/DisplayOptions.ts"/>
/// <reference path="UI/RightSideBar/LayerButtonArea.ts"/>
/// <reference path="UI/RightSideBar/LayerDisplay.ts"/>
/// <reference path="UI/RightSideBar/LayerDisplayContainer.ts"/>
/// <reference path="UI/RightSideBar/RightSideBar.ts"/>

/// <reference path="UI/Cursor.ts"/>
/// <reference path="UI/CanvasDisplay.ts"/>
/// <reference path="UI/UImanager.ts"/>


/*
	Input
*/
/// <reference path="Input/InputCodes.ts"/>
/// <reference path="Input/InputData.ts"/>
/// <reference path="Input/InputContext.ts"/>
/// <reference path="Input/InputContextGenerators.ts"/>
/// <reference path="Input/InputMapper.ts"/>
/// <reference path="Input/PointerCapture.ts"/>
/// <reference path="Input/InputHandler.ts"/>


/*
	Painting
*/
/// <reference path="Painting/ToolTypes.ts"/>
/// <reference path="Painting/Stroke.ts"/>
/// <reference path="Painting/Layer.ts"/>
/// <reference path="Painting/StrokeManager.ts"/>
/// <reference path="Painting/LayerManager.ts"/>
/// <reference path="Painting/BrushSprite.ts"/>
/// <reference path="Painting/Brush.ts"/>
/// <reference path="Painting/PaintingContext.ts"/>
/// <reference path="Painting/Invoker/Command.ts"/>
/// <reference path="Painting/Invoker/Invoker.ts"/>


/*
	Top level
*/
/// <reference path="RenderingCoordinator.ts"/>
/// <reference path="StatsDisplay.ts"/>



module KPaint {
	/*
		Global Variables
	*/
	export var renderer: PIXI.WebGLRenderer;
	export var stage = new PIXI.Container();

	export var paintingContext: PaintingContext.PaintingContext;
	export var renderCoordinator: RenderingCoordinator;

	var statsD: StatsDisplay;
	


	export function main(): void {
		PIXI.SCALE_MODES.DEFAULT = PIXI.SCALE_MODES.NEAREST;

		var tmpRenderer = PIXI.autoDetectRenderer(
			utils.clamp(window.innerWidth - 20, 800, 1400),
			utils.clamp(window.innerHeight - 20, 400, 1000),
			{
				transparent: true,
				antialias: false,
				forceFXAA: false,
				roundPixels: false
			}
		);
		if (tmpRenderer.type !== PIXI.RENDERER_TYPE.WEBGL) {
			console.error('Non-WebGL renderer initialized. Aborting.');
			return;
		}

		renderer = <PIXI.WebGLRenderer>tmpRenderer;
		document.body.appendChild(renderer.view);

		stage.interactive = false;
		stage.interactiveChildren = false;


		// Context for painting
		paintingContext = new PaintingContext.PaintingContext(1000, 1000, 5);
		renderCoordinator = new RenderingCoordinator(paintingContext);
		PaintingContext.BrushSprite.allocatePool();

		// Input
		Input.init();


		// UI
		UI.init(renderer, paintingContext);
		

		// initial settings
		paintingContext.init();
		paintingContext.gamma = 2.2;
		paintingContext.brushSize = 150;
		paintingContext.brushSizeMinPct = 0.01;
		paintingContext.brushSoftness = 0.1;
		paintingContext.brushFlow = 0.6;
		paintingContext.brushSpacing = 0.03;

		// unload message
		utils.setOnBeforeUnload();


		// stats
		statsD = new StatsDisplay(10, renderer.view.clientHeight - 160, 200, 200);
		stage.addChild(statsD.container);
		statsD.setUpdateInterval();


		// initial render
		renderCoordinator.requestRender();
	}
}
window.onload = () => KPaint.main();