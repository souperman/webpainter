// http://stackoverflow.com/questions/5306680/move-an-array-element-from-one-array-position-to-another
Array.prototype.move = function (idx, newIdx) {
    if (newIdx >= this.length) {
        var k = newIdx - this.length;
        while ((k--) + 1) {
            this.push(undefined);
        }
    }
    this.splice(newIdx, 0, this.splice(idx, 1)[0]);
    return this;
};
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/find
if (!Array.prototype.find) {
    Array.prototype.find = function (predicate) {
        if (this === null) {
            throw new TypeError('Array.prototype.find called on null or undefined');
        }
        if (typeof predicate !== 'function') {
            throw new TypeError('predicate must be a function');
        }
        var list = Object(this);
        var length = list.length >>> 0;
        var thisArg = arguments[1];
        var value;
        for (var i = 0; i < length; i++) {
            value = list[i];
            if (predicate.call(thisArg, value, i, list)) {
                return value;
            }
        }
        return undefined;
    };
}
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/of
if (!Array.prototype.of) {
    Array.prototype.of = function () {
        var elements = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            elements[_i - 0] = arguments[_i];
        }
        return Array.prototype.slice.call(elements);
    };
}
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/includes
if (!Array.prototype.includes) {
    Array.prototype.includes = function (searchElement) {
        var O = Object(this);
        var len = parseInt(O.length) || 0;
        if (len === 0) {
            return false;
        }
        var n = parseInt(arguments[1]) || 0;
        var k;
        if (n >= 0) {
            k = n;
        }
        else {
            k = len + n;
            if (k < 0) {
                k = 0;
            }
        }
        var currentElement;
        while (k < len) {
            currentElement = O[k];
            if (searchElement === currentElement ||
                (searchElement !== searchElement && currentElement !== currentElement)) {
                return true;
            }
            k++;
        }
        return false;
    };
}
// bind F12 to stop code execution in chrome
window.addEventListener('keydown', function (e) {
    if (e.keyCode === 123)
        debugger;
});
var KPaint;
(function (KPaint) {
    /*
        
    */
    var IPool = (function () {
        function IPool(_name, _chunkSize) {
            this._name = _name;
            this._chunkSize = _chunkSize;
            this._pool = [];
            this._uniqueId = 0;
        }
        IPool.prototype.get = function () {
            var pool = this._pool;
            // create new if pool is empty
            if (0 == pool.length) {
                console.warn([this._name, ' object pool empty. Allocating more.'].join(''));
                this.allocate();
            }
            return pool.pop();
        };
        IPool.prototype.recycle = function (item) {
            this._pool.push(item);
        };
        IPool.prototype.allocate = function () {
            var pool = this._pool;
            for (var i = 0, n = this._chunkSize; i < n; i++) {
                pool.push(this.createOne());
            }
        };
        // each pool has it's own set of unique IDs, so make sure to not have more than 1 pool of each object
        IPool.prototype.getUniqueId = function () {
            return this._uniqueId++;
        };
        return IPool;
    })();
    KPaint.IPool = IPool;
})(KPaint || (KPaint = {}));
var KPaint;
(function (KPaint) {
    /*
        vec objects have several aliased accessors to make them easier to use for different purposes
    
        vec2: xy
        vec3: xyz, rgb, hsv, pointer
        vec4: xyzw, rgba, hsva
    
    */
    var Vec2 = (function () {
        function Vec2(x, y) {
            if (x === void 0) { x = 0; }
            if (y === void 0) { y = 0; }
            this.x = x;
            this.y = y;
        }
        Vec2.prototype.xy = function (x, y) {
            this.x = x;
            this.y = y;
            return this;
        };
        Vec2.prototype.isZero = function () {
            return this.x === 0 && this.y === 0;
        };
        Vec2.prototype.dot = function (rhs) {
            return this.x * rhs.x + this.y * rhs.y;
        };
        Vec2.prototype.length = function () {
            return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2));
        };
        Vec2.prototype.invert = function () {
            console.assert(!this.isZero(), 'Cannot invert zero length vector');
            this.x = 1 / this.x;
            this.y = 1 / this.y;
            return this;
        };
        Vec2.prototype.normalize = function () {
            var len = this.length();
            this.x /= len;
            this.y /= len;
            return this;
        };
        Vec2.prototype.clamp = function (max) {
            this.normalize()
                .multiply(max);
            return this;
        };
        Vec2.prototype.plus = function (n) {
            this.x += n;
            this.y += n;
            return this;
        };
        Vec2.prototype.plusVec = function (rhs) {
            this.x += rhs.x;
            this.y += rhs.y;
            return this;
        };
        Vec2.prototype.minus = function (n) {
            this.x -= n;
            this.y -= n;
            return this;
        };
        Vec2.prototype.minusVec = function (rhs) {
            this.x -= rhs.x;
            this.y -= rhs.y;
            return this;
        };
        Vec2.prototype.multiply = function (n) {
            this.x *= n;
            this.y *= n;
            return this;
        };
        Vec2.prototype.multiplyVec = function (rhs) {
            this.x *= rhs.x;
            this.y *= rhs.y;
            return this;
        };
        Vec2.prototype.divide = function (n) {
            this.x /= n;
            this.y /= n;
            return this;
        };
        Vec2.prototype.divideVec = function (rhs) {
            this.x /= rhs.x;
            this.y /= rhs.y;
            return this;
        };
        Vec2.prototype.modulo = function (n) {
            this.x %= n;
            this.y %= n;
            return this;
        };
        Vec2.prototype.moduloVec = function (rhs) {
            this.x %= rhs.x;
            this.y %= rhs.y;
            return this;
        };
        Vec2.prototype.distance = function (other) {
            return Math.sqrt(this.dot(other));
        };
        Vec2.prototype.equals = function (rhs) {
            return this.x === rhs.x && this.y === rhs.y;
        };
        Vec2.prototype.copyFrom = function (source) {
            this.x = source.x;
            this.y = source.y;
            return this;
        };
        Vec2.lerp = function (out, start, end, percent) {
            return out
                .copyFrom(end)
                .minusVec(start)
                .multiply(percent)
                .plusVec(start);
        };
        Vec2.nlerp = function (out, start, end, percent) {
            return out
                .copyFrom(end)
                .minusVec(start)
                .multiply(percent)
                .plusVec(start)
                .normalize();
        };
        return Vec2;
    })();
    KPaint.Vec2 = Vec2;
    // r, g, b
    // h, s, v
    // x, y, pressure
    var Vec3 = (function () {
        function Vec3(x, y, z) {
            if (x === void 0) { x = 0; }
            if (y === void 0) { y = 0; }
            if (z === void 0) { z = 0; }
            this.x = x;
            this.y = y;
            this.z = z;
        }
        Object.defineProperty(Vec3.prototype, "r", {
            // color
            get: function () { return this.x; },
            set: function (value) { this.x = value; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Vec3.prototype, "g", {
            get: function () { return this.y; },
            set: function (value) { this.y = value; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Vec3.prototype, "b", {
            get: function () { return this.z; },
            set: function (value) { this.z = value; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Vec3.prototype, "h", {
            get: function () { return this.x; },
            set: function (value) { this.x = value; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Vec3.prototype, "s", {
            get: function () { return this.y; },
            set: function (value) { this.y = value; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Vec3.prototype, "v", {
            get: function () { return this.z; },
            set: function (value) { this.z = value; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Vec3.prototype, "pressure", {
            // pointer
            get: function () { return this.z; },
            set: function (value) { this.z = value; },
            enumerable: true,
            configurable: true
        });
        Vec3.prototype.xyz = function (x, y, z) {
            this.x = x;
            this.y = y;
            this.z = z;
            return this;
        };
        Vec3.prototype.rgb = function (r, g, b) {
            this.x = r;
            this.y = g;
            this.z = b;
            return this;
        };
        Vec3.prototype.hsv = function (r, s, v) {
            this.x = r;
            this.y = s;
            this.z = v;
            return this;
        };
        Vec3.prototype.isZero = function () {
            return this.x === 0 && this.y === 0 && this.z === 0;
        };
        Vec3.prototype.dot = function (rhs) {
            return this.x * rhs.x + this.y * rhs.y + this.z * rhs.z;
        };
        Vec3.prototype.dot2D = function (rhs) {
            return this.x * rhs.x + this.y * rhs.y;
        };
        Vec3.prototype.length = function () {
            return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2) + Math.pow(this.z, 2));
        };
        Vec3.prototype.length2D = function () {
            return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2));
        };
        Vec3.prototype.distance2D = function (dst) {
            return Math.sqrt(Math.pow((dst.x - this.x), 2) + Math.pow((dst.y - this.y), 2));
        };
        Vec3.prototype.plus = function (rhs) {
            this.x += rhs;
            this.y += rhs;
            this.z += rhs;
            return this;
        };
        Vec3.prototype.plusVec = function (rhs) {
            this.x += rhs.x;
            this.y += rhs.y;
            this.z += rhs.z;
            return this;
        };
        Vec3.prototype.minus = function (rhs) {
            this.x -= rhs;
            this.y -= rhs;
            this.z -= rhs;
            return this;
        };
        Vec3.prototype.minusVec = function (rhs) {
            this.x -= rhs.x;
            this.y -= rhs.y;
            this.z -= rhs.z;
            return this;
        };
        Vec3.prototype.multiply = function (percent) {
            this.x *= percent;
            this.y *= percent;
            this.z *= percent;
            return this;
        };
        Vec3.prototype.multiplyVec = function (rhs) {
            this.x *= rhs.x;
            this.y *= rhs.y;
            this.z *= rhs.z;
            return this;
        };
        Vec3.prototype.divide = function (rhs) {
            console.assert(rhs !== 0);
            this.x /= rhs;
            this.y /= rhs;
            this.z /= rhs;
            return this;
        };
        Vec3.prototype.divideVec = function (rhs) {
            this.x /= rhs.x;
            this.y /= rhs.y;
            this.z /= rhs.z;
            return this;
        };
        Vec3.prototype.modulo = function (n) {
            this.x %= n;
            this.y %= n;
            this.z %= n;
            return this;
        };
        Vec3.prototype.moduloVec = function (rhs) {
            this.x %= rhs.x;
            this.y %= rhs.y;
            this.z %= rhs.z;
            return this;
        };
        Vec3.prototype.pow = function (n) {
            this.x = Math.pow(this.x, n);
            this.y = Math.pow(this.y, n);
            this.z = Math.pow(this.z, n);
            return this;
        };
        Vec3.prototype.equals = function (rhs) {
            return this.x === rhs.x
                && this.y === rhs.y
                && this.z === rhs.z;
        };
        Vec3.prototype.copyFrom = function (rhs) {
            this.x = rhs.x;
            this.y = rhs.y;
            this.z = rhs.z;
            return this;
        };
        Vec3.lerp = function (out, start, end, percent) {
            return out
                .copyFrom(end)
                .minusVec(start)
                .multiply(percent)
                .plusVec(start);
        };
        return Vec3;
    })();
    KPaint.Vec3 = Vec3;
    var Vec4 = (function () {
        function Vec4(x, y, z, w) {
            if (x === void 0) { x = 0; }
            if (y === void 0) { y = 0; }
            if (z === void 0) { z = 0; }
            if (w === void 0) { w = 0; }
            this.x = x;
            this.y = y;
            this.z = z;
            this.w = w;
        }
        Object.defineProperty(Vec4.prototype, "h", {
            // color
            get: function () { return this.x; },
            set: function (value) { this.x = value; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Vec4.prototype, "s", {
            get: function () { return this.y; },
            set: function (value) { this.y = value; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Vec4.prototype, "v", {
            get: function () { return this.z; },
            set: function (value) { this.z = value; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Vec4.prototype, "r", {
            get: function () { return this.x; },
            set: function (value) { this.x = value; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Vec4.prototype, "g", {
            get: function () { return this.y; },
            set: function (value) { this.y = value; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Vec4.prototype, "b", {
            get: function () { return this.z; },
            set: function (value) { this.z = value; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Vec4.prototype, "a", {
            get: function () { return this.w; },
            set: function (value) { this.w = value; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Vec4.prototype, "width", {
            // area
            get: function () { return this.z; },
            set: function (value) { this.z = value; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Vec4.prototype, "height", {
            get: function () { return this.w; },
            set: function (value) { this.w = value; },
            enumerable: true,
            configurable: true
        });
        Vec4.prototype.xyzw = function (x, y, z, w) {
            this.x = x;
            this.y = y;
            this.z = z;
            this.w = w;
            return this;
        };
        Vec4.prototype.rgba = function (r, g, b, a) {
            this.x = r;
            this.y = g;
            this.z = b;
            this.w = a;
            return this;
        };
        Vec4.prototype.hsva = function (h, s, v, a) {
            this.x = h;
            this.y = s;
            this.z = v;
            this.w = a;
            return this;
        };
        Vec4.prototype.length = function () {
            return Math.sqrt(Math.pow((this.width - this.x), 2) + Math.pow((this.height - this.y), 2));
        };
        Vec4.prototype.equals = function (rhs) {
            return this.x === rhs.x
                && this.y === rhs.y
                && this.z === rhs.z
                && this.w === rhs.w;
        };
        Vec4.prototype.copyFrom = function (rhs) {
            this.x = rhs.x;
            this.y = rhs.y;
            this.z = rhs.z;
            this.w = rhs.w;
            return this;
        };
        Vec4.prototype.contains = function (x, y) {
            if (this.x > x)
                return false;
            if (this.x + this.z < x)
                return false;
            if (this.y > y)
                return false;
            if (this.y + this.w < y)
                return false;
            return true;
        };
        Vec4.prototype.contains2D = function (vec) {
            if (this.x > vec.x)
                return false;
            if (this.x + this.z < vec.x)
                return false;
            if (this.y > vec.y)
                return false;
            if (this.y + this.w < vec.y)
                return false;
            return true;
        };
        return Vec4;
    })();
    KPaint.Vec4 = Vec4;
})(KPaint || (KPaint = {}));
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var KPaint;
(function (KPaint) {
    var op;
    (function (op) {
        var vec3;
        (function (vec3) {
            var Vec3Pool = (function (_super) {
                __extends(Vec3Pool, _super);
                function Vec3Pool() {
                    _super.apply(this, arguments);
                }
                Vec3Pool.prototype.createOne = function () {
                    return new KPaint.Vec3();
                };
                return Vec3Pool;
            })(KPaint.IPool);
            var _pool = new Vec3Pool('Vec3', 10000);
            _pool.allocate();
            function get(x, y, z) {
                if (x === void 0) { x = 0; }
                if (y === void 0) { y = 0; }
                if (z === void 0) { z = 0; }
                return _pool.get().xyz(x, y, z);
            }
            vec3.get = get;
            function clone(vec) {
                return get(vec.x, vec.y, vec.z);
            }
            vec3.clone = clone;
            function recycle(vec) {
                _pool.recycle(vec);
            }
            vec3.recycle = recycle;
        })(vec3 = op.vec3 || (op.vec3 = {}));
    })(op = KPaint.op || (KPaint.op = {}));
})(KPaint || (KPaint = {}));
var KPaint;
(function (KPaint) {
    /*
        Takes RGB values from the first input and puts the HSV equivalent into the second input

        Expected ranges
        rgb: [0 : 1] for all values
        hsv: [0 : 1] for all values
    */
    function rgbToHsv(rgb, hsv) {
        var r = rgb.r, g = rgb.g, b = rgb.b, h, max = Math.max(r, g, b), min = Math.min(r, g, b), d = max - min;
        hsv.y = (max == 0 ? 0 : d / max);
        hsv.z = max;
        switch (max) {
            case min:
                h = 0;
                break;
            case r:
                h = (g - b) / d + (g < b ? 6 : 0);
                break;
            case g:
                h = (b - r) / d + 2;
                break;
            case b:
                h = (r - g) / d + 4;
                break;
        }
        h /= 6;
        hsv.x = h;
    }
    KPaint.rgbToHsv = rgbToHsv;
    /*
        Takes HSV values from the first input and puts the RGB equivalent into the second input

        Expected ranges
        rgb: [0 : 1] for all values
        hsv: [0 : 1] for all values
    */
    function hsvToRgb(hsv, rgb) {
        var h = hsv.x, s = hsv.y, v = hsv.z, r, g, b, i, f, p, q, t;
        i = Math.floor(h * 6);
        f = h * 6 - i;
        p = v * (1 - s);
        q = v * (1 - f * s);
        t = v * (1 - (1 - f) * s);
        switch (i % 6) {
            case 0:
                r = v, g = t, b = p;
                break;
            case 1:
                r = q, g = v, b = p;
                break;
            case 2:
                r = p, g = v, b = t;
                break;
            case 3:
                r = p, g = q, b = v;
                break;
            case 4:
                r = t, g = p, b = v;
                break;
            case 5:
                r = v, g = p, b = q;
                break;
        }
        rgb.rgb(r, g, b);
    }
    KPaint.hsvToRgb = hsvToRgb;
    /*
        Transforms the base-10 representation of rgb hex color
    */
    function rgbToHex(rgb) {
        return (Math.floor(rgb.r * 255) << 16)
            + (Math.floor(rgb.g * 255) << 8)
            + Math.floor(rgb.b * 255);
    }
    KPaint.rgbToHex = rgbToHex;
})(KPaint || (KPaint = {}));
var KPaint;
(function (KPaint) {
    /*
        Used for interpolating lines. Can use different interpolation algorithms by setting
        the interpolator varible to a function with the correct parameters and output.
        
        Grab the interpolated point from Interpolator.newPoints, which removes and returns an
        array of points. These objects need to be recycled into their Object Pool after usage
        as they are unique references.

    */
    var Interpolator = (function () {
        function Interpolator() {
            this._previousPoint = new KPaint.Vec3();
            this.newPts = []; // all interpolated points go into this array
            this.spacingPx = 1; // number of pixels between each draw point
        }
        /*
            End the line by calling the clear function (after extracting the points from newPoints).
            Clearing will recycle all stored points, and reinitialize the internal arrays
        */
        Interpolator.prototype.clear = function () {
            var newPts = this.newPts;
            for (var i = 0, n = newPts.length; i < n; i++) {
                KPaint.op.vec3.recycle(newPts.pop());
            }
            newPts.length = 0;
        };
        /*
            Set the starting point for a line
        */
        Interpolator.prototype.setInitialDrawPoint = function (pointer) {
            this._previousPoint.copyFrom(pointer);
        };
        /*
            Interpolate between previous point and the supplied one.
        */
        Interpolator.prototype.interpolate = function (end) {
            var start = this._previousPoint;
            var dist = KPaint.utils.distance(end.x, end.y, start.x, start.y);
            var spacingPx = this.spacingPx;
            var vec3CloneFunc = KPaint.op.vec3.clone;
            // only interpolate if distance is far enough
            if (dist > spacingPx) {
                var newPts = this.newPts;
                var steps = Math.floor(dist / spacingPx);
                // percent to increment per step
                var p = spacingPx / dist;
                // how much to increment each step (p * delta)
                var xStep = p * (end.x - start.x);
                var yStep = p * (end.y - start.y);
                var zStep = p * (end.z - start.z);
                // interpolate
                for (var i = 0; i < steps; i++) {
                    start.x += xStep;
                    start.y += yStep;
                    start.z += zStep;
                    newPts.push(vec3CloneFunc(start));
                }
            }
        };
        /*
            pressure should decrease spacing

            spacingPx is the maximum spacing in pixels, the actual spacing should be adjusted between each point
            on the line. It space between points a and b should be equal to (spacingPx * b.pressure).

            For this reason I can't use my previous method, since I need to know exactly how many steps there should
            be between points a and b.

            This new method is definitely more computationally expensive, but I think time spend on interpolation
            was rather minimal to begin with.
        */
        Interpolator.prototype.interpolate2 = function (end) {
            var start = this._previousPoint;
            var dist = start.distance2D(end);
            var spacingPx = this.spacingPx;
            var endSpacing = Math.max(spacingPx * end.pressure, 1);
            var newPts = this.newPts;
            var p = 1;
            var vec3obj = KPaint.op.vec3;
            var mostRecent;
            if (newPts.length > 0) {
                mostRecent = newPts[newPts.length - 1];
            }
            while (dist > endSpacing && p > 0) {
                // There must be a way to do this without recalculating p and deltas for every point.
                p = (spacingPx * start.z) / dist;
                start.x += p * (end.x - start.x);
                start.y += p * (end.y - start.y);
                start.z += p * (end.z - start.z);
                dist = start.distance2D(end);
                // don't add perfect duplicates
                if (mostRecent === undefined || mostRecent.equals(start) === false) {
                    mostRecent = vec3obj.clone(start);
                    newPts.push(mostRecent);
                }
            }
        };
        return Interpolator;
    })();
    KPaint.Interpolator = Interpolator;
})(KPaint || (KPaint = {}));
var KPaint;
(function (KPaint) {
    var utils;
    (function (utils) {
        // measure the distance between two coordinates
        function distance(x0, y0, x1, y1) {
            x0 = x0 > x1 ? x0 - x1 : x1 - x0;
            y0 = y0 > y1 ? y0 - y1 : y1 - y0;
            return Math.sqrt(x0 * x0 + y0 * y0);
        }
        utils.distance = distance;
        // clamp a number to a specified range
        function clamp(n, min, max) {
            if (n <= min)
                return min;
            else if (n >= max)
                return max;
            return n;
        }
        utils.clamp = clamp;
        // modulus with expected behaviour
        utils.mod = function (n, m) { return ((n % m) + m) % m; };
        // expects x to be in range [0, 1]
        utils.smoothstep = function (x) { return x * x * (3 - 2 * x); };
        // expects x to be in range [0, 1]
        utils.smootherstep = function (x) { return x * x * x * (x * (x * 6 - 15) + 10); };
        // exponentially scale from 0 to 1
        utils.expostep = function (x) {
            if (x === 0.0)
                return 0.0;
            return Math.pow(2.718281828459, (1 - (1 / (x * x))));
        };
        function getMatrixGlobal(sprite) {
            var mat = new PIXI.Matrix();
            var sr = Math.sin(sprite.rotation);
            var cr = Math.cos(sprite.rotation);
            mat.a = sprite.scale.x; //width / sprite.width; // sprite.scale.x ?
            mat.b = 0;
            mat.c = 0;
            mat.d = sprite.scale.y; //height / sprite.height; // sprite.scale.y ?
            mat.tx = sprite.x;
            mat.ty = sprite.y;
            return mat;
        }
        function isOnHtmlCanvas(pt, canvas) {
            if (pt.x < 0 || pt.x > canvas.clientWidth)
                return false;
            if (pt.y < 0 || pt.y > canvas.clientHeight)
                return false;
            return true;
        }
        utils.isOnHtmlCanvas = isOnHtmlCanvas;
        function setOnBeforeUnload() {
            var msg = function () {
                return 'All progress will be lost.';
            };
            window.onbeforeunload = msg;
        }
        utils.setOnBeforeUnload = setOnBeforeUnload;
    })(utils = KPaint.utils || (KPaint.utils = {}));
})(KPaint || (KPaint = {}));
/// <reference path="Utils.ts"/>
var KPaint;
(function (KPaint) {
    var CircularBuffer = (function () {
        function CircularBuffer(length) {
            this._next = 0;
            this._front = -1;
            this.buffer = new Float32Array(length);
        }
        Object.defineProperty(CircularBuffer.prototype, "front", {
            get: function () { return this._front; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(CircularBuffer.prototype, "back", {
            get: function () { return this._next; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(CircularBuffer.prototype, "length", {
            get: function () { return this.buffer.length; },
            enumerable: true,
            configurable: true
        });
        CircularBuffer.prototype.push = function (n) {
            var buffer = this.buffer;
            var next = this._next;
            buffer[next] = n;
            this._front = next;
            this._next = ((1 + next) % buffer.length);
        };
        CircularBuffer.prototype.pop = function () {
            var buffer = this.buffer;
            var length = buffer.length;
            var next = this._next;
            next = ((1 - next % length) + length) % length;
            this._next = next;
            return buffer[next];
        };
        CircularBuffer.prototype.reset = function () {
            var buf = this.buffer;
            for (var i = 0, n = buf.length; i < n; i++) {
                buf[i] = 0;
            }
        };
        return CircularBuffer;
    })();
    KPaint.CircularBuffer = CircularBuffer;
})(KPaint || (KPaint = {}));
// IDs for callbacks 
var KPaint;
(function (KPaint) {
    var Events;
    (function (Events) {
        (function (ID) {
            // Used as a placeholder. Shouldn't exist in finalized code
            ID[ID["NONE"] = 0] = "NONE";
            //  Log in console
            ID[ID["LOG_RENDERING_STATS"] = 1] = "LOG_RENDERING_STATS";
            // Browser events
            ID[ID["POINTER_MOVE"] = 2] = "POINTER_MOVE";
            ID[ID["POINTER_UP"] = 3] = "POINTER_UP";
            ID[ID["UI_POINTER_DOWN"] = 4] = "UI_POINTER_DOWN";
            ID[ID["UI_POINTER_DRAG"] = 5] = "UI_POINTER_DRAG";
            ID[ID["UI_POINTER_UP"] = 6] = "UI_POINTER_UP";
            ID[ID["PAGE_RESIZED"] = 7] = "PAGE_RESIZED";
            ID[ID["PAGE_SCROLL"] = 8] = "PAGE_SCROLL";
            // Rendering
            ID[ID["CANVAS_RENDERED"] = 9] = "CANVAS_RENDERED";
            ID[ID["POINTERS_DISPATCHED"] = 10] = "POINTERS_DISPATCHED";
            // Brush
            ID[ID["BRUSH_BASE_TEXTURE_CHANGED"] = 11] = "BRUSH_BASE_TEXTURE_CHANGED";
            ID[ID["BRUSH_SIZE"] = 12] = "BRUSH_SIZE";
            ID[ID["BRUSH_SIZE_MIN"] = 13] = "BRUSH_SIZE_MIN";
            ID[ID["BRUSH_FLOW"] = 14] = "BRUSH_FLOW";
            ID[ID["BRUSH_SOFTNESS"] = 15] = "BRUSH_SOFTNESS";
            ID[ID["BRUSH_SPACING"] = 16] = "BRUSH_SPACING";
            ID[ID["BRUSH_COLOR_H"] = 17] = "BRUSH_COLOR_H";
            ID[ID["BRUSH_COLOR_S"] = 18] = "BRUSH_COLOR_S";
            ID[ID["BRUSH_COLOR_V"] = 19] = "BRUSH_COLOR_V";
            // Stroke
            ID[ID["STROKE_START"] = 20] = "STROKE_START";
            ID[ID["STROKE_ADDPOINTS"] = 21] = "STROKE_ADDPOINTS";
            ID[ID["STROKE_END"] = 22] = "STROKE_END";
            ID[ID["STROKE_RASTERIZED"] = 23] = "STROKE_RASTERIZED";
            // Move
            ID[ID["MOVE_CANVAS_START"] = 24] = "MOVE_CANVAS_START";
            ID[ID["MOVE_CANVAS_CONTINUE"] = 25] = "MOVE_CANVAS_CONTINUE";
            ID[ID["MOVE_CANVAS_END"] = 26] = "MOVE_CANVAS_END";
            ID[ID["MOVE_CANVAS_MOVED"] = 27] = "MOVE_CANVAS_MOVED";
            // Zoom
            ID[ID["ZOOM_CANVAS_START"] = 28] = "ZOOM_CANVAS_START";
            ID[ID["ZOOM_CANVAS_CONTINUE"] = 29] = "ZOOM_CANVAS_CONTINUE";
            ID[ID["ZOOM_CANVAS_END"] = 30] = "ZOOM_CANVAS_END";
            ID[ID["ZOOM_SET_ZOOM"] = 31] = "ZOOM_SET_ZOOM";
            // Layer
            ID[ID["LAYER_SELECTED"] = 32] = "LAYER_SELECTED";
            ID[ID["LAYER_CREATED"] = 33] = "LAYER_CREATED";
            ID[ID["LAYER_DELETED"] = 34] = "LAYER_DELETED";
            ID[ID["LAYER_ALPHA"] = 35] = "LAYER_ALPHA";
            ID[ID["LAYER_PIXEL_LOCKED"] = 36] = "LAYER_PIXEL_LOCKED";
            ID[ID["LAYER_MOVE"] = 37] = "LAYER_MOVE";
            ID[ID["LAYER_MOVE_END"] = 38] = "LAYER_MOVE_END";
            // Invoker
            ID[ID["INVOKER_UNDONE"] = 39] = "INVOKER_UNDONE";
            ID[ID["INVOKER_REDONE"] = 40] = "INVOKER_REDONE";
            // DisplayArea
            ID[ID["DISPLAY_AREA_RESIZED"] = 41] = "DISPLAY_AREA_RESIZED";
            /*
                All the events above here are only to communicate between the painting context and the UI.
    
                Below are all the input events that are handed from the UI to the painting context.
            */
            // Color
            ID[ID["INPUT_COLOR_CHANGE_H"] = 42] = "INPUT_COLOR_CHANGE_H";
            ID[ID["INPUT_COLOR_CHANGE_S"] = 43] = "INPUT_COLOR_CHANGE_S";
            ID[ID["INPUT_COLOR_CHANGE_V"] = 44] = "INPUT_COLOR_CHANGE_V";
            // Undo & Redo
            ID[ID["INPUT_CANVAS_UNDO"] = 45] = "INPUT_CANVAS_UNDO";
            ID[ID["INPUT_CANVAS_REDO"] = 46] = "INPUT_CANVAS_REDO";
            // Brush
            ID[ID["INPUT_TOOL_BRUSH_PAINT"] = 47] = "INPUT_TOOL_BRUSH_PAINT";
            ID[ID["INPUT_TOOL_BRUSH_BLUR"] = 48] = "INPUT_TOOL_BRUSH_BLUR";
            ID[ID["INPUT_TOOL_BRUSH_ERASE"] = 49] = "INPUT_TOOL_BRUSH_ERASE";
            ID[ID["INPUT_TOOL_MOVER"] = 50] = "INPUT_TOOL_MOVER";
            ID[ID["INPUT_TOOL_ZOOMER"] = 51] = "INPUT_TOOL_ZOOMER";
            ID[ID["INPUT_TOOL_COLOR_PICKER"] = 52] = "INPUT_TOOL_COLOR_PICKER";
            ID[ID["INPUT_COLOR_SWAP"] = 53] = "INPUT_COLOR_SWAP";
            ID[ID["INPUT_BRUSH_SIZE"] = 54] = "INPUT_BRUSH_SIZE";
            ID[ID["INPUT_BRUSH_SIZE_MIN"] = 55] = "INPUT_BRUSH_SIZE_MIN";
            ID[ID["INPUT_BRUSH_FLOW"] = 56] = "INPUT_BRUSH_FLOW";
            ID[ID["INPUT_BRUSH_SPACING"] = 57] = "INPUT_BRUSH_SPACING";
            ID[ID["INPUT_BRUSH_SOFTNESS"] = 58] = "INPUT_BRUSH_SOFTNESS";
            // Layer
            ID[ID["INPUT_LAYER_ALPHA_CHANGED"] = 59] = "INPUT_LAYER_ALPHA_CHANGED";
            ID[ID["INPUT_LAYER_SELECTED"] = 60] = "INPUT_LAYER_SELECTED";
            ID[ID["INPUT_LAYER_NEW"] = 61] = "INPUT_LAYER_NEW";
            ID[ID["INPUT_LAYER_MERGE"] = 62] = "INPUT_LAYER_MERGE";
            ID[ID["INPUT_LAYER_CLEAR"] = 63] = "INPUT_LAYER_CLEAR";
            ID[ID["INPUT_LAYER_DELETE"] = 64] = "INPUT_LAYER_DELETE";
            ID[ID["INPUT_LAYER_LOCK_PIXELS"] = 65] = "INPUT_LAYER_LOCK_PIXELS";
            ID[ID["INPUT_LAYER_MOVE_START"] = 66] = "INPUT_LAYER_MOVE_START";
            ID[ID["INPUT_LAYER_MOVE_UP"] = 67] = "INPUT_LAYER_MOVE_UP";
            ID[ID["INPUT_LAYER_MOVE_DOWN"] = 68] = "INPUT_LAYER_MOVE_DOWN";
            ID[ID["INPUT_LAYER_MOVE_END"] = 69] = "INPUT_LAYER_MOVE_END";
            // Canvas
            ID[ID["INPUT_MOVE_CANVAS"] = 70] = "INPUT_MOVE_CANVAS";
            ID[ID["INPUT_ZOOM_CANVAS"] = 71] = "INPUT_ZOOM_CANVAS";
            ID[ID["INPUT_ZOOM_CANVAS_RESET"] = 72] = "INPUT_ZOOM_CANVAS_RESET";
            ID[ID["INPUT_ZOOM_CANVAS_ADD"] = 73] = "INPUT_ZOOM_CANVAS_ADD";
            ID[ID["INPUT_ZOOM_CANVAS_SUBTRACT"] = 74] = "INPUT_ZOOM_CANVAS_SUBTRACT";
            ID[ID["INPUT_PICK_COLOR"] = 75] = "INPUT_PICK_COLOR";
            ID[ID["INPUT_DRAG_BRUSH_SIZE"] = 76] = "INPUT_DRAG_BRUSH_SIZE";
            ID[ID["INPUT_DRAG_BRUSH_SIZE_PREVIEW"] = 77] = "INPUT_DRAG_BRUSH_SIZE_PREVIEW";
            // MISC
            ID[ID["INPUT_DOWNLOAD_IMAGE"] = 78] = "INPUT_DOWNLOAD_IMAGE";
            ID[ID["INPUT_GAMMA_TOGGLE"] = 79] = "INPUT_GAMMA_TOGGLE";
        })(Events.ID || (Events.ID = {}));
        var ID = Events.ID;
        ;
    })(Events = KPaint.Events || (KPaint.Events = {}));
})(KPaint || (KPaint = {}));
/// <reference path="EventIDs.ts"/>
/*
    Events is used for passing messages between unrelated modules through callback functions.
    Multiple callbacks can be registered for each ID

    Separating the modules makes it easier to track down bugs as any communication between modules
    has to go through a single channel.
*/
var KPaint;
(function (KPaint) {
    var Events;
    (function (Events) {
        var callbackMap = {};
        // register a callback
        Events.register = function (id, callback) {
            var cb = callbackMap[id];
            if (cb === undefined || cb === null) {
                cb = [];
                callbackMap[id] = cb;
            }
            // don't allow infinite callbacks (for example from a circular event chain)
            if (cb.length > 1000) {
                console.log(['Events.register: 1000 callbacks have been set for ID: ', id, '. Fix your code.'].join(''));
                return false;
            }
            cb.push(callback);
            return true;
        };
        Events.unregister = function (id, callback) {
            var cb = callbackMap[id];
            if (cb === undefined || cb === null)
                return false;
            // get index of callback
            var idx = cb.indexOf(callback);
            if (idx === -1)
                return false;
            cb.splice(idx, 1);
            return true;
        };
        Events.emit = function (id) {
            var data = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                data[_i - 1] = arguments[_i];
            }
            var callbackList = callbackMap[id];
            if (callbackList === undefined || callbackList === null) {
                //console.warn(['Event ', id, ' does not have any callback associated with it.'].join(''));
                //console.trace();
                return;
            }
            for (var i = 0, n = callbackList.length; i < n; i++) {
                callbackList[i](data);
            }
        };
    })(Events = KPaint.Events || (KPaint.Events = {}));
})(KPaint || (KPaint = {}));
var KPaint;
(function (KPaint) {
    var Sha;
    (function (Sha) {
        Sha.shaderDefaultTextureUniformsGenerator = function () {
            var extra = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                extra[_i - 0] = arguments[_i];
            }
            var o = {};
            // default
            o['uSampler'] = { type: 'sampler2D', value: 0 };
            o['projectionMatrix'] = {
                type: 'mat3', value: new Float32Array([
                    1, 0, 0,
                    0, 1, 0,
                    0, 0, 1
                ])
            };
            // extra
            if (extra !== null && extra !== undefined) {
                for (var i = 0, n = extra.length; i < n; i += 2) {
                    o[extra[i]] = extra[i + 1];
                }
            }
            return o;
        };
        Sha.shaderDefaultTextureAttributesGenerator = function () {
            var extra = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                extra[_i - 0] = arguments[_i];
            }
            var o = {};
            // default
            o['aVertexPosition'] = 0;
            o['aTextureCoord'] = 0;
            o['aColor'] = 0;
            // extra
            if (extra !== null && extra !== undefined) {
                for (var i = 0, n = extra.length; i < n; i += 2) {
                    o.extra[i] = extra[i + 1];
                }
            }
            return o;
        };
        // copy of the default texture shader that PIXI uses
        Sha.SHADER_DEFAULT_TEXTURE_VERT = [
            'precision lowp float;',
            'attribute vec2 aVertexPosition;',
            'attribute vec2 aTextureCoord;',
            'attribute vec4 aColor;',
            'uniform mat3 projectionMatrix;',
            'varying vec2 vTextureCoord;',
            'varying vec4 vColor;',
            'void main(void){',
            '	gl_Position = vec4((projectionMatrix * vec3(aVertexPosition, 1.0)).xy, 0.0, 1.0);',
            '   vTextureCoord = aTextureCoord;',
            '   vColor = vec4(aColor.rgb * aColor.a, aColor.a);',
            '}'
        ].join('\n');
        /*
            GLSL function for converting hsv to rgb.
    
            vec3 hsv2rgb(vec3);
        */
        Sha.SHADER_FUNC_hsv2rgb = [
            // TODO: figure out which of the two functions is more correct. They each have slightly different 
            // gradients between primary hues. Sam's looks smoother, so I'm sticking to that for now.
            // https://www.shadertoy.com/view/MsS3Wc - Iñigo Quiles 
            /*'vec3 hsv2rgb(in vec3 c) {',
            '	vec3 rgb = clamp(abs(mod(c.x * 6.0 + vec3(0.0, 4.0, 2.0), 6.0) - 3.0) - 1.0, 0.0, 1.0);',
            '	rgb = rgb * rgb * (3.0 - 2.0 * rgb);',
            '	return c.z * mix(vec3(1.0), rgb, c.y);',
            '}',*/
            // http://lolengine.net/blog/2013/07/27/rgb-to-hsv-in-glsl - Sam Hocevar
            'vec3 hsv2rgb(vec3 c) {',
            '	vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);',
            '	vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);',
            '	return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y); ',
            '}',
        ].join('\n');
        /*
            GLSL function for converting rgb to hsv.
    
            vec3 rgb2hsv(vec3);
        */
        Sha.SHADER_FUNC_rgb2hsv = [
            // http://lolengine.net/blog/2013/07/27/rgb-to-hsv-in-glsl - Sam Hocevar
            'vec3 rgb2hsv(vec3 c) {',
            '	vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);',
            '	vec4 p = c.g < c.b ? vec4(c.bg, K.wz) : vec4(c.gb, K.xy);',
            '	vec4 q = c.r < p.x ? vec4(p.xyw, c.r) : vec4(c.r, p.yzx);',
            '	float d = q.x - min(q.w, q.y);',
            '	float e = 1.0e-10;',
            '	return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);',
            '}'
        ].join('\n');
        /*
            Variation of smoothstep
    
            https://en.wikipedia.org/wiki/Smoothstep#Variations
        */
        Sha.SHADER_FUNC_smootherstep = [
            'float smootherstep(float edge0, float edge1, float x) {',
            '	x = clamp((x - edge0)/(edge1 - edge0), 0.0, 1.0);',
            '	return x * x * x * (x * (x * 6.0 - 15.0) + 10.0);',
            '}'
        ].join('\n');
        /*
            Defines constants for PI and TAU (2PI)
        */
        Sha.SHADER_DEFINE_PI = [
            '\n#ifndef PI',
            '	#define PI 3.14159265359',
            '#endif',
            '#ifndef TAU',
            '	#define TAU 6.283185307179586',
            '#endif\n'
        ].join('\n');
        /*
            RNG
        */
        Sha.SHADER_FUNC_rand = [
            'float rand(float n) {',
            '	return fract(sin(n) * 43758.5453123);',
            '} ',
            'float rand(vec2 n) { ',
            '	return fract(sin(dot(n, vec2(12.9898, 4.1414))) * 43758.5453);',
            '}',
        ].join('\n');
        /*
            pow with vectors
        */
        Sha.SHADER_FUNC_pow = [
            'vec2 pow(vec2 vec, float power) {',
            '	return vec2(pow(vec.x, power), pow(vec.y, power));',
            '}',
            'vec3 pow(vec3 vec, float power) {',
            '	return vec3(pow(vec.x, power), pow(vec.y, power), pow(vec.z, power));',
            '}',
            'vec4 pow(vec4 vec, float power) {',
            '	return vec4(pow(vec.x, power), pow(vec.y, power), pow(vec.z, power), pow(vec.w, power));',
            '}',
        ].join('');
        /*
            Defines the gamma factor
        */
        /*const SHADER_DEFINE_GAMMA_FACTOR = [
            '\n#ifndef GAMMA_FACTOR',
            '	#define GAMMA_FACTOR 2.2',
            '#endif\n',
        ].join('\n');
    
    
        // Gamma correction functions. Gamma is defined in the above string
        export const SHADER_FUNC_linearToOutput = [
            SHADER_DEFINE_GAMMA_FACTOR,
    
            'vec4 linearToOutput( in vec4 a ) {',
            '	#ifdef GAMMA_FACTOR',
            '		return pow(a, vec4(1.0 / float(GAMMA_FACTOR)));',
            '	#else',
            '		return a;',
            '	#endif',
            '}',
            'vec3 linearToOutput( in vec3 a ) {',
            '	#ifdef GAMMA_FACTOR',
            '		return pow(a, vec3(1.0 / float(GAMMA_FACTOR)));',
            '	#else',
            '		return a;',
            '	#endif',
            '}',
            'float linearToOutput( in float a ) {',
            '	#ifdef GAMMA_FACTOR',
            '		return pow(a, (1.0 / float(GAMMA_FACTOR)));',
            '	#else',
            '		return a;',
            '	#endif',
            '}',
        ].join('\n');*/
        /*
            Converts from rgb to greyscale using the values found here
            http://alienryderflex.com/hsp.html
        
            Basic: humans see certain hues more clearly, so simply averaging the rgb values doesn't
            generate a good greyscale. For example in HSV, a fully saturated blue is significantly
            darker than a fully saturated red, and yet if you convert them to greyscale by averaging,
            they will both generate the same tone.
        
            We need to take our human brightness perception into account. To do this we assign r, g,
            and b, separate multipliers that correspond with how brightly we perceive them. These
            multipliers add up to a total of 1.0.
        
            r * 0.299
            g * 0.587
            b * 0.114
    
        */
        Sha.SHADER_FUNC_toGreyscale = [
            'float toGreyscale( in vec3 color ) {',
            '	return 0.299 * color.r + 0.587 * color.g + 0.114 * color.b;',
            '}',
            'vec3 toGreyscale( in vec3 color ) {',
            '	return vec3(toGreyscale(color));',
            '}',
            'vec4 toGreyscale( in vec4 color ) {',
            '	return vec4(toGreyscale(color.rgb), color.a);',
            '}',
        ].join('');
        /*
            Previously used this to generator my cursor outline.
    
            https://en.wikipedia.org/wiki/Sobel_operator#Alternative_operators
        */
        Sha.SHADER_FUNC_sobelOperator = [
            'float sobelOperator() {',
            '	float alpha = 0.0;',
            // top
            '	alpha -= texture2D(uSampler, vTextureCoord + vec2(-stepSize.x,	-stepSize.y)).a * 3.0;',
            '	alpha -= texture2D(uSampler, vTextureCoord + vec2(0.0,			-stepSize.y)).a * 10.0;',
            '	alpha -= texture2D(uSampler, vTextureCoord + vec2(stepSize.x,	-stepSize.y)).a * 3.0;',
            // middle
            '	alpha -= texture2D(uSampler, vTextureCoord + vec2(-stepSize.x,	0.0)).a * 10.0;',
            '	alpha += texture2D(uSampler, vTextureCoord + vec2(0.0,			0.0)).a * 52.0;',
            '	alpha -= texture2D(uSampler, vTextureCoord + vec2(stepSize.x,	0.0)).a * 10.0;',
            // bottom
            '	alpha -= texture2D(uSampler, vTextureCoord + vec2(-stepSize.x,	stepSize.y)).a * 3.0;',
            '	alpha -= texture2D(uSampler, vTextureCoord + vec2(0.0,			stepSize.y)).a * 10.0;',
            '	alpha -= texture2D(uSampler, vTextureCoord + vec2(stepSize.x,	stepSize.y)).a * 3.0;',
            '	return alpha;',
            '}',
        ].join('');
    })(Sha = KPaint.Sha || (KPaint.Sha = {}));
})(KPaint || (KPaint = {}));
/// <reference path="Common.ts"/>
var KPaint;
(function (KPaint) {
    var Sha;
    (function (Sha) {
        /*
            Custom shader for adjusting alpha values on a texture
        */
        var AlphaFilter = (function (_super) {
            __extends(AlphaFilter, _super);
            function AlphaFilter(a) {
                if (a === void 0) { a = 1; }
                _super.call(this, 
                // vertex shader
                null, 
                // fragment shader
                [
                    'precision mediump float;',
                    'varying vec2 vTextureCoord;',
                    'varying vec4 vColor;',
                    'uniform sampler2D uSampler;',
                    'uniform float alpha;',
                    'void main(void) {',
                    '	vec4 tex = texture2D(uSampler, vTextureCoord);',
                    '	tex *= alpha;',
                    '	gl_FragColor = tex;',
                    '}'
                ].join('\n'), 
                // uniforms
                {
                    alpha: { type: '1f', value: a }
                });
            }
            Object.defineProperty(AlphaFilter.prototype, "a", {
                get: function () {
                    return this.uniforms.alpha.value;
                },
                set: function (value) {
                    this.uniforms.alpha.value = value;
                },
                enumerable: true,
                configurable: true
            });
            return AlphaFilter;
        })(PIXI.AbstractFilter);
        Sha.AlphaFilter = AlphaFilter;
    })(Sha = KPaint.Sha || (KPaint.Sha = {}));
})(KPaint || (KPaint = {}));
/// <reference path="Common.ts"/>
var KPaint;
(function (KPaint) {
    var Sha;
    (function (Sha) {
        var BrushShader = (function (_super) {
            __extends(BrushShader, _super);
            function BrushShader(softness) {
                if (softness === void 0) { softness = 0.01; }
                _super.call(this, null, [
                    'precision mediump float;',
                    'varying vec2 vTextureCoord;',
                    'uniform vec4 color;',
                    'uniform float radius;',
                    'uniform float border;',
                    Sha.SHADER_FUNC_smootherstep,
                    'void main(void) {',
                    '	vec2 uv =  vTextureCoord - 0.5;',
                    '	float dist = sqrt(dot(uv, uv));',
                    '	float t = 1.0 - smootherstep(radius, radius + border, dist);',
                    '	gl_FragColor = vec4(color.rgb * color.a * t, color.a * t);',
                    '}'
                ].join(''), {
                    color: { type: '4f', value: [1.0, 1.0, 1.0, 1.0] },
                    radius: { type: '1f' },
                    border: { type: '1f' }
                });
                this._rgba = new KPaint.Vec4();
                this.softness = softness;
            }
            Object.defineProperty(BrushShader.prototype, "red", {
                get: function () { return this._rgba.r; },
                set: function (value) {
                    this._rgba.r = value;
                    this.uniforms.color.value[0] = Math.pow(value, this._gamma);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BrushShader.prototype, "green", {
                get: function () { return this._rgba.g; },
                set: function (value) {
                    this._rgba.g = value;
                    this.uniforms.color.value[1] = Math.pow(value, this._gamma);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BrushShader.prototype, "blue", {
                get: function () { return this._rgba.b; },
                set: function (value) {
                    this._rgba.b = value;
                    this.uniforms.color.value[2] = Math.pow(value, this._gamma);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BrushShader.prototype, "alpha", {
                get: function () { return this._rgba.a; },
                set: function (value) {
                    this._rgba.a = value;
                    this.uniforms.color.value[3] = Math.pow(value, this._gamma);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BrushShader.prototype, "softness", {
                get: function () {
                    return this._softness;
                },
                set: function (value) {
                    this._softness = value;
                    var border = 0.5 * value;
                    this.uniforms.border.value = border;
                    this.uniforms.radius.value = 0.5 - border;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BrushShader.prototype, "gamma", {
                get: function () {
                    return this._gamma;
                },
                set: function (value) {
                    this._gamma = value;
                },
                enumerable: true,
                configurable: true
            });
            return BrushShader;
        })(PIXI.AbstractFilter);
        Sha.BrushShader = BrushShader;
        /*
         *
         *			O U T L I N E   F O R   T H E   B R U S H
         *
         */
        var BrushOutlineShader = (function (_super) {
            __extends(BrushOutlineShader, _super);
            function BrushOutlineShader(diameter) {
                _super.call(this, null, [
                    'precision mediump float;',
                    'varying vec2 vTextureCoord;',
                    'uniform float radius;',
                    'uniform float border1;',
                    'uniform float border2;',
                    'void main(void) {',
                    '	vec2 uv = vTextureCoord - 0.5;',
                    '	float dist = sqrt(dot(uv, uv));',
                    // outer outline
                    '	float t = 1.0;',
                    '	t += smoothstep(radius, radius + border1, dist);',
                    '	t -= smoothstep(radius - border1, radius, dist);',
                    // inner outline
                    '	float radius2 = radius - border1;',
                    '	float t2 = 1.0;',
                    '	t2 += smoothstep(radius2, radius2 + border2, dist);',
                    '	t2 -= smoothstep(radius2 - border2, radius2, dist);',
                    // colors for each outline
                    '	vec4 color1 = vec4(vec3(0.0), 1.0) * (1.0 - t);',
                    '	vec4 color2 = vec4(1.0) * (1.0 - t2);',
                    // blend normally
                    '	gl_FragColor = color2 + color1 * (1.0 - color2.a);',
                    '}'
                ].join(''), Sha.shaderDefaultTextureUniformsGenerator('border1', { type: '1f' }, 'border2', { type: '1f' }, 'radius', { type: '1f' }));
                this._diameter = 0;
                this.diameter = diameter;
            }
            Object.defineProperty(BrushOutlineShader.prototype, "diameter", {
                get: function () {
                    return this._diameter;
                },
                set: function (diameter) {
                    this._diameter = diameter;
                    var borders = 1 / diameter;
                    this.uniforms.border1.value = borders;
                    this.uniforms.border2.value = borders;
                    this.uniforms.radius.value = 0.5 - borders - borders;
                },
                enumerable: true,
                configurable: true
            });
            return BrushOutlineShader;
        })(PIXI.AbstractFilter);
        Sha.BrushOutlineShader = BrushOutlineShader;
    })(Sha = KPaint.Sha || (KPaint.Sha = {}));
})(KPaint || (KPaint = {}));
/// <reference path="Common.ts"/>
var KPaint;
(function (KPaint) {
    var Sha;
    (function (Sha) {
        /*
            Generate the check pattern seen in the background in photoshop when layers are clear.
        */
        var CheckerPatternFilter = (function (_super) {
            __extends(CheckerPatternFilter, _super);
            function CheckerPatternFilter() {
                _super.call(this, null, [
                    'precision mediump float;',
                    'varying vec2 vTextureCoord;',
                    'varying vec4 vColor;',
                    'uniform sampler2D uSampler;',
                    'uniform vec2 scale;',
                    'uniform vec2 checkers;',
                    'void main(void) {',
                    '	vec2 uv =  scale * vTextureCoord;',
                    '	float x = mod(floor(uv.x * checkers.x), 2.0);',
                    '	float y = mod(floor(uv.y * checkers.y), 2.0);',
                    '	bool isDark = bool(mod(x + y, 2.0));',
                    '	gl_FragColor = vec4(vec3(isDark?0.8:1.0), 1.0);',
                    '}'
                ].join(''), {
                    scale: { type: '2f', value: [1, 1] },
                    checkers: { type: '2f', value: [64.5, 64.5] }
                });
            }
            return CheckerPatternFilter;
        })(PIXI.AbstractFilter);
        Sha.CheckerPatternFilter = CheckerPatternFilter;
    })(Sha = KPaint.Sha || (KPaint.Sha = {}));
})(KPaint || (KPaint = {}));
/// <reference path="Common.ts"/>
var KPaint;
(function (KPaint) {
    var Sha;
    (function (Sha) {
        var ColorFilter = (function (_super) {
            __extends(ColorFilter, _super);
            /*
                The given color is converted from gamma to linear under the assumption that gamma is 2.2
            */
            function ColorFilter(r, g, b, a) {
                if (a === void 0) { a = 1; }
                _super.call(this, 
                // vertex shader
                null, 
                // fragment shader
                [
                    'precision mediump float;',
                    'varying vec2 vTextureCoord;',
                    'varying vec4 vColor;',
                    'uniform sampler2D uSampler;',
                    'uniform vec3 color;',
                    'void main(void) {',
                    '	vec4 tex = texture2D(uSampler, vTextureCoord);',
                    '	tex.rgb = color * tex.a;',
                    '	gl_FragColor = tex;',
                    '}'
                ].join(''), 
                // uniforms
                {
                    color: { type: '3f', value: [] }
                });
                this._gamma = 1;
                this._rgb = new KPaint.Vec3();
                this.r = r;
                this.g = g;
                this.b = b;
            }
            Object.defineProperty(ColorFilter.prototype, "r", {
                get: function () {
                    return this._rgb.r;
                },
                set: function (value) {
                    this._rgb.r = value;
                    this.uniforms.color.value[0] = Math.pow(value, this._gamma);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ColorFilter.prototype, "g", {
                get: function () {
                    return this._rgb.g;
                },
                set: function (value) {
                    this._rgb.g = value;
                    this.uniforms.color.value[1] = Math.pow(value, this._gamma);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ColorFilter.prototype, "b", {
                get: function () {
                    return this._rgb.b;
                },
                set: function (value) {
                    this._rgb.b = value;
                    this.uniforms.color.value[2] = Math.pow(value, this._gamma);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ColorFilter.prototype, "a", {
                get: function () {
                    return this.uniforms.alpha.value;
                },
                set: function (value) {
                    this.uniforms.alpha.value = Math.pow(value, this._gamma);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ColorFilter.prototype, "gamma", {
                get: function () {
                    return this._gamma;
                },
                set: function (value) {
                    this._gamma = value;
                    var rgb = this._rgb;
                    this.r = rgb.r;
                    this.g = rgb.g;
                    this.b = rgb.b;
                },
                enumerable: true,
                configurable: true
            });
            return ColorFilter;
        })(PIXI.AbstractFilter);
        Sha.ColorFilter = ColorFilter;
    })(Sha = KPaint.Sha || (KPaint.Sha = {}));
})(KPaint || (KPaint = {}));
var KPaint;
(function (KPaint) {
    var Sha;
    (function (Sha) {
        /*
            Expects that the texture it is drawn on has a 1:1 resolution
    
            Inputs should be normalized [0 : 1]
        */
        var HueCircleFilter = (function (_super) {
            __extends(HueCircleFilter, _super);
            function HueCircleFilter(radius, width) {
                _super.call(this, 
                // vertex shader
                null, 
                // fragment shader
                [
                    'precision mediump float;',
                    Sha.SHADER_DEFINE_PI,
                    'varying vec2 vTextureCoord;',
                    'uniform float radius;',
                    'uniform float width;',
                    Sha.SHADER_FUNC_hsv2rgb,
                    'void main(void) {',
                    '	float border = width * 0.1;',
                    '	float rad0 = radius - width * 0.5;',
                    '	float rad1 = radius + width * 0.5;',
                    '	vec2 uv = vTextureCoord - 0.5;',
                    '	float dist = sqrt(dot(uv, uv));',
                    // a defines alpha at current pixel
                    '	float a = 0.0;',
                    '	a += smoothstep(rad0, rad0 + border, dist);',
                    '	a -= smoothstep(rad1 - border, rad1, dist);',
                    // calculate radians from center or something
                    '	float angle = atan(uv.y, uv.x);',
                    // The atan return values from [-Pi, Pi]. Map it to [-0.5, 0.5] by dividing by Tau then add 0.5
                    '	float h = (angle / TAU) + 0.5;',
                    '	gl_FragColor = vec4(hsv2rgb(vec3(h, 1.0, 1.0)) * a, a);',
                    '}'
                ].join(''), 
                // uniforms
                {
                    radius: { type: '1f', value: radius },
                    width: { type: '1f', value: width }
                });
            }
            return HueCircleFilter;
        })(PIXI.AbstractFilter);
        Sha.HueCircleFilter = HueCircleFilter;
    })(Sha = KPaint.Sha || (KPaint.Sha = {}));
})(KPaint || (KPaint = {}));
/// <reference path="Common.ts"/>
var KPaint;
(function (KPaint) {
    var Sha;
    (function (Sha) {
        var LinearToGammaFilter = (function (_super) {
            __extends(LinearToGammaFilter, _super);
            function LinearToGammaFilter() {
                _super.call(this, null, [
                    'precision mediump float;',
                    'varying vec2 vTextureCoord;',
                    'varying vec4 vColor;',
                    'uniform sampler2D uSampler;',
                    'uniform float invGamma;',
                    'void main(void) {',
                    '	vec4 tex = texture2D(uSampler, vTextureCoord);',
                    '	tex = pow(tex, vec4(invGamma));',
                    '	gl_FragColor = tex;',
                    '}'
                ].join(''), {
                    invGamma: { type: '1f' }
                });
                this._gamma = 1;
                this.gamma = this._gamma;
            }
            Object.defineProperty(LinearToGammaFilter.prototype, "gamma", {
                get: function () {
                    return this._gamma;
                },
                set: function (value) {
                    this._gamma = value;
                    this.uniforms.invGamma.value = 1 / value;
                },
                enumerable: true,
                configurable: true
            });
            return LinearToGammaFilter;
        })(PIXI.AbstractFilter);
        Sha.LinearToGammaFilter = LinearToGammaFilter;
    })(Sha = KPaint.Sha || (KPaint.Sha = {}));
})(KPaint || (KPaint = {}));
var KPaint;
(function (KPaint) {
    var Sha;
    (function (Sha) {
        /*
            An AbstractFilter to show the part of a texture that will be erased by another texture.
            Apply this filter to the texture that's being erased from, and set the other texture as the mask.
    
            This can be used to store the erased data so it can be undone later on.
        */
        var EraseReversalFilter = (function (_super) {
            __extends(EraseReversalFilter, _super);
            function EraseReversalFilter(maskTexture) {
                _super.call(this, 
                // Vert
                null, 
                // Frag
                [
                    'precision mediump float;',
                    'varying vec2 vTextureCoord;',
                    'uniform sampler2D uSampler;',
                    'uniform sampler2D mask;',
                    'void main(void) {',
                    '	vec4 original = texture2D(uSampler, vTextureCoord);',
                    '	vec4 mask = texture2D(mask, vTextureCoord);',
                    '	gl_FragColor = vec4(original.rgb, ceil(mask.a));',
                    '}'
                ].join(''), 
                // Uniforms
                {
                    mask: { type: 'sampler2D', value: maskTexture }
                });
            }
            Object.defineProperty(EraseReversalFilter.prototype, "mask", {
                get: function () {
                    return this.uniforms.mask.value;
                },
                set: function (m) {
                    this.uniforms.mask.value = m;
                },
                enumerable: true,
                configurable: true
            });
            return EraseReversalFilter;
        })(PIXI.AbstractFilter);
        Sha.EraseReversalFilter = EraseReversalFilter;
    })(Sha = KPaint.Sha || (KPaint.Sha = {}));
})(KPaint || (KPaint = {}));
var KPaint;
(function (KPaint) {
    var Sha;
    (function (Sha) {
        /*
            Filter that will map HSV saturation and value to x and y coordinates respectively,
            given a hue and location.
        
            Used for rendering the middle(square) section of the color wheel.
    
            Color wheels usually use either HSV or HSL color formats. This one uses HSV(also known as HSB) because I
            find it easier to use than HSL. I think CIELAB is an alternative, but I've never tried
            using it.
        */
        /*
            Expects that the texture it is drawn on has a 1:1 resolution
    
            Inputs should be normalized [0 : 1]
        */
        var SaturationValueFilter2 = (function (_super) {
            __extends(SaturationValueFilter2, _super);
            function SaturationValueFilter2(hue) {
                _super.call(this, 
                // vertex shader
                null, 
                // fragment shader
                [
                    'precision mediump float;',
                    'varying vec2 vTextureCoord;',
                    'uniform float hue;',
                    Sha.SHADER_FUNC_hsv2rgb,
                    'void main(void) {',
                    '	vec2 uv = vTextureCoord;',
                    '	uv.y = 1.0 - uv.y;',
                    // combine with hue
                    '	vec3 hsv = vec3(hue, uv.x, uv.y);',
                    // convert to rgb, then set as fragColor
                    '	gl_FragColor = vec4(hsv2rgb(hsv), 1.0);',
                    '}'
                ].join(''), 
                // uniforms
                {
                    hue: { type: '1f', value: hue }
                });
            }
            Object.defineProperty(SaturationValueFilter2.prototype, "hue", {
                get: function () {
                    return this.uniforms.hue.value;
                },
                set: function (value) {
                    this.uniforms.hue.value = value;
                },
                enumerable: true,
                configurable: true
            });
            return SaturationValueFilter2;
        })(PIXI.AbstractFilter);
        Sha.SaturationValueFilter2 = SaturationValueFilter2;
    })(Sha = KPaint.Sha || (KPaint.Sha = {}));
})(KPaint || (KPaint = {}));
var KPaint;
(function (KPaint) {
    var Sha;
    (function (Sha) {
        /*
            Expects that the texture it is drawn on has a 1:1 resolution
    
            Inputs should be normalized [0 : 1]
        */
        var SelectorCircleFilter = (function (_super) {
            __extends(SelectorCircleFilter, _super);
            function SelectorCircleFilter() {
                _super.call(this, 
                // vertex shader
                null, 
                // fragment shader
                [
                    'precision mediump float;',
                    'varying vec2 vTextureCoord;',
                    'void main(void) {',
                    '	float border = 0.0;',
                    '	float rad = 0.25;',
                    '	float rad1 = 0.5;',
                    // UV coordinate
                    '	vec2 uv = vTextureCoord - 0.5;',
                    // a defines where to draw
                    '	float dist = sqrt(dot(uv, uv));',
                    '	float a = 0.0;',
                    '	a += smoothstep(rad, rad + border, dist);',
                    '	a -= smoothstep(rad1 - border, rad1, dist);',
                    '	gl_FragColor = vec4(vec3(1.0), a);',
                    '}'
                ].join(''));
            }
            return SelectorCircleFilter;
        })(PIXI.AbstractFilter);
        Sha.SelectorCircleFilter = SelectorCircleFilter;
    })(Sha = KPaint.Sha || (KPaint.Sha = {}));
})(KPaint || (KPaint = {}));
/// <reference path="../Common.ts"/>
var KPaint;
(function (KPaint) {
    var Sha;
    (function (Sha) {
        var HorizontalHueFilter = (function (_super) {
            __extends(HorizontalHueFilter, _super);
            function HorizontalHueFilter(area, canvas) {
                var scaleX = 1 / (area.width / canvas.clientWidth), scaleY = 1 / (area.height / canvas.clientHeight);
                var drawAX = area.x / canvas.clientWidth, drawAY = area.y / canvas.clientHeight;
                _super.call(this, 
                // vertex shader
                null, 
                // fragment shader
                [
                    'precision mediump float;',
                    'varying vec2 vTextureCoord;',
                    'varying vec4 vColor;',
                    'uniform sampler2D uSampler;',
                    'uniform vec2 uResolution;',
                    'uniform vec2 scale;',
                    'uniform vec2 drawPoint;',
                    Sha.SHADER_FUNC_hsv2rgb,
                    'void main(void) {',
                    // get [0.0, 1.0] values
                    '	vec2 uv = (gl_FragCoord.xy / uResolution);',
                    // flip the y-axis
                    '	uv.y = 1.0 - uv.y;',
                    // set starting point and scale
                    '	uv -= drawPoint;',
                    '	uv *= scale;',
                    '	float hue = uv.x;',
                    '	vec3 rgb = hsv2rgb(vec3(hue, 1.0, 1.0));',
                    // brightness to alpha
                    '	gl_FragColor = vec4(rgb, 1.0);',
                    '}'
                ].join('\n'), 
                // uniforms
                {
                    uResolution: { type: '2f', value: [canvas.clientWidth, canvas.clientHeight] },
                    scale: { type: '2f', value: [scaleX, scaleY] },
                    drawPoint: { type: '2f', value: [drawAX, drawAY] }
                });
            }
            return HorizontalHueFilter;
        })(PIXI.AbstractFilter);
        Sha.HorizontalHueFilter = HorizontalHueFilter;
    })(Sha = KPaint.Sha || (KPaint.Sha = {}));
})(KPaint || (KPaint = {}));
/// <reference path="../Common.ts"/>
var KPaint;
(function (KPaint) {
    var Sha;
    (function (Sha) {
        var HorizontalSaturationFilter = (function (_super) {
            __extends(HorizontalSaturationFilter, _super);
            function HorizontalSaturationFilter(area, canvas, hue, value) {
                var scaleX = 1 / (area.width / canvas.clientWidth), scaleY = 1 / (area.height / canvas.clientHeight);
                var drawAX = area.x / canvas.clientWidth, drawAY = area.y / canvas.clientHeight;
                _super.call(this, 
                // vertex shader
                null, 
                // fragment shader
                [
                    'precision mediump float;',
                    'varying vec2 vTextureCoord;',
                    'varying vec4 vColor;',
                    'uniform sampler2D uSampler;',
                    'uniform vec2 uResolution;',
                    'uniform vec2 scale;',
                    'uniform vec2 drawPoint;',
                    'uniform float hue;',
                    'uniform float value;',
                    Sha.SHADER_FUNC_hsv2rgb,
                    'void main(void) {',
                    // get [0.0, 1.0] values
                    '	vec2 uv = (gl_FragCoord.xy / uResolution);',
                    // flip the y-axis
                    '	uv.y = 1.0 - uv.y;',
                    // set starting point and scale
                    '	uv -= drawPoint;',
                    '	uv *= scale;',
                    '	float saturation = uv.x;',
                    '	vec3 rgb = hsv2rgb(vec3(hue, saturation, value));',
                    // brightness to alpha
                    '	gl_FragColor = vec4(rgb, 1.0);',
                    '}'
                ].join('\n'), 
                // uniforms
                {
                    uResolution: { type: '2f', value: [canvas.clientWidth, canvas.clientHeight] },
                    scale: { type: '2f', value: [scaleX, scaleY] },
                    drawPoint: { type: '2f', value: [drawAX, drawAY] },
                    hue: { type: '1f', value: hue },
                    value: { type: '1f', value: value }
                });
            }
            Object.defineProperty(HorizontalSaturationFilter.prototype, "hue", {
                get: function () {
                    return this.uniforms.hue.value;
                },
                set: function (value) {
                    this.uniforms.hue.value = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(HorizontalSaturationFilter.prototype, "value", {
                get: function () {
                    return this.uniforms.value.value;
                },
                set: function (value) {
                    this.uniforms.value.value = value;
                },
                enumerable: true,
                configurable: true
            });
            return HorizontalSaturationFilter;
        })(PIXI.AbstractFilter);
        Sha.HorizontalSaturationFilter = HorizontalSaturationFilter;
    })(Sha = KPaint.Sha || (KPaint.Sha = {}));
})(KPaint || (KPaint = {}));
/// <reference path="../Common.ts"/>
var KPaint;
(function (KPaint) {
    var Sha;
    (function (Sha) {
        var HorizontalValueFilter = (function (_super) {
            __extends(HorizontalValueFilter, _super);
            function HorizontalValueFilter(area, canvas, hue, saturation) {
                var scaleX = 1 / (area.width / canvas.clientWidth), scaleY = 1 / (area.height / canvas.clientHeight);
                var drawAX = area.x / canvas.clientWidth, drawAY = area.y / canvas.clientHeight;
                _super.call(this, 
                // vertex shader
                null, 
                // fragment shader
                [
                    'precision mediump float;',
                    'varying vec2 vTextureCoord;',
                    'varying vec4 vColor;',
                    'uniform sampler2D uSampler;',
                    'uniform vec2 uResolution;',
                    'uniform vec2 scale;',
                    'uniform vec2 drawPoint;',
                    'uniform float hue;',
                    'uniform float saturation;',
                    Sha.SHADER_FUNC_hsv2rgb,
                    'void main(void) {',
                    // get [0.0, 1.0] values
                    '	vec2 uv = (gl_FragCoord.xy / uResolution);',
                    // flip the y-axis
                    '	uv.y = 1.0 - uv.y;',
                    // set starting point and scale
                    '	uv -= drawPoint;',
                    '	uv *= scale;',
                    '	float value = uv.x;',
                    '	vec3 rgb = hsv2rgb(vec3(hue, saturation, value));',
                    // brightness to alpha
                    '	gl_FragColor = vec4(rgb, 1.0);',
                    '}'
                ].join('\n'), 
                // uniforms
                {
                    uResolution: { type: '2f', value: [canvas.clientWidth, canvas.clientHeight] },
                    scale: { type: '2f', value: [scaleX, scaleY] },
                    drawPoint: { type: '2f', value: [drawAX, drawAY] },
                    hue: { type: '1f', value: hue },
                    saturation: { type: '1f', value: saturation }
                });
            }
            Object.defineProperty(HorizontalValueFilter.prototype, "hue", {
                get: function () {
                    return this.uniforms.hue.value;
                },
                set: function (value) {
                    this.uniforms.hue.value = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(HorizontalValueFilter.prototype, "saturation", {
                get: function () {
                    return this.uniforms.saturation.value;
                },
                set: function (value) {
                    this.uniforms.saturation.value = value;
                },
                enumerable: true,
                configurable: true
            });
            return HorizontalValueFilter;
        })(PIXI.AbstractFilter);
        Sha.HorizontalValueFilter = HorizontalValueFilter;
    })(Sha = KPaint.Sha || (KPaint.Sha = {}));
})(KPaint || (KPaint = {}));
var KPaint;
(function (KPaint) {
    var UI;
    (function (UI) {
        var DisplayObject = (function () {
            function DisplayObject(x, y) {
                this._container = new PIXI.Container();
                // for use by the toLocal and vecToLocal functions. Make sure not to send this outside of the class!
                this._localVec = new KPaint.Vec3();
                this.x = x;
                this.y = y;
            }
            Object.defineProperty(DisplayObject.prototype, "x", {
                get: function () { return this._container.x; },
                set: function (value) { this._container.x = value; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DisplayObject.prototype, "y", {
                get: function () { return this._container.y; },
                set: function (value) { this._container.y = value; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(DisplayObject.prototype, "visible", {
                get: function () { return this._container.visible; },
                set: function (value) { this._container.visible = value; },
                enumerable: true,
                configurable: true
            });
            DisplayObject.prototype.addChild = function (object) {
                this._container.addChild(object._container);
            };
            DisplayObject.prototype.addChildAt = function (object, idx) {
                this._container.addChildAt(object._container, idx);
            };
            DisplayObject.prototype.addChildPIXI = function (object) {
                this._container.addChild(object);
            };
            DisplayObject.prototype.removeChild = function (object) {
                this._container.removeChild(object._container);
            };
            DisplayObject.prototype.setParent = function (object) {
                object.addChild(this);
            };
            DisplayObject.prototype.setParentPIXI = function (container) {
                container.addChild(this._container);
            };
            DisplayObject.prototype.toLocal = function (x, y) {
                return this._localVec.xyz(x - this.x, y - this.y, 0);
            };
            DisplayObject.prototype.vecToLocal = function (vec) {
                return this._localVec.xyz(vec.x - this.x, vec.y - this.y, vec.z || 0);
            };
            return DisplayObject;
        })();
        UI.DisplayObject = DisplayObject;
    })(UI = KPaint.UI || (KPaint.UI = {}));
})(KPaint || (KPaint = {}));
/// <reference path="DisplayObject.ts"/>
var KPaint;
(function (KPaint) {
    var UI;
    (function (UI) {
        var Clickable2 = (function (_super) {
            __extends(Clickable2, _super);
            function Clickable2(x, y, width, height) {
                _super.call(this, x, y);
                this.isActive = false;
                this._cParent = null;
                this.width = width;
                this.height = height;
            }
            Object.defineProperty(Clickable2.prototype, "cParent", {
                get: function () {
                    return this._cParent;
                },
                set: function (parent) {
                    this._cParent = parent;
                },
                enumerable: true,
                configurable: true
            });
            /*
                The coordinates supplied are assumed to be local (within the parent if one exists)
                
                The coordinate will be used if either the click coordinate is within bounds
                or this IClickable is currently active.
            */
            Clickable2.prototype.click = function (pointer, setActive) {
                if (setActive === void 0) { setActive = true; }
                this.clickHandler(pointer);
                /*
                    set active after calling the handler, because in some cases the handler's logic will use the
                    element's isActive property to determine whether the click is on the element or a child element.
                */
                if (setActive) {
                    this.isActive = true;
                }
            };
            Clickable2.prototype.contains = function (pointer) {
                if (this.x > pointer.x)
                    return false;
                if (this.x + this.width < pointer.x)
                    return false;
                if (this.y > pointer.y)
                    return false;
                if (this.y + this.height < pointer.y)
                    return false;
                return true;
            };
            // deactivate if active and call releaseHandler
            Clickable2.prototype.releaseClick = function () {
                this.releaseHandler();
                this.isActive = false;
            };
            return Clickable2;
        })(UI.DisplayObject);
        UI.Clickable2 = Clickable2;
        var ClickableContainer2 = (function (_super) {
            __extends(ClickableContainer2, _super);
            function ClickableContainer2(x, y, width, height) {
                _super.call(this, x, y, width, height);
                this._clickables = [];
                this._local = new KPaint.Vec3();
            }
            ClickableContainer2.prototype.addClickListener = function (c) {
                this._clickables.push(c);
                c.cParent = this;
            };
            ClickableContainer2.prototype.removeClickable = function (c) {
                var idx = this._clickables.indexOf(c);
                if (idx >= 0) {
                    this._clickables.splice(idx, 1);
                    c.cParent = null;
                }
            };
            ClickableContainer2.prototype.clickHandler = function (pointer) {
                var clickables = this._clickables, i = 0, n = clickables.length;
                // translate to local coordinates
                var local = this._local;
                local.copyFrom(pointer);
                local.x -= this.x;
                local.y -= this.y;
                // check if any sub element is active
                for (; i < n; i++) {
                    if (clickables[i].isActive) {
                        clickables[i].click(local, false);
                        return;
                    }
                }
                // check if the click is on a sub element
                for (i = 0; i < n; i++) {
                    if (clickables[i].contains(local)) {
                        clickables[i].click(local, true);
                        return;
                    }
                }
            };
            // release any clicks that is active
            ClickableContainer2.prototype.releaseHandler = function () {
                var clickables = this._clickables;
                for (var i = 0, n = clickables.length; i < n; i++) {
                    if (clickables[i].isActive) {
                        clickables[i].releaseClick();
                    }
                }
            };
            return ClickableContainer2;
        })(Clickable2);
        UI.ClickableContainer2 = ClickableContainer2;
    })(UI = KPaint.UI || (KPaint.UI = {}));
})(KPaint || (KPaint = {}));
/// <reference path="Clickable.ts"/>
var KPaint;
(function (KPaint) {
    var UI;
    (function (UI) {
        var Button = (function (_super) {
            __extends(Button, _super);
            function Button(x, y, width, height, color, text, emit) {
                if (text === void 0) { text = null; }
                if (emit === void 0) { emit = null; }
                _super.call(this, x, y, width, height);
                this._stayDown = false;
                this._bg = new PIXI.Graphics();
                this._bgClicked = new PIXI.Graphics();
                this._text = new PIXI.Text('', { font: '11px Arial', fill: 0x000000, strokeThickness: 0.25 });
                x = 0;
                y = 0;
                if (emit)
                    this.emit = emit;
                if (text)
                    this._text.text = text;
                this._text.x = width / 2;
                this._text.y = height / 2;
                this._text.anchor.x = 0.5;
                this._text.anchor.y = 0.5;
                this._bg = this.generateBg(color);
                var clickedColor = ((0xffffff - color) > color) ? (color + 0x222222) : (color - 0x222222);
                this._bgClicked = this.generateBg(clickedColor);
                this._bgClicked.visible = false;
                this.addChildPIXI(this._bg);
                this.addChildPIXI(this._bgClicked);
                this.addChildPIXI(this._text);
            }
            Object.defineProperty(Button.prototype, "stayDown", {
                get: function () {
                    return this._stayDown;
                },
                set: function (value) {
                    this._stayDown = value;
                    if (value === true) {
                        this.setButtonPressed();
                    }
                    else {
                        this.setButtonNotPressed();
                    }
                },
                enumerable: true,
                configurable: true
            });
            Button.prototype.setButtonPressed = function () {
                this._bg.visible = false;
                this._bgClicked.visible = true;
            };
            Button.prototype.setButtonNotPressed = function () {
                this._bg.visible = true;
                this._bgClicked.visible = false;
            };
            Button.prototype.clickHandler = function (pt) {
                if (this.emit !== null && !this.isActive) {
                    this.setButtonPressed();
                    KPaint.Events.emit(this.emit);
                }
            };
            Button.prototype.releaseHandler = function () {
                if (this.stayDown !== true) {
                    this.setButtonNotPressed();
                }
            };
            Button.prototype.generateBg = function (color) {
                var lineColor = ((0xffffff - color) > color) ? 0xdddddd : 0x222222;
                var width = this.width, height = this.height;
                return new PIXI.Graphics()
                    .beginFill(color)
                    .lineStyle(2, lineColor, 1)
                    .drawRect(0, 0, width, height)
                    .moveTo(0, 0)
                    .lineTo(width, 0)
                    .lineTo(width, height)
                    .lineTo(0, height)
                    .lineTo(0, 0)
                    .endFill();
            };
            return Button;
        })(UI.Clickable2);
        UI.Button = Button;
        // a button that will stay down when pressed, and pressing it again will take it back up
        // works just like a checkbox in html
        var TwoStateButton = (function (_super) {
            __extends(TwoStateButton, _super);
            function TwoStateButton(x, y, width, height, color, text, emit) {
                if (text === void 0) { text = null; }
                if (emit === void 0) { emit = null; }
                _super.call(this, x, y, width, height, color, text, emit);
                this._isDown = false;
            }
            TwoStateButton.prototype.clickHandler = function (pt) {
                if (this._isDown) {
                    this.setButtonNotPressed();
                    this._isDown = false;
                }
                else {
                    this.setButtonPressed();
                    this._isDown = true;
                }
            };
            TwoStateButton.prototype.releaseHandler = function () { };
            return TwoStateButton;
        })(Button);
        UI.TwoStateButton = TwoStateButton;
    })(UI = KPaint.UI || (KPaint.UI = {}));
})(KPaint || (KPaint = {}));
var KPaint;
(function (KPaint) {
    var UI;
    (function (UI) {
        /*
            A selector is sprite that shows a dot
            It has two color components: an inner circle, and a ring around the outside.
            The graphics are procedurally generated
        */
        var Selector = (function (_super) {
            __extends(Selector, _super);
            function Selector(x, y, radius) {
                _super.call(this, x, y);
                this._radius = radius;
                this.colorHsv = new KPaint.Vec3();
                this._outer = new PIXI.RenderTexture(KPaint.renderer, radius * 2, radius * 2);
                this._selector = new PIXI.Sprite(this._outer);
                var sel = this._selector;
                sel.shader = new KPaint.Sha.SelectorCircleFilter();
                sel.anchor.x = 0.5;
                sel.anchor.y = 0.5;
                // color  filter
                var rgb = this._localVec;
                KPaint.hsvToRgb(this.colorHsv, rgb);
                this._colorFilter = new KPaint.Sha.ColorFilter(rgb.r, rgb.g, rgb.b);
                sel.filters = [this._colorFilter];
                this.addChildPIXI(sel);
            }
            Selector.prototype.updateColor = function () {
                this._selector.cacheAsBitmap = false;
                var rgb = this._localVec;
                KPaint.hsvToRgb(this.colorHsv, rgb);
                var colFilter = this._colorFilter;
                colFilter.r = rgb.r;
                colFilter.g = rgb.g;
                colFilter.b = rgb.b;
                this._selector.cacheAsBitmap = true;
            };
            return Selector;
        })(UI.DisplayObject);
        UI.Selector = Selector;
    })(UI = KPaint.UI || (KPaint.UI = {}));
})(KPaint || (KPaint = {}));
var KPaint;
(function (KPaint) {
    var UI;
    (function (UI) {
        var Slider = (function (_super) {
            __extends(Slider, _super);
            function Slider(x, y, width, height, paddingLeft, paddingRight, preText, eventEmit, eventListen) {
                var _this = this;
                if (eventEmit === void 0) { eventEmit = null; }
                if (eventListen === void 0) { eventListen = null; }
                _super.call(this, x, y, width, height);
                this.centerArea = new KPaint.Vec4();
                this._background = new PIXI.Graphics();
                this._preText = new PIXI.Text('', { font: '14px arial', strokeThickness: 0.3 });
                this._postText = new PIXI.Text('', { font: '12px arial' });
                this._maxNumber = 1;
                x = 0;
                y = 0;
                var midX = x;
                var midY = y + height / 2;
                this._midY = midY;
                this.centerArea.xyzw(x + paddingLeft, y, width - paddingLeft - paddingRight, height);
                this._selector = new UI.Selector(midX, midY, Math.round(width / 32));
                this.generateSliderLine();
                var pre = this._preText;
                pre.text = preText;
                pre.x = x + paddingLeft / 2;
                pre.y = midY;
                pre.anchor.y = 0.5;
                pre.anchor.x = 0.5;
                var post = this._postText;
                post.x = x + width - paddingRight / 2;
                post.y = midY;
                post.anchor.y = 0.5;
                post.anchor.x = 0.5;
                // add to container
                this.addChildPIXI(this._background);
                this.addChildPIXI(this._preText);
                this.addChildPIXI(this._postText);
                this.addChild(this._selector);
                this.percent = 0.0;
                // register event
                this._eventEmit = eventEmit;
                this._eventListen = eventListen;
                KPaint.Events.register(eventListen, function (n) { return _this.number = n[0]; });
            }
            Object.defineProperty(Slider.prototype, "percent", {
                get: function () {
                    return (this._selector.x - this.centerArea.x) / this.centerArea.width;
                },
                set: function (value) {
                    var x = this.centerArea.x + this.centerArea.width * value;
                    this._selector.x = x;
                    this._selector.y = this._midY;
                    this._postText.text = this.percent.toFixed(2).toString();
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Slider.prototype, "number", {
                get: function () {
                    return (this.percent * this._maxNumber);
                },
                set: function (value) {
                    this.percent = KPaint.utils.clamp(value / this._maxNumber, 0, 1);
                    this._postText.text = this.number.toFixed(2).toString();
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Slider.prototype, "max", {
                get: function () {
                    return this._maxNumber;
                },
                set: function (value) {
                    this._maxNumber = value;
                },
                enumerable: true,
                configurable: true
            });
            Slider.prototype.clickHandler = function (pt) {
                var x = pt.x, y = this._midY, sx = this.centerArea.x, sw = this.centerArea.width;
                if (x < sx)
                    x = sx;
                if (x > sx + sw)
                    x = sx + sw;
                this._selector.x = x;
                // emit event, if one is registered
                if (this._eventEmit !== null) {
                    KPaint.Events.emit(this._eventEmit, this.number);
                }
            };
            Slider.prototype.releaseHandler = function () { };
            Slider.prototype.generateSliderLine = function () {
                var a = this.centerArea;
                // background
                this._background.beginFill(0x000000)
                    .lineStyle(4, 0x000000, 1.0)
                    .moveTo(a.x, this._midY)
                    .lineTo(a.x + a.width, this._midY)
                    .endFill();
            };
            return Slider;
        })(UI.Clickable2);
        UI.Slider = Slider;
    })(UI = KPaint.UI || (KPaint.UI = {}));
})(KPaint || (KPaint = {}));
var KPaint;
(function (KPaint) {
    var UI;
    (function (UI) {
        /*
            Slider that uses an area rather than a line. The area can have a filter set on it.
    
            By setting a filter you can define a gradient over the area.
        */
        var AreaSlider = (function (_super) {
            __extends(AreaSlider, _super);
            function AreaSlider(x, y, width, height, paddingLeft, paddingRight, preText, eventId) {
                var _this = this;
                _super.call(this, x, y, width, height);
                this.centerArea = new KPaint.Vec4();
                this._background = new PIXI.Sprite();
                this._preText = new PIXI.Text('', { font: '16px arial', strokeThickness: 1 });
                this._postText = new PIXI.Text('', { font: '12px arial' });
                this.handleCallback = function (percent) {
                    _this.percent = percent[0];
                };
                this._emitEvent = eventId;
                var ca = this.centerArea;
                x = 0;
                y = 0;
                var midX = x;
                var midY = y + height / 2;
                this._midY = midY;
                this.centerArea.xyzw(x + paddingLeft, y + height * 0.3, width - paddingLeft - paddingRight, height * 0.4);
                this._selector = new UI.Selector(midX, midY, Math.round(width / 32));
                var pre = this._preText;
                pre.text = preText;
                pre.x = x + paddingLeft / 2;
                pre.y = midY;
                pre.anchor.y = 0.5;
                pre.anchor.x = 0.5;
                var post = this._postText;
                post.x = x + width - paddingRight / 2;
                post.y = midY;
                post.anchor.y = 0.5;
                post.anchor.x = 0.5;
                var rt = new PIXI.RenderTexture(KPaint.renderer, ca.width, ca.height);
                this._background = new PIXI.Sprite(rt);
                this._background.x = ca.x;
                this._background.y = ca.y;
                this.addChildPIXI(this._background);
                this.addChildPIXI(pre);
                this.addChildPIXI(post);
                this.addChild(this._selector);
                this.percent = 0.0;
            }
            Object.defineProperty(AreaSlider.prototype, "percent", {
                get: function () {
                    return (this._selector.x - this.centerArea.x) / this.centerArea.width;
                },
                set: function (value) {
                    this._selector.x = this.centerArea.x + this.centerArea.width * value;
                    this._selector.y = this._midY;
                    this._postText.text = this.percent.toFixed(2).toString();
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(AreaSlider.prototype, "cacheAsBitmat", {
                get: function () { return this._background.cacheAsBitmap; },
                set: function (value) { this._background.cacheAsBitmap = value; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(AreaSlider.prototype, "areaFilter", {
                get: function () {
                    return this._background.shader;
                },
                set: function (filter) {
                    this._background.shader = filter;
                },
                enumerable: true,
                configurable: true
            });
            // event to emit when this slider is changed
            AreaSlider.prototype.setEmitEvent = function (event) {
                this._emitEvent = event;
            };
            // slider should react to this eventId
            AreaSlider.prototype.setListenEvent = function (event) {
                KPaint.Events.register(event, this.handleCallback);
            };
            AreaSlider.prototype.clickHandler = function (pt) {
                var x = pt.x, y = this._midY, sx = this.centerArea.x, sw = this.centerArea.width;
                if (x < sx)
                    x = sx;
                if (x > sx + sw)
                    x = sx + sw;
                this._selector.x = x;
                this._postText.text = this.percent.toFixed(2).toString();
                // emit event, if one is registered
                if (this._emitEvent !== null)
                    KPaint.Events.emit(this._emitEvent, this.percent);
            };
            AreaSlider.prototype.releaseHandler = function () { };
            return AreaSlider;
        })(UI.Clickable2);
        UI.AreaSlider = AreaSlider;
        // extensions
        var HueSlider = (function (_super) {
            __extends(HueSlider, _super);
            function HueSlider(x, y, width, height, paddingLeft, paddingRight) {
                var _this = this;
                _super.call(this, x, y, width, height, paddingLeft, paddingRight, 'H');
                this.hue = function (v) {
                    var value = v[0];
                    // different parts of the spectrum have different brightness values assigned to them
                    if (value > .575 || value < .1)
                        value = 1;
                    else
                        value = 0;
                    _this._selector.colorHsv.z = value;
                    _this._selector.updateColor();
                };
                this.areaFilter = new KPaint.Sha.HorizontalHueFilter(this.centerArea, KPaint.renderer.view);
                this.hue([0]);
                // Events
                KPaint.Events.register(KPaint.Events.ID.BRUSH_COLOR_H, this.hue);
                this.setListenEvent(KPaint.Events.ID.BRUSH_COLOR_H);
                this.setEmitEvent(KPaint.Events.ID.INPUT_COLOR_CHANGE_H);
            }
            return HueSlider;
        })(AreaSlider);
        UI.HueSlider = HueSlider;
        var SaturationSlider = (function (_super) {
            __extends(SaturationSlider, _super);
            function SaturationSlider(x, y, width, height, paddingLeft, paddingRight, hue, value) {
                var _this = this;
                _super.call(this, x, y, width, height, paddingLeft, paddingRight, 'S');
                this.hue = function (value) {
                    _this.areaFilter.hue = value[0];
                };
                this.value = function (value) {
                    _this.areaFilter.value = value[0];
                    _this._selector.colorHsv.z = Math.round(1 - value[0]);
                    _this._selector.updateColor();
                };
                this.areaFilter = new KPaint.Sha.HorizontalSaturationFilter(this.centerArea, KPaint.renderer.view, hue, value);
                this.value([value]);
                // Events
                KPaint.Events.register(KPaint.Events.ID.BRUSH_COLOR_H, this.hue);
                KPaint.Events.register(KPaint.Events.ID.BRUSH_COLOR_V, this.value);
                this.setListenEvent(KPaint.Events.ID.BRUSH_COLOR_S);
                this.setEmitEvent(KPaint.Events.ID.INPUT_COLOR_CHANGE_S);
            }
            return SaturationSlider;
        })(AreaSlider);
        UI.SaturationSlider = SaturationSlider;
        var ValueSlider = (function (_super) {
            __extends(ValueSlider, _super);
            function ValueSlider(x, y, width, height, paddingLeft, paddingRight, hue, saturation) {
                var _this = this;
                _super.call(this, x, y, width, height, paddingLeft, paddingRight, 'V');
                this.hue = function (value) {
                    _this.areaFilter.hue = value[0];
                };
                this.saturation = function (value) {
                    _this.areaFilter.saturation = value[0];
                };
                this.value = function (value) {
                    _this._selector.colorHsv.z = Math.round(1 - value[0]);
                    _this._selector.updateColor();
                };
                this.areaFilter = new KPaint.Sha.HorizontalValueFilter(this.centerArea, KPaint.renderer.view, hue, saturation);
                this.value([0]);
                // Events
                KPaint.Events.register(KPaint.Events.ID.BRUSH_COLOR_H, this.hue);
                KPaint.Events.register(KPaint.Events.ID.BRUSH_COLOR_S, this.saturation);
                KPaint.Events.register(KPaint.Events.ID.BRUSH_COLOR_V, this.value);
                this.setListenEvent(KPaint.Events.ID.BRUSH_COLOR_V);
                this.setEmitEvent(KPaint.Events.ID.INPUT_COLOR_CHANGE_V);
            }
            return ValueSlider;
        })(AreaSlider);
        UI.ValueSlider = ValueSlider;
    })(UI = KPaint.UI || (KPaint.UI = {}));
})(KPaint || (KPaint = {}));
var KPaint;
(function (KPaint) {
    var UI;
    (function (UI) {
        var ColorWheel = (function (_super) {
            __extends(ColorWheel, _super);
            function ColorWheel(x, y, width, height) {
                var _this = this;
                _super.call(this, x, y, width, height);
                this._hPositions = [];
                this.hueChangedCallback = function (h) {
                    var idx = Math.round(h * (_this._hPositions.length - 1));
                    var pos = _this._hPositions[idx];
                    _this._hSelector.x = pos.x;
                    _this._hSelector.y = pos.y;
                    _this.setSelectorColor();
                };
                var colorRing = new PIXI.Sprite(new PIXI.RenderTexture(KPaint.renderer, width, height));
                colorRing.shader = new KPaint.Sha.HueCircleFilter(0.4, 0.06);
                colorRing.cacheAsBitmap = true;
                this._colorRing = colorRing;
                // selector
                this.generateHPositions();
                var initialPos = this._hPositions[0];
                this._hSelector = new UI.Selector(initialPos.x, initialPos.y, Math.round(width / 32));
                // middle element
                this._svElement = new UI.ColorWheelSvArea(width / 4, height / 4, width / 2, height / 2);
                this.setSelectorColor();
                // add to containers
                this.addChildPIXI(colorRing);
                this.addChild(this._hSelector);
                this.addChild(this._svElement);
                KPaint.Events.register(KPaint.Events.ID.BRUSH_COLOR_H, function (v) { return _this.hueChangedCallback(v[0]); });
            }
            Object.defineProperty(ColorWheel.prototype, "hue", {
                get: function () { return this._svElement.hue; },
                set: function (value) { this._svElement.hue = value; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ColorWheel.prototype, "saturation", {
                get: function () { return this._svElement.hue; },
                set: function (value) { this._svElement.hue = value; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ColorWheel.prototype, "value", {
                get: function () { return this._svElement.hue; },
                set: function (value) { this._svElement.hue = value; },
                enumerable: true,
                configurable: true
            });
            ColorWheel.prototype.setSelectorPosition = function (x, y) {
                // find the closest position in _hPositions
                var hPos = this._hPositions, closest = hPos[0], closestDist = 9999, idx = -1, n = hPos.length, pos, dist, distFunc = KPaint.utils.distance;
                for (var i = 0; i < n; i++) {
                    pos = hPos[i];
                    dist = distFunc(x, y, pos.x, pos.y);
                    if (closestDist > dist) {
                        closest = pos;
                        closestDist = dist;
                        idx = i;
                    }
                }
                // set selector's position to that of closest
                this._hSelector.x = closest.x;
                this._hSelector.y = closest.y;
                // calculate hue from index
                var h = idx / hPos.length;
                this._svElement.hue = h;
                this.setSelectorColor();
            };
            ColorWheel.prototype.setSelectorColor = function () {
                var h = this._svElement.hue;
                // different parts of the spectrum have different brightness values assigned to them
                if (h > .575 || h < .1)
                    h = 1;
                else
                    h = 0;
                this._hSelector.colorHsv.z = h;
                this._hSelector.updateColor();
            };
            // fill _hPositions with potential positions for the hue selector
            ColorWheel.prototype.generateHPositions = function (n) {
                if (n === void 0) { n = 256; }
                var cosFunc = Math.cos, sinFunc = Math.sin, PI = Math.PI;
                var hPos = this._hPositions;
                var centerX = this.x + this.width / 2;
                var centerY = this.y + this.height / 2;
                var radius = (this.width - centerX) * 0.8;
                var radian = Math.PI * 2 / (n + 1);
                var x, y;
                for (var i = 0; i < n; i++) {
                    x = centerX + radius * cosFunc(i * radian - PI);
                    y = centerY + radius * sinFunc(i * radian - PI);
                    hPos.push(new KPaint.Vec2(x, y));
                }
            };
            /*
                Once an element is clicked, it is set as the active elements, and all
                mouse movements will be sent to that element. Use releaseClick()
                to deactivate the element on mouseup.
            */
            ColorWheel.prototype.clickHandler = function (pt) {
                var local = this.vecToLocal(pt);
                // check if child element is active
                if (this._svElement.isActive) {
                    this._svElement.click(local);
                }
                else if (this.isActive) {
                    this.setSelectorPosition(local.x, local.y);
                }
                else if (this._svElement.contains(local)) {
                    this._svElement.click(local);
                }
                else {
                    this.setSelectorPosition(local.x, local.y);
                    this.isActive = true;
                }
            };
            ColorWheel.prototype.releaseHandler = function () {
                this._svElement.releaseClick();
            };
            return ColorWheel;
        })(UI.Clickable2);
        UI.ColorWheel = ColorWheel;
    })(UI = KPaint.UI || (KPaint.UI = {}));
})(KPaint || (KPaint = {}));
var KPaint;
(function (KPaint) {
    var UI;
    (function (UI) {
        // saturation and value area of the color wheel
        var ColorWheelSvArea = (function (_super) {
            __extends(ColorWheelSvArea, _super);
            function ColorWheelSvArea(x, y, width, height) {
                var _this = this;
                _super.call(this, x, y, width, height);
                this._colorHsv = new KPaint.Vec3();
                // update colors without emitting an event
                this.updateHue = function (value) {
                    var svSpr = _this._svAreaSprite;
                    _this._colorHsv.h = value;
                    svSpr.cacheAsBitmap = false;
                    _this._svAreaFilter.hue = value;
                    svSpr.cacheAsBitmap = true;
                };
                this.updateSaturation = function (value) {
                    _this._colorHsv.s = value;
                    _this.updateSelectorPosition();
                };
                this.updateValue = function (value) {
                    _this._colorHsv.v = value;
                    _this.updateSelectorPosition();
                };
                var colorHsv = this._colorHsv;
                colorHsv.h = 0.0;
                colorHsv.s = 0.0;
                colorHsv.v = 0.0;
                this._svSelector = new UI.Selector(0, 0, Math.round(width / 18));
                this.updateSelectorColor();
                // filter
                this._svAreaFilter = new KPaint.Sha.SaturationValueFilter2(0.0);
                // sprite to apply filter to
                var rt = new PIXI.RenderTexture(KPaint.renderer, width, height);
                var spr = new PIXI.Sprite(rt);
                spr.shader = this._svAreaFilter;
                spr.cacheAsBitmap = true;
                this._svAreaSprite = spr;
                // add children to container
                this.addChildPIXI(this._svAreaSprite);
                this.addChild(this._svSelector);
                this.updateSelectorPosition();
                // events 
                KPaint.Events.register(KPaint.Events.ID.BRUSH_COLOR_H, function (v) { return _this.updateHue(v[0]); });
                KPaint.Events.register(KPaint.Events.ID.BRUSH_COLOR_S, function (v) { return _this.updateSaturation(v[0]); });
                KPaint.Events.register(KPaint.Events.ID.BRUSH_COLOR_V, function (v) { return _this.updateValue(v[0]); });
            }
            Object.defineProperty(ColorWheelSvArea.prototype, "hue", {
                // using the setters will emit events
                get: function () {
                    return this._colorHsv.h;
                },
                set: function (h) {
                    this.updateHue(h);
                    KPaint.Events.emit(KPaint.Events.ID.INPUT_COLOR_CHANGE_H, h);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ColorWheelSvArea.prototype, "saturation", {
                get: function () {
                    return this._colorHsv.s;
                },
                set: function (s) {
                    this.updateSaturation(s);
                    KPaint.Events.emit(KPaint.Events.ID.INPUT_COLOR_CHANGE_S, s);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ColorWheelSvArea.prototype, "value", {
                get: function () {
                    return this._colorHsv.v;
                },
                set: function (v) {
                    this.updateValue(v);
                    KPaint.Events.emit(KPaint.Events.ID.INPUT_COLOR_CHANGE_V, v);
                },
                enumerable: true,
                configurable: true
            });
            ColorWheelSvArea.prototype.clickHandler = function (pt) {
                // create local copies for adjustment
                var local = this.vecToLocal(pt), x = local.x, y = local.y;
                var width = this.width;
                var height = this.height;
                var selector = this._svSelector;
                // keep the selector within bounds
                if (x < 0)
                    x = 0;
                else if (x > width)
                    x = width;
                if (y < 0)
                    y = 0;
                else if (y > height)
                    y = height;
                // set saturation and value
                this.saturation = x / width;
                this.value = 1 - (y / height);
                // set selector position
                selector.x = x;
                selector.y = y;
            };
            ColorWheelSvArea.prototype.releaseHandler = function () { };
            // updates selector position based on current color
            ColorWheelSvArea.prototype.updateSelectorPosition = function () {
                var colorHsv = this._colorHsv;
                var selector = this._svSelector;
                // set selector position
                selector.x = this.width * colorHsv.s;
                selector.y = this.height * (1 - colorHsv.v);
                this._svAreaFilter.hue = colorHsv.h;
                this.updateSelectorColor();
            };
            ColorWheelSvArea.prototype.updateSelectorColor = function () {
                var colorHsv = this._colorHsv;
                var v = colorHsv.v;
                var selector = this._svSelector;
                v *= (1 - colorHsv.s);
                v = Math.round(1 - v);
                selector.colorHsv.v = v;
                selector.updateColor();
            };
            return ColorWheelSvArea;
        })(UI.Clickable2);
        UI.ColorWheelSvArea = ColorWheelSvArea;
    })(UI = KPaint.UI || (KPaint.UI = {}));
})(KPaint || (KPaint = {}));
var KPaint;
(function (KPaint) {
    var UI;
    (function (UI) {
        var BrushButtonArea = (function (_super) {
            __extends(BrushButtonArea, _super);
            function BrushButtonArea(x, y, width, height) {
                var _this = this;
                _super.call(this, x, y, width, height);
                this._buttons = [];
                x = 0;
                y = 0;
                var spacePerButton = height / 2;
                var padding = 4;
                var buttonSize = spacePerButton - padding * 2;
                var color = 0x999999;
                var brushPaintButton = new UI.Button(x + padding, y + padding, buttonSize, buttonSize, color, 'Paint', KPaint.Events.ID.INPUT_TOOL_BRUSH_PAINT);
                brushPaintButton.stayDown = true;
                x += spacePerButton;
                var brushBlurButton = new UI.Button(x + padding, y + padding, buttonSize, buttonSize, color, 'Blur', KPaint.Events.ID.INPUT_TOOL_BRUSH_BLUR);
                x += spacePerButton;
                var brushEraseButton = new UI.Button(x + padding, y + padding, buttonSize, buttonSize, color, 'Erase', KPaint.Events.ID.INPUT_TOOL_BRUSH_ERASE);
                y += spacePerButton;
                x -= spacePerButton * 2;
                var zoomerToolButton = new UI.Button(x + padding, y + padding, buttonSize, buttonSize, color, 'Zoom', KPaint.Events.ID.INPUT_TOOL_ZOOMER);
                x += spacePerButton;
                var moverToolButton = new UI.Button(x + padding, y + padding, buttonSize, buttonSize, color, 'Move', KPaint.Events.ID.INPUT_TOOL_MOVER);
                x += spacePerButton;
                var fillerToolButton = new UI.Button(x + padding, y + padding, buttonSize, buttonSize, color, 'Pick', KPaint.Events.ID.INPUT_TOOL_COLOR_PICKER);
                // add to buttons array
                var buttons = this._buttons;
                buttons.push(brushPaintButton);
                buttons.push(brushEraseButton);
                buttons.push(brushBlurButton);
                buttons.push(moverToolButton);
                buttons.push(zoomerToolButton);
                buttons.push(fillerToolButton);
                var button;
                for (var i = 0, n = buttons.length; i < n; i++) {
                    button = buttons[i];
                    this.addChild(button);
                    this.addClickListener(button);
                }
                // callbacks to set the active brush mode
                KPaint.Events.register(KPaint.Events.ID.INPUT_TOOL_BRUSH_PAINT, function () { _this.useButton(KPaint.Events.ID.INPUT_TOOL_BRUSH_PAINT); });
                KPaint.Events.register(KPaint.Events.ID.INPUT_TOOL_BRUSH_BLUR, function () { _this.useButton(KPaint.Events.ID.INPUT_TOOL_BRUSH_BLUR); });
                KPaint.Events.register(KPaint.Events.ID.INPUT_TOOL_BRUSH_ERASE, function () { _this.useButton(KPaint.Events.ID.INPUT_TOOL_BRUSH_ERASE); });
                KPaint.Events.register(KPaint.Events.ID.INPUT_TOOL_ZOOMER, function () { _this.useButton(KPaint.Events.ID.INPUT_TOOL_ZOOMER); });
                KPaint.Events.register(KPaint.Events.ID.INPUT_TOOL_MOVER, function () { _this.useButton(KPaint.Events.ID.INPUT_TOOL_MOVER); });
                KPaint.Events.register(KPaint.Events.ID.INPUT_TOOL_COLOR_PICKER, function () { _this.useButton(KPaint.Events.ID.INPUT_TOOL_COLOR_PICKER); });
            }
            BrushButtonArea.prototype.useButton = function (event) {
                var buttons = this._buttons;
                var button;
                for (var i = 0, n = buttons.length; i < n; i++) {
                    button = buttons[i];
                    if (button.emit === event) {
                        button.stayDown = true;
                    }
                    else {
                        button.stayDown = false;
                    }
                }
            };
            // expanding upon the default handler for custom functionality
            BrushButtonArea.prototype.clickHandler = function (pt) {
                if (!_super.prototype.clickHandler.call(this, pt) || this.isActive) {
                    return false;
                }
                var buttons = this._buttons;
                var button;
                for (var i = 0, n = buttons.length; i < n; i++) {
                    button = buttons[i];
                    if (button.isActive) {
                        this.useButton(button.emit);
                    }
                }
                return true;
            };
            return BrushButtonArea;
        })(UI.ClickableContainer2);
        UI.BrushButtonArea = BrushButtonArea;
    })(UI = KPaint.UI || (KPaint.UI = {}));
})(KPaint || (KPaint = {}));
var KPaint;
(function (KPaint) {
    var UI;
    (function (UI) {
        var BrushSettings = (function (_super) {
            __extends(BrushSettings, _super);
            function BrushSettings(x, y, width, height) {
                _super.call(this, x, y, width, height);
                x = 0;
                y = 0;
                var elementHeight = height / 5;
                // brush size slider
                this._brushSize = new UI.Slider(x, y, width, elementHeight, width * 0.2, width * 0.2, 'size', KPaint.Events.ID.INPUT_BRUSH_SIZE, KPaint.Events.ID.BRUSH_SIZE);
                var bSize = this._brushSize;
                bSize.max = 200;
                // brush softness
                y += elementHeight;
                this._brushSizeMin = new UI.Slider(x, y, width, elementHeight, width * 0.2, width * 0.2, 'min', KPaint.Events.ID.INPUT_BRUSH_SIZE_MIN, KPaint.Events.ID.BRUSH_SIZE_MIN);
                var bSizeMin = this._brushSizeMin;
                // brush softness
                y += elementHeight;
                this._brushSoft = new UI.Slider(x, y, width, elementHeight, width * 0.2, width * 0.2, 'soft', KPaint.Events.ID.INPUT_BRUSH_SOFTNESS, KPaint.Events.ID.BRUSH_SOFTNESS);
                var bSoft = this._brushSoft;
                // brush softness
                y += elementHeight;
                this._brushAlpha = new UI.Slider(x, y, width, elementHeight, width * 0.2, width * 0.2, 'flow', KPaint.Events.ID.INPUT_BRUSH_FLOW, KPaint.Events.ID.BRUSH_FLOW);
                var bAlph = this._brushAlpha;
                // brush flow slider
                y += elementHeight;
                this._brushSpacing = new UI.Slider(x, y, width, elementHeight, width * 0.2, width * 0.2, 'spac', KPaint.Events.ID.INPUT_BRUSH_SPACING, KPaint.Events.ID.BRUSH_SPACING);
                var bSpace = this._brushSpacing;
                // add to container
                this.addChild(bSize);
                this.addChild(bSizeMin);
                this.addChild(bSoft);
                this.addChild(bAlph);
                this.addChild(bSpace);
                // add clicks
                this.addClickListener(bSize);
                this.addClickListener(bSizeMin);
                this.addClickListener(bSoft);
                this.addClickListener(bAlph);
                this.addClickListener(bSpace);
            }
            return BrushSettings;
        })(UI.ClickableContainer2);
        UI.BrushSettings = BrushSettings;
    })(UI = KPaint.UI || (KPaint.UI = {}));
})(KPaint || (KPaint = {}));
var KPaint;
(function (KPaint) {
    var UI;
    (function (UI) {
        var ColorDisplay = (function (_super) {
            __extends(ColorDisplay, _super);
            function ColorDisplay(x, y, width, height) {
                var _this = this;
                _super.call(this, x, y, width, height);
                this._primaryColorHsv = new KPaint.Vec3(0, 0, 0);
                this._secondaryColorHsv = new KPaint.Vec3(0, 0, 1);
                this._colorFilterPrimary = new KPaint.Sha.ColorFilter(0, 0, 0);
                this._colorFilterSecondary = new KPaint.Sha.ColorFilter(1, 1, 1);
                this._isSwapped = false;
                this.swap = function () {
                    // swap colors
                    var hsv = _this._secondaryColorHsv;
                    _this._secondaryColorHsv = _this._primaryColorHsv;
                    _this._primaryColorHsv = hsv;
                    _this.updateColorPrimary();
                    _this.updateColorSecondary();
                    var evEmit = KPaint.Events.emit;
                    var evId = KPaint.Events.ID;
                    evEmit(evId.INPUT_COLOR_CHANGE_H, hsv.h);
                    evEmit(evId.INPUT_COLOR_CHANGE_S, hsv.s);
                    evEmit(evId.INPUT_COLOR_CHANGE_V, hsv.v);
                };
                this.updateHue = function (value) {
                    _this._primaryColorHsv.x = value;
                    _this.updateColorPrimary();
                };
                this.updateSaturation = function (value) {
                    _this._primaryColorHsv.y = value;
                    _this.updateColorPrimary();
                };
                this.updateValue = function (value) {
                    _this._primaryColorHsv.z = value;
                    _this.updateColorPrimary();
                };
                x = 0;
                y = 0;
                var cSize = height * 0.67;
                var graph = new PIXI.Graphics()
                    .beginFill(0xff0000)
                    .drawRect(0, 0, cSize, cSize)
                    .endFill();
                var rt = new PIXI.RenderTexture(KPaint.renderer, cSize, cSize);
                rt.render(graph);
                graph.destroy(true);
                var primarySprite = new PIXI.Sprite(rt);
                primarySprite.x = x;
                primarySprite.y = y;
                x += height * 0.33;
                y += height * 0.33;
                var secondarySprite = new PIXI.Sprite(rt);
                secondarySprite.x = x;
                secondarySprite.y = y;
                // add color filters
                var cfPrimary = this._colorFilterPrimary;
                var cfSecondary = this._colorFilterSecondary;
                primarySprite.filters = [cfPrimary];
                secondarySprite.filters = [cfSecondary];
                this._primarySprite = primarySprite;
                this._secondarySprite = secondarySprite;
                // remove gamma correction on the color filters
                cfPrimary.gamma = 1;
                cfSecondary.gamma = 1;
                // set colors
                this.updateColorPrimary();
                this.updateColorSecondary();
                // add to container
                this.addChildPIXI(secondarySprite);
                this.addChildPIXI(primarySprite);
                // events
                KPaint.Events.register(KPaint.Events.ID.BRUSH_COLOR_H, function (v) { return _this.updateHue(v[0]); });
                KPaint.Events.register(KPaint.Events.ID.BRUSH_COLOR_S, function (v) { return _this.updateSaturation(v[0]); });
                KPaint.Events.register(KPaint.Events.ID.BRUSH_COLOR_V, function (v) { return _this.updateValue(v[0]); });
                KPaint.Events.register(KPaint.Events.ID.INPUT_COLOR_SWAP, this.swap);
            }
            ColorDisplay.prototype.clickHandler = function (pt) {
                if (!this.isActive) {
                    KPaint.Events.emit(KPaint.Events.ID.INPUT_COLOR_SWAP);
                }
            };
            ColorDisplay.prototype.releaseHandler = function () { };
            ColorDisplay.prototype.updateColorPrimary = function () {
                this._container.cacheAsBitmap = false;
                var rgb = this._localVec;
                KPaint.hsvToRgb(this._primaryColorHsv, rgb);
                var colorFilter = this._colorFilterPrimary;
                colorFilter.r = rgb.r;
                colorFilter.g = rgb.g;
                colorFilter.b = rgb.b;
                this._container.cacheAsBitmap = true;
            };
            ColorDisplay.prototype.updateColorSecondary = function () {
                this._container.cacheAsBitmap = false;
                var rgb = this._localVec;
                KPaint.hsvToRgb(this._secondaryColorHsv, rgb);
                var colorFilter = this._colorFilterSecondary;
                colorFilter.r = rgb.r;
                colorFilter.g = rgb.g;
                colorFilter.b = rgb.b;
                this._container.cacheAsBitmap = true;
            };
            return ColorDisplay;
        })(UI.Clickable2);
        UI.ColorDisplay = ColorDisplay;
    })(UI = KPaint.UI || (KPaint.UI = {}));
})(KPaint || (KPaint = {}));
var KPaint;
(function (KPaint) {
    var UI;
    (function (UI) {
        var HsvSliderArea = (function (_super) {
            __extends(HsvSliderArea, _super);
            function HsvSliderArea(x, y, width, height, h, s, v) {
                _super.call(this, x, y, width, height);
                x = 0;
                y = 0;
                var sliderX = x;
                var sliderY = y;
                var sliderWidth = width;
                var sliderHeight = height / 3;
                var paddingLeft = width * 0.15;
                var paddingRight = width * 0.15;
                // create the sliderS
                this._hSlider = new UI.HueSlider(sliderX, sliderY, sliderWidth, sliderHeight, paddingLeft, paddingRight);
                sliderY += sliderHeight;
                this._sSlider = new UI.SaturationSlider(sliderX, sliderY, sliderWidth, sliderHeight, paddingLeft, paddingRight, h, v);
                sliderY += sliderHeight;
                this._vSlider = new UI.ValueSlider(sliderX, sliderY, sliderWidth, sliderHeight, paddingLeft, paddingRight, h, s);
                // add to container
                this.addChild(this._hSlider);
                this.addChild(this._sSlider);
                this.addChild(this._vSlider);
                // add children to Clickablecontainer
                this.addClickListener(this._hSlider);
                this.addClickListener(this._sSlider);
                this.addClickListener(this._vSlider);
            }
            return HsvSliderArea;
        })(UI.ClickableContainer2);
        UI.HsvSliderArea = HsvSliderArea;
    })(UI = KPaint.UI || (KPaint.UI = {}));
})(KPaint || (KPaint = {}));
var KPaint;
(function (KPaint) {
    var UI;
    (function (UI) {
        var SideBarLeft = (function (_super) {
            __extends(SideBarLeft, _super);
            function SideBarLeft(x, y, width, height, bgColor) {
                _super.call(this, x, y, width, height);
                // misc
                this._background = new PIXI.Graphics();
                this._bgColor = 0;
                x = 0;
                y = 0;
                var spaceBetweenElements = 8;
                // background
                this._bgColor = bgColor;
                this.generateBackground();
                // sub elements
                var elHeight = width;
                this._wheel = new UI.ColorWheel(x, y, width, width);
                var w = this._wheel;
                w.value = 0;
                // Hsv sliders
                y += elHeight;
                y += spaceBetweenElements;
                elHeight = width * 0.45;
                this._hsvSliderArea = new UI.HsvSliderArea(x, y, width, elHeight, w.hue, w.saturation, w.value);
                // TODO: Brush/Erase buttons
                y += elHeight;
                y += spaceBetweenElements;
                elHeight = width * 0.4;
                this._buttonArea = new UI.BrushButtonArea(x, y, width * 0.6, elHeight);
                // color display
                this._colorDisplay = new UI.ColorDisplay(x + width * 0.6 + elHeight / 4, y + elHeight / 4, elHeight, elHeight / 2);
                // brush sliders
                y += elHeight;
                y += spaceBetweenElements;
                elHeight = width * 0.9;
                this._brushSettings = new UI.BrushSettings(x, y, width, elHeight);
                y += elHeight;
                y += spaceBetweenElements;
                elHeight = width * 0.15;
                this._downloadButton = new UI.Button(x + spaceBetweenElements, y, elHeight, elHeight, bgColor + 0x111111, 'DL', KPaint.Events.ID.INPUT_DOWNLOAD_IMAGE);
                // add children to container
                this.addChildPIXI(this._background);
                this.addChild(w);
                this.addChild(this._hsvSliderArea);
                this.addChild(this._buttonArea);
                this.addChild(this._colorDisplay);
                this.addChild(this._brushSettings);
                this.addChild(this._downloadButton);
                // add click listeners to children
                this.addClickListener(w);
                this.addClickListener(this._hsvSliderArea);
                this.addClickListener(this._buttonArea);
                this.addClickListener(this._colorDisplay);
                this.addClickListener(this._brushSettings);
                this.addClickListener(this._downloadButton);
            }
            SideBarLeft.prototype.resize = function (width, height) {
                var bigger = height > this.height;
                this.height = height;
                if (bigger === true) {
                    this.generateBackground();
                }
            };
            SideBarLeft.prototype.generateBackground = function () {
                this._background
                    .clear()
                    .beginFill(this._bgColor)
                    .drawRect(this.x, this.y, this.width, this.height)
                    .endFill();
            };
            return SideBarLeft;
        })(UI.ClickableContainer2);
        UI.SideBarLeft = SideBarLeft;
    })(UI = KPaint.UI || (KPaint.UI = {}));
})(KPaint || (KPaint = {}));
/*
    Displays a miniature version of the canvas and can be used to move the view frame around by clicking on it

    Generally these only update after the stroke is finished for performance reasons.
*/
var KPaint;
(function (KPaint) {
    var UI;
    (function (UI) {
        var CanvasNavigator = (function (_super) {
            __extends(CanvasNavigator, _super);
            function CanvasNavigator(x, y, width, height, padding) {
                var _this = this;
                _super.call(this, x, y, width, height);
                this._canvasArea = new KPaint.Vec4();
                this._location = new KPaint.Vec2();
                this.update = function () {
                    if (KPaint.paintingContext.isMidStroke === true) {
                        return;
                    }
                    _this._canvasSprite.cacheAsBitmap = false;
                    _this._rt.clear();
                    _this._rt.render(KPaint.paintingContext.container);
                    _this._canvasSprite.cacheAsBitmap = true;
                    KPaint.renderCoordinator.requestRender();
                };
                var ctxSize = KPaint.paintingContext.size;
                this._rt = new PIXI.RenderTexture(KPaint.renderer, ctxSize.x, ctxSize.y);
                var cSpr = new PIXI.Sprite(this._rt);
                // anchor to center
                cSpr.anchor.x = 0.5;
                cSpr.anchor.y = 0.5;
                cSpr.x = x + width / 2;
                cSpr.y = y + height / 2;
                // set area depending on resolution
                var res = ctxSize.x / ctxSize.y;
                if (res < 1) {
                    cSpr.width = width * res - padding * 2;
                    cSpr.height = height - padding * 2;
                }
                else {
                    cSpr.width = width - padding * 2;
                    cSpr.height = height * (1 / res) - padding * 2;
                }
                this._canvasArea.xyzw(cSpr.x - cSpr.width / 2, cSpr.y - cSpr.height / 2, cSpr.width, cSpr.height);
                // add to container
                this._canvasSprite = cSpr;
                this.addChildPIXI(cSpr);
                this.update();
                // events
                KPaint.Events.register(KPaint.Events.ID.CANVAS_RENDERED, this.update);
            }
            CanvasNavigator.prototype.clickHandler = function (pointer) {
                var ca = this._canvasArea;
                var x = (pointer.x - ca.x) / ca.width;
                var y = (pointer.y - ca.y) / ca.height;
                var clamp = KPaint.utils.clamp;
                x = clamp(x, 0, 1);
                y = clamp(y, 0, 1);
                this._location.xy(x, y);
                KPaint.Events.emit(KPaint.Events.ID.INPUT_MOVE_CANVAS, this._location);
            };
            CanvasNavigator.prototype.releaseHandler = function () { };
            return CanvasNavigator;
        })(UI.Clickable2);
        UI.CanvasNavigator = CanvasNavigator;
    })(UI = KPaint.UI || (KPaint.UI = {}));
})(KPaint || (KPaint = {}));
var KPaint;
(function (KPaint) {
    var UI;
    (function (UI) {
        var DisplayOptions = (function (_super) {
            __extends(DisplayOptions, _super);
            function DisplayOptions(x, y, width, height) {
                _super.call(this, x, y, width, height);
                x = 0;
                y = 0;
                var buttonSize = height / 3;
                var elWidth = width - buttonSize - 4;
                this._zoomSlider = new UI.Slider(x, y, elWidth, height, width * 0.25, width * 0.15, 'Zoom', KPaint.Events.ID.INPUT_ZOOM_CANVAS, KPaint.Events.ID.ZOOM_SET_ZOOM);
                this._zoomSlider.max = 5;
                x += elWidth;
                this._buttonZoomPlus = new UI.Button(x, y, buttonSize, buttonSize, 0x999999, '+', KPaint.Events.ID.INPUT_ZOOM_CANVAS_ADD);
                y += buttonSize;
                this._buttonZoomMinus = new UI.Button(x, y, buttonSize, buttonSize, 0x999999, '-', KPaint.Events.ID.INPUT_ZOOM_CANVAS_SUBTRACT);
                y += buttonSize;
                this._buttonZoomReset = new UI.Button(x, y, buttonSize, buttonSize, 0x999999, 'R', KPaint.Events.ID.INPUT_ZOOM_CANVAS_RESET);
                // add to container
                this.addChild(this._zoomSlider);
                this.addChild(this._buttonZoomPlus);
                this.addChild(this._buttonZoomMinus);
                this.addChild(this._buttonZoomReset);
                // add clickables
                this.addClickListener(this._zoomSlider);
                this.addClickListener(this._buttonZoomPlus);
                this.addClickListener(this._buttonZoomMinus);
                this.addClickListener(this._buttonZoomReset);
            }
            return DisplayOptions;
        })(UI.ClickableContainer2);
        UI.DisplayOptions = DisplayOptions;
    })(UI = KPaint.UI || (KPaint.UI = {}));
})(KPaint || (KPaint = {}));
var KPaint;
(function (KPaint) {
    var UI;
    (function (UI) {
        /*
            Container class for the buttons that define operations on layers.
            
            Buttons:
                New:	Creates a new layer above the currently selected one.
                Merge:	Merges the currently selected layer down.
                Clear:	Clears the currently selected layer.
                Delete:	Deletes the currently selected layer.
                Lock:	Locks the pixels on the currently selected layer.
    
            Usually there would be a button for merging every layer, but that's not completely necessary, so
            I'll only add that if I have time.
        */
        var LayerButtonArea = (function (_super) {
            __extends(LayerButtonArea, _super);
            function LayerButtonArea(x, y, width, height) {
                _super.call(this, x, y, width, height);
                this.container = new PIXI.Container();
                x = 0;
                y = 0;
                var spacePerButton = width / 5;
                var sliderHeight = height - spacePerButton;
                var bPad = 4;
                var buttonSize = spacePerButton - bPad * 2;
                this._buttonNewLayer = new UI.Button(x + bPad, y + bPad, buttonSize, buttonSize, 0x999999, 'New', KPaint.Events.ID.INPUT_LAYER_NEW);
                x += spacePerButton;
                this._buttonMergeLayer = new UI.Button(x + bPad, y + bPad, buttonSize, buttonSize, 0x999999, 'Merge', KPaint.Events.ID.INPUT_LAYER_MERGE);
                x += spacePerButton;
                this._buttonClearLayer = new UI.Button(x + bPad, y + bPad, buttonSize, buttonSize, 0x999999, 'Clear', KPaint.Events.ID.INPUT_LAYER_CLEAR);
                x += spacePerButton;
                this._buttonDeleteLayer = new UI.Button(x + bPad, y + bPad, buttonSize, buttonSize, 0x999999, 'Delete', KPaint.Events.ID.INPUT_LAYER_DELETE);
                x += spacePerButton;
                this._buttonLockLayer = new UI.TwoStateButton(x + bPad, y + bPad, buttonSize, buttonSize, 0x999999, 'Lock', KPaint.Events.ID.INPUT_LAYER_LOCK_PIXELS);
                x = 0;
                y += spacePerButton;
                this._layerAlpha = new UI.Slider(x, y, width, sliderHeight, width * 0.25, width * 0.15, 'Alpha', KPaint.Events.ID.INPUT_LAYER_ALPHA_CHANGED, KPaint.Events.ID.LAYER_ALPHA);
                // add to container
                this.addChild(this._buttonNewLayer);
                this.addChild(this._buttonMergeLayer);
                this.addChild(this._buttonClearLayer);
                this.addChild(this._buttonDeleteLayer);
                this.addChild(this._buttonLockLayer);
                this.addChild(this._layerAlpha);
                // add listener
                this.addClickListener(this._buttonNewLayer);
                this.addClickListener(this._buttonMergeLayer);
                this.addClickListener(this._buttonClearLayer);
                this.addClickListener(this._buttonDeleteLayer);
                this.addClickListener(this._buttonLockLayer);
                this.addClickListener(this._layerAlpha);
            }
            return LayerButtonArea;
        })(UI.ClickableContainer2);
        UI.LayerButtonArea = LayerButtonArea;
    })(UI = KPaint.UI || (KPaint.UI = {}));
})(KPaint || (KPaint = {}));
var KPaint;
(function (KPaint) {
    var UI;
    (function (UI) {
        var LayerDisplay = (function (_super) {
            __extends(LayerDisplay, _super);
            function LayerDisplay(x, y, width, height) {
                _super.call(this, x, y, width, height);
                this.layer = null;
                this._bg = new PIXI.Graphics();
                this._name = new PIXI.Text('default', { font: '16px arial', strokeThickness: 0.5 });
                this._alpha = new PIXI.Text('default', { font: '12px arial' });
                this.addChildPIXI(this._bg);
                this.addChildPIXI(this._name);
                this.addChildPIXI(this._alpha);
            }
            Object.defineProperty(LayerDisplay.prototype, "alpha", {
                get: function () {
                    return this.layer.alpha;
                },
                set: function (value) {
                    this.layer.alpha = value;
                    this._alpha.text = [(value * 100).toFixed(0).toString(), '%'].join('');
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(LayerDisplay.prototype, "layerId", {
                get: function () {
                    return this.layer.id;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(LayerDisplay.prototype, "name", {
                get: function () {
                    return this._name.text;
                },
                enumerable: true,
                configurable: true
            });
            LayerDisplay.prototype.initialize = function (x, y, width, height, layer) {
                this.layer = layer;
                this.x = x;
                this.y = y;
                this.width = width;
                this.height = height;
                var name = this._name;
                name.text = layer.name;
                name.x = width / 3;
                name.y = height / 3;
                name.anchor.y = 0.5;
                this.alpha = layer.container.alpha;
                var alpha = this._alpha;
                alpha.x = width / 3;
                alpha.y = height * 2 / 3;
                alpha.anchor.y = 0.5;
                this.generateGraphics(6);
            };
            LayerDisplay.prototype.select = function () {
                KPaint.Events.emit(KPaint.Events.ID.INPUT_LAYER_SELECTED, this.layer);
            };
            LayerDisplay.prototype.clickHandler = function () {
                this.select();
            };
            LayerDisplay.prototype.releaseHandler = function () { };
            LayerDisplay.prototype.generateGraphics = function (padding) {
                var width = this.width, height = this.height;
                this._bg
                    .clear()
                    .lineStyle(3, 0x000000, 1)
                    .moveTo(padding, padding) // pt 0
                    .lineTo(width - padding, padding) // pt 1
                    .lineTo(width - padding, height - padding) // pt 3
                    .lineTo(padding, height - padding) // pt 4
                    .lineTo(padding, padding) // pt 0
                    .endFill();
            };
            return LayerDisplay;
        })(UI.Clickable2);
        UI.LayerDisplay = LayerDisplay;
    })(UI = KPaint.UI || (KPaint.UI = {}));
})(KPaint || (KPaint = {}));
var KPaint;
(function (KPaint) {
    var UI;
    (function (UI) {
        // object pool
        var LayerDisplayPool = (function (_super) {
            __extends(LayerDisplayPool, _super);
            function LayerDisplayPool() {
                _super.call(this, 'LayerDisplay', 4);
                this.allocate();
            }
            LayerDisplayPool.prototype.createOne = function () {
                return new UI.LayerDisplay(0, 0, 0, 0);
            };
            return LayerDisplayPool;
        })(KPaint.IPool);
        UI.LayerDisplayPool = LayerDisplayPool;
        var LayerDisplayContainer = (function (_super) {
            __extends(LayerDisplayContainer, _super);
            function LayerDisplayContainer(x, y, width, height) {
                var _this = this;
                _super.call(this, x, y, width, height);
                this._pool = new LayerDisplayPool();
                this._displays = [];
                this._ldPositions = [];
                this._selectedGraphic = new PIXI.Graphics();
                this._selectedIdx = 0;
                this.addLayerDisplay = function (arr) {
                    // create
                    var ld = _this._pool.get();
                    ld.initialize(0, _this._selectedIdx * _this._ldHeight, _this.width, _this._ldHeight, arr[0]);
                    var idx = arr[1];
                    // add to container, set listener
                    _this.addChildAt(ld, idx + 1); // idx 0 is reserved for bg
                    _this._displays.splice(idx, 0, ld);
                    _this.addClickListener(ld);
                    // Reposition display objects to ensure they stay separate.
                    _this.repositionDisplays();
                };
                this.layerAlphaChange = function (value) {
                    var disp = _this._displays[_this._selectedIdx];
                    disp.alpha = value[0];
                };
                this.layerSelected = function (layer) {
                    var d = _this._displays;
                    for (var i = 0, n = d.length; i < n; i++) {
                        if (d[i].layerId === layer[0].id) {
                            _this._selectedIdx = i;
                            _this._selectedGraphic.y = d[i].y;
                            return;
                        }
                    }
                };
                this.removeLayer = function (layer) {
                    var displays = _this._displays;
                    for (var i = 0, n = displays.length; i < n; i++) {
                        if (displays[i].layerId === layer[0].id) {
                            var removed = displays.splice(i, 1)[0];
                            _this._pool.recycle(removed);
                            _this.removeChild(removed);
                            _this.removeClickable(removed);
                            break;
                        }
                    }
                    if (_this._selectedIdx !== 0) {
                        _this._selectedIdx--;
                    }
                    _this.repositionDisplays();
                };
                /*
                    Expects:
                        arr[0]: Layer
                        arr[0]: number (index)
                */
                this.moveLayer = function (arr) {
                    var displays = _this._displays;
                    for (var i = 0, n = displays.length; i < n; i++) {
                        if (displays[i].layerId === arr[0].id) {
                            _this._displays.move(i, arr[1]);
                        }
                    }
                    _this.repositionDisplays();
                };
                this._ldHeight = Math.round(height / (height / 60));
                this._selectedGraphic
                    .beginFill(0xaaaaaa)
                    .drawRect(0, 0, width, this._ldHeight)
                    .endFill();
                // add children
                this.addChildPIXI(this._selectedGraphic);
                // register events
                KPaint.Events.register(KPaint.Events.ID.LAYER_ALPHA, this.layerAlphaChange);
                KPaint.Events.register(KPaint.Events.ID.LAYER_SELECTED, this.layerSelected);
                KPaint.Events.register(KPaint.Events.ID.LAYER_CREATED, this.addLayerDisplay);
                KPaint.Events.register(KPaint.Events.ID.LAYER_DELETED, this.removeLayer);
                KPaint.Events.register(KPaint.Events.ID.LAYER_MOVE, this.moveLayer);
            }
            LayerDisplayContainer.prototype.repositionDisplays = function () {
                var d = this._displays;
                var h = this._ldHeight;
                for (var i = 0, n = d.length; i < n; i++) {
                    d[i].y = (n - i - 1) * h;
                }
                this._selectedGraphic.y = d[this._selectedIdx].y;
            };
            LayerDisplayContainer.prototype.clickHandler = function (pointer) {
                // on pointer down
                if (this.isActive === false) {
                    _super.prototype.clickHandler.call(this, pointer);
                    KPaint.Events.emit(KPaint.Events.ID.INPUT_LAYER_MOVE_START);
                    return;
                }
                // update local
                var local = this.vecToLocal(pointer);
                // on drag
                var selectedIdx = this._selectedIdx;
                var displays = this._displays;
                // go down
                if (selectedIdx > 0) {
                    if (local.y > (displays[selectedIdx - 1].y)) {
                        KPaint.Events.emit(KPaint.Events.ID.INPUT_LAYER_MOVE_DOWN, this._selectedIdx);
                    }
                }
                // go up
                if (selectedIdx < displays.length - 1) {
                    if (local.y < (displays[selectedIdx + 1].y + this._ldHeight)) {
                        KPaint.Events.emit(KPaint.Events.ID.INPUT_LAYER_MOVE_UP, this._selectedIdx);
                    }
                }
            };
            LayerDisplayContainer.prototype.releaseHandler = function () {
                _super.prototype.releaseHandler.call(this);
                KPaint.Events.emit(KPaint.Events.ID.INPUT_LAYER_MOVE_END);
            };
            return LayerDisplayContainer;
        })(UI.ClickableContainer2);
        UI.LayerDisplayContainer = LayerDisplayContainer;
    })(UI = KPaint.UI || (KPaint.UI = {}));
})(KPaint || (KPaint = {}));
var KPaint;
(function (KPaint) {
    var UI;
    (function (UI) {
        var SideBarRight = (function (_super) {
            __extends(SideBarRight, _super);
            function SideBarRight(x, y, width, height, bgColor) {
                _super.call(this, x, y, width, height);
                this._background = new PIXI.Graphics();
                this._bgColor = 0;
                x = 0;
                y = 0;
                // navigator
                var elHeight = width;
                this._navigator = new UI.CanvasNavigator(x, y, width, elHeight, 10);
                // display options
                y += elHeight;
                elHeight = width * 0.25;
                this._displayOptions = new UI.DisplayOptions(x, y, width, elHeight);
                // layer buttons
                y += elHeight * 1.2;
                elHeight = width * 0.35;
                this._layerButtons = new UI.LayerButtonArea(x, y, width, elHeight);
                // layers
                y += elHeight;
                height -= y;
                this._layerViews = new UI.LayerDisplayContainer(0, y, width, height);
                // generate bg
                this._bgColor = bgColor;
                this.generateBackground();
                this.addChildPIXI(this._background);
                this.addChild(this._displayOptions);
                this.addChild(this._navigator);
                this.addChild(this._layerButtons);
                this.addChild(this._layerViews);
                this.addClickListener(this._navigator);
                this.addClickListener(this._displayOptions);
                this.addClickListener(this._layerButtons);
                this.addClickListener(this._layerViews);
            }
            SideBarRight.prototype.resize = function (width, height) {
                var bigger = height > this.height;
                this.height = height;
                if (bigger)
                    this.generateBackground();
            };
            SideBarRight.prototype.generateBackground = function () {
                this._background
                    .clear()
                    .beginFill(this._bgColor)
                    .drawRect(0, 0, this.width, this.height)
                    .endFill();
            };
            return SideBarRight;
        })(UI.ClickableContainer2);
        UI.SideBarRight = SideBarRight;
    })(UI = KPaint.UI || (KPaint.UI = {}));
})(KPaint || (KPaint = {}));
var KPaint;
(function (KPaint) {
    var UI;
    (function (UI) {
        /*
            Cursor that is rendered on the canvas as a PIXI.Sprite object.
            
            Can be used to override the default cursor with a PIXI.Sprite based one.
    
            I can't figure out how to set the normal cursor's image without using a
            url to an image or cursor object. I want to set the cursor to be the outline
            of the current brush.
        */
        var CCursor = (function () {
            function CCursor(diameter) {
                var _this = this;
                this.sprite = new PIXI.Sprite();
                this._zoom = 1;
                this._previewMode = false;
                this._previewOffset = new KPaint.Vec2();
                this.move = function (point) {
                    _this.x = point[0].x;
                    _this.y = point[0].y;
                };
                this.changeSize = function (size) {
                    _this._diameter = size[0];
                    _this.update();
                };
                this.changeZoom = function (value) {
                    _this._zoom = value[0];
                    _this.update();
                };
                this.sizePreview = function (pointer) {
                    _this._previewMode = true;
                    _this._previewOffset.xy(pointer[0].x, pointer[0].y);
                };
                this.endSizePreview = function () {
                    if (_this._previewMode === true) {
                        _this._previewMode = false;
                        _this.x -= _this._previewOffset.x;
                        _this.y -= _this._previewOffset.y;
                        _this._previewOffset.xy(0, 0);
                    }
                };
                this._diameter = diameter;
                this._rt = new PIXI.RenderTexture(KPaint.renderer, diameter, diameter);
                var sprite = new PIXI.Sprite(this._rt);
                sprite.anchor.x = 0.5;
                sprite.anchor.y = 0.5;
                this.sprite = sprite;
                this._shader = new KPaint.Sha.BrushOutlineShader(diameter);
                sprite.shader = this._shader;
                KPaint.Events.register(KPaint.Events.ID.BRUSH_SIZE, this.changeSize);
                KPaint.Events.register(KPaint.Events.ID.ZOOM_SET_ZOOM, this.changeZoom);
                KPaint.Events.register(KPaint.Events.ID.POINTER_MOVE, this.move);
                KPaint.Events.register(KPaint.Events.ID.INPUT_DRAG_BRUSH_SIZE_PREVIEW, this.sizePreview);
                KPaint.Events.register(KPaint.Events.ID.POINTER_UP, this.endSizePreview);
            }
            Object.defineProperty(CCursor.prototype, "x", {
                get: function () {
                    return this.sprite.x;
                },
                set: function (x) {
                    if (this._previewMode === true) {
                        x += this._previewOffset.x;
                    }
                    this.sprite.x = x;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(CCursor.prototype, "y", {
                get: function () {
                    return this.sprite.y;
                },
                set: function (y) {
                    if (this._previewMode === true) {
                        y += this._previewOffset.y;
                    }
                    this.sprite.y = y;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(CCursor.prototype, "visible", {
                get: function () {
                    return this.sprite.visible;
                },
                set: function (val) {
                    this.sprite.visible = val;
                },
                enumerable: true,
                configurable: true
            });
            CCursor.prototype.update = function () {
                // update rendertexture
                var di = this._diameter * this._zoom;
                // resize the texture to reflect the size of the cursor
                // I want to avoid rescaling since that looks awful in PIXI
                this._rt.resize(di, di, true);
                // update shader
                this._shader.diameter = di;
            };
            return CCursor;
        })();
        UI.CCursor = CCursor;
    })(UI = KPaint.UI || (KPaint.UI = {}));
})(KPaint || (KPaint = {}));
var KPaint;
(function (KPaint) {
    var UI;
    (function (UI) {
        var CanvasDisplay = (function (_super) {
            __extends(CanvasDisplay, _super);
            function CanvasDisplay(x, y, width, height, context) {
                var _this = this;
                _super.call(this, x, y);
                this.offCenter = new KPaint.Vec2();
                this._area = new KPaint.Vec4();
                this._clickPos = new KPaint.Vec2(null, null);
                this._brushStrokePoints = [];
                /*
                    Set scale such that the whole canvas is on screen.
                */
                this.scaleToFit = function () {
                    var area = _this._area;
                    var ctxSize = _this._context.size;
                    // pad the size so that there is some space between the canvas and the UI
                    var xScale = area.width / (ctxSize.x + 40), yScale = area.height / (ctxSize.y + 40);
                    _this.scale = Math.min(xScale, yScale);
                    _this.offCenter.xy(0.5, 0.5);
                    _this.updatePosition();
                };
                /*
                    Move to a normalized in the range [0, 1].
        
                    Used by the navigator.
                */
                this.moveToLocation = function (nLoc) {
                    var ctxSize = _this._context.size;
                    var scale = _this.scale;
                    var x = (1 - nLoc[0].x) * ctxSize.x, y = (1 - nLoc[0].y) * ctxSize.y;
                    x -= (ctxSize.x) / 2;
                    y -= (ctxSize.y) / 2;
                    _this.offCenter.xy(x, y);
                    // update
                    _this.updatePosition();
                };
                /*
                    Continue moving the canvas
                */
                this.moveCanvas = function (pointer) {
                    var ctxSize = _this._context.size;
                    var offSet = _this.offCenter;
                    var scale = _this.scale;
                    var clickPos = _this._clickPos;
                    var area = _this._area;
                    var clamp = KPaint.utils.clamp;
                    var local = _this.vecToLocal(pointer[0]);
                    if (clickPos.x === null) {
                        clickPos.xy(local.x, local.y);
                    }
                    // add to offset
                    offSet.x += (local.x - clickPos.x) / scale;
                    offSet.y += (local.y - clickPos.y) / scale;
                    // save click position for next time this function is called
                    clickPos.xy(local.x, local.y);
                    // clamp the offset so it doesn't go too far offscreenz
                    var xMax = area.width / 2 + (ctxSize.x * scale) / 4, yMax = area.height / 2 + (ctxSize.y * scale) / 4;
                    offSet.x = clamp(offSet.x, -xMax, xMax);
                    offSet.y = clamp(offSet.y, -yMax, yMax);
                    // update
                    _this.updatePosition();
                };
                /*
                    Stop moving the canvas
                */
                this.moveCanvasEnd = function () {
                    _this._clickPos.xy(null, null);
                };
                this._initialScale = 1;
                /*
                    Continue zooming the canvas
                */
                this.zoomCanvas = function (pointer) {
                    var local = _this.vecToLocal(pointer[0]), clickPos = _this._clickPos;
                    if (clickPos.x === null) {
                        clickPos.xy(local.x, local.y);
                        _this._initialScale = _this.scale;
                    }
                    // do some weird stuff
                    var d = (local.x - clickPos.x) / _this._area.width;
                    d *= 5;
                    _this.scale = _this._initialScale + d;
                    // update
                    _this.updatePosition();
                };
                this.zoomCanvasEnd = function () {
                    _this._clickPos.xy(null, null);
                };
                /*
                    Increase brush size through ctrl-alt-dragging
                */
                this.brushSize = function (pointer) {
                    var local = _this.vecToLocal(pointer[0]);
                    var clickPos = _this._clickPos;
                    if (clickPos.x === null) {
                        clickPos.xy(local.x, local.y);
                    }
                    // output the size event
                    var evEmit = KPaint.Events.emit;
                    var evId = KPaint.Events.ID;
                    evEmit(evId.INPUT_BRUSH_SIZE, Math.abs(local.x - clickPos.x));
                    // get delta vector for the cursor preview
                    local.x = 0;
                    local.y = (clickPos.y - local.y);
                    // output the preview cursor event
                    evEmit(evId.INPUT_DRAG_BRUSH_SIZE_PREVIEW, local);
                };
                this.brushSizeEnd = function (pointer) {
                    _this._clickPos.xy(null, null);
                };
                /*
                    Brush strokes
                */
                this.brushStrokeStart = function (pointer) {
                    var local = _this.toContextCoord(pointer[0]);
                    _this._context.startStroke(local);
                };
                this.brushStroke = function (pointer) {
                    if (_this._context.isMidStroke === true) {
                        var local = _this.toContextCoord(pointer[0]);
                        _this._brushStrokePoints.push(KPaint.op.vec3.clone(local));
                    }
                };
                this.brushStrokePointersDispatched = function () {
                    if (_this._brushStrokePoints.length > 0) {
                        var pts = _this._brushStrokePoints;
                        _this._context.addPoint(pts);
                        var vec3op = KPaint.op.vec3;
                        for (var i = 0, n = pts.length; i < n; i++) {
                            vec3op.recycle(pts[i]);
                        }
                        pts.length = 0;
                    }
                };
                this.brushStrokeEnd = function () {
                    if (_this._brushStrokePoints.length > 0) {
                        _this.brushStrokePointersDispatched();
                    }
                    if (_this._context.isMidStroke === true) {
                        _this._context.endStroke();
                        KPaint.renderCoordinator.requestRender();
                    }
                };
                this.pickColor = function (pointer) {
                    var local = _this.vecToLocal(pointer[0]);
                    // convert to canvas coordinate
                    var pos = _this._context.container.position;
                    local.x -= pos.x;
                    local.y -= pos.y;
                    local.divide(_this.scale);
                    _this._context.pickColor(local);
                };
                this._area.xyzw(x, y, width, height);
                this._context = context;
                this.addChildPIXI(context.container);
                // listen to events
                var evRegister = KPaint.Events.register;
                var evId = KPaint.Events.ID;
                // paint
                evRegister(evId.STROKE_START, this.brushStrokeStart);
                evRegister(evId.STROKE_ADDPOINTS, this.brushStroke);
                evRegister(evId.STROKE_END, this.brushStrokeEnd);
                evRegister(evId.POINTERS_DISPATCHED, this.brushStrokePointersDispatched);
                // move
                evRegister(evId.INPUT_MOVE_CANVAS, this.moveToLocation);
                evRegister(evId.MOVE_CANVAS_START, this.moveCanvas);
                evRegister(evId.MOVE_CANVAS_CONTINUE, this.moveCanvas);
                evRegister(evId.MOVE_CANVAS_END, this.moveCanvasEnd);
                // zoom
                evRegister(evId.ZOOM_CANVAS_START, this.zoomCanvas);
                evRegister(evId.ZOOM_CANVAS_CONTINUE, this.zoomCanvas);
                evRegister(evId.ZOOM_CANVAS_END, this.zoomCanvasEnd);
                evRegister(evId.INPUT_ZOOM_CANVAS, function (a) { return _this.scale = a[0]; });
                evRegister(evId.INPUT_ZOOM_CANVAS_ADD, function (a) { return _this.scale *= 1.1; });
                evRegister(evId.INPUT_ZOOM_CANVAS_SUBTRACT, function (a) { return _this.scale *= .9; });
                evRegister(evId.INPUT_ZOOM_CANVAS_RESET, this.scaleToFit);
                // misc
                evRegister(evId.INPUT_DRAG_BRUSH_SIZE, this.brushSize);
                evRegister(evId.POINTER_UP, this.brushSizeEnd);
                evRegister(evId.INPUT_PICK_COLOR, this.pickColor);
            }
            Object.defineProperty(CanvasDisplay.prototype, "scale", {
                get: function () {
                    return this._context.container.scale.x;
                },
                set: function (value) {
                    value = KPaint.utils.clamp(value, 0.02, 5);
                    var ctxScale = this._context.container.scale;
                    ctxScale.x = value;
                    ctxScale.y = value;
                    this.updatePosition();
                    KPaint.Events.emit(KPaint.Events.ID.ZOOM_SET_ZOOM, this.scale);
                },
                enumerable: true,
                configurable: true
            });
            CanvasDisplay.prototype.updatePosition = function () {
                var ctxCon = this._context.container;
                var offSet = this.offCenter;
                var scale = this.scale;
                var area = this._area;
                var size = this._context.size;
                var xCenter = area.width / 2, yCenter = area.height / 2;
                var xScaledHalfSize = ((size.x * scale) / 2), yScaledHalfSize = ((size.y * scale) / 2);
                // center - scaled context halfsize + offset
                ctxCon.x = xCenter - xScaledHalfSize + offSet.x * scale;
                ctxCon.y = yCenter - yScaledHalfSize + offSet.y * scale;
            };
            /*
                Resize the area that this element uses
            */
            CanvasDisplay.prototype.resizeArea = function (width, height) {
                var area = this._area;
                area.width = width;
                area.height = height;
                this._context.resizeViewArea(width, height);
                this.scaleToFit();
            };
            CanvasDisplay.prototype.contains = function (global) {
                return this._area.contains2D(global);
            };
            /*
                Modifies _local to context version of global coordinate, and then return it.
            */
            CanvasDisplay.prototype.toContextCoord = function (global) {
                var ctxCon = this._context.container;
                var scale = this.scale;
                var local = this._localVec;
                var area = this._area;
                local.x = (global.x - area.x - ctxCon.x) / scale;
                local.y = (global.y - area.y - ctxCon.y) / scale;
                local.z = global.z;
                return local;
            };
            return CanvasDisplay;
        })(UI.DisplayObject);
        UI.CanvasDisplay = CanvasDisplay;
    })(UI = KPaint.UI || (KPaint.UI = {}));
})(KPaint || (KPaint = {}));
var KPaint;
(function (KPaint) {
    var UI;
    (function (UI) {
        var uiManager;
        function init(renderer, ctx) {
            var bounds = renderer.view.getBoundingClientRect();
            uiManager = new UIManager(bounds, renderer, ctx);
            uiManager.setParentPIXI(KPaint.stage);
        }
        UI.init = init;
        function clickTarget(pointer) {
            if (uiManager.canvasDisplay.contains(pointer)) {
                return KPaint.Input.ClickTarget.Canvas;
            }
            else if (uiManager.contains(pointer)) {
                return KPaint.Input.ClickTarget.UI;
            }
            return KPaint.Input.ClickTarget.None;
        }
        UI.clickTarget = clickTarget;
        var UIManager = (function (_super) {
            __extends(UIManager, _super);
            function UIManager(bounds, renderer, ctx) {
                var _this = this;
                _super.call(this, 0, 0, bounds.width, bounds.height);
                var bgColor = 0x888888;
                var x = 0, y = 0, sideBarWidth = 200;
                // cursor
                this.cursor = new UI.CCursor(0);
                // left side bar
                this.sideBarLeft = new UI.SideBarLeft(x, y, sideBarWidth, bounds.height, bgColor);
                // the canvas that's being drawn on
                x += sideBarWidth;
                this.canvasDisplay = new UI.CanvasDisplay(x, y, bounds.width - sideBarWidth * 2, bounds.height, ctx);
                // right side bar
                x = bounds.width - sideBarWidth;
                this.sideBarRight = new UI.SideBarRight(x, y, sideBarWidth, bounds.height, bgColor);
                this.canvasDisplay.scaleToFit();
                // add to container
                this.addChild(this.canvasDisplay);
                this.addChildPIXI(this.cursor.sprite);
                this.addChild(this.sideBarLeft);
                this.addChild(this.sideBarRight);
                // click listeners
                this.addClickListener(this.sideBarLeft);
                this.addClickListener(this.sideBarRight);
                // events
                var evRegister = KPaint.Events.register;
                var evId = KPaint.Events.ID;
                evRegister(evId.PAGE_RESIZED, function (a) { return _this.resize(a[0], a[1]); });
                evRegister(evId.UI_POINTER_DOWN, function (a) { return _this.click(a[0]); });
                evRegister(evId.UI_POINTER_UP, function () { return _this.releaseClick(); });
                evRegister(evId.UI_POINTER_DRAG, function (a) {
                    if (_this.isClicked()) {
                        _this.click(a[0]);
                    }
                });
            }
            UIManager.prototype.isClicked = function () {
                return this.sideBarLeft.isActive
                    || this.sideBarRight.isActive;
            };
            UIManager.prototype.isOnSidebar = function (pt) {
                return this.sideBarLeft.contains(pt)
                    || this.sideBarRight.contains(pt);
            };
            UIManager.prototype.moveCursor = function (pt) {
                // Cursor
                if (KPaint.utils.isOnHtmlCanvas(pt, KPaint.renderer.view) && !this.isOnSidebar(pt)) {
                    // set standard cursor
                    document.body.style.cursor = 'none';
                }
                else {
                    // set painting cursor
                    document.body.style.cursor = '';
                }
                // set new position
                this.cursor.x = pt.x;
                this.cursor.y = pt.y;
            };
            UIManager.prototype.resize = function (width, height) {
                this.canvasDisplay.resizeArea(width - 400, height);
                this.sideBarLeft.resize(width, height);
                this.sideBarRight.resize(width, height);
                this.sideBarRight.x = width - 200;
                KPaint.renderer.resize(width, height);
            };
            return UIManager;
        })(UI.ClickableContainer2);
        UI.UIManager = UIManager;
    })(UI = KPaint.UI || (KPaint.UI = {}));
})(KPaint || (KPaint = {}));
var KPaint;
(function (KPaint) {
    var Input;
    (function (Input_1) {
        // key code for KeyboardEvent.which
        (function (Input) {
            // Pointer events
            Input[Input["PointerDrag"] = -5] = "PointerDrag";
            Input[Input["PointerMove"] = -4] = "PointerMove";
            Input[Input["PointerUp"] = -3] = "PointerUp";
            Input[Input["PointerDown"] = -2] = "PointerDown";
            // 
            Input[Input["None"] = 0] = "None";
            // Arrow keys
            Input[Input["ArrowLeft"] = 37] = "ArrowLeft";
            Input[Input["ArrowUp"] = 38] = "ArrowUp";
            Input[Input["ArrowRight"] = 39] = "ArrowRight";
            Input[Input["ArrowDown"] = 40] = "ArrowDown";
            // Numeric
            Input[Input["Zero"] = 48] = "Zero";
            Input[Input["One"] = 49] = "One";
            Input[Input["Two"] = 50] = "Two";
            Input[Input["Three"] = 51] = "Three";
            Input[Input["Four"] = 52] = "Four";
            Input[Input["Five"] = 53] = "Five";
            Input[Input["Sex"] = 55] = "Sex";
            Input[Input["Seven"] = 56] = "Seven";
            Input[Input["Eight"] = 57] = "Eight";
            Input[Input["Nine"] = 58] = "Nine";
            // Uppercase(shift) alphabet
            Input[Input["A"] = 65] = "A";
            Input[Input["B"] = 66] = "B";
            Input[Input["C"] = 67] = "C";
            Input[Input["D"] = 68] = "D";
            Input[Input["E"] = 69] = "E";
            Input[Input["F"] = 70] = "F";
            Input[Input["G"] = 71] = "G";
            Input[Input["H"] = 72] = "H";
            Input[Input["I"] = 73] = "I";
            Input[Input["J"] = 74] = "J";
            Input[Input["K"] = 75] = "K";
            Input[Input["L"] = 76] = "L";
            Input[Input["M"] = 77] = "M";
            Input[Input["N"] = 78] = "N";
            Input[Input["O"] = 79] = "O";
            Input[Input["P"] = 80] = "P";
            Input[Input["Q"] = 81] = "Q";
            Input[Input["R"] = 82] = "R";
            Input[Input["S"] = 83] = "S";
            Input[Input["T"] = 84] = "T";
            Input[Input["U"] = 85] = "U";
            Input[Input["V"] = 86] = "V";
            Input[Input["W"] = 87] = "W";
            Input[Input["X"] = 88] = "X";
            Input[Input["Y"] = 89] = "Y";
            Input[Input["Z"] = 90] = "Z";
            // misc
            Input[Input["Enter"] = 13] = "Enter";
            Input[Input["Multiply"] = 106] = "Multiply";
            Input[Input["Plus"] = 107] = "Plus";
            Input[Input["Minus"] = 109] = "Minus";
            Input[Input["Divide"] = 111] = "Divide";
            Input[Input["Comma"] = 198] = "Comma";
            Input[Input["Dot"] = 190] = "Dot";
        })(Input_1.Input || (Input_1.Input = {}));
        var Input = Input_1.Input;
    })(Input = KPaint.Input || (KPaint.Input = {}));
})(KPaint || (KPaint = {}));
var KPaint;
(function (KPaint) {
    var Input;
    (function (Input) {
        /*
            Where the pointerDown event occured
        */
        (function (ClickTarget) {
            ClickTarget[ClickTarget["None"] = 0] = "None";
            ClickTarget[ClickTarget["Canvas"] = 1] = "Canvas";
            ClickTarget[ClickTarget["UI"] = 2] = "UI";
        })(Input.ClickTarget || (Input.ClickTarget = {}));
        var ClickTarget = Input.ClickTarget;
        /*
            Object used to pass input data between functions in the input module
        */
        var InputData = (function () {
            function InputData(which, shift, alt, ctrl, pointer) {
                this.which = which;
                this.shift = shift;
                this.alt = alt;
                this.ctrl = ctrl;
                this.pointer = pointer;
                this.target = ClickTarget.None;
            }
            // quickly set every input variable
            InputData.prototype.setInput = function (which, shift, alt, ctrl) {
                if (shift === void 0) { shift = false; }
                if (alt === void 0) { alt = false; }
                if (ctrl === void 0) { ctrl = false; }
                this.which = which;
                this.shift = shift;
                this.alt = alt;
                this.ctrl = ctrl;
            };
            // uniquely identify any combination of input and modifiers
            InputData.prototype.toHash = function () {
                return (this.which << 5)
                    + (this.target << 3)
                    + ((this.shift === true ? 1 : 0) << 2)
                    + ((this.alt === true ? 1 : 0) << 1)
                    + ((this.ctrl === true ? 1 : 0));
            };
            return InputData;
        })();
        Input.InputData = InputData;
    })(Input = KPaint.Input || (KPaint.Input = {}));
})(KPaint || (KPaint = {}));
var KPaint;
(function (KPaint) {
    var Input;
    (function (Input) {
        /*
            Map keyboard input to a specific event.
        */
        var InputContext = (function () {
            function InputContext() {
                this.disabled = false;
                this._inputMap = {};
            }
            InputContext.prototype.addInputEvent = function (event, input, anyTarget) {
                if (anyTarget === true) {
                    input.target = Input.ClickTarget.None;
                    this.add(input.toHash(), event);
                    input.target = Input.ClickTarget.Canvas;
                    this.add(input.toHash(), event);
                    input.target = Input.ClickTarget.UI;
                    this.add(input.toHash(), event);
                }
                else {
                    this.add(input.toHash(), event);
                }
            };
            InputContext.prototype.add = function (key, event) {
                var inputMap = this._inputMap;
                if (inputMap[key] !== undefined) {
                    console.warn([
                        'Overwriting input event for key: ', key, '.',
                        ' Old event: ', inputMap[key], '.',
                        ' New Event: ', event, '.'
                    ].join(''));
                }
                inputMap[key] = event;
            };
            InputContext.prototype.onNotify = function (key, input) {
                var evId = this._inputMap[key];
                if (evId === undefined) {
                    return;
                }
                KPaint.Events.emit(evId, input.pointer);
            };
            return InputContext;
        })();
        Input.InputContext = InputContext;
    })(Input = KPaint.Input || (KPaint.Input = {}));
})(KPaint || (KPaint = {}));
var KPaint;
(function (KPaint) {
    var Input;
    (function (Input) {
        /*
            General input
        */
        function createGeneralInputContext(input) {
            var context = new Input.InputContext();
            var evId = KPaint.Events.ID;
            // Undo
            input.setInput(Input.Input.Z, false, false, true);
            context.addInputEvent(evId.INPUT_CANVAS_UNDO, input, true);
            // Redo
            input.setInput(Input.Input.Z, true, false, true);
            context.addInputEvent(evId.INPUT_CANVAS_REDO, input, true);
            // Tool: paint
            input.setInput(Input.Input.Q);
            context.addInputEvent(evId.INPUT_TOOL_BRUSH_PAINT, input, true);
            // Tool: paint
            input.setInput(Input.Input.B);
            context.addInputEvent(evId.INPUT_TOOL_BRUSH_PAINT, input, true);
            // Tool: blur
            input.setInput(Input.Input.W);
            context.addInputEvent(evId.INPUT_TOOL_BRUSH_BLUR, input, true);
            // Tool: erase
            input.setInput(Input.Input.E);
            context.addInputEvent(evId.INPUT_TOOL_BRUSH_ERASE, input, true);
            // Tool: mover
            input.setInput(Input.Input.V);
            context.addInputEvent(evId.INPUT_TOOL_MOVER, input, true);
            // Tool: zoomer
            input.setInput(Input.Input.Z);
            context.addInputEvent(evId.INPUT_TOOL_ZOOMER, input, true);
            // Tool: color picker
            input.setInput(Input.Input.R);
            context.addInputEvent(evId.INPUT_TOOL_COLOR_PICKER, input, true);
            // Color swap
            input.setInput(Input.Input.X);
            context.addInputEvent(evId.INPUT_COLOR_SWAP, input, true);
            // Color swap
            input.setInput(Input.Input.G);
            context.addInputEvent(evId.INPUT_GAMMA_TOGGLE, input, true);
            // Pointer Move/Up for all combinations of modifiers
            for (var i = 0, s = false, a = false, c = false; i < 8; i++) {
                input.setInput(Input.Input.PointerMove, s, a, c);
                context.addInputEvent(evId.POINTER_MOVE, input, true);
                input.which = Input.Input.PointerUp;
                context.addInputEvent(evId.POINTER_UP, input, true);
                s = !s;
                if (i % 2 === 0)
                    a = !a;
                if (i % 4 === 0)
                    c = !c;
            }
            // Color selection through alt-clicking
            var prevTar = input.target;
            input.target = Input.ClickTarget.Canvas;
            input.setInput(Input.Input.PointerDown, false, true, false);
            context.addInputEvent(evId.INPUT_PICK_COLOR, input, false);
            input.which = Input.Input.PointerDrag;
            context.addInputEvent(evId.INPUT_PICK_COLOR, input, false);
            // Brush size change through ctrl-alt-clicking
            input.setInput(Input.Input.PointerDown, false, true, true);
            context.addInputEvent(evId.INPUT_DRAG_BRUSH_SIZE, input, false);
            input.which = Input.Input.PointerDrag;
            context.addInputEvent(evId.INPUT_DRAG_BRUSH_SIZE, input, false);
            // set target back to previous
            input.target = prevTar;
            return context;
        }
        Input.createGeneralInputContext = createGeneralInputContext;
        /*
            Clicks on the UI
        */
        function createUIinputContext(input) {
            var context = new Input.InputContext();
            var evId = KPaint.Events.ID;
            // store target before altering
            var prevTar = input.target;
            input.target = Input.ClickTarget.UI;
            // pointer down with UI as the target
            input.setInput(Input.Input.PointerDown);
            context.addInputEvent(evId.UI_POINTER_DOWN, input, true);
            // pointer up with UI as the target
            input.setInput(Input.Input.PointerUp);
            context.addInputEvent(evId.UI_POINTER_UP, input, true);
            // pointer drag with UI as the target
            input.setInput(Input.Input.PointerDrag);
            context.addInputEvent(evId.UI_POINTER_DRAG, input, true);
            // set target back to previous
            input.target = prevTar;
            return context;
        }
        Input.createUIinputContext = createUIinputContext;
        /*
            TOOL: PAINT, ERASE, BLUR
        */
        function createPaintingInputContext(input) {
            // store target before altering
            var prevTar = input.target;
            input.target = Input.ClickTarget.Canvas;
            var context = new Input.InputContext();
            var evId = KPaint.Events.ID;
            // pointer down
            input.setInput(Input.Input.PointerDown);
            context.addInputEvent(evId.STROKE_START, input, false);
            input.shift = true;
            context.addInputEvent(evId.STROKE_START, input, false);
            // pointer drag
            input.setInput(Input.Input.PointerDrag);
            context.addInputEvent(evId.STROKE_ADDPOINTS, input, false);
            input.shift = true;
            context.addInputEvent(evId.STROKE_ADDPOINTS, input, false);
            // pointer up
            input.setInput(Input.Input.PointerUp);
            context.addInputEvent(evId.STROKE_END, input, false);
            input.shift = true;
            context.addInputEvent(evId.STROKE_END, input, false);
            // set target back to previous
            input.target = prevTar;
            return context;
        }
        Input.createPaintingInputContext = createPaintingInputContext;
        /*
            TOOL: MOVE
        */
        function createMovingCanvasContext(input) {
            // store target before altering
            var prevTar = input.target;
            input.target = Input.ClickTarget.Canvas;
            var context = new Input.InputContext();
            var evId = KPaint.Events.ID;
            // pointer down
            input.setInput(Input.Input.PointerDown);
            context.addInputEvent(evId.MOVE_CANVAS_START, input, false);
            input.shift = true;
            context.addInputEvent(evId.MOVE_CANVAS_START, input, false);
            // pointer drag
            input.setInput(Input.Input.PointerDrag);
            context.addInputEvent(evId.MOVE_CANVAS_CONTINUE, input, false);
            input.shift = true;
            context.addInputEvent(evId.MOVE_CANVAS_CONTINUE, input, false);
            // pointer up
            input.setInput(Input.Input.PointerUp);
            context.addInputEvent(evId.MOVE_CANVAS_END, input, false);
            input.shift = true;
            context.addInputEvent(evId.MOVE_CANVAS_END, input, false);
            // set target back to previous
            input.target = prevTar;
            return context;
        }
        Input.createMovingCanvasContext = createMovingCanvasContext;
        /*
            TOOL: ZOOM
        */
        function createZoomingCanvasContext(input) {
            // store target before altering
            var prevTar = input.target;
            input.target = Input.ClickTarget.Canvas;
            var context = new Input.InputContext();
            var evId = KPaint.Events.ID;
            // pointer down
            input.setInput(Input.Input.PointerDown);
            context.addInputEvent(evId.ZOOM_CANVAS_START, input, false);
            input.shift = true;
            context.addInputEvent(evId.ZOOM_CANVAS_START, input, false);
            // pointer drag
            input.setInput(Input.Input.PointerDrag);
            context.addInputEvent(evId.ZOOM_CANVAS_CONTINUE, input, false);
            input.shift = true;
            context.addInputEvent(evId.ZOOM_CANVAS_CONTINUE, input, false);
            // pointer up
            input.setInput(Input.Input.PointerUp);
            context.addInputEvent(evId.ZOOM_CANVAS_END, input, false);
            input.shift = true;
            context.addInputEvent(evId.ZOOM_CANVAS_END, input, false);
            // set target back to previous
            input.target = prevTar;
            return context;
        }
        Input.createZoomingCanvasContext = createZoomingCanvasContext;
        /*
            TOOL: COLOR PICKER
        */
        function createColorPickerContext(input) {
            // store target before altering
            var prevTar = input.target;
            input.target = Input.ClickTarget.Canvas;
            var context = new Input.InputContext();
            var evId = KPaint.Events.ID;
            // pointer down
            input.setInput(Input.Input.PointerDown);
            context.addInputEvent(evId.INPUT_PICK_COLOR, input, false);
            input.shift = true;
            context.addInputEvent(evId.INPUT_PICK_COLOR, input, false);
            // pointer drag
            input.setInput(Input.Input.PointerDrag);
            context.addInputEvent(evId.INPUT_PICK_COLOR, input, false);
            input.shift = true;
            context.addInputEvent(evId.INPUT_PICK_COLOR, input, false);
            // pointer up
            input.setInput(Input.Input.PointerUp);
            context.addInputEvent(evId.INPUT_PICK_COLOR, input, false);
            input.shift = true;
            context.addInputEvent(evId.INPUT_PICK_COLOR, input, false);
            // set target back to previous
            input.target = prevTar;
            return context;
        }
        Input.createColorPickerContext = createColorPickerContext;
    })(Input = KPaint.Input || (KPaint.Input = {}));
})(KPaint || (KPaint = {}));
/// <reference path="../Generic/Ipool.ts"/>
var KPaint;
(function (KPaint) {
    var Input;
    (function (Input) {
        /*
            Input context type to sort the InputContexts so that they can be added on-demand, since some things require the same
            type of input. For example painting and moving canvas both use the PointerDrag event, but I only ever want to use one
            of those functions at a time, so I can remove and add InputContexts depending on what tool is selected.
        */
        (function (InputContextType) {
            // things that should always receive input are put under general. ie tool selection, color swapping
            InputContextType[InputContextType["General"] = 0] = "General";
            // just for sending pointer input I think
            InputContextType[InputContextType["UI"] = 1] = "UI";
            // painting, erasing, or blurring all fit under painting.
            InputContextType[InputContextType["Painting"] = 2] = "Painting";
            // move the view of the canvas
            InputContextType[InputContextType["MovingCanvas"] = 3] = "MovingCanvas";
            // zoom in or out from the canvas
            InputContextType[InputContextType["ZoomingCanvas"] = 4] = "ZoomingCanvas";
            // resize the brush (like in SAI)
            InputContextType[InputContextType["ResizingBrush"] = 5] = "ResizingBrush";
            // picking color off of the canvas
            InputContextType[InputContextType["ColorPicking"] = 6] = "ColorPicking";
        })(Input.InputContextType || (Input.InputContextType = {}));
        var InputContextType = Input.InputContextType;
        /*
            Object for managing active InputContexts
        */
        var InputMapper = (function () {
            function InputMapper() {
                this._contexts = {};
                this._data = new Input.InputData(0, false, false, false, null);
            }
            InputMapper.prototype.addContext = function (type, context) {
                var contexts = this._contexts;
                if (contexts[type] !== undefined) {
                    console.warn(['Overwriting input context for type: ', type, '.'].join(''));
                }
                contexts[type] = context;
            };
            InputMapper.prototype.enableContext = function (type) {
                this.setContextDisabled(type, false);
            };
            InputMapper.prototype.disableContext = function (type) {
                this.setContextDisabled(type, true);
            };
            InputMapper.prototype.notify = function (inputData) {
                var inputKey = inputData.toHash();
                var contexts = this._contexts;
                for (var ctxKey in contexts) {
                    if (contexts[ctxKey].disabled === false) {
                        contexts[ctxKey].onNotify(inputKey, inputData);
                    }
                }
            };
            InputMapper.prototype.setContextDisabled = function (type, value) {
                var contexts = this._contexts;
                if (contexts[type] === undefined) {
                    console.warn(['Context not found for type ', type, '.'].join(''));
                    return;
                }
                contexts[type].disabled = value;
            };
            return InputMapper;
        })();
        Input.InputMapper = InputMapper;
    })(Input = KPaint.Input || (KPaint.Input = {}));
})(KPaint || (KPaint = {}));
var KPaint;
(function (KPaint) {
    var Input;
    (function (Input) {
        var InputDataPool = (function (_super) {
            __extends(InputDataPool, _super);
            function InputDataPool() {
                _super.call(this, 'InputData', 500);
                this.allocate();
            }
            InputDataPool.prototype.createOne = function () {
                return new Input.InputData(Input.Input.None, false, false, false, new KPaint.Vec3());
            };
            return InputDataPool;
        })(KPaint.IPool);
        var PointerEventCapture = (function () {
            function PointerEventCapture() {
                this._captured = [];
                this._pool = new InputDataPool();
            }
            /*
                Store a pointer event until when it needs to be used
            */
            PointerEventCapture.prototype.captureEvent = function (pointer, data) {
                var cap = this._pool.get();
                cap.setInput(data.which, data.shift, data.alt, data.ctrl);
                cap.target = data.target;
                cap.pointer.copyFrom(pointer);
                this._captured.push(cap);
            };
            /*
                Dispatch all the captured events
            */
            PointerEventCapture.prototype.dispatchEvents = function (inputMap) {
                var events = this._captured;
                var pool = this._pool;
                // notify for every input
                var ev;
                for (var i = 0, n = events.length; i < n; i++) {
                    ev = events[i];
                    inputMap.notify(ev);
                    pool.recycle(ev);
                }
                events.length = 0;
                KPaint.Events.emit(KPaint.Events.ID.POINTERS_DISPATCHED);
            };
            PointerEventCapture.prototype.isEmpty = function () {
                return this._captured.length === 0;
            };
            return PointerEventCapture;
        })();
        Input.PointerEventCapture = PointerEventCapture;
    })(Input = KPaint.Input || (KPaint.Input = {}));
})(KPaint || (KPaint = {}));
/// <reference path="../Generic/CircularBuffer.ts"/>
var KPaint;
(function (KPaint) {
    var Input;
    (function (Input) {
        // stats
        //let inputHandlingMs: number[] = [];
        var inputHandlingMs = new KPaint.CircularBuffer(300);
        function storeInputHandlingDuration(t) {
            //inputHandlingMs.shift();
            inputHandlingMs.push(t);
            var sum = 0, min = Number.MAX_VALUE, max = Number.MIN_VALUE, handle;
            var buf = inputHandlingMs.buffer;
            for (var i = 0, n = buf.length; i < n; i++) {
                handle = buf[i];
                sum += handle;
                if (handle < min) {
                    min = handle;
                }
                else if (handle > max) {
                    max = handle;
                }
            }
            Input.avgInputHandlingMs = sum / buf.length;
            Input.minInputHandlingMs = min;
            Input.maxInputHandlingMs = max;
        }
        Input.avgInputHandlingMs = 1;
        Input.minInputHandlingMs = 1;
        Input.maxInputHandlingMs = 1;
        // functionality
        var mouseDown = false;
        var pointerEventCapture = new Input.PointerEventCapture();
        var currentPointer = new KPaint.Vec3(0, 0, 0.5);
        var canvasClientPos = new KPaint.Vec2;
        var inputMap = new Input.InputMapper();
        var inputData = new Input.InputData(0, false, false, false, currentPointer);
        var currentToolContext = Input.InputContextType.Painting;
        var pointers = false;
        function init() {
            // init stats collection
            /*for (let i = 0; i < 300; i++) {
                inputHandlingMs.push(10);
            }*/
            // pointer events
            var v = document.body.addEventListener;
            v('keydown', onKeyDown);
            // if the browser supports has pointer events implemented, and the browser is not firefox (because firefox has only half support)
            if (window.PointerEvent !== undefined && navigator.userAgent.toLowerCase().indexOf('firefox') < 0) {
                pointers = true;
                v('pointerdown', onPointerDown);
                v('pointerup', onPointerUp);
                v('pointermove', onPointerMove);
            }
            else {
                v('mousedown', onMouseDown);
                v('touchdown', onMouseDown);
                v('mouseup', onMouseUp);
                v('touchup', onMouseUp);
                v('mousemove', onMouseMove);
                v('touchmove', onMouseMove);
            }
            // add input contexts
            inputMap.addContext(Input.InputContextType.General, Input.createGeneralInputContext(inputData));
            inputMap.addContext(Input.InputContextType.UI, Input.createUIinputContext(inputData));
            inputMap.addContext(Input.InputContextType.Painting, Input.createPaintingInputContext(inputData));
            inputMap.addContext(Input.InputContextType.MovingCanvas, Input.createMovingCanvasContext(inputData));
            inputMap.addContext(Input.InputContextType.ZoomingCanvas, Input.createZoomingCanvasContext(inputData));
            inputMap.addContext(Input.InputContextType.ColorPicking, Input.createColorPickerContext(inputData));
            // disable all but one(painting) of the tool contexts
            inputMap.disableContext(Input.InputContextType.MovingCanvas);
            inputMap.disableContext(Input.InputContextType.ZoomingCanvas);
            inputMap.disableContext(Input.InputContextType.ColorPicking);
            // listen to tool events so we can swap contexts
            var evRegister = KPaint.Events.register;
            var evId = KPaint.Events.ID;
            evRegister(evId.INPUT_TOOL_BRUSH_PAINT, function () { return setToolContext(Input.InputContextType.Painting); });
            evRegister(evId.INPUT_TOOL_BRUSH_ERASE, function () { return setToolContext(Input.InputContextType.Painting); });
            evRegister(evId.INPUT_TOOL_BRUSH_BLUR, function () { return setToolContext(Input.InputContextType.Painting); });
            evRegister(evId.INPUT_TOOL_MOVER, function () { return setToolContext(Input.InputContextType.MovingCanvas); });
            evRegister(evId.INPUT_TOOL_ZOOMER, function () { return setToolContext(Input.InputContextType.ZoomingCanvas); });
            evRegister(evId.INPUT_TOOL_COLOR_PICKER, function () { return setToolContext(Input.InputContextType.ColorPicking); });
            evRegister(evId.CANVAS_RENDERED, function () {
                dispatchPointerEvents();
                KPaint.renderCoordinator.requestRender();
            });
            // window events
            window.onresize = onResize;
            window.onscroll = onScroll;
            // init canvasClientPos
            onScroll();
        }
        Input.init = init;
        function setToolContext(type) {
            inputMap.disableContext(currentToolContext);
            currentToolContext = type;
            inputMap.enableContext(currentToolContext);
        }
        function onPointerDown(ev) {
            var start = performance.now();
            // handling
            mouseDown = true;
            setCanvasPoint_PointerEvent(ev);
            setInputData(Input.Input.PointerDown, ev);
            inputData.target = KPaint.UI.clickTarget(currentPointer);
            if (ev.button !== 2) {
                capturePointerEvent();
                dispatchPointerEvents();
            }
            KPaint.renderCoordinator.requestRender();
            storeInputHandlingDuration(performance.now() - start);
        }
        Input.onPointerDown = onPointerDown;
        function onMouseDown(ev) {
            var start = performance.now();
            // handling
            mouseDown = true;
            setCanvasPoint_NonPointerEvent(ev);
            setInputData(Input.Input.PointerDown, ev);
            inputData.target = KPaint.UI.clickTarget(currentPointer);
            if (ev.button !== 2) {
                capturePointerEvent();
                dispatchPointerEvents();
            }
            KPaint.renderCoordinator.requestRender();
            storeInputHandlingDuration(performance.now() - start);
        }
        Input.onMouseDown = onMouseDown;
        function onPointerUp(ev) {
            var start = performance.now();
            // handling
            mouseDown = false;
            setCanvasPoint_PointerEvent(ev);
            setInputData(Input.Input.PointerUp, ev);
            capturePointerEvent();
            dispatchPointerEvents();
            // reset target after notifying since notifications are dependant on target
            inputData.target = Input.ClickTarget.None;
            KPaint.renderCoordinator.requestRender();
            storeInputHandlingDuration(performance.now() - start);
        }
        Input.onPointerUp = onPointerUp;
        function onMouseUp(ev) {
            var start = performance.now();
            // handling
            mouseDown = false;
            setCanvasPoint_NonPointerEvent(ev);
            setInputData(Input.Input.PointerUp, ev);
            capturePointerEvent();
            dispatchPointerEvents();
            // reset target after notifying since notifications are dependant on target
            inputData.target = Input.ClickTarget.None;
            KPaint.renderCoordinator.requestRender();
            storeInputHandlingDuration(performance.now() - start);
        }
        Input.onMouseUp = onMouseUp;
        function onPointerMove(ev) {
            var start = performance.now();
            // handling
            setCanvasPoint_PointerEvent(ev);
            setInputData(Input.Input.PointerMove, ev);
            // send for pointer move
            inputMap.notify(inputData);
            if (mouseDown && ev.button !== 2) {
                // send for pointer drag
                inputData.which = Input.Input.PointerDrag;
                capturePointerEvent();
                dispatchPointerEvents();
            }
            KPaint.renderCoordinator.requestRender();
            storeInputHandlingDuration(performance.now() - start);
        }
        Input.onPointerMove = onPointerMove;
        function onMouseMove(ev) {
            var start = performance.now();
            // handling
            setCanvasPoint_NonPointerEvent(ev);
            setInputData(Input.Input.PointerMove, ev);
            // send for pointer move
            inputMap.notify(inputData);
            if (mouseDown && ev.button !== 2) {
                // send for pointer drag
                inputData.which = Input.Input.PointerDrag;
                capturePointerEvent();
                dispatchPointerEvents();
            }
            KPaint.renderCoordinator.requestRender();
            storeInputHandlingDuration(performance.now() - start);
        }
        Input.onMouseMove = onMouseMove;
        function onKeyDown(ev) {
            var start = performance.now();
            setInputData(ev.which, ev);
            inputMap.notify(inputData);
            // render
            KPaint.renderCoordinator.requestRender();
            storeInputHandlingDuration(performance.now() - start);
        }
        function onResize() {
            var width = KPaint.utils.clamp(window.innerWidth - 20, 800, 1400), height = KPaint.utils.clamp(window.innerHeight - 20, 400, 1000);
            KPaint.Events.emit(KPaint.Events.ID.PAGE_RESIZED, width, height);
            KPaint.renderCoordinator.requestRender();
        }
        function onScroll() {
            var bounds = KPaint.renderer.view.getBoundingClientRect();
            canvasClientPos.x = bounds.left;
            canvasClientPos.y = bounds.top;
            KPaint.Events.emit(KPaint.Events.ID.PAGE_SCROLL);
        }
        // the supplied MouseEvent will be modified by PEP to contain pressure information, so it needs to be 'any'
        // type in order to extract this data
        function setCanvasPoint_PointerEvent(ev) {
            // translate to canvas coordinate
            currentPointer.x = ev.clientX - canvasClientPos.x;
            currentPointer.y = ev.clientY - canvasClientPos.y;
            currentPointer.pressure = (ev.pointerType === 'pen' ? ev.pressure : 1.0);
        }
        function setCanvasPoint_NonPointerEvent(ev) {
            // translate to canvas coordinate
            currentPointer.x = ev.clientX - canvasClientPos.x;
            currentPointer.y = ev.clientY - canvasClientPos.y;
            currentPointer.pressure = 1.0;
        }
        function setInputData(which, ev) {
            inputData.setInput(which, ev.shiftKey, ev.altKey, ev.ctrlKey);
        }
        function capturePointerEvent() {
            pointerEventCapture.captureEvent(currentPointer, inputData);
        }
        function dispatchPointerEvents() {
            if (KPaint.renderCoordinator.waitingToRender === false) {
                pointerEventCapture.dispatchEvents(inputMap);
                KPaint.renderCoordinator.requestRender();
            }
            else if (pointerEventCapture.isEmpty() === false) {
                setTimeout(dispatchPointerEvents, 2);
            }
        }
    })(Input = KPaint.Input || (KPaint.Input = {}));
})(KPaint || (KPaint = {}));
var KPaint;
(function (KPaint) {
    (function (Tool) {
        Tool[Tool["None"] = 0] = "None";
        Tool[Tool["Paint"] = 1] = "Paint";
        Tool[Tool["Erase"] = 2] = "Erase";
        Tool[Tool["Blur"] = 3] = "Blur";
        Tool[Tool["Move"] = 4] = "Move";
        Tool[Tool["Zoom"] = 5] = "Zoom";
    })(KPaint.Tool || (KPaint.Tool = {}));
    var Tool = KPaint.Tool;
})(KPaint || (KPaint = {}));
// attempt to improve the stroke class
var KPaint;
(function (KPaint) {
    var PaintingContext;
    (function (PaintingContext) {
        (function (StrokeType) {
            StrokeType[StrokeType["None"] = 0] = "None";
            StrokeType[StrokeType["Paint"] = 1] = "Paint";
            StrokeType[StrokeType["Erase"] = 2] = "Erase";
            StrokeType[StrokeType["Blur"] = 3] = "Blur";
        })(PaintingContext.StrokeType || (PaintingContext.StrokeType = {}));
        var StrokeType = PaintingContext.StrokeType;
        /*
            The stroke class contains a collection of sprites that represent points in a brush stroke.
    
            A brush stroke begins when the pointerdown event occurs, and end when the pointerup event occurs.
            In between these events, points should also be when pointermove events occur.
        
            To ensure smoothness, some form of interpolation should be used between these points, since even
            an intuous5 has a polling rate of just 200hz, while the width and height of the drawing area is
            expected to exceeds 1000px.
        */
        var Stroke = (function () {
            function Stroke(strokeManager) {
                this.container = new PIXI.Container();
                this.id = null;
                this.type = StrokeType.None;
                this.area = new KPaint.Vec4();
                this.previewRT = null;
                this._maxSize = new KPaint.Vec2();
                this._brushSprites = [];
                this._flattenable = false;
                this._strokeManager = strokeManager;
                this._particles = new PIXI.ParticleContainer(3000, {
                    scale: false,
                    position: false,
                    alpha: false,
                    rotation: false,
                    uvs: false
                });
                this._rt = new PIXI.RenderTexture(KPaint.renderer, 0, 0);
                this.flattenedStroke = new PIXI.Sprite(this._rt);
                //this.flattenedStroke.visible = false;
                this.container.addChild(this.flattenedStroke);
                this.container.addChild(this._particles);
            }
            Object.defineProperty(Stroke.prototype, "isFlat", {
                get: function () { return this._flattenable === false; },
                enumerable: true,
                configurable: true
            });
            Stroke.prototype.addPoints = function (sprites) {
                var brushSprites = this._brushSprites;
                var particles = this._particles;
                var l = particles.children.length;
                var sprite;
                for (var i = 0, n = sprites.length; i < n; i++, l++) {
                    if (l >= 3000) {
                        this.flatten();
                        l = 0;
                    }
                    sprite = sprites[i];
                    brushSprites.push(sprite);
                    particles.addChild(sprite.sprite);
                }
                this._flattenable = true;
            };
            /*
                Turn the sprites in the particlecontainer into a single texture to ensure we don't have too
                many sprites in our container structure. PIXI can handle tens of thousands, but if a stroke
                puts one sprite at each pixel it passes, a long one can actually be tens of thousands.
            */
            Stroke.prototype.flatten = function () {
                if (!this._flattenable) {
                    return;
                }
                /*let children = this._particles.children;
                let child = <PIXI.Sprite>children[0];
    
                let halfW = child.width / 2, halfH = child.height / 2;
                let xMin = child.x - halfW;
                let yMin = child.y - halfH;
                let xMax = child.x + halfW;
                let yMax = child.y + halfH;
                
                let ax = 99999;
                let ay = 99999;
                let axw = 0;
                let ayh = 0;
    
                for (let i = 1, n = children.length; i < n; i++) {
                    child = <PIXI.Sprite>children[i];
    
                    // coordinates of each corner
                    ax = child.x - halfW;
                    ay = child.y - halfH;
                    axw = child.x + halfW;
                    ayh = child.y + halfH;
                    
    
                    if (ax < xMin) xMin = ax;
                    if (ay < yMin) yMin = ay;
                    if (axw > xMax) xMax = axw;
                    if (ayh > yMax) yMax = ayh;
                }
                let mSize = this._maxSize;
                if (xMin < 0) xMin = 0;
                if (yMin < 0) yMin = 0;
                if (xMax > mSize.x) xMax = mSize.x;
                if (yMax > mSize.y) yMax = mSize.y;
    
                xMin |= 0;
                yMin |= 0;
                xMax |= 0;
                yMax |= 0;
    
                let flattened = this.flattenedStroke;
    
                let increaseLeft = 0;
                let increaseTop = 0;
                let increaseRight = 0;
                let increaseBot = 0;
    
                
                let fw = 0, fh = 0;
                if (flattened.visible === false) {
                    increaseRight = (xMax - xMin);
                    increaseBot = (yMax - yMin);
                    flattened.visible = true;
                    console.warn(xMin, yMin, xMax, yMax);
                }
                else {
                    let fx = flattened.x | 0;
                    let fy = flattened.y | 0;
                    fw = flattened.width;
                    fh = flattened.height;
    
                    // distance from 0,0 to x,y
                    if (xMin < fx) {
                        increaseLeft = (fx - xMin);
                    }
                    if (yMin < fy) {
                        increaseTop = (fy - yMin);
                    }
                    console.warn('incr 1', increaseLeft, increaseTop, increaseRight, increaseBot);
    
                    // distance to increase in the opposite directions
                    if (xMax > fx + fw) {
                        increaseRight = (xMax - fx + fw);
                    }
                    if (yMax > fy + fh) {
                        increaseBot = (yMax - fy + fh);
                    }
                    console.warn('incr 2', increaseLeft, increaseTop, increaseRight, increaseBot);
                }
    
                if (increaseLeft !== 0 || increaseTop !== 0 || increaseRight !== 0 || increaseBot !== 0) {
                    let rt = this._rt;
                    flattened.x = increaseLeft;
                    flattened.y = increaseTop;
    
                    let prt = this.previewRT;
                    prt.resize(
                        fw + increaseLeft + increaseRight,
                        fh + increaseTop + increaseBot,
                        true
                    );
    
                    // render
                    prt.render(this.container);
    
                    // swap the texture
                    flattened.texture = prt;
                    flattened.x = xMin - increaseLeft;
                    flattened.y = yMin - increaseTop;
                    this._rt = prt;
                    this.previewRT = rt;
                    rt.resize(0, 0, true);
    
                }
                else {*/
                // render to the rendertexture
                this._rt.render(this._particles);
                //}
                //console.log(this.flattenedStroke.x|0, this.flattenedStroke.y|0, this._rt.width, this._rt.height);
                // empty the particles container
                this.recycleSprites();
                this._flattenable = false;
            };
            // initialize all variables
            Stroke.prototype.initialize = function (id, width, height, type) {
                this.id = id;
                this.type = type;
                switch (type) {
                    case StrokeType.None:
                    case StrokeType.Paint:
                        this.flattenedStroke.blendMode = PIXI.BLEND_MODES.NORMAL;
                        break;
                    case StrokeType.Erase:
                        this.flattenedStroke.blendMode = PIXI.BLEND_MODES.ERASE;
                        break;
                    default:
                        console.error(['Unsupported StrokeType: ', type].join(''));
                        this.flattenedStroke.blendMode = PIXI.BLEND_MODES.NORMAL;
                        return;
                }
                this._rt.resize(width, height);
                this._maxSize.xy(width, height);
            };
            // resets variables and recycles through the StrokeManager
            Stroke.prototype.recycle = function () {
                this.id = null;
                this.type = StrokeType.None;
                // remove parent
                var container = this.container;
                if (container.parent) {
                    container.parent.removeChild(container);
                }
                // empty the particles container
                this.recycleSprites();
                this._rt.clear();
                //this._rt.resize(0, 0, true);
                //this.flattenedStroke.visible = false;
                this._flattenable = false;
                this._strokeManager.recycle(this);
            };
            // removes all brush sprites from the stroke and recycles them.
            Stroke.prototype.recycleSprites = function () {
                // empty the particles container
                this._particles.children.length = 0;
                // recycle the brushsprites
                var children = this._brushSprites;
                var brushSpr = PaintingContext.BrushSprite;
                for (var i = 0, n = children.length; i < n; i++) {
                    brushSpr.recycle(children[i]);
                }
                children.length = 0;
            };
            return Stroke;
        })();
        PaintingContext.Stroke = Stroke;
    })(PaintingContext = KPaint.PaintingContext || (KPaint.PaintingContext = {}));
})(KPaint || (KPaint = {}));
// attempt at optimizing the layer class
var KPaint;
(function (KPaint) {
    var PaintingContext;
    (function (PaintingContext) {
        var Layer = (function () {
            function Layer(id) {
                this.container = new PIXI.Container();
                this.isClear = true;
                this.size = new KPaint.Vec2();
                this._activeStroke = null;
                this._alphaShader = new KPaint.Sha.AlphaFilter();
                this.id = id;
                this.name = ['Layer ', id.toString()].join('');
                this._rt = new PIXI.RenderTexture(KPaint.renderer, 0, 0);
                this._sprite = new PIXI.Sprite(this._rt);
                this._sprite.shader = this._alphaShader;
                // add to container
                this.container.addChild(this._sprite);
            }
            Object.defineProperty(Layer.prototype, "alpha", {
                get: function () {
                    return this._alphaShader.a;
                },
                set: function (value) {
                    this._alphaShader.a = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Layer.prototype, "id", {
                get: function () {
                    return this._id;
                },
                set: function (value) {
                    this.name = ['Layer ', value.toString()].join('');
                    this._id = value;
                },
                enumerable: true,
                configurable: true
            });
            Layer.prototype.startStroke = function (stroke) {
                if (stroke === null) {
                    console.error('Failed to start stroke. Stroke is null.');
                    return;
                }
                this._activeStroke = stroke;
                // add preview
                this.container.addChild(stroke.container);
            };
            Layer.prototype.endStroke = function () {
                // remove preview
                this.container.removeChild(this._activeStroke.container);
                this._activeStroke = null;
            };
            Layer.prototype.render = function (stroke) {
                if (stroke.isFlat === false) {
                    stroke.flatten();
                }
                this._rt.render(stroke.flattenedStroke);
                this.isClear = false;
            };
            Layer.prototype.renderLayer = function (layer) {
                this._rt.render(layer.container);
                this.isClear = false;
            };
            Layer.prototype.restoreLayer = function (spr) {
                var rt = this._rt;
                rt.clear();
                rt.render(spr);
                this.isClear = false;
            };
            Layer.prototype.copyTo = function (rt) {
                rt.render(this._sprite);
            };
            // clear the primary layer
            Layer.prototype.clear = function () {
                this._rt.clear();
                this.isClear = true;
            };
            Layer.prototype.resize = function (width, height) {
                this.size.xy(width, height);
                this._rt.resize(width, height, true);
            };
            Layer.prototype.initialize = function (id) {
                // TODO: confirm this works correctly
                this.id = id;
                this.name = null;
                this.isClear = true;
                var c = this.container;
                c.visible = true;
                c.alpha = 1.0;
                // remove parent
                if (c.parent !== null) {
                    c.parent.removeChild(c);
                }
                // remove any child other than the render sprite
                if (c.children.length > 1) {
                    var children = c.removeChildren(1);
                    for (var i = 0, n = children.length; i < n; i++) {
                        children[i].destroy();
                    }
                }
                this._rt.clear();
            };
            return Layer;
        })();
        PaintingContext.Layer = Layer;
    })(PaintingContext = KPaint.PaintingContext || (KPaint.PaintingContext = {}));
})(KPaint || (KPaint = {}));
/*
    Manager the lifetime of strokes and functions as an invoker (from the Command Pattern)
*/
var KPaint;
(function (KPaint) {
    var PaintingContext;
    (function (PaintingContext) {
        var StrokePool = (function (_super) {
            __extends(StrokePool, _super);
            function StrokePool(sm) {
                _super.call(this, 'Stroke', 11);
                this._sm = sm;
                this.allocate();
            }
            StrokePool.prototype.createOne = function () {
                return new PaintingContext.Stroke(this._sm);
            };
            return StrokePool;
        })(KPaint.IPool);
        var StrokeManager = (function () {
            function StrokeManager(width, height) {
                this.strokeType = PaintingContext.StrokeType.None;
                this.activeStroke = null;
                this._pool = new StrokePool(this);
                this._strokes = []; // all strokes outside of the stroke pool
                this._size = new KPaint.Vec2();
                this._size.xy(width, height);
            }
            StrokeManager.prototype.beginStroke = function () {
                if (this.strokeType === PaintingContext.StrokeType.None) {
                    console.error(['Attempted to start a stroke with strokeType ', this.strokeType].join(''));
                    return;
                }
                // initialize stroke
                var pool = this._pool;
                var stroke = pool.get();
                this._strokes.push(stroke);
                var size = this._size;
                stroke.initialize(pool.getUniqueId(), size.x, size.y, this.strokeType);
                // set as current
                this.activeStroke = stroke;
            };
            StrokeManager.prototype.endStroke = function () {
                var activeStroke = this.activeStroke;
                activeStroke = null;
            };
            StrokeManager.prototype.recycle = function (stroke) {
                var strokes = this._strokes;
                var idx = strokes.indexOf(stroke);
                strokes.splice(idx, 1);
                this._pool.recycle(stroke);
            };
            return StrokeManager;
        })();
        PaintingContext.StrokeManager = StrokeManager;
    })(PaintingContext = KPaint.PaintingContext || (KPaint.PaintingContext = {}));
})(KPaint || (KPaint = {}));
var KPaint;
(function (KPaint) {
    var PaintingContext;
    (function (PaintingContext) {
        var LayerPool = (function (_super) {
            __extends(LayerPool, _super);
            function LayerPool() {
                _super.call(this, 'Layer', 2);
                this.allocate();
            }
            LayerPool.prototype.createOne = function () {
                return new PaintingContext.Layer(-1);
            };
            return LayerPool;
        })(KPaint.IPool);
        /*
            This class manager the all Layer objects.
        
            Creation and deletion of layer objects happens only in this class, and all references to
            layer objects will be taken from here.
    
            When decribing a layer as being "above/below" it is meant in terms of what's seen on the canvas,
            which is not necessarily reflected by their actual index positions.
        */
        var LayerManager = (function () {
            function LayerManager(width, height) {
                var _this = this;
                this.container = new PIXI.Container();
                this.layers = [];
                this.activeLayer = null;
                this._size = new KPaint.Vec2();
                this._pool = new LayerPool();
                this._indexMoveCurrent = null; // tracks the movement of the active layer for a move command
                this._activeLayerIndex = 0;
                /*
                    Select a layer as the active layer
                */
                this.selectLayer = function (layer) {
                    if (layer === null) {
                        console.error('Failed to set active layer: ', layer);
                        console.trace();
                        return;
                    }
                    _this.activeLayer = layer;
                    _this._activeLayerIndex = _this.getIndex(layer);
                    KPaint.Events.emit(KPaint.Events.ID.LAYER_SELECTED, layer);
                    _this.setActiveLayerAlpha(layer.alpha);
                };
                /*
                    Start moving a layer
                */
                this.moveLayerStart = function () {
                    _this._indexMoveCurrent = 0;
                };
                /*
                    Move the active layer up 1 index
                */
                this.moveLayerUp = function () {
                    var indexMoveCurrent = _this._indexMoveCurrent + 1;
                    var activeLayer = _this.activeLayer;
                    _this.move(_this._activeLayerIndex, indexMoveCurrent);
                    KPaint.Events.emit(KPaint.Events.ID.LAYER_MOVE, activeLayer, indexMoveCurrent);
                    _this.selectLayer(activeLayer);
                    _this._indexMoveCurrent = indexMoveCurrent;
                };
                /*
                    Move the active layer down 1 index
                */
                this.moveLayerDown = function () {
                    var indexMoveCurrent = _this._indexMoveCurrent - 1;
                    var activeLayer = _this.activeLayer;
                    _this.move(_this._activeLayerIndex, indexMoveCurrent);
                    KPaint.Events.emit(KPaint.Events.ID.LAYER_MOVE, activeLayer, indexMoveCurrent);
                    _this.selectLayer(activeLayer);
                    _this._indexMoveCurrent = indexMoveCurrent;
                };
                /*
                    End a move command
        
                    If this._indexMoveCurrent is not equal to 0, then it will emit an event causing a moveLayer
                    command to be added to the invoker.
                */
                this.moveLayerEnd = function () {
                    var indexMoveCurrent = _this._indexMoveCurrent;
                    // emit some event with the datas to create the 
                    if (indexMoveCurrent !== 0) {
                        KPaint.Events.emit(KPaint.Events.ID.LAYER_MOVE_END, _this.activeLayer, indexMoveCurrent);
                    }
                    // reset variables
                    _this._indexMoveCurrent = null;
                };
                this._size.xy(width, height);
                var evRegister = KPaint.Events.register;
                var evId = KPaint.Events.ID;
                evRegister(evId.INPUT_LAYER_MOVE_START, this.moveLayerStart);
                evRegister(evId.INPUT_LAYER_MOVE_UP, this.moveLayerUp);
                evRegister(evId.INPUT_LAYER_MOVE_DOWN, this.moveLayerDown);
                evRegister(evId.INPUT_LAYER_MOVE_END, this.moveLayerEnd);
            }
            Object.defineProperty(LayerManager.prototype, "aboveActive", {
                get: function () {
                    return this.getLayerAbove(this.activeLayer);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(LayerManager.prototype, "belowActive", {
                get: function () {
                    return this.getLayerBelow(this.activeLayer);
                },
                enumerable: true,
                configurable: true
            });
            /*
                Changes the alpha value of the active layer
            */
            LayerManager.prototype.setActiveLayerAlpha = function (value) {
                this.activeLayer.alpha = value;
                KPaint.Events.emit(KPaint.Events.ID.LAYER_ALPHA, value);
            };
            /*
                Create a new layer
            */
            LayerManager.prototype.createLayer = function () {
                var idx = this._activeLayerIndex;
                if (idx < this.layers.length) {
                    idx += 1;
                }
                var layer = this.getLayerFromPool();
                this.add(layer, idx);
                KPaint.Events.emit(KPaint.Events.ID.LAYER_CREATED, layer, idx);
                this.selectLayer(layer);
                return layer;
            };
            /*
                Add a layer to the manager. For use by the deleteLayer and newLayer command
            */
            LayerManager.prototype.addLayer = function (layer, idx) {
                if (layer === null) {
                    console.error('Failed to add layer: Layer is null.');
                    return;
                }
                var layerIdx = this.layers.indexOf(layer);
                if (layerIdx > -1) {
                    console.warn('Failed to add layer - layer already exists in LayerManager: ', layer);
                    return;
                }
                this.add(layer, idx);
                KPaint.Events.emit(KPaint.Events.ID.LAYER_CREATED, layer, idx);
            };
            /*
                Removes a layer from the manager. For use by the deleteLayer and newLayer command
            */
            LayerManager.prototype.removeLayer = function (layer) {
                if (layer === null) {
                    console.error('Failed to remove layer: Layer is null.');
                    return -1;
                }
                var layerIdx = this.layers.indexOf(layer);
                if (layerIdx < 0) {
                    console.warn('Failed to remove layer - layer not found: ', layer);
                    this.selectLayer(this.layers[0]);
                    return -1;
                }
                // remove
                this.remove(layerIdx);
                KPaint.Events.emit(KPaint.Events.ID.LAYER_DELETED, layer);
                // set a new activeLayer
                if (layerIdx < (this.layers.length)) {
                    this.selectLayer(this.layers[layerIdx]);
                }
                else {
                    this.selectLayer(this.layers[layerIdx - 1]);
                }
                return layerIdx;
            };
            /*
                Merge the layer with the layer directly underneath
            */
            LayerManager.prototype.mergeLayerDown = function (layer) {
                if (layer === null) {
                    console.error('Failed to merge layer: Layer is null.');
                    return;
                }
                if (this.layers.length <= 1) {
                    console.warn('LayerManager must contain at least 2 layer to merge.');
                    return;
                }
                // find index
                var idx = this.layers.indexOf(layer);
                // layer not found
                if (idx < 0) {
                    console.error('Failed to merge layer - layer not found: ', layer);
                    console.trace();
                    return;
                }
                // active layer is last layer. Can't merge downwards
                if (idx === 0) {
                    console.warn('Failed to merge layer - lowest layer cannot be merged from.');
                    return;
                }
                // merge to the layer beneath the active layer
                this.layers[idx - 1].renderLayer(layer);
            };
            /*
                Find a layer based on its ID
    
                Returns: first layer with the supplied id.
                Failure: returns null
            */
            LayerManager.prototype.getLayerById = function (id) {
                var layers = this.layers;
                for (var i = 0, n = layers.length; i < n; i++) {
                    if (layers[i].id === id) {
                        return layers[i];
                    }
                }
                console.error('Failed to find layer with id ', id);
                return null;
            };
            /*
                Returns the layer above a given layer
    
                Returns null on failure
            */
            LayerManager.prototype.getLayerAbove = function (layer) {
                var layers = this.layers;
                var idx = layers.indexOf(layer);
                for (var i = 0, n = layers.length - 1; i < n; i++) {
                    if (layers[i].id === layer.id) {
                        return layers[i + 1];
                    }
                }
                return null;
            };
            /*
                Returns the layer below a given layer
    
                Returns null on failure
            */
            LayerManager.prototype.getLayerBelow = function (layer) {
                var layers = this.layers;
                var idx = layers.indexOf(layer);
                for (var i = 1, n = layers.length; i < n; i++) {
                    if (layers[i].id === layer.id) {
                        return layers[i - 1];
                    }
                }
                return null;
            };
            /*
                Returns index of a given layer.
    
                Returns -1 on failure
            */
            LayerManager.prototype.getIndex = function (layer) {
                return this.layers.indexOf(layer);
            };
            /*
                Change the index of a layer. Moves the layer n indices.
    
                n can be positive or negative
            */
            LayerManager.prototype.moveIndex = function (layer, n) {
                var idx = this.layers.indexOf(layer);
                var increment = (n >= 0 ? 1 : -1);
                var iNext;
                var endIdx = idx + n;
                var eventEmitFunc = KPaint.Events.emit;
                var evId = KPaint.Events.ID.LAYER_MOVE;
                for (var i = idx; i != endIdx; i = iNext) {
                    iNext = i + increment;
                    this.move(i, iNext);
                    eventEmitFunc(evId, layer, iNext);
                    this.selectLayer(layer);
                }
            };
            /*
                Every time a layer is added, it should be added to both the layers array and the container.
            */
            LayerManager.prototype.add = function (layer, idx) {
                this.layers.splice(idx, 0, layer);
                this.container.addChildAt(layer.container, idx);
            };
            /*
                Every time a layer's index is moved, it should be moved in both the layers array, and in the container.
            */
            LayerManager.prototype.move = function (oldIdx, newIdx) {
                this.layers.move(oldIdx, newIdx);
                this.container.children.move(oldIdx, newIdx);
            };
            /*
                Every time a layer is removed, it should be removed from both the layers array and the container.
            */
            LayerManager.prototype.remove = function (idx, deleteCount) {
                if (deleteCount === void 0) { deleteCount = 1; }
                this.layers.splice(idx, deleteCount);
                this.container.children.splice(idx, deleteCount);
            };
            /*
                Object pool functions below
            */
            // returns (and removes) a layer from the LayerPool
            LayerManager.prototype.getLayerFromPool = function () {
                // otherwise grab from the pool
                var pool = this._pool;
                var l = pool.get();
                var size = this._size;
                l.id = pool.getUniqueId();
                l.name = ['Layer ', l.id.toString()].join('');
                if (!l.size.equals(size)) {
                    l.resize(size.x, size.y);
                }
                return l;
            };
            // adds a layer back to the LayerPool
            LayerManager.prototype.recycleLayer = function (layer) {
                // remove layer from the layers array
                var layers = this.layers;
                var idx = layers.indexOf(layer);
                if (idx >= 0) {
                    layers.splice(idx, 1);
                }
                layer.initialize(-1);
                this._pool.recycle(layer);
            };
            return LayerManager;
        })();
        PaintingContext.LayerManager = LayerManager;
    })(PaintingContext = KPaint.PaintingContext || (KPaint.PaintingContext = {}));
})(KPaint || (KPaint = {}));
/// <reference path="../Generic/IPool.ts"/>
var KPaint;
(function (KPaint) {
    var PaintingContext;
    (function (PaintingContext) {
        var BSprPool = (function (_super) {
            __extends(BSprPool, _super);
            function BSprPool() {
                _super.apply(this, arguments);
            }
            BSprPool.prototype.createOne = function () {
                return new BrushSprite();
            };
            return BSprPool;
        })(KPaint.IPool);
        var BrushSprite = (function () {
            function BrushSprite() {
                this.sprite = new PIXI.Sprite();
                this.sprite.visible = false;
                this.sprite.anchor.x = this.sprite.anchor.y = .5;
            }
            BrushSprite.get = function () { return this._pool.get(); };
            BrushSprite.recycle = function (item) { this._pool.recycle(item); };
            BrushSprite.allocatePool = function () { this._pool.allocate(); };
            Object.defineProperty(BrushSprite.prototype, "x", {
                get: function () { return this.sprite.x; },
                set: function (value) { this.sprite.x = value; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BrushSprite.prototype, "y", {
                get: function () { return this.sprite.y; },
                set: function (value) { this.sprite.y = value; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BrushSprite.prototype, "scale", {
                get: function () { return this.sprite.scale.x; },
                set: function (value) {
                    var scale = this.sprite.scale;
                    scale.x = scale.y = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BrushSprite.prototype, "rotation", {
                get: function () { return this.sprite.rotation; },
                set: function (value) { this.sprite.rotation = value; },
                enumerable: true,
                configurable: true
            });
            BrushSprite.prototype.removeTexture = function () {
                var sprite = this.sprite;
                sprite.texture = null;
                sprite.visible = false;
            };
            BrushSprite.prototype.setTexture = function (tex) {
                var sprite = this.sprite;
                sprite.texture = tex;
                sprite.visible = true;
            };
            // static object pools methods
            BrushSprite._pool = new BSprPool('BrushSprite', 5000);
            return BrushSprite;
        })();
        PaintingContext.BrushSprite = BrushSprite;
    })(PaintingContext = KPaint.PaintingContext || (KPaint.PaintingContext = {}));
})(KPaint || (KPaint = {}));
var KPaint;
(function (KPaint) {
    var PaintingContext;
    (function (PaintingContext) {
        var Brush = (function () {
            function Brush() {
                var _this = this;
                this._interpolator = new KPaint.Interpolator();
                this._colorHsv = new KPaint.Vec3();
                this._colorRgb = new KPaint.Vec3();
                this._flow = 1.0;
                this._size = 1;
                this._minSizePct = 0.01;
                this._spacing = 0.05;
                this.rotation = 0.0; // ?
                this._texture = new PIXI.RenderTexture(KPaint.renderer, 0, 0);
                this._texGen = new PIXI.RenderTexture(KPaint.renderer, 0, 0);
                this._texGenSpr = new PIXI.Sprite(this._texGen);
                this._brushShader = new KPaint.Sha.BrushShader(0.01);
                this._texGenSpr.shader = this._brushShader;
                var evRegister = KPaint.Events.register;
                var evId = KPaint.Events.ID;
                evRegister(evId.INPUT_COLOR_CHANGE_H, function (v) { return _this.hue = v[0]; });
                evRegister(evId.INPUT_COLOR_CHANGE_S, function (v) { return _this.saturation = v[0]; });
                evRegister(evId.INPUT_COLOR_CHANGE_V, function (v) { return _this.value = v[0]; });
                evRegister(evId.INPUT_BRUSH_SIZE, function (v) { return _this.size = v[0]; });
            }
            Object.defineProperty(Brush.prototype, "size", {
                get: function () { return this._size; },
                set: function (size) {
                    size = KPaint.utils.clamp(size, 1, 200);
                    size = Math.floor(size);
                    KPaint.Events.emit(KPaint.Events.ID.BRUSH_SIZE, size);
                    this._size = size;
                    this.spacing = this._spacing;
                    // resize textures
                    this._texGen.resize(size, size, true);
                    this._texture.resize(size, size, true);
                    this.updateTexture();
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Brush.prototype, "sizeMinPct", {
                get: function () { return this._minSizePct; },
                set: function (value) {
                    value = KPaint.utils.clamp(value, 0.01, 1);
                    this._minSizePct = value;
                    KPaint.Events.emit(KPaint.Events.ID.BRUSH_SIZE_MIN, value);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Brush.prototype, "softness", {
                get: function () { return this._brushShader.softness; },
                set: function (value) {
                    value = KPaint.utils.clamp(value, 0.01, 1);
                    KPaint.Events.emit(KPaint.Events.ID.BRUSH_SOFTNESS, value);
                    this._brushShader.softness = value;
                    this.updateTexture();
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Brush.prototype, "flow", {
                get: function () { return this._flow; },
                set: function (value) {
                    value = KPaint.utils.clamp(value, 0.01, 1);
                    this._flow = value;
                    this._brushShader.alpha = KPaint.utils.expostep(value * .6) + value * .4;
                    this.updateColor();
                    this.updateTexture();
                    KPaint.Events.emit(KPaint.Events.ID.BRUSH_FLOW, value);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Brush.prototype, "spacing", {
                get: function () { return this._spacing; },
                set: function (value) {
                    value = KPaint.utils.clamp(value, 0.01, 1);
                    this._spacing = value;
                    KPaint.Events.emit(KPaint.Events.ID.BRUSH_SPACING, value);
                    this._interpolator.spacingPx = value * this.size;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Brush.prototype, "hue", {
                get: function () { return this._colorHsv.h; },
                set: function (v) {
                    this._colorHsv.h = v;
                    this.updateColor();
                    this.updateTexture();
                    KPaint.Events.emit(KPaint.Events.ID.BRUSH_COLOR_H, v);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Brush.prototype, "saturation", {
                get: function () { return this._colorHsv.h; },
                set: function (v) {
                    this._colorHsv.s = v;
                    this.updateColor();
                    this.updateTexture();
                    KPaint.Events.emit(KPaint.Events.ID.BRUSH_COLOR_S, v);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Brush.prototype, "value", {
                get: function () { return this._colorHsv.h; },
                set: function (v) {
                    this._colorHsv.v = v;
                    this.updateColor();
                    this.updateTexture();
                    KPaint.Events.emit(KPaint.Events.ID.BRUSH_COLOR_V, v);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Brush.prototype, "gamma", {
                get: function () { return this._brushShader.gamma; },
                set: function (v) {
                    this._brushShader.gamma = v;
                    this.updateColor();
                },
                enumerable: true,
                configurable: true
            });
            /*
                Return a sprite with brush's attributes
                Used for drawing
            */
            Brush.prototype.getSprite = function (pt) {
                var sprite = PaintingContext.BrushSprite.get();
                sprite.setTexture(this._texture);
                //let sprite = op.sprite.get(this._texture);
                // set the brush attributes
                //sprite.anchor.x = 0.5;								// center
                //sprite.anchor.y = 0.5;								// center
                sprite.x = pt.x; // position
                sprite.y = pt.y; // position
                sprite.scale = pt.z; // size
                //sprite.scale.y = pt.z;								// size
                sprite.rotation = this.rotation; // rotation
                return sprite;
            };
            Brush.prototype.getSprites = function (pts) {
                var interpolator = this._interpolator;
                var minSizePct = this._minSizePct;
                var i, n;
                // add to interpolator
                for (i = 0, n = pts.length; i < n; i++) {
                    pts[i].pressure *= (1 - minSizePct);
                    pts[i].pressure += minSizePct;
                    interpolator.interpolate2(pts[i]);
                }
                // get results
                var newPts = interpolator.newPts;
                // create sprites from those results
                var results = [];
                for (i = 0, n = newPts.length; i < n; i++) {
                    results.push(this.getSprite(newPts[i]));
                }
                interpolator.clear();
                return results;
            };
            Brush.prototype.startStroke = function (pt) {
                this._interpolator.setInitialDrawPoint(pt);
            };
            Brush.prototype.endStroke = function () {
                this._interpolator.clear();
            };
            Brush.prototype.onCanvasResize = function (width, height) {
            };
            Brush.prototype.updateColor = function () {
                var shader = this._brushShader;
                var rgb = this._colorRgb;
                KPaint.hsvToRgb(this._colorHsv, rgb);
                shader.red = rgb.r;
                shader.green = rgb.g;
                shader.blue = rgb.b;
            };
            Brush.prototype.updateTexture = function () {
                var texture = this._texture;
                texture.clear();
                texture.render(this._texGenSpr);
            };
            return Brush;
        })();
        PaintingContext.Brush = Brush;
    })(PaintingContext = KPaint.PaintingContext || (KPaint.PaintingContext = {}));
})(KPaint || (KPaint = {}));
var KPaint;
(function (KPaint) {
    var PaintingContext;
    (function (PaintingContext_1) {
        var PaintingContext = (function () {
            function PaintingContext(width, height, maxCommands) {
                var _this = this;
                this.container = new PIXI.Container();
                this.size = new KPaint.Vec2();
                this._isMidStroke = false;
                this._gammaFilter = new KPaint.Sha.LinearToGammaFilter();
                this._gammaRt = null; // used for converting to gamma before downloading
                this._rgb = new KPaint.Vec3();
                this._hsv = new KPaint.Vec3();
                this.toggleGamma = function () {
                    if (_this.gamma === 1) {
                        _this.gamma = 2.2;
                    }
                    else {
                        _this.gamma = 1;
                    }
                };
                this.layerNew = function () {
                    if (_this._layerManager.layers.length >= 11) {
                        console.warn('LayerManager currently supports only up to 11 layers.');
                        return;
                    }
                    var invoker = _this._invoker;
                    var commie = invoker.addCommand(PaintingContext_1.Command.CommandType.NewLayer);
                    commie.layerId = _this.activeLayer.id;
                    invoker.redo();
                };
                this.layerClear = function () {
                    var invoker = _this._invoker;
                    var commie = invoker.addCommand(PaintingContext_1.Command.CommandType.ClearLayer);
                    commie.layerId = _this.activeLayer.id;
                    invoker.redo();
                };
                this.layerDelete = function () {
                    var invoker = _this._invoker;
                    var commie = invoker.addCommand(PaintingContext_1.Command.CommandType.DeleteLayer);
                    commie.layerId = _this.activeLayer.id;
                    invoker.redo();
                };
                this.layerMerge = function () {
                    var lm = _this._layerManager;
                    var below = lm.belowActive;
                    if (below === null) {
                        console.warn('Failed to merge layer: No layer below.');
                        return;
                    }
                    var invoker = _this._invoker;
                    var commie = invoker.addCommand(PaintingContext_1.Command.CommandType.MergeLayer);
                    commie.layerId = lm.activeLayer.id;
                    commie.layerIdBelow = below.id;
                    invoker.redo();
                };
                this.layerMove = function (data) {
                    var invoker = _this._invoker;
                    var commie = invoker.addCommand(PaintingContext_1.Command.CommandType.MoveLayer);
                    commie.layerId = data[0].id;
                    commie.n = data[1];
                    invoker.redo();
                };
                this.size.xy(width, height);
                this._maxCommands = maxCommands;
                this._brush = new PaintingContext_1.Brush();
                this._layerManager = new PaintingContext_1.LayerManager(width, height);
                this._strokeManager = new PaintingContext_1.StrokeManager(width, height);
                this._invoker = new PaintingContext_1.Command.Invoker(maxCommands, this._layerManager);
                this.tool = KPaint.Tool.Paint;
                this._background = new PIXI.RenderTexture(KPaint.renderer, width, height);
                this._backgroundSprite = new PIXI.Sprite(this._background);
                this._backgroundSprite.shader = new KPaint.Sha.CheckerPatternFilter();
                this._previewRT = new PIXI.RenderTexture(KPaint.renderer, 0, 0);
                // TODO: add gamma correction to the canvas sprite
                this._canvasRT = new PIXI.RenderTexture(KPaint.renderer, width, height);
                this._canvasSpr = new PIXI.Sprite(this._canvasRT);
                this._canvasSpr.shader = this._gammaFilter;
                // add to container
                this.container.addChild(this._backgroundSprite);
                this.container.addChild(this._canvasSpr);
                var evRegister = KPaint.Events.register;
                var evId = KPaint.Events.ID;
                evRegister(evId.INPUT_LAYER_NEW, this.layerNew);
                evRegister(evId.INPUT_LAYER_CLEAR, this.layerClear);
                evRegister(evId.INPUT_LAYER_DELETE, this.layerDelete);
                evRegister(evId.INPUT_LAYER_MERGE, this.layerMerge);
                evRegister(evId.LAYER_MOVE_END, this.layerMove);
                evRegister(evId.INPUT_TOOL_BRUSH_PAINT, function () { return _this.tool = KPaint.Tool.Paint; });
                evRegister(evId.INPUT_TOOL_BRUSH_BLUR, function () { return _this.tool = KPaint.Tool.Blur; });
                evRegister(evId.INPUT_TOOL_BRUSH_ERASE, function () { return _this.tool = KPaint.Tool.Erase; });
                evRegister(evId.INPUT_TOOL_MOVER, function () { return _this.tool = KPaint.Tool.Move; });
                evRegister(evId.INPUT_TOOL_ZOOMER, function () { return _this.tool = KPaint.Tool.Zoom; });
                evRegister(evId.INPUT_BRUSH_SOFTNESS, function (value) { return _this.brushSoftness = value[0]; });
                evRegister(evId.INPUT_BRUSH_FLOW, function (value) { return _this.brushFlow = value[0]; });
                evRegister(evId.INPUT_BRUSH_SPACING, function (value) { return _this.brushSpacing = value[0]; });
                evRegister(evId.INPUT_BRUSH_SIZE_MIN, function (value) { return _this.brushSizeMinPct = value[0]; });
                evRegister(evId.INPUT_LAYER_ALPHA_CHANGED, function (value) { return _this._layerManager.setActiveLayerAlpha(value[0]); });
                evRegister(evId.INPUT_LAYER_SELECTED, function (layer) { return _this._layerManager.selectLayer(layer[0]); });
                evRegister(evId.INPUT_CANVAS_REDO, function () { return _this._invoker.redo(); });
                evRegister(evId.INPUT_CANVAS_UNDO, function () { return _this._invoker.undo(); });
                evRegister(evId.INPUT_DOWNLOAD_IMAGE, function (value) { return _this.downloadImage(value[0]); });
                evRegister(evId.INPUT_GAMMA_TOGGLE, this.toggleGamma);
            }
            Object.defineProperty(PaintingContext.prototype, "isMidStroke", {
                get: function () {
                    return this._isMidStroke;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PaintingContext.prototype, "gamma", {
                get: function () {
                    return this._gammaFilter.gamma;
                },
                set: function (value) {
                    this._gammaFilter.gamma = value;
                    this._brush.gamma = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PaintingContext.prototype, "tool", {
                get: function () {
                    return this._tool;
                },
                set: function (tool) {
                    this._tool = tool;
                    switch (tool) {
                        case KPaint.Tool.Paint:
                            this._strokeManager.strokeType = PaintingContext_1.StrokeType.Paint;
                            break;
                        case KPaint.Tool.Erase:
                            this._strokeManager.strokeType = PaintingContext_1.StrokeType.Erase;
                            break;
                        case KPaint.Tool.Blur:
                            this._strokeManager.strokeType = PaintingContext_1.StrokeType.Blur;
                            break;
                        default:
                            this._strokeManager.strokeType = PaintingContext_1.StrokeType.None;
                            break;
                    }
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PaintingContext.prototype, "brushSize", {
                get: function () { return this._brush.size; },
                set: function (value) { this._brush.size = value; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PaintingContext.prototype, "brushSizeMinPct", {
                get: function () { return this._brush.sizeMinPct; },
                set: function (value) { this._brush.sizeMinPct = value; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PaintingContext.prototype, "brushFlow", {
                get: function () { return this._brush.flow; },
                set: function (value) { this._brush.flow = value; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PaintingContext.prototype, "brushSpacing", {
                get: function () { return this._brush.spacing; },
                set: function (value) { this._brush.spacing = value; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PaintingContext.prototype, "brushSoftness", {
                get: function () { return this._brush.softness; },
                set: function (value) { this._brush.softness = value; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PaintingContext.prototype, "brushRotation", {
                get: function () { return this._brush.rotation; },
                set: function (value) { this._brush.rotation = value; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PaintingContext.prototype, "colorHue", {
                get: function () { return this._brush.hue; },
                set: function (value) { this._brush.hue = value; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PaintingContext.prototype, "colorSaturation", {
                get: function () { return this._brush.saturation; },
                set: function (value) { this._brush.saturation = value; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PaintingContext.prototype, "colorValue", {
                get: function () { return this._brush.value; },
                set: function (value) { this._brush.value = value; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PaintingContext.prototype, "activeLayer", {
                get: function () { return this._layerManager.activeLayer; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PaintingContext.prototype, "activeStroke", {
                get: function () { return this._strokeManager.activeStroke; },
                enumerable: true,
                configurable: true
            });
            // init after UI has been created
            PaintingContext.prototype.init = function () {
                // initial layer
                this._layerManager.createLayer();
            };
            /*
                Changes the pressure curve.
    
                The default pressure curve is not good for painting, so adjust it.
    
                Using smootherstep https://en.wikipedia.org/wiki/Smoothstep#Variations
            */
            PaintingContext.prototype.adjustPressure = function (point) {
                var p = KPaint.utils.clamp(point.pressure, 0, 1);
                point.pressure = KPaint.utils.smootherstep(p);
            };
            PaintingContext.prototype.startStroke = function (point) {
                if (this._isMidStroke === true) {
                    console.warn('Attempted to start new stroke with stroke in progress.');
                    return false;
                }
                var brush = this._brush;
                // adjust pressure
                this.adjustPressure(point);
                // interpolator
                brush.startStroke(point);
                this._strokeManager.beginStroke();
                this.activeLayer.startStroke(this.activeStroke);
                // draw
                this.activeStroke.previewRT = this._previewRT; // test
                this.activeStroke.addPoints([brush.getSprite(point)]);
                // set pointer state
                this._isMidStroke = true;
                return true;
            };
            PaintingContext.prototype.addPoint = function (pointers) {
                if (this._isMidStroke === false) {
                    console.warn('Attempted to add point to stroke with no stroke in progress.');
                    return false;
                }
                // adjust pressure
                var pt;
                for (var i = 0, n = pointers.length; i < n; i++) {
                    pt = pointers[i];
                    this.adjustPressure(pt);
                }
                // interpolate and draw
                var stroke = this.activeStroke;
                stroke.addPoints(this._brush.getSprites(pointers));
                // can't preview erasing strokes like with normal ones
                if (stroke.type === PaintingContext_1.StrokeType.Erase) {
                    stroke.flatten();
                }
                return true;
            };
            PaintingContext.prototype.endStroke = function () {
                if (this._isMidStroke === false || this.activeStroke === null) {
                    console.warn('Attempted to end stroke with no stroke in progress.');
                    return false;
                }
                var activeLayer = this.activeLayer;
                // add command
                var commie = this._invoker.addCommand(PaintingContext_1.Command.CommandType.Brush);
                commie.layerId = activeLayer.id;
                commie.stroke = this.activeStroke;
                this._invoker.redo();
                // end stroke
                this._strokeManager.endStroke();
                activeLayer.endStroke();
                this._brush.endStroke();
                this._previewRT = this.activeStroke.previewRT; // test
                // set pointer state
                this._isMidStroke = false;
                return true;
            };
            PaintingContext.prototype.resizeViewArea = function (width, height) {
                this._brush.onCanvasResize(width, height);
                this.size.xy(width, height);
            };
            PaintingContext.prototype.pickColor = function (pointer) {
                var data = this._canvasRT.getPixel(pointer.x, pointer.y);
                // Don't pick if color has no alpha. There's also a rare case where alpha will return as -6.246364137041382e-8
                var a = data[3];
                if (a <= 0) {
                    return;
                }
                var rgb = this._rgb, hsv = this._hsv;
                rgb.rgb(data[0], data[1], data[2]);
                // textures are defined so that all transparent pixels are fully black
                // so we multiply the color by the inverse of the pixel's alpha to get the real color
                var gamma = this.gamma;
                a = Math.pow(a, 1 / gamma);
                rgb.pow(1 / gamma);
                rgb.multiply(1 / a);
                KPaint.rgbToHsv(rgb, hsv);
                var evEmit = KPaint.Events.emit;
                var evId = KPaint.Events.ID;
                evEmit(evId.INPUT_COLOR_CHANGE_H, hsv.h);
                evEmit(evId.INPUT_COLOR_CHANGE_S, hsv.s);
                evEmit(evId.INPUT_COLOR_CHANGE_V, hsv.v);
            };
            /*
                Render layers
    
                TODO: use double buffer
            */
            PaintingContext.prototype.render = function () {
                var canvasRt = this._canvasRT;
                canvasRt.clear();
                canvasRt.render(this._layerManager.container);
                KPaint.Events.emit(KPaint.Events.ID.CANVAS_RENDERED);
            };
            PaintingContext.prototype.downloadImage = function (filename) {
                var gammaRt = this._gammaRt;
                var size = this.size;
                // render the inner canvas to a texture
                if (gammaRt === null) {
                    gammaRt = new PIXI.RenderTexture(KPaint.renderer, size.x, size.y);
                    this._gammaRt = gammaRt;
                }
                else {
                    gammaRt.resize(size.x, size.y, true);
                }
                gammaRt.clear();
                gammaRt.render(this._canvasSpr);
                // download the texture through its URI
                filename = filename || new Date().toLocaleDateString();
                var link = document.createElement('a');
                link.download = filename;
                link.href = gammaRt.getCanvas().toDataURL();
                link.click();
                link.remove();
                gammaRt.resize(0, 0, true);
            };
            return PaintingContext;
        })();
        PaintingContext_1.PaintingContext = PaintingContext;
    })(PaintingContext = KPaint.PaintingContext || (KPaint.PaintingContext = {}));
})(KPaint || (KPaint = {}));
var KPaint;
(function (KPaint) {
    var PaintingContext;
    (function (PaintingContext) {
        var Command;
        (function (Command) {
            /*
                Enumerator used to identify what the command does
            */
            (function (CommandType) {
                CommandType[CommandType["None"] = 0] = "None";
                CommandType[CommandType["Brush"] = 1] = "Brush";
                CommandType[CommandType["ClearLayer"] = 2] = "ClearLayer";
                CommandType[CommandType["NewLayer"] = 3] = "NewLayer";
                CommandType[CommandType["DeleteLayer"] = 4] = "DeleteLayer";
                CommandType[CommandType["MergeLayer"] = 5] = "MergeLayer";
                CommandType[CommandType["MoveLayer"] = 6] = "MoveLayer";
            })(Command.CommandType || (Command.CommandType = {}));
            var CommandType = Command.CommandType;
            /*
                Generic command
            */
            var Commie = (function () {
                function Commie(invoker, lm) {
                    var _this = this;
                    this.layerId = null;
                    this.layerIdAbove = null;
                    this.layerIdBelow = null;
                    this.stroke = null;
                    this.n = null;
                    this.m = null;
                    this._type = CommandType.None;
                    this._storedLayer = null;
                    this._selectedLayerId = null;
                    this._initialized = false;
                    // callbacks 
                    this._do = null;
                    this._undo = null;
                    this._onShifted = null;
                    this._onPopped = null;
                    /*
                        Brush.
            
                        Save the layer and render the stroke onto the layer.
            
                        Undo restores the layer.
            
                        Calls stroke.recycle() when being popped or shifted off the invoker
                    */
                    this._brushRedo = function () {
                        // make a copy of the layer the first time executing this
                        if (_this._initialized === false) {
                            _this.storeLayerTex();
                        }
                        _this.lm.activeLayer.render(_this.stroke);
                    };
                    this._brushUndo = function () {
                        _this.restoreLayerTex();
                    };
                    this._brushRecycle = function () {
                        _this.stroke.recycle();
                    };
                    /*
                        Clear layer
            
                        Stores the layer, then clears.
            
                        Undoing restores the state.
                    */
                    this._layerClearRedo = function () {
                        // make a copy of the layer the first time executing this
                        if (_this._initialized === false) {
                            _this.storeLayerTex();
                        }
                        _this.layer.clear();
                    };
                    this._layerClearUndo = function () {
                        _this.restoreLayerTex();
                    };
                    /*
                        New layer
            
                        Creates a new layer on first execution.
                    
                        Hides and unhides otherwise.
            
                        Deletes the layer if popped off the invoker.
            
                        Stores the layer in this._storedLayer
                        Stores the layer's index in this.n
                    */
                    this._layerNewRedo = function () {
                        if (_this._storedLayer === null) {
                            _this._storedLayer = _this.lm.createLayer();
                        }
                        else {
                            _this.lm.addLayer(_this._storedLayer, _this.n);
                        }
                        _this.lm.selectLayer(_this._storedLayer);
                    };
                    this._layerNewUndo = function () {
                        _this.n = _this.lm.removeLayer(_this._storedLayer);
                    };
                    this._layerNewPopped = function () {
                        _this.lm.recycleLayer(_this._storedLayer);
                    };
                    /*
                        Delete layer
            
                        Hides the layer until it is shifted off the invoker, then deletes it.
            
                        Stores the layer in this._storedLayer
                        Stores the layer's index in this.n
                    */
                    this._layerDeleteRedo = function () {
                        if (_this._initialized === false) {
                            _this._storedLayer = _this.layer;
                        }
                        _this.n = _this.lm.removeLayer(_this._storedLayer);
                    };
                    this._layerDeleteUndo = function () {
                        _this.lm.addLayer(_this._storedLayer, _this.n);
                    };
                    this._layerDeleteShifted = function () {
                        _this.lm.recycleLayer(_this._storedLayer);
                    };
                    /*
                        Merge layer
            
                        Store both initial layer, and below layer.
                    
                        Redo should render initial layer on the below one, and clear the initial layer.
                        Undo should restore both to their original states.
                    
                        Needs to recycle the secondary RenderTexture when being shifted or popped
                    */
                    this._layerMergeRedo = function () {
                        var layer = _this.layer, below = _this.layerBelow;
                        if (_this._initialized === false) {
                            _this.storeLayerTex();
                            _this.storeSecondLayerTex(below);
                            _this.n = layer.alpha;
                            _this.m = below.alpha;
                        }
                        // render
                        below.renderLayer(layer);
                        layer.clear();
                    };
                    this._layerMergeUndo = function () {
                        _this.restoreLayerTex();
                        _this.restoreSecondLayerTex();
                    };
                    this._layerMergeRecycle = function () {
                        _this.onRecycleSecondLayer();
                    };
                    /*
                        Move layer
                    */
                    this._layerMoveRedo = function () {
                        if (_this._initialized === false) {
                            return;
                        }
                        // TODO: implement
                        _this.lm.moveIndex(_this.layer, _this.n);
                    };
                    this._layerMoveUndo = function () {
                        _this.lm.moveIndex(_this.layer, -_this.n);
                    };
                    this.invoker = invoker;
                    this.lm = lm;
                    this._undoTexture = new PIXI.RenderTexture(KPaint.renderer, 0, 0);
                    this._undoTexture2 = new PIXI.RenderTexture(KPaint.renderer, 0, 0);
                    this._undoSprite = new PIXI.Sprite(this._undoTexture);
                    this._undoSprite2 = new PIXI.Sprite(this._undoTexture2);
                }
                Object.defineProperty(Commie.prototype, "layerAbove", {
                    get: function () {
                        return this.lm.getLayerById(this.layerIdAbove);
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(Commie.prototype, "layer", {
                    get: function () {
                        return this.lm.getLayerById(this.layerId);
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(Commie.prototype, "layerBelow", {
                    get: function () {
                        return this.lm.getLayerById(this.layerIdBelow);
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(Commie.prototype, "type", {
                    get: function () {
                        return this._type;
                    },
                    set: function (value) {
                        this._type = value;
                        this._do = null;
                        this._undo = null;
                        this._onShifted = null;
                        this._onPopped = null;
                        switch (value) {
                            case CommandType.Brush:
                                this._do = this._brushRedo;
                                this._undo = this._brushUndo;
                                this._onShifted = this._brushRecycle;
                                this._onPopped = this._brushRecycle;
                                break;
                            case CommandType.ClearLayer:
                                this._do = this._layerClearRedo;
                                this._undo = this._layerClearUndo;
                                break;
                            case CommandType.NewLayer:
                                this._do = this._layerNewRedo;
                                this._undo = this._layerNewUndo;
                                this._onPopped = this._layerNewPopped;
                                break;
                            case CommandType.DeleteLayer:
                                this._do = this._layerDeleteRedo;
                                this._undo = this._layerDeleteUndo;
                                this._onShifted = this._layerDeleteShifted;
                                break;
                            case CommandType.MergeLayer:
                                this._do = this._layerMergeRedo;
                                this._undo = this._layerMergeUndo;
                                this._onShifted = this._layerMergeRecycle;
                                this._onPopped = this._layerMergeRecycle;
                                break;
                            case CommandType.MoveLayer:
                                this._do = this._layerMoveRedo;
                                this._undo = this._layerMoveUndo;
                            case CommandType.None:
                                break;
                            default:
                                console.error('Unknown CommandType: ', value);
                                this._type = CommandType.None;
                                return;
                        }
                    },
                    enumerable: true,
                    configurable: true
                });
                Commie.prototype.do = function () {
                    var lm = this.lm;
                    if (this._initialized === false) {
                        this._selectedLayerId = lm.activeLayer.id;
                    }
                    lm.selectLayer(lm.getLayerById(this._selectedLayerId));
                    this._do();
                    this._initialized = true;
                };
                Commie.prototype.undo = function () {
                    this._undo();
                    this.lm.selectLayer(this.lm.getLayerById(this._selectedLayerId));
                };
                Commie.prototype.onShifted = function () {
                    if (this._onShifted !== null) {
                        this._onShifted();
                    }
                };
                Commie.prototype.onPopped = function () {
                    if (this._onPopped !== null) {
                        this._onPopped();
                    }
                };
                Commie.prototype.initialize = function () {
                    // Only initialize the variables that can change. 
                    // Invoker and layermanager will always be the same.
                    this.type = CommandType.None;
                    this.layerId = null;
                    this.layerIdAbove = null;
                    this.layerIdBelow = null;
                    // misc
                    this.stroke = null;
                    this.n = null;
                    this.m = null;
                    this._undoTexture.resize(0, 0, true);
                    this._undoTexture2.resize(0, 0, true);
                    this._storedLayer = null;
                    this._initialized = false;
                };
                /*
                    Misc Storage functions  for layers
                */
                Commie.prototype.storeLayerTex = function () {
                    var layer = this.layer;
                    this._undoTexture.resize(layer.size.x, layer.size.y, true);
                    this._undoTexture.clear();
                    //copy the layer as 1.0 alpha
                    var a = layer.alpha;
                    layer.alpha = 1;
                    layer.copyTo(this._undoTexture);
                    layer.alpha = a;
                };
                Commie.prototype.restoreLayerTex = function () {
                    this.layer.restoreLayer(this._undoSprite);
                };
                // storage of an extra layer if needed (spoiler: needed for merging)
                Commie.prototype.storeSecondLayerTex = function (layer) {
                    var rt = this._undoTexture2;
                    ;
                    rt.resize(layer.size.x, layer.size.y, true);
                    rt.clear();
                    //copy the layer as 1.0 alpha
                    var a = layer.alpha;
                    layer.alpha = 1;
                    layer.copyTo(rt);
                    layer.alpha = a;
                };
                Commie.prototype.restoreSecondLayerTex = function () {
                    this.layerBelow.restoreLayer(this._undoSprite2);
                };
                // set the second layer's texture size to 0x0 on recycle since it is rarely used
                Commie.prototype.onRecycleSecondLayer = function () {
                    this._undoTexture2.resize(0, 0, true);
                };
                return Commie;
            })();
            Command.Commie = Commie;
        })(Command = PaintingContext.Command || (PaintingContext.Command = {}));
    })(PaintingContext = KPaint.PaintingContext || (KPaint.PaintingContext = {}));
})(KPaint || (KPaint = {}));
/// <reference path="Command.ts"/>
var KPaint;
(function (KPaint) {
    var PaintingContext;
    (function (PaintingContext) {
        var Command;
        (function (Command) {
            var CommandPool = (function (_super) {
                __extends(CommandPool, _super);
                function CommandPool(invoker, lm, max) {
                    _super.call(this, 'Command', max);
                    this.invoker = invoker;
                    this.lm = lm;
                    this.allocate();
                }
                CommandPool.prototype.createOne = function () {
                    return new Command.Commie(this.invoker, this.lm);
                };
                return CommandPool;
            })(KPaint.IPool);
            /*
                Uses command pattern to store and handle previous input commands allowing for undoing and redoing of them.
        
                _nextCommandIdx should be pointing to the next command to be executed, which means that it should be incremented
                after do() and decremented when exceeding _maxCommands
            */
            var Invoker = (function () {
                function Invoker(maxSize, lm) {
                    var _this = this;
                    // active commands
                    this._commands = [];
                    this._nextCommandIdx = 0;
                    /*
                        Do (execute) a command.
                    */
                    this.redo = function () {
                        if (_this._nextCommandIdx >= _this._commands.length
                            || _this._nextCommandIdx < 0) {
                            console.warn([
                                'Cannot redo: ',
                                '_nextCommandIdx: ', _this._nextCommandIdx,
                                ', _commands.length: ', _this._commands.length,
                                ', _maxCommands: ', _this._maxCommands
                            ].join(''));
                            return false;
                        }
                        // do
                        _this._commands[_this._nextCommandIdx].do();
                        KPaint.Events.emit(KPaint.Events.ID.INVOKER_REDONE);
                        KPaint.renderCoordinator.requestRender();
                        _this._nextCommandIdx++;
                        return true;
                    };
                    /*
                        Undo (un-execute) a command.
                    */
                    this.undo = function () {
                        if (_this._nextCommandIdx < 1) {
                            console.warn([
                                'Cannot undo: ',
                                '_nextCommandIdx: ', _this._nextCommandIdx,
                                ', _commands.length: ', _this._commands.length,
                                ', _maxCommands: ', _this._maxCommands
                            ].join(''));
                            return false;
                        }
                        // undo
                        _this._commands[_this._nextCommandIdx - 1].undo();
                        KPaint.Events.emit(KPaint.Events.ID.INVOKER_UNDONE);
                        KPaint.renderCoordinator.requestRender();
                        _this._nextCommandIdx--;
                        return true;
                    };
                    this._maxCommands = maxSize;
                    this._pool = new CommandPool(this, lm, maxSize + 1);
                }
                Invoker.prototype.addCommand = function (type) {
                    var command = this._pool.get();
                    command.type = type;
                    var commands = this._commands;
                    // remove commands that are infront of current position if any
                    if (commands.length > this._nextCommandIdx) {
                        var pool = this._pool;
                        var toRecycle;
                        for (var i = 0, n = commands.length - this._nextCommandIdx; i < n; i++) {
                            toRecycle = commands.pop();
                            toRecycle.onPopped();
                            // TODO: recyle command
                            toRecycle.initialize();
                            pool.recycle(toRecycle);
                        }
                    }
                    // add
                    commands.push(command);
                    // ensure the commands array doesnt exceed max length
                    if (this._commands.length > this._maxCommands) {
                        var toRecycle = this._commands.shift();
                        toRecycle.onShifted();
                        // TODO: recyle command
                        toRecycle.initialize();
                        this._pool.recycle(toRecycle);
                        this._nextCommandIdx--;
                    }
                    return command;
                };
                return Invoker;
            })();
            Command.Invoker = Invoker;
        })(Command = PaintingContext.Command || (PaintingContext.Command = {}));
    })(PaintingContext = KPaint.PaintingContext || (KPaint.PaintingContext = {}));
})(KPaint || (KPaint = {}));
/// <reference path="Generic/CircularBuffer.ts"/>
var KPaint;
(function (KPaint) {
    /*
        Class that decides when to render based on a max FPS.

        You can request a render through the requestRender function, and the coordinator will
        decide how long to wait before rendering.
    */
    var RenderingCoordinator = (function () {
        function RenderingCoordinator(context) {
            var _this = this;
            // current requestAnimationFrame() id
            this._requestId = null;
            // contains the last 100 rendering durations and rendering times
            this._renderDurations = new KPaint.CircularBuffer(100);
            this._renderTimes = new KPaint.CircularBuffer(100);
            // stats
            this._currentFPS = 0;
            this._avgFrameTime = 0;
            this._minFrameTime = 0;
            this._maxFrameTime = 0;
            this._statsUpdated = false;
            // the rendering function
            this.render = function () {
                // render
                var start = performance.now();
                _this._context.render();
                KPaint.renderer.render(KPaint.stage);
                var duration = performance.now() - start;
                _this._renderDurations.push(duration);
                _this._renderTimes.push(start);
                // clear the request id
                _this._requestId = null;
                _this._statsUpdated = false;
            };
            this._context = context;
        }
        Object.defineProperty(RenderingCoordinator.prototype, "currentFPS", {
            get: function () {
                if (this._statsUpdated === false) {
                    this.updateStats();
                }
                return this._currentFPS;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(RenderingCoordinator.prototype, "avgFrameTime", {
            // Ms per frame for the last 100 frames
            get: function () {
                if (this._statsUpdated === false) {
                    this.updateStats();
                }
                return this._avgFrameTime;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(RenderingCoordinator.prototype, "minFrameTime", {
            get: function () {
                if (this._statsUpdated === false) {
                    this.updateStats();
                }
                return this._minFrameTime;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(RenderingCoordinator.prototype, "maxFrameTime", {
            get: function () {
                if (this._statsUpdated === false) {
                    this.updateStats();
                }
                return this._maxFrameTime;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(RenderingCoordinator.prototype, "waitingToRender", {
            // whether a render request is currently being processed
            get: function () {
                return this._requestId !== null;
            },
            enumerable: true,
            configurable: true
        });
        // request that a render happen as soon as possible
        RenderingCoordinator.prototype.requestRender = function () {
            if (this._requestId === null) {
                this._requestId = requestAnimationFrame(this.render);
            }
        };
        // force the coordinator to render immediately
        RenderingCoordinator.prototype.forceRender = function () {
            // clear timeout if one exists
            var requestId = this._requestId;
            if (requestId !== null) {
                cancelAnimationFrame(requestId);
            }
            this.render();
        };
        // update stats for rendering
        RenderingCoordinator.prototype.updateStats = function () {
            var durations = this._renderDurations.buffer, times = this._renderTimes.buffer, arrLen = durations.length, arrPos = this._renderTimes.back;
            var duraSum = 0, duraMin = Number.MAX_VALUE, duraMax = Number.MIN_VALUE, dura;
            // assumes duration.length === times.length
            for (var i = 0, n = arrLen; i < n; i++) {
                dura = durations[i];
                duraSum += dura;
                if (dura < duraMin) {
                    duraMin = dura;
                }
                else if (dura > duraMax) {
                    duraMax = dura;
                }
            }
            var cur, prev = times[arrPos], deltaSum = 0;
            for (var i = 0, n = arrLen; i < n; i++) {
                cur = times[(i + arrPos) % arrLen];
                deltaSum += cur - prev;
                prev = cur;
            }
            this._currentFPS = 1000 / (deltaSum / (arrLen - 1));
            this._avgFrameTime = duraSum / arrLen;
            this._minFrameTime = duraMin;
            this._maxFrameTime = duraMax;
            this._statsUpdated = true;
        };
        return RenderingCoordinator;
    })();
    KPaint.RenderingCoordinator = RenderingCoordinator;
})(KPaint || (KPaint = {}));
var KPaint;
(function (KPaint) {
    /*
        Basic object for displaying stats
    */
    var StatsDisplay = (function () {
        function StatsDisplay(x, y, width, height) {
            var _this = this;
            this.container = new PIXI.Container();
            this.area = new KPaint.Vec4();
            this._logging = null;
            this._text = new PIXI.Text('-', { font: '14px arial', strokeThickness: 0.3, fill: 0xdd0000 });
            this.setUpdateInterval = function () {
                if (_this._logging === null) {
                    _this._logging = setInterval(_this.update, 100);
                    _this.container.visible = true;
                }
                else {
                    clearInterval(_this._logging);
                    _this._logging = null;
                    _this.container.visible = false;
                }
            };
            this.update = function () {
                var renderString = [
                    'FPS: ', KPaint.renderCoordinator.currentFPS.toFixed(2), '\n',
                    'Rendering\n',
                    '  Avg: ', KPaint.renderCoordinator.avgFrameTime.toFixed(2), 'ms\n',
                    '  Min: ', KPaint.renderCoordinator.minFrameTime.toFixed(2), 'ms\n',
                    '  Max: ', KPaint.renderCoordinator.maxFrameTime.toFixed(2), 'ms\n',
                    'Input\n',
                    '  Avg: ', KPaint.Input.avgInputHandlingMs.toFixed(2), 'ms\n',
                    '  Min: ', KPaint.Input.minInputHandlingMs.toFixed(2), 'ms\n',
                    '  Max: ', KPaint.Input.maxInputHandlingMs.toFixed(2), 'ms',
                ].join('');
                _this._text.text = renderString;
            };
            this.area.xyzw(x, y, width, height);
            var text = this._text;
            text.x = x;
            text.y = y;
            this.container.addChild(text);
            this.container.visible = false;
            KPaint.Events.register(KPaint.Events.ID.LOG_RENDERING_STATS, this.setUpdateInterval);
        }
        return StatsDisplay;
    })();
    KPaint.StatsDisplay = StatsDisplay;
})(KPaint || (KPaint = {}));
// reference all the standalone components first
/*
    Generics
*/
/// <reference path="Generic/JavaScriptExtensions.ts"/>
/// <reference path="Generic/IPool.ts"/>
/// <reference path="Generic/Vec.ts"/>
/// <reference path="Generic/Vec3Pool.ts"/>
/// <reference path="Generic/Color.ts"/>
/// <reference path="Generic/Interpolation.ts"/>
/// <reference path="Generic/Utils.ts"/>
/// <reference path="Generic/CircularBuffer.ts"/>
/*
    Events
*/
/// <reference path="Events/EventIDs.ts"/>
/// <reference path="Events/Events.ts"/>
/*
    Shaders
*/
/// <reference path="Shaders/Common.ts"/>
/// <reference path="Shaders/AlphaFilter.ts"/>
/// <reference path="Shaders/BrushFilter.ts"/>
/// <reference path="Shaders/CheckerPatternFilter.ts"/>
/// <reference path="Shaders/ColorFilter.ts"/>
/// <reference path="Shaders/HueCircleFilter.ts"/>
/// <reference path="Shaders/LinearToGammaFilter.ts"/>
/// <reference path="Shaders/ReverseEraseFilter.ts"/>
/// <reference path="Shaders/SaturationValueFilter.ts"/>
/// <reference path="Shaders/SelectorCircleFilter.ts"/>
/// <reference path="Shaders/SliderFilters/HueFilter.ts"/>
/// <reference path="Shaders/SliderFilters/SaturationFilter.ts"/>
/// <reference path="Shaders/SliderFilters/ValueFilter.ts"/>
/*
    UI
*/
/// <reference path="UI/Generic/DisplayObject.ts"/>
/// <reference path="UI/Generic/Clickable.ts"/>
/// <reference path="UI/Generic/Button.ts"/>
/// <reference path="UI/Generic/Selector.ts"/>
/// <reference path="UI/Generic/Slider.ts"/>
/// <reference path="UI/Generic/AreaSlider.ts"/>
/// <reference path="UI/LeftSideBar/ColorWheel/ColorWheel.ts"/>
/// <reference path="UI/LeftSideBar/ColorWheel/ColorWheelSvArea.ts"/>
/// <reference path="UI/LeftSideBar/BrushButtonArea.ts"/>
/// <reference path="UI/LeftSideBar/BrushSettings.ts"/>
/// <reference path="UI/LeftSideBar/ColorDisplay.ts"/>
/// <reference path="UI/LeftSideBar/HsvSliderArea.ts"/>
/// <reference path="UI/LeftSideBar/LeftSideBar.ts"/>
/// <reference path="UI/RightSideBar/Navigator.ts"/>
/// <reference path="UI/RightSideBar/DisplayOptions.ts"/>
/// <reference path="UI/RightSideBar/LayerButtonArea.ts"/>
/// <reference path="UI/RightSideBar/LayerDisplay.ts"/>
/// <reference path="UI/RightSideBar/LayerDisplayContainer.ts"/>
/// <reference path="UI/RightSideBar/RightSideBar.ts"/>
/// <reference path="UI/Cursor.ts"/>
/// <reference path="UI/CanvasDisplay.ts"/>
/// <reference path="UI/UImanager.ts"/>
/*
    Input
*/
/// <reference path="Input/InputCodes.ts"/>
/// <reference path="Input/InputData.ts"/>
/// <reference path="Input/InputContext.ts"/>
/// <reference path="Input/InputContextGenerators.ts"/>
/// <reference path="Input/InputMapper.ts"/>
/// <reference path="Input/PointerCapture.ts"/>
/// <reference path="Input/InputHandler.ts"/>
/*
    Painting
*/
/// <reference path="Painting/ToolTypes.ts"/>
/// <reference path="Painting/Stroke.ts"/>
/// <reference path="Painting/Layer.ts"/>
/// <reference path="Painting/StrokeManager.ts"/>
/// <reference path="Painting/LayerManager.ts"/>
/// <reference path="Painting/BrushSprite.ts"/>
/// <reference path="Painting/Brush.ts"/>
/// <reference path="Painting/PaintingContext.ts"/>
/// <reference path="Painting/Invoker/Command.ts"/>
/// <reference path="Painting/Invoker/Invoker.ts"/>
/*
    Top level
*/
/// <reference path="RenderingCoordinator.ts"/>
/// <reference path="StatsDisplay.ts"/>
var KPaint;
(function (KPaint) {
    KPaint.stage = new PIXI.Container();
    var statsD;
    function main() {
        PIXI.SCALE_MODES.DEFAULT = PIXI.SCALE_MODES.NEAREST;
        var tmpRenderer = PIXI.autoDetectRenderer(KPaint.utils.clamp(window.innerWidth - 20, 800, 1400), KPaint.utils.clamp(window.innerHeight - 20, 400, 1000), {
            transparent: true,
            antialias: false,
            forceFXAA: false,
            roundPixels: false
        });
        if (tmpRenderer.type !== PIXI.RENDERER_TYPE.WEBGL) {
            console.error('Non-WebGL renderer initialized. Aborting.');
            return;
        }
        KPaint.renderer = tmpRenderer;
        document.body.appendChild(KPaint.renderer.view);
        KPaint.stage.interactive = false;
        KPaint.stage.interactiveChildren = false;
        // Context for painting
        KPaint.paintingContext = new KPaint.PaintingContext.PaintingContext(1000, 1000, 5);
        KPaint.renderCoordinator = new KPaint.RenderingCoordinator(KPaint.paintingContext);
        KPaint.PaintingContext.BrushSprite.allocatePool();
        // Input
        KPaint.Input.init();
        // UI
        KPaint.UI.init(KPaint.renderer, KPaint.paintingContext);
        // initial settings
        KPaint.paintingContext.init();
        KPaint.paintingContext.gamma = 2.2;
        KPaint.paintingContext.brushSize = 150;
        KPaint.paintingContext.brushSizeMinPct = 0.01;
        KPaint.paintingContext.brushSoftness = 0.1;
        KPaint.paintingContext.brushFlow = 0.6;
        KPaint.paintingContext.brushSpacing = 0.03;
        // unload message
        KPaint.utils.setOnBeforeUnload();
        // stats
        statsD = new KPaint.StatsDisplay(10, KPaint.renderer.view.clientHeight - 160, 200, 200);
        KPaint.stage.addChild(statsD.container);
        statsD.setUpdateInterval();
        // initial render
        KPaint.renderCoordinator.requestRender();
    }
    KPaint.main = main;
})(KPaint || (KPaint = {}));
window.onload = function () { return KPaint.main(); };
//# sourceMappingURL=app.js.map